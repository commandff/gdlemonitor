package aids

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/juju/ratelimit"
)

// //////////////////////////////////////////////////////////////
// Persistance Cache
// in-memory database, with periodic marshals to json
// mostly to persist state for development; most can be nulled in production.
// //////////////////////////////////////////////////////////////
var P *Persistance
var WD string

type Persistance struct {
	C          GDLEMonitorConf           `json:"config"`
	Cd         GDLEMonitorConfig_Dynamic `json:"conf"`
	GDLEcfg    map[string]interface{}    `json:"gdle"`
	GDLEcfg_ts time.Time                 `json:"gdle_ts"`
	Dirty      bool                      `json:"-"`

	JWTKeys   map[string]string `json:"jwt_keys"`
	JWTKeys_m sync.Mutex        `json:"-"`

	JWTState   map[string]*Claims `json:"jwt_state"`
	JWTState_m sync.Mutex         `json:"-"`

	IP   map[string]*IP_Result `json:"ip"`
	IP_m sync.Mutex            `json:"-"`

	UserLimiter   map[string]*Userlimiter `json:"-"`
	UserLimiter_m sync.Mutex              `json:"-"`

	GlobalLimiter *ratelimit.Bucket `json:"-"`

	TLS   map[string]*TLSCert `json:"tls"`
	TLS_m sync.Mutex          `json:"-"`
}

type GDLEMonitorConf struct {
	Gdle struct {
		Config string   `json:"config"`
		Logs   []string `json:"logs"`
	} `json:"gdle"`
	Heartbeat     int               `json:"heartbeat"`
	BindIP        string            `json:"bind_ip"`
	DomainName    string            `json:"domain_name"`
	DomainContact string            `json:"domain_contact"`
	ENV           map[string]string `json:"env"`
	SpamAddress   string            `json:"spam_address"`
	Whitelist     []string          `json:"whitelist"`
	API           struct {
		Port                  int  `json:"port"`
		RedirectNonsecurePort bool `json:"redirect_nonsecure_port"`
		AccountLimitEmail     int  `json:"account_limit_email"`
		AccountLimitIP        int  `json:"account_limit_ip"`
		AccountSQMax          int  `json:"account_sq_max"`
		AccountSQMin          int  `json:"account_sq_min"`
		AccountRequireEmail   bool `json:"require_email"`
		AccountLimitProxy     bool `json:"account_limit_proxy"`
		LoginLimitProxy       bool `json:"login_limit_proxy"`
	} `json:"api"`
}

type GDLEMonitorConfig_Dynamic struct {
	Origin       string `json:"origin"`    // set in service.go@gdlemonitormain(), used in templating, API ref, javascript
	ACServerName string `json:"title"`     // set in service.go@readServerConfig(), used in templating, API ref, javascript
	ACServer     string `json:"ac_server"` // set in service.go@readServerConfig()(), used in templating, API ref, javascript
	PublicIP     string `json:"public_ip"` // set in service.go@gdlemonitormain(), used in templating, API ref, javascript, domain, tls certs
}

// split up the various types into their own cleanup routines.
func (per *Persistance) HeartBeat() {
	per.JWTKeys_Cleanup()
	per.JWTState_Cleanup()
	per.Intel_Cleanup()
	per.UserLimiter_Cleanup()
	per.Write() // write persistance data
}

//go:embed config-defaults.json
var config_defaults []byte

func (per *Persistance) New() *Persistance {
	per.C = GDLEMonitorConf{
		ENV: make(map[string]string),
	}
	if err := json.Unmarshal(config_defaults, &per.C); err != nil {
		fmt.Fprintf(os.Stderr, "failed to parse (embedded) config-defaults.json: %v\n", err)
	}

	per.JWTKeys = make(map[string]string)
	per.JWTState = make(map[string]*Claims)
	per.IP = make(map[string]*IP_Result)
	per.UserLimiter = make(map[string]*Userlimiter)
	per.GlobalLimiter = ratelimit.NewBucket(20*time.Millisecond, 15)
	per.TLS = map[string]*TLSCert{}
	return per
}

func (per *Persistance) Write() {
	if per.Dirty {
		per.Dirty = false
		file, _ := json.Marshal(per)
		targetFile := filepath.Join(WD, "gdlemonitor.json")
		err := os.WriteFile(targetFile+"~", file, 0644)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error Writing to %s: %v\n", targetFile, err)
			return
		}
		err = os.Rename(targetFile+"~", targetFile)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error Renaming %s: %v\n", targetFile, err)

			return
		}
		fmt.Fprintf(os.Stderr, "wrote %db to gdlemonitor.json\n", len(file))
	}
}

func (per *Persistance) Read() bool {
	byteValue, err := os.ReadFile(filepath.Join(WD, "gdlemonitor.json"))
	if err != nil {
		per.Dirty = true
		return false
	}
	if err := json.Unmarshal(byteValue, &per); err != nil {
		fmt.Fprintf(os.Stderr, "failed to parse gdlemonitor.json: %v", err)
		per.Dirty = true
		return false
	}
	return true
}
