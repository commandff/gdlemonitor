package aids

import (
	"time"

	"github.com/juju/ratelimit"
)

// //////////////////////////////////////////////////////////////
// User Rate Limiter
//
// Is not stored in persistance cache; but needs to be maintained and culled
// //////////////////////////////////////////////////////////////

type Userlimiter struct {
	RateLimiter *ratelimit.Bucket
	LastSeen    time.Time
}

func (per *Persistance) UserrateLimiter_Get(ip string) *Userlimiter {
	per.UserLimiter_m.Lock()
	defer per.UserLimiter_m.Unlock()

	if v, ok := per.UserLimiter[ip]; ok {
		v.LastSeen = time.Now().UTC()
		return v
	}
	res := Userlimiter{RateLimiter: ratelimit.NewBucket(275*time.Millisecond, 5), LastSeen: time.Now().UTC()}
	per.UserLimiter[ip] = &res
	return &res
}

func (per *Persistance) UserLimiter_Cleanup() {
	per.UserLimiter_m.Lock()
	for ip, v := range P.UserLimiter {
		if time.Since(v.LastSeen) > 30*time.Minute {
			delete(per.UserLimiter, ip)
		}
	}
	per.UserLimiter_m.Unlock()
}
