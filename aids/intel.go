package aids

import (
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"time"
)

// //////////////////////////////////////////////////////////////
// returns public ip address. on error, it returns 0.0.0.0
func GetPublicIP() string {
	if req, err := Intel_httpClient.Get("http://ip-api.com/json/"); err == nil {
		defer req.Body.Close()
		if body, err := io.ReadAll(req.Body); err == nil {
			var ip struct{ Query string }
			if err = json.Unmarshal(body, &ip); err == nil {
				return ip.Query
			}
		}
	}
	return "0.0.0.0"
}

////////////////////////////////////////////////////////////////
// IP persistance cache
//
////////////////////////////////////////////////////////////////

type IP_Result struct {
	LastChecked   time.Time `json:"last_checked"`
	ContinentCode string    `json:"continentCode"`
	CountryCode   string    `json:"countryCode"`
	Region        string    `json:"region"`
	City          string    `json:"city"`
	Lat           float64   `json:"lat"`
	Lon           float64   `json:"lon"`
	Offset        int       `json:"offset"`
	Reverse       string    `json:"reverse"`
	Mobile        bool      `json:"mobile"`
	Proxy         bool      `json:"proxy"`
	Hosting       bool      `json:"hosting"`
}

var (
	IP_whitelist     []*net.IPNet
	Intel_httpClient http.Client = http.Client{Timeout: 10 * time.Second}
)

// Should be called before GetResult, to parse the default whitelist, and validate contact email
func Intel_Init(whitelist []string) error {
	IP_whitelist = nil
	for _, cidr := range whitelist {
		_, block, err := net.ParseCIDR(cidr)
		if err != nil {
			return fmt.Errorf("parse error on %q: %v", cidr, err)
		}
		IP_whitelist = append(IP_whitelist, block)
	}
	return nil
}

// Internal getResult
func (per *Persistance) Pull_ip(ip string) *IP_Result {
	netip := net.ParseIP(ip)
	if netip == nil {
		DebugLog.Printf("Pull_ip(%s): Invalid Address", ip)
		return &IP_Result{}
	}
	if netip.IsLoopback() || netip.IsLinkLocalUnicast() || netip.IsLinkLocalMulticast() || netip.IsPrivate() {
		DebugLog.Printf("Pull_ip(%s): Local", ip)
		return &IP_Result{}
	}
	for _, block := range IP_whitelist {
		if block.Contains(netip) {
			DebugLog.Printf("Pull_ip(%s): whitelist Contains %s", ip, block.String())
			return &IP_Result{}
		}
	}

	per.IP_m.Lock()
	defer per.IP_m.Unlock()
	response, ok := per.IP[ip]

	if ok && time.Since(response.LastChecked) < time.Hour*12 {
		return response
	}

	ipapireq, err := Intel_httpClient.Get(fmt.Sprintf("http://ip-api.com/json/%s?fields=52629718", ip))
	if err != nil {
		DebugLog.Printf("Pull_ip(%s): %s", ip, err.Error())
		return &IP_Result{}
	}
	defer ipapireq.Body.Close()
	if ipapireq.StatusCode != 200 {
		DebugLog.Printf("Pull_ip(%s): req.StatusCode: %d", ip, ipapireq.StatusCode)
		return &IP_Result{}
	}
	response = &IP_Result{}
	if err := json.NewDecoder(ipapireq.Body).Decode(&response); err != nil {
		return &IP_Result{}
	}
	response.LastChecked = time.Now().UTC()
	per.IP[ip] = response
	per.Dirty = true
	// DebugLog.Printf("Pull_ip(%s): Fresh result: %#v", ip, response)

	return response
}

func (per *Persistance) Intel_Cleanup() {
	per.IP_m.Lock()
	for ip, v := range per.IP {
		if time.Since(v.LastChecked) > time.Hour*12 {
			delete(per.IP, ip)
			per.Dirty = true
		}
	}
	per.IP_m.Unlock()
}
