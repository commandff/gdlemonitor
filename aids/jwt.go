package aids

import (
	"fmt"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/commandff/gdlemonitor/models"
)

type Claims struct {
	models.Claim
	jwt.StandardClaims
}

// JWT Key - these are server-wide keys, used to digitally sign and validate tokens. Internally
const JWT_key_change_interval int64 = 1800 // seconds
const JWT_state_lifetime int64 = 1800      // seconds
const JWT_cookie_name string = "gdlemonitor"

// //////////////////////////////////////////////////////////////
// verify access level
// returns authenticated claims struct, if access level is >= reqLevel.
// returns nil if access level < reqLevel
// returns nil if client has no claims
// if reqLevel >= 0, sends HTTP 401
func VerifyAccess(reqLevel int, w http.ResponseWriter, r *http.Request) *Claims {
	acct := GetJWTClaims(w, r)
	if acct != nil && acct.Access >= reqLevel {
		return acct
	}
	if reqLevel >= 0 {
		http.Error(w, fmt.Sprintf("Requires %s", Access(reqLevel)), http.StatusUnauthorized)
	}
	return nil
}
func DropJWT(w http.ResponseWriter, claimsId string) {
	http.SetCookie(w, &http.Cookie{Name: JWT_cookie_name, Domain: P.C.DomainName, Path: "/", MaxAge: -1})
	P.JWTState_Del(claimsId)
}
func GetJWTClaims(w http.ResponseWriter, r *http.Request) *Claims {
	var err error
	cur_ts := time.Now().UTC().Unix()
	cookie, err := r.Cookie(JWT_cookie_name)
	if err != nil {
		return nil
	}
	toxic_dirty_client_provided_claims := &Claims{}
	_, err = jwt.ParseWithClaims(cookie.Value, toxic_dirty_client_provided_claims, getJWTKey)

	if err != nil {
		DropJWT(w, toxic_dirty_client_provided_claims.Id)
		return nil
	}
	// intentional disconnect- the only thing read from the client, is the `Id`
	// field. everything else is pulled back from the cache
	client_provided_id := toxic_dirty_client_provided_claims.Id
	P.JWTState_m.Lock()
	claims, ok := P.JWTState[client_provided_id]
	P.JWTState_m.Unlock()
	if !ok || claims.ExpiresAt <= cur_ts {
		DropJWT(w, claims.Id)
		return nil
	}

	// refresh token, if it has lived for 120 seconds
	if cur_ts-claims.IssuedAt >= 120 {
		RefreshJWT(claims, w)
	}
	return claims
	// P.Auth_m.Lock()
	// defer P.Auth_m.Unlock()
	// acct, ok := P.Auth[claims.Username]
	// if !ok {
	// 	DropJWT(w, claims.Id)
	// 	return nil
	// }
	// return acct
}

func RefreshJWT(claims *Claims, w http.ResponseWriter) {
	cur_ts := time.Now().UTC().Unix()
	claims.IssuedAt = cur_ts
	claims.ExpiresAt = cur_ts + JWT_state_lifetime
	tokenString, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString(makeJWTKey(cur_ts))
	if CheckInternalError(err, &w) {
		return
	}
	P.Dirty = true
	http.SetCookie(w, &http.Cookie{
		Name:    JWT_cookie_name,
		Domain:  P.C.DomainName,
		Path:    "/",
		Value:   tokenString,
		Expires: time.Unix(claims.ExpiresAt, 0),
		Secure:  true,
	})
}

func IssueJWT(acct *models.Account, w http.ResponseWriter) {
	cur_ts := time.Now().UTC().Unix()
	id := RandString(32, SQLpasswordBytes)
	claims := &Claims{
		Claim: acct.Claim,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  cur_ts,
			NotBefore: cur_ts,
			ExpiresAt: cur_ts + JWT_state_lifetime,
			Id:        id,
		},
	}
	// DebugLog.Printf("Issue Token for claims: %#v", claims)
	tokenString, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString(makeJWTKey(cur_ts))
	if CheckInternalError(err, &w) {
		return
	}
	// DebugLog.Printf("Set Cookie: %s", tokenString)
	P.JWTState_Add(claims)
	http.SetCookie(w, &http.Cookie{
		Name:    JWT_cookie_name,
		Domain:  P.C.DomainName,
		Path:    "/",
		Value:   tokenString,
		Expires: time.Unix(claims.ExpiresAt, 0),
		Secure:  true,
	})
}
