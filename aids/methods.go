package aids

import (
	"fmt"
	"math/rand"
	"os/user"
	"strconv"
	"strings"
)

const HTTPCookieBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$%&'()*+-./:<>?@[]^_`{|}~"
const URLBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_~!$&'()*+,;=:@"
const SQLpasswordBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
const JWTkeyBytes = " !#$%&()*+,-./0123456789:;<=>?&ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_abcdefghijklmnopqrstuvwxyz{|}"
const HexBytes = "0123456789abcdef"

func RandString(n int, source string) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = source[rand.Int63()%int64(len(source))]
	}
	return string(b)
}

func Access(access int) string {
	switch access {
	case 0:
		return "None"
	case 1:
		return "User"
	case 3:
		return "Advocate"
	case 4:
		return "Sentinel"
	case 6:
		return "Admin"
	default:
		return fmt.Sprintf("error(%d)", access)
	}
}
func Format(n int) string {
	in := strconv.FormatInt(int64(n), 10)
	numOfDigits := len(in)
	if n < 0 {
		numOfDigits-- // First character is the - sign (not a digit)
	}
	numOfCommas := (numOfDigits - 1) / 3

	out := make([]byte, len(in)+numOfCommas)
	if n < 0 {
		in, out[0] = in[1:], '-'
	}

	for i, j, k := len(in)-1, len(out)-1, 0; ; i, j = i-1, j-1 {
		out[j] = in[i]
		if i == 0 {
			return string(out)
		}
		if k++; k == 3 {
			j, k = j-1, 0
			out[j] = ','
		}
	}
}
func ByteCountIEC(b uint64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := uint64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.2f%ciB", float64(b)/float64(div), "KMGTPE"[exp])
}

func GetUser() string {
	currentUser, err := user.Current()
	if err != nil {
		return "unknown"
	} else {
		if strings.Contains(currentUser.Username, "\\") {
			return strings.Split(currentUser.Username, "\\")[1]
		} else {
			return currentUser.Username
		}
	}
}
func Sifplurral(input int) string {
	if input == 0 {
		return ""
	}
	return "s"
}
