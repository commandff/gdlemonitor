package aids

import (
	"fmt"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt"
)

////////////////////////////////////////////////////////////////
// JWT Key Persistance Cache
//
// These are the JWT Encryption Keys; 32^90 possible combinations
// this is useless, cracking this accomplishes nothing; accounts are still held by their "id" (bearer token)
////////////////////////////////////////////////////////////////

// issue token
func makeJWTKey(cur_ts int64) []byte {
	claim_interval := fmt.Sprint(cur_ts - (cur_ts % JWT_key_change_interval))
	P.JWTKeys_m.Lock()
	jwtKey, ok := P.JWTKeys[claim_interval]
	if !ok {
		jwtKey = RandString(32, JWTkeyBytes)
		// DebugLog.Printf("Generated key INT:%s for ts %d: %s", claim_interval, cur_ts, jwtKey)
		P.JWTKeys[claim_interval] = jwtKey
		P.Dirty = true
	}
	P.JWTKeys_m.Unlock()
	return []byte(jwtKey)
}

func getJWTKey(token *jwt.Token) (interface{}, error) {
	claims, ok := token.Claims.(*Claims)
	if !ok {
		return nil, fmt.Errorf("failed to parse claims")
	}
	//potential issue- basing decryption key off of client-provided interval; TODO: confirm this doesn't lead to any null keys, or the like
	claim_interval := fmt.Sprint(claims.IssuedAt - (claims.IssuedAt % JWT_key_change_interval))
	P.JWTKeys_m.Lock()
	defer P.JWTKeys_m.Unlock()
	if jwtKey, ok := P.JWTKeys[claim_interval]; ok {
		return []byte(jwtKey), nil
	}
	// DebugLog.Printf("No such interval INT:%s for ts %d", claim_interval, claims.IssuedAt)
	return nil, fmt.Errorf("no such interval")
}

func (per *Persistance) JWTKeys_Cleanup() {
	minval := time.Now().UTC().Unix() - (JWT_state_lifetime + JWT_key_change_interval) // need to hold expired keys, until the end of the lifetime of what they signed...
	per.JWTKeys_m.Lock()
	for k := range per.JWTKeys {
		i, _ := strconv.ParseInt(k, 10, 64)
		if i < minval {
			delete(per.JWTKeys, k)
			per.Dirty = true
		}
	}
	per.JWTKeys_m.Unlock()
}
