package aids

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	crand "crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"net"
	"os"
	"strings"
	"time"
)

const self_signed_exp time.Duration = time.Hour * 24 * 365 * 2

type TLSCert struct {
	Subject string `json:"subject"`

	Provider string   `json:"provider"`
	Domain   []string `json:"domain"`
	IP       []net.IP `json:"ip"`

	CRT    []byte    `json:"crt"`
	KEY    []byte    `json:"key"`
	Serial int64     `json:"serial"`
	Iss    time.Time `json:"iss"`
	Exp    time.Time `json:"exp"`

	Tls *tls.Certificate `json:"-"`
}

// LOCKS TLSCert | Loads or generates key, requires Subject
func GetCert(subject string) *TLSCert {
	P.TLS_m.Lock()
	defer P.TLS_m.Unlock()
	tlscrt, ok := P.TLS[subject]
	if ok {
		if tlscrt.Exp.After(time.Now()) {
			if tlscrt.Tls == nil {

				cert, _ := tls.X509KeyPair(tlscrt.CRT, tlscrt.KEY)
				tlscrt.Tls = &cert
			}
			return tlscrt
		}
	} else {
		tlscrt = &TLSCert{
			Subject: subject,
		}
		P.TLS[subject] = tlscrt
	}
	DebugLog.Printf("GetCert(%#v) Checking self-signed tls cert", subject)

	dirty, err := tlscrt.resolvCertDom()
	if err != nil {
		DebugLog.Printf("GetCert(%#v) tlscrt.resolvCertDom() error: %v", subject, err.Error())
		return tlscrt
	}
	if dirty || tlscrt.CRT == nil || tlscrt.KEY == nil || time.Now().After(tlscrt.Exp) {
		DebugLog.Printf("GetCert(%#v) GENERATING self-signed tls cert", subject)
		err = tlscrt.Generate()
		if err != nil {
			DebugLog.Printf("GetCert(%#v) tlscrt.Generate() error: %v", subject, err.Error())
			return tlscrt
		}
		P.Dirty = true
		P.Write() // write new certs to disk
	}
	cert, _ := tls.X509KeyPair(tlscrt.CRT, tlscrt.KEY)
	tlscrt.Tls = &cert
	return tlscrt
}

// Expects LOCKED TLSCert | Sets (and consumes) Provider, Domain, IP; Requires Subject
func (tlscrt *TLSCert) resolvCertDom() (dirty bool, err error) {
	var addr []string
	addr, err = net.LookupHost(tlscrt.Subject)
	if err != nil {
		return
	}
	Ns, _ := net.LookupNS(tlscrt.Subject)
	DNS := make([]string, len(Ns))
	for i, ns := range Ns {
		if ns.Host[len(ns.Host)-1] == '.' {
			DNS[i] = ns.Host[:len(ns.Host)-1]
		} else {
			DNS[i] = ns.Host + tlscrt.Subject
		}
		_dnsProvider := resolvDnsProvider(DNS[i])
		if _dnsProvider != "" {
			if tlscrt.Provider != "" && tlscrt.Provider != _dnsProvider {
				DebugLog.Printf("Miss-matched DNS Providers, from %#v to %#v", tlscrt.Provider, _dnsProvider)
			}
			tlscrt.Provider = _dnsProvider
		}
	}

	tlscrt.Domain = []string{tlscrt.Subject}
	if len(tlscrt.IP) > 0 && len(addr) > 0 && !tlscrt.IP[0].Equal(net.ParseIP(addr[0])) {
		DebugLog.Printf("%s read new ip %s (was %s)", tlscrt.Subject, addr[0], tlscrt.IP[0].String())
		dirty = true
	}
	tlscrt.IP = make([]net.IP, len(addr))
	var isRemote bool = false
	for i, a := range addr {
		tlscrt.IP[i] = net.ParseIP(a)
		if !tlscrt.IP[i].IsLoopback() && !tlscrt.IP[i].IsPrivate() {
			if a != P.Cd.PublicIP {
				DebugLog.Printf("%s ip invalid, read %s expecting %s", tlscrt.Subject, a, P.Cd.PublicIP)
			}
			var dom []string
			dom, err = net.LookupAddr(tlscrt.IP[i].String())
			if err != nil {
				return
			}
			for _, b := range dom {
				tlscrt.Domain = append(tlscrt.Domain, strings.TrimSuffix(b, "."))
			}
			isRemote = true
		}
	}
	if isRemote {
		tlscrt.Domain = append(tlscrt.Domain, "*."+tlscrt.Subject)
	} else if len(addr) > 0 {
		hostname, _ := os.Hostname()
		tlscrt.Domain = append(tlscrt.Domain, hostname)

	}
	envVar := []string{}
	if lut, ok := dnsProviderLUT[tlscrt.Provider]; ok {
		for _, e := range lut {
			if val, ook := P.C.ENV[e.Name]; ook {
				envVar = append(envVar, fmt.Sprintf("%#v:%#v", e.Name, val))
			} else {
				envVar = append(envVar, fmt.Sprintf("%#v:null", e.Name))
			}
		}
	}

	DebugLog.Printf("TLSCert.resolvCertDom(%s) IPs: %v Domains: %#v Provider:%#v {%s}",
		tlscrt.Subject, tlscrt.IP, tlscrt.Domain, tlscrt.Provider, strings.Join(envVar, ","))
	return
}

type EnvVar struct {
	Name string
	Desc string
}

var dnsProviderLUT = map[string][]EnvVar{
	"cloudflare": {
		{"CF_API_EMAIL", "CloudFlare Account email"},
		{"CF_API_KEY", "CloudFlare API key"},
		{"CF_DNS_API_TOKEN", "CloudFlare API token with DNS:Edit permission"},
		{"CF_ZONE_API_TOKEN", "CloudFlare API token with Zone:Read permission"},
	},
	"digitalocean": {
		{"DO_AUTH_TOKEN", "Digital Ocean Authentication token"},
	},
	"godaddy": {
		{"GODADDY_API_KEY", "GoDaddy API key"},
		{"GODADDY_API_SECRET", "GoDaddyAPI secret"},
	},
}

func resolvDnsProvider(ns string) string {
	switch ns {
	case "ns1.digitalocean.com":
		fallthrough
	case "ns2.digitalocean.com":
		fallthrough
	case "ns3.digitalocean.com":
		return "digitalocean"
	case "ns07.domaincontrol.com":
		fallthrough
	case "ns08.domaincontrol.com":
		return "godaddy"
	case "brianna.ns.cloudflare.com":
		fallthrough
	case "james.ns.cloudflare.com":
		return "cloudflare"
	default:
		return ""
	}
}

// Expects LOCKED TLSCert | Sets CRT, KEY, Serial, Iss, Exp; requires Domain, IP
func (tlscrt *TLSCert) Generate() error {
	tlscrt.Iss = time.Now().UTC()
	tlscrt.Serial = (tlscrt.Iss.UnixMilli() / 420)
	tlscrt.Exp = tlscrt.Iss.Add(self_signed_exp)
	template := x509.Certificate{
		SerialNumber:          big.NewInt(tlscrt.Serial),
		Subject:               pkix.Name{CommonName: tlscrt.Domain[0]},
		DNSNames:              tlscrt.Domain,
		IPAddresses:           tlscrt.IP,
		NotBefore:             tlscrt.Iss,
		NotAfter:              tlscrt.Exp,
		BasicConstraintsValid: true,
		IsCA:                  true,
		SignatureAlgorithm:    x509.ECDSAWithSHA384,
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageCertSign | x509.KeyUsageKeyAgreement | x509.KeyUsageDataEncipherment,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
	}
	privateKey, err := ecdsa.GenerateKey(elliptic.P384(), crand.Reader)
	if err != nil {
		return err
	}
	derBytes, err := x509.CreateCertificate(crand.Reader, &template, &template, &privateKey.PublicKey, privateKey)
	if err != nil {
		return err
	}
	eckeybytes, err := x509.MarshalECPrivateKey(privateKey)
	if err != nil {
		return err
	}
	var _key, _crt bytes.Buffer
	err = pem.Encode(&_key, &pem.Block{Type: "EC PRIVATE KEY", Bytes: eckeybytes})
	if err != nil {
		return err
	}
	err = pem.Encode(&_crt, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	if err != nil {
		return err
	}
	tlscrt.KEY = _key.Bytes()
	tlscrt.CRT = _crt.Bytes()
	DebugLog.Printf("Generated new Self-Signed TLS Cert for %s S/N %d exp: %s", tlscrt.Domain[0], tlscrt.Serial, self_signed_exp)
	return nil
}
