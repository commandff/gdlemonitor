package aids

import (
	"time"
)

////////////////////////////////////////////////////////////////
// JWT State Persistance Cache
//
////////////////////////////////////////////////////////////////

func (per *Persistance) JWTState_Add(claims *Claims) {
	per.JWTState_m.Lock()
	per.JWTState[claims.Id] = claims
	per.JWTState_m.Unlock()
	per.Dirty = true
}

func (per *Persistance) JWTState_Del(Id string) {
	per.JWTState_m.Lock()
	if _, ok := per.JWTState[Id]; ok {
		delete(per.JWTState, Id)
		per.Dirty = true
	}
	per.JWTState_m.Unlock()
}

func (per *Persistance) JWTState_Cleanup() {
	per.JWTState_m.Lock()
	for key, claims := range per.JWTState {
		if time.Now().UTC().Unix() > claims.ExpiresAt {
			delete(per.JWTState, key)
			per.Dirty = true
		}
	}
	per.JWTState_m.Unlock()
}
