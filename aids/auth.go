package aids

import (
	"errors"
	"fmt"
	"net"
	"regexp"
	"strings"

	"gitlab.com/commandff/gdlemonitor/models"
)

var reusernamevalid = regexp.MustCompile(`^[a-zA-Z0-9_]{3,}$`)

func Check_username(username string) error {
	if reusernamevalid.MatchString(username) {
		return nil
	}
	return errors.New("username invalid: does not match `^[a-zA-Z0-9_]{3,}$`")
}

var repasswordvalid = regexp.MustCompile(`^[a-z0-9&?!*^%#@\-_=|]{8,60}$`)

func Check_password(password string) error {
	if repasswordvalid.MatchString(password) {
		return nil
	}
	return errors.New("password invalid: does not match `^[a-z0-9&?!*^%#@\\-_=|]{8,60}$`")
}

var reemailvalid = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func Check_email(email string) error {
	if !reemailvalid.MatchString(email) || len(email) < 4 || len(email) > 253 {
		return errors.New("email address is not valid")
	}
	_, err := net.LookupMX(email[strings.Index(email, "@")+1:])
	if err != nil {
		return errors.New("email address is not valid")
	}
	return nil
}

func Check_security(security *[]models.Account_security) error {
	if security == nil {
		if P.C.API.AccountSQMin > 0 {
			return fmt.Errorf("missing Security Questions. %d required", P.C.API.AccountSQMin)
		}
		return nil
	}
	if len(*security) > P.C.API.AccountSQMax {
		return fmt.Errorf("too many Security Questions. %d allowed", P.C.API.AccountSQMax)
	}
	if P.C.API.AccountSQMin > len(*security) {
		return fmt.Errorf("too few Security Questions. %d required", P.C.API.AccountSQMin)
	}
	for i, s := range *security {
		if len(s.Q) == 0 {
			return fmt.Errorf("empty Security Question %d", 1+i)
		}
		if len(s.A) == 0 {
			return fmt.Errorf("empty Security Answer %d", 1+i)
		}
	}
	return nil
}
