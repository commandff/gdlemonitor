package aids

import "net/http"

// if err != nil, writes http 500 with err.Error(), return true
// if no error, return false
func CheckInternalError(err error, w *http.ResponseWriter) bool {
	if err != nil {
		http.Error(*w, err.Error(), http.StatusInternalServerError)
		return true
	}
	return false
}
