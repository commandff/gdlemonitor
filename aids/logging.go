package aids

import (
	"bufio"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"

	"golang.org/x/sys/windows/svc/debug"
)

var Elog debug.Log

var tails []chan int

func Setup_logging() {
	EWInfo = &eWInfo{}
	InfoLog = log.New(EWInfo, "", log.Lshortfile|log.Lmsgprefix)
	EWError = &eWError{}
	ErrorLog = log.New(EWError, "", log.Lshortfile|log.Lmsgprefix)
	EWDebug = &eWDebug{}
	DebugLog = log.New(EWDebug, "", log.Lshortfile|log.Lmsgprefix)
	//log.SetOutput(io.Discard)
}

func LoadTails() {
	// stime := time.Now().UTC().
	var wg sync.WaitGroup
	for _, kevorkian := range tails {
		wg.Add(1)
		go func(k chan int) {
			k <- 0
			wg.Done()
		}(kevorkian)
	}
	wg.Wait()
	tails = nil
	for _, f := range P.C.Gdle.Logs {
		kev := make(chan int)
		tails = append(tails, kev)
		go tail(kev, f)
	}
	// DebugLog.Printf("Loaded %d log file tails, over %v", len(tails), time.Since(stime))
}

func tail(k chan int, file string) {
	for {
		err := innerTail(k, file)
		if err == nil {
			break
		}
		ErrorLog.Printf("tailFile %s Error:%v waiting 5 minutes", file, err)
		time.Sleep(time.Minute * 5)
	}
}

func innerTail(k <-chan int, file string) error {
	fn := filepath.Join(WD, file)
	f, err := os.Open(fn)
	if err != nil {
		return err
	}
	defer f.Close()
	oldSize, err := f.Seek(0, io.SeekEnd) // seek to 0 bytes from the end of the file
	if err != nil {
		return err
	}
	newSize := oldSize
	r := bufio.NewReader(f)
	var info fs.FileInfo
	if err != nil {
		return err
	}
	// InfoLog.Printf("tailFile %s started at pos %d", file, oldSize)
	for {
		for line, _, err := r.ReadLine(); err != io.EOF; line, _, err = r.ReadLine() {
			Elog.Info(100, fmt.Sprintf("[%s] %s", file, line))
		}
		for {
			time.Sleep(time.Second)
			select {
			case <-k:
				return nil
			default:
			}
			info, err = f.Stat()
			if err != nil {
				return err
			}
			newSize = info.Size()
			if newSize != oldSize {
				if newSize < oldSize {
					f.Seek(0, 0)
				} else {
					f.Seek(oldSize, io.SeekStart)
				}
				r = bufio.NewReader(f)
				oldSize = newSize
				break
			}
		}
	}
}

var (
	InfoLog  *log.Logger
	ErrorLog *log.Logger
	DebugLog *log.Logger
	EWInfo   *eWInfo
	EWError  *eWError
	EWDebug  *eWDebug
)

type eWInfo struct {
	io.Writer
}

func (e eWInfo) Write(p []byte) (int, error) {
	return len(p), Elog.Info(1, string(p[:len(p)-1]))
}

type eWError struct {
	io.Writer
}

func (e eWError) Write(p []byte) (int, error) {
	return len(p), Elog.Error(1, string(p[:len(p)-1]))
}

type eWDebug struct {
	io.Writer
}

var ISDebug bool = false

func (e eWDebug) Write(p []byte) (int, error) {
	if ISDebug {
		return len(p), Elog.Info(100, string(p[:len(p)-1]))
	}
	return 0, nil
}
