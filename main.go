package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/commandff/gdlemonitor/aids"
	"gitlab.com/commandff/gdlemonitor/gdle"
	"gitlab.com/commandff/gdlemonitor/www"
	"golang.org/x/sys/windows"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/debug"
	"golang.org/x/sys/windows/svc/eventlog"
	"golang.org/x/sys/windows/svc/mgr"
)

var svcName *string

func main() {
	os.Setenv("TZ", "GMT")
	loc, _ := time.LoadLocation("GMT")
	time.Local = loc // -> this is setting the global timezone
	fi, _ := os.Stat(os.Args[0])
	www.Base_html_modTime = fi.ModTime()
	aids.WD = filepath.Dir(os.Args[0])
	svcName = flag.String("svcname", "GDLEMonitor", "Service name to report as")
	aids.P = new(aids.Persistance).New()
	aids.P.Read()
	rand.Seed(time.Now().UTC().UnixNano()) //seeds the RNG, for password generation later.
	//read GDLE Config from file.
	aids.P.GDLEcfg = gdle.Cfg_Read(filepath.Join(aids.WD, aids.P.C.Gdle.Config))
	if aids.P.GDLEcfg == nil {
		fmt.Fprintf(os.Stderr, "Unable to read GDLE server config: %s", filepath.Join(aids.WD, aids.P.C.Gdle.Config))
	}
	// if we are running in a windows service, go do that.
	inService, err := svc.IsWindowsService()
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to determine if we are running in service: %v", err)
		os.Exit(2)
	}
	if inService {
		aids.Elog, err = eventlog.Open(*svcName)
		if err != nil {
			fmt.Fprintf(os.Stderr, "eventlog.Open(%s) error: %v\n", *svcName, err)
			os.Exit(2)
		}
		defer aids.Elog.Close()
		err = svc.Run(*svcName, &myservice{})
		if err != nil {
			aids.ErrorLog.Printf("%s service failed: %v", *svcName, err)
			os.Exit(2)
		}
		os.Exit(0)
	}

	if len(os.Args) == 2 && strings.ToLower(os.Args[1]) == "install" {
		CheckElevate()
		m, err := mgr.Connect()
		if err != nil {
			fmt.Fprintf(os.Stderr, "mgr.Connect() error: %v\n", err)
			os.Exit(2)
		}
		defer m.Disconnect()
		s, err := m.OpenService(*svcName)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Installing %s Service...\n", *svcName)
			s, err = m.CreateService(*svcName, os.Args[0], mgr.Config{
				ServiceType:      windows.SERVICE_WIN32_OWN_PROCESS, // ServiceType      uint32 1=Kernel Mode, 2=Kernel Mode w/FS, 4=Network, 16=Standalone, 32=Shared
				StartType:        windows.SERVICE_AUTO_START,        // StartType        uint32 0 = Boot, 1 = System, 2 = Autostart, 3 = Manual Start, 4 = Disabled
				ErrorControl:     windows.SERVICE_ERROR_NORMAL,      // ErrorControl     uint32 0=Ignore, 1=Normal, 2=Severe, 3=Critical
				BinaryPathName:   os.Args[0],                        // BinaryPathName   string // fully qualified path to the service binary file, can also include arguments for an auto-start service
				LoadOrderGroup:   "",                                // LoadOrderGroup   string
				TagId:            0,                                 // TagId            uint32
				Dependencies:     []string{"MariaDB"},               // Dependencies     []string
				ServiceStartName: "",                                // ServiceStartName string // name of the account under which the service should run
				DisplayName:      *svcName,                          // DisplayName      string
				Password:         "",                                // Password         string
				Description:      "Monitors, and maintains GDLE.",   // Description      string
				SidType:          windows.SERVICE_SID_TYPE_NONE,     // SidType          uint32 // one of SERVICE_SID_TYPE, the type of sid to use for the service
				DelayedAutoStart: false,                             // DelayedAutoStart bool   // the service is started after other auto-start services are started plus a short delay
			})
			if err != nil {
				fmt.Fprintf(os.Stderr, "m.CreateService() error: %v\n", err)
				os.Exit(2)
			}
			defer s.Close()
			err = eventlog.InstallAsEventCreate(*svcName, eventlog.Error|eventlog.Warning|eventlog.Info)
			if err != nil {
				s.Delete()
				fmt.Fprintf(os.Stderr, "SetupEventLogSource() failed %v\n", err)
				os.Exit(2)
			}
			fmt.Fprintf(os.Stderr, "%s service installed.\n", *svcName)
		}
		defer s.Close()
		// service exists- status?

		status, err := s.Query()
		if err != nil {
			fmt.Fprintf(os.Stderr, "s.Query() error: %v\n", err)
			os.Exit(2)
		}
		fmt.Fprintf(os.Stderr, "%s Service: %#v\n", *svcName, status)
		os.Exit(0) // end of no args block
	}

	aids.Elog = debug.New(*svcName)
	defer aids.Elog.Close()
	aids.ISDebug = true
	fmt.Fprintf(os.Stderr, "starting %s service\n", *svcName)
	err = debug.Run(*svcName, &myservice{})
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s service failed: %v\n", *svcName, err)
		os.Exit(2)
	}
	fmt.Fprintf(os.Stderr, "%s service stopped\n", *svcName)
	os.Exit(0)
}

// Check if the current user has elevated privs; if not, dump err to stdout, and bail
func CheckElevate() {
	if _, err := os.Open("\\\\.\\PHYSICALDRIVE0"); err != nil {
		fmt.Fprintf(os.Stderr, "This program requires account elevation\n")
		os.Exit(2)
	}
}
