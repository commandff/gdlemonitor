package gdle

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/commandff/gdlemonitor/www"
)

func Cfg_Read(filePath string) map[string]interface{} {
	var server_schema struct {
		Properties map[string]interface{} `json:"properties"`
	}
	schemaBytes, err := www.FS.ReadFile("schema/server.json")
	if err != nil {
		return nil
	}
	json.Unmarshal(schemaBytes, &server_schema)
	inFile, err := os.Open(filePath)
	if err != nil {
		return nil
	}
	defer inFile.Close()
	newConfig := make(map[string]interface{})
	scanner := bufio.NewScanner(inFile)
	for scanner.Scan() {
		line := strings.Trim(scanner.Text(), "\r\n ") // trim leading and trailing spaces
		if len(line) == 0 {
			continue
		} // if we still have data
		charo := (line[0] & 95)       // grab the first byte of the line, and clear the capitilization bit
		if charo > 90 || charo < 65 { // character is not [a-zA-Z]
			continue
		}
		kva := strings.SplitAfterN(line, "=", 2)
		if len(kva) != 2 {
			continue
		}
		key := strings.ToLower(strings.Trim(kva[0][:len(kva[0])-1], " "))
		value := strings.Trim(kva[1], "\r ")
		schema_interface, ok := server_schema.Properties[key]
		if !ok {
			continue
		}
		schema_type_int, ok := schema_interface.(map[string]interface{})["type"]
		if !ok {
			continue
		}
		schema_type, ok := schema_type_int.(string)
		if !ok {
			continue
		}

		switch schema_type {
		case "number":
			fval, err := strconv.ParseFloat(value, 64)
			if err == nil {
				newConfig[key] = fval
			}
		case "integer":
			ival, err := strconv.ParseInt(value, 10, 64)
			if err == nil {
				newConfig[key] = ival
			}
		case "boolean":
			if value == "1" {
				newConfig[key] = true
			} else if value == "0" {
				newConfig[key] = false
			}
		case "string":
			newConfig[key] = value
		}

	}
	if len(newConfig) < 5 {
		return nil
	}
	return newConfig
}

func Cfg_Save(targetFile string, gdleConfig map[string]interface{}) error {
	var server_schema struct {
		Properties map[string]interface{} `json:"properties"`
	}
	schemaBytes, err := www.FS.ReadFile("schema/server.json")
	if err != nil {
		return err
	}
	json.Unmarshal(schemaBytes, &server_schema)
	f, err := os.Create(targetFile + "~")
	if err != nil {
		return err
	}
	keys := make([]string, 0, len(gdleConfig))
	for k := range gdleConfig {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		if len(k) > 0 {
			schema_interface, ok := server_schema.Properties[k]
			if !ok {
				continue
			}
			schema_type_int, ok := schema_interface.(map[string]interface{})["type"]
			if !ok {
				continue
			}
			schema_type, ok := schema_type_int.(string)
			if !ok {
				continue
			}
			switch schema_type {
			case "number":
				fmt.Fprintf(f, "%s=%g\n", k, gdleConfig[k])
			case "integer":
				fmt.Fprintf(f, "%s=%.0f\n", k, gdleConfig[k])
			case "boolean":
				if gdleConfig[k].(bool) {
					fmt.Fprintf(f, "%s=1\n", k)
				} else {
					fmt.Fprintf(f, "%s=0\n", k)
				}
			case "string":
				fmt.Fprintf(f, "%s=%s\n", k, gdleConfig[k])
			}
		}
	}
	err = f.Close()
	if err != nil {
		return err
	}
	err = os.Rename(targetFile+"~", targetFile)
	if err != nil {
		return err
	}
	return nil
}
