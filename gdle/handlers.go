package gdle

// // GET /gdle/spells/{id}.json
// func Handle_get_gdle_spells_id_json(w http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)
// 	id64, _ := strconv.ParseUint(vars["id"], 10, 64)
// 	spell_json := Spells_get(int(id64))
// 	if len(spell_json) == 2 {
// 		w.WriteHeader(http.StatusNotFound)
// 		return
// 	}
// 	w.Header().Set("Content-Type", "application/json")
// 	w.Write(spell_json)
// }

// // PUT /gdle/spells/{id}.json
// func Handle_put_gdle_spells_id_json(w http.ResponseWriter, r *http.Request) {
// 	claims := aids.VerifyAccess(4, w, r)
// 	if claims == nil {
// 		return
// 	}
// 	body, err := io.ReadAll(r.Body)
// 	if aids.CheckInternalError(err, &w) {
// 		return
// 	}
// 	if !json.Valid(body) {
// 		w.WriteHeader(http.StatusBadRequest)
// 		return
// 	}
// 	vars := mux.Vars(r)
// 	id64, _ := strconv.ParseUint(vars["id"], 10, 64)
// 	err = Spells_set(int(id64), string(body))
// 	if aids.CheckInternalError(err, &w) {
// 		return
// 	}
// 	w.WriteHeader(http.StatusNoContent)
// }

// // GET /gdle/spells.json
// func Handle_get_gdle_spells_json(w http.ResponseWriter, r *http.Request) {
// 	if Spells_list == nil {
// 		Spells_m.Lock()
// 		defer Spells_m.Unlock() // Read Spells, INSIDE of Spells_m lock; preventing other checks until it is populated :smart:
// 		err := Spells_Read(aids.WD)
// 		if err != nil {
// 			w.WriteHeader(http.StatusNotFound)
// 			return
// 		}
// 	}
// 	w.Header().Set("Content-Type", "application/json")
// 	w.Header().Add("Content-Encoding", "gzip")
// 	w.Write(Spells_list)
// }

// // GET /gdle/icons/{id}.png
// func Handle_get_gdle_icons_id_png(w http.ResponseWriter, r *http.Request) {
// 	// vars := mux.Vars(r)
// 	// id64, _ := strconv.ParseUint(vars["id"], 10, 64)
// 	// id := uint32(id64)
// 	// Icons_m.Lock()
// 	// defer Icons_m.Unlock()
// 	// data, ok := Icons[id]
// 	// if !ok {
// 	w.WriteHeader(http.StatusNotFound)
// 	// return
// 	// }
// 	// // todo - wrap these in gzip, add heavy cache headers
// 	// w.Header().Set("Content-Type", "image/png")
// 	// w.Write(data)
// }

// // GET /gdle/wcid/{id}.json
// func Handle_get_gdle_wcid_id_json(w http.ResponseWriter, r *http.Request) {
// 	// vars := mux.Vars(r)
// 	// id64, _ := strconv.ParseUint(vars["id"], 10, 64)
// 	// id := int(id64)
// 	// WCID_m.Lock()
// 	// defer WCID_m.Unlock()
// 	// data, ok := WCID[id]
// 	// if !ok {
// 	w.WriteHeader(http.StatusNotFound)
// 	// 	return
// 	// }
// 	// bv, err := json.MarshalIndent(data, "", "    ")
// 	// if aids.CheckInternalError(err, &w) {
// 	// 	return
// 	// }
// 	// w.Header().Set("Content-Type", "application/json")
// 	// w.Write(bv)
// }
