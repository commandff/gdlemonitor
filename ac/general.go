// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

// type JSON_General struct {
// 	LastModified      *string           `json:"lastModified,omitempty"`
// 	ModifiedBy        *string           `json:"modifiedBy,omitempty"`
// 	Changelog         *[]JSON_Changelog `json:"changelog,omitempty"`
// 	UserChangeSummary *string           `json:"userChangeSummary,omitempty"`
// 	IsDone            *bool             `json:"isDone,omitempty"`
// }
// type JSON_Changelog struct {
// 	Created string `json:"created"`
// 	Author  string `json:"author"`
// 	Comment string `json:"comment"`
// }

type Position struct {
	Objcell_id uint32 `json:"objcell_id"`
	Frame      Frame  `json:"frame"`
}

func (t *Position) UnPack(s *SBuffer) any {
	t = new(Position)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func (pos *Position) ToString() string {
	return fmt.Sprintf("0x%08X %s", pos.Objcell_id, pos.Frame.ToString())
}

type Frame struct {
	Origin Vector3    `json:"origin"`
	Angles Quaternion `json:"angles"`
}

func (t *Frame) UnPack(s *SBuffer) any {
	t = new(Frame)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func (f *Frame) ToString() string {
	return fmt.Sprintf("[%s] %s", f.Origin.ToString(), f.Angles.ToString())
}

type Quaternion struct {
	W float32 `json:"w"`
	Vector3
}

func (q *Quaternion) ToString() string {
	return fmt.Sprintf("%s %s", strconv.FormatFloat(float64(q.W), 'f', -1, 32), q.Vector3.ToString())
}

func (t *Quaternion) UnPack(s *SBuffer) any {
	t = new(Quaternion)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Vector3 struct {
	X float32 `json:"x"`
	Y float32 `json:"y"`
	Z float32 `json:"z"`
}

func (v *Vector3) ToString() string {
	return fmt.Sprintf("%s %s %s", strconv.FormatFloat(float64(v.X), 'f', -1, 32), strconv.FormatFloat(float64(v.Y), 'f', -1, 32), strconv.FormatFloat(float64(v.Z), 'f', -1, 32))
}

func (t *Vector3) UnPack(s *SBuffer) any {
	t = new(Vector3)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type SockAddr struct {
	Family int16
	Port   uint16
	Addr   [4]byte
}

func (t *SockAddr) UnPack(s *SBuffer) any {
	t = new(SockAddr)
	binary.Read(s, binary.LittleEndian, t)
	return t
}
func (t *SockAddr) String() string {
	return fmt.Sprintf("%d,%d.%d.%d.%d:%d", t.Family, t.Addr[0], t.Addr[1], t.Addr[2], t.Addr[3], t.Port)
}

type AttributeCache struct {
	Strength     *Attribute          `json:"strength"`
	Endurance    *Attribute          `json:"endurance"`
	Quickness    *Attribute          `json:"quickness"`
	Coordination *Attribute          `json:"coordination"`
	Focus        *Attribute          `json:"focus"`
	Self         *Attribute          `json:"self"`
	Health       *SecondaryAttribute `json:"health"`
	Stamina      *SecondaryAttribute `json:"stamina"`
	Mana         *SecondaryAttribute `json:"mana"`
}

func (t *AttributeCache) UnPack(s *SBuffer) any {
	t = new(AttributeCache)
	contentFlags := ReadT[uint32](s)
	if contentFlags&0x0001 != 0 {
		t.Strength = ReadTP[Attribute](s)
	}
	if contentFlags&0x0002 != 0 {
		t.Endurance = ReadTP[Attribute](s)
	}
	if contentFlags&0x0004 != 0 {
		t.Quickness = ReadTP[Attribute](s)
	}
	if contentFlags&0x0008 != 0 {
		t.Coordination = ReadTP[Attribute](s)
	}
	if contentFlags&0x0010 != 0 {
		t.Focus = ReadTP[Attribute](s)
	}
	if contentFlags&0x0020 != 0 {
		t.Self = ReadTP[Attribute](s)
	}
	if contentFlags&0x0040 != 0 {
		t.Health = ReadTP[SecondaryAttribute](s)
	}
	if contentFlags&0x0080 != 0 {
		t.Stamina = ReadTP[SecondaryAttribute](s)
	}
	if contentFlags&0x0100 != 0 {
		t.Mana = ReadTP[SecondaryAttribute](s)
	}
	// log.Printf("AttributeCache.UnPack(): %s", ToJSON(t))
	return t
}

type Attribute struct {
	LevelFromCp uint32 `json:"level_from_cp"`
	InitLevel   uint32 `json:"init_level"`
	CpSpent     uint32 `json:"cp_spent"`
}

func (t *Attribute) UnPack(s *SBuffer) any {
	t = new(Attribute)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type SecondaryAttribute struct {
	Attribute
	Current uint32 `json:"current"`
}

func (t *SecondaryAttribute) UnPack(s *SBuffer) any {
	t = new(SecondaryAttribute)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Skill struct {
	LevelFromPp           uint16  `json:"level_from_pp"`
	Padding               uint16  `json:"-"`
	Sac                   int32   `json:"sac"`
	Pp                    uint32  `json:"pp"`
	InitLevel             uint32  `json:"init_level"`
	ResistanceOfLastCheck int32   `json:"resistance_of_last_check"`
	LastTimeUsed          float64 `json:"last_used_time"`
}

func (t *Skill) UnPack(s *SBuffer) any {
	t = new(Skill)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Body struct {
	BodyPartTable HashMap[*BodyPart] `json:"body_part_table"`
}

func (t *Body) UnPack(s *SBuffer) any {
	t = new(Body)
	t.BodyPartTable.UnPack(s)
	return t
}

type BodyPart struct {
	Dtype  DType                  `json:"dtype"`
	Dval   int32                  `json:"dval"`
	Dvar   float32                `json:"dvar"`
	Acache ArmorCache             `json:"acache"`
	Bh     BodyHeight             `json:"bh"`
	Bpsd   *BodyPartSelectionData `json:"bpsd,omitempty"`
}

func (t *BodyPart) UnPack(s *SBuffer) any {
	t = new(BodyPart)
	hasBPSD := ReadT[int32](s) == 1
	t.Dtype = ReadT[DType](s)
	t.Dval = ReadT[int32](s)
	t.Dvar = ReadT[float32](s)
	t.Acache.UnPack(s)
	t.Bh = ReadT[BodyHeight](s)
	if hasBPSD {
		t.Bpsd = ReadTP[BodyPartSelectionData](s)
	}
	return t
}

type ArmorCache struct {
	BaseArmor       int32 `json:"base_armor"`
	ArmorVsSlash    int32 `json:"armor_vs_slash"`
	ArmorVsPierce   int32 `json:"armor_vs_pierce"`
	ArmorVsBludgeon int32 `json:"armor_vs_bludgeon"`
	ArmorVsCold     int32 `json:"armor_vs_cold"`
	ArmorVsFire     int32 `json:"armor_vs_fire"`
	ArmorVsAcid     int32 `json:"armor_vs_acid"`
	ArmorVsElectric int32 `json:"armor_vs_electric"`
	ArmorVsNether   int32 `json:"armor_vs_nether"`
}

func (t *ArmorCache) UnPack(s *SBuffer) any {
	t = new(ArmorCache)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type BodyPartSelectionData struct {
	Hlf float32 `json:"HLF"`
	Mlf float32 `json:"MLF"`
	Llf float32 `json:"LLF"`
	Hrf float32 `json:"HRF"`
	Mrf float32 `json:"MRF"`
	Lrf float32 `json:"LRF"`
	Hlb float32 `json:"HLB"`
	Mlb float32 `json:"MLB"`
	Llb float32 `json:"LLB"`
	Hrb float32 `json:"HRB"`
	Mrb float32 `json:"MRB"`
	Lrb float32 `json:"LRB"`
}

func (t *BodyPartSelectionData) UnPack(s *SBuffer) any {
	t = new(BodyPartSelectionData)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func (b *BodyPartSelectionData) ToString() string {
	return fmt.Sprintf("Hlf:%f,Mlf:%f,Llf:%f,Hrf:%f,Mrf:%f,Lrf:%f,Hlb:%f,Mlb:%f,Llb:%f,Hrb:%f,Mrb:%f,Lrb:%f",
		b.Hlf, b.Mlf, b.Llf,
		b.Hrf, b.Mrf, b.Lrf,
		b.Hlb, b.Mlb, b.Llb,
		b.Hrb, b.Mrb, b.Lrb,
	)
}

type AttributeID uint32
type AttributeID2nd uint32
type STypeBool uint32
type STypeInt uint32
type STypeDID uint32
type STypeIID uint32
type STypeFloat uint32
type STypeInt64 uint32
type STypeString uint32
type STypePosition uint32
type VendorType uint32
type SoundType uint32
type PScriptType uint32
type EmoteCategoryKey uint32
type EmoteType uint32
type STypeSkill uint32
type WeenieType uint32
type BodyHeight uint32
type RegenerationType uint32

var AttributeIDEnum []string = []string{"Undef", "Strength", "Endurance", "Quickness", "Coordination", "Focus", "Self"}
var AttributeID2ndEnum []string = []string{"Undef", "MaxHealth", "Health", "MaxStamina", "Stamina", "MaxMana", "Mana"}
var STypeBoolEnum []string = []string{"UNDEF", "STUCK", "OPEN", "LOCKED", "ROT_PROOF", "ALLEGIANCE_UPDATE_REQUEST", "AI_USES_MANA", "AI_USE_HUMAN_MAGIC_ANIMATIONS", "ALLOW_GIVE", "CURRENTLY_ATTACKING", "ATTACKER_AI", "IGNORE_COLLISIONS", "REPORT_COLLISIONS", "ETHEREAL", "GRAVITY_STATUS", "LIGHTS_STATUS", "SCRIPTED_COLLISION", "INELASTIC", "VISIBILITY", "ATTACKABLE", "SAFE_SPELL_COMPONENTS", "ADVOCATE_STATE", "INSCRIBABLE", "DESTROY_ON_SELL", "UI_HIDDEN", "IGNORE_HOUSE_BARRIERS", "HIDDEN_ADMIN", "PK_WOUNDER", "PK_KILLER", "NO_CORPSE", "UNDER_LIFESTONE_PROTECTION", "ITEM_MANA_UPDATE_PENDING", "GENERATOR_STATUS", "RESET_MESSAGE_PENDING", "DEFAULT_OPEN", "DEFAULT_LOCKED", "DEFAULT_ON", "OPEN_FOR_BUSINESS", "IS_FROZEN", "DEAL_MAGICAL_ITEMS", "LOGOFF_IM_DEAD", "REPORT_COLLISIONS_AS_ENVIRONMENT", "ALLOW_EDGE_SLIDE", "ADVOCATE_QUEST", "IS_ADMIN", "IS_ARCH", "IS_SENTINEL", "IS_ADVOCATE", "CURRENTLY_POWERING_UP", "GENERATOR_ENTERED_WORLD", "NEVER_FAIL_CASTING", "VENDOR_SERVICE", "AI_IMMOBILE", "DAMAGED_BY_COLLISIONS", "IS_DYNAMIC", "IS_HOT", "IS_AFFECTING", "AFFECTS_AIS", "SPELL_QUEUE_ACTIVE", "GENERATOR_DISABLED", "IS_ACCEPTING_TELLS", "LOGGING_CHANNEL", "OPENS_ANY_LOCK", "UNLIMITED_USE", "GENERATED_TREASURE_ITEM", "IGNORE_MAGIC_RESIST", "IGNORE_MAGIC_ARMOR", "AI_ALLOW_TRADE", "SPELL_COMPONENTS_REQUIRED", "IS_SELLABLE", "IGNORE_SHIELDS_BY_SKILL", "NODRAW", "ACTIVATION_UNTARGETED", "HOUSE_HAS_GOTTEN_PRIORITY_BOOT_POS", "GENERATOR_AUTOMATIC_DESTRUCTION", "HOUSE_HOOKS_VISIBLE", "HOUSE_REQUIRES_MONARCH", "HOUSE_HOOKS_ENABLED", "HOUSE_NOTIFIED_HUD_OF_HOOK_COUNT", "AI_ACCEPT_EVERYTHING", "IGNORE_PORTAL_RESTRICTIONS", "REQUIRES_BACKPACK_SLOT", "DONT_TURN_OR_MOVE_WHEN_GIVING", "NPC_LOOKS_LIKE_OBJECT", "IGNORE_CLO_ICONS", "APPRAISAL_HAS_ALLOWED_WIELDER", "CHEST_REGEN_ON_CLOSE", "LOGOFF_IN_MINIGAME", "PORTAL_SHOW_DESTINATION", "PORTAL_IGNORES_PK_ATTACK_TIMER", "NPC_INTERACTS_SILENTLY", "RETAINED", "IGNORE_AUTHOR", "LIMBO", "APPRAISAL_HAS_ALLOWED_ACTIVATOR", "EXISTED_BEFORE_ALLEGIANCE_XP_CHANGES", "IS_DEAF", "IS_PSR", "INVINCIBLE", "IVORYABLE", "DYABLE", "CAN_GENERATE_RARE", "CORPSE_GENERATED_RARE", "NON_PROJECTILE_MAGIC_IMMUNE", "ACTD_RECEIVED_ITEMS", "EXECUTING_EMOTE", "FIRST_ENTER_WORLD_DONE", "RECALLS_DISABLED", "RARE_USES_TIMER", "ACTD_PREORDER_RECEIVED_ITEMS", "AFK", "IS_GAGGED", "PROC_SPELL_SELF_TARGETED", "IS_ALLEGIANCE_GAGGED", "EQUIPMENT_SET_TRIGGER_PIECE", "UNINSCRIBE", "WIELD_ON_USE", "CHEST_CLEARED_WHEN_CLOSED", "NEVER_ATTACK", "SUPPRESS_GENERATE_EFFECT", "TREASURE_CORPSE", "EQUIPMENT_SET_ADD_LEVEL", "BARBER_ACTIVE", "TOP_LAYER_PRIORITY", "NO_HELD_ITEM_SHOWN", "LOGIN_AT_LIFESTONE", "OLTHOI_PK", "ACCOUNT_15_DAYS", "HAD_NO_VITAE", "NO_OLTHOI_TALK", "AUTOWIELD_LEFT", "MERGE_LOCKED"}
var STypeIntEnum []string = []string{"UNDEF", "ITEM_TYPE", "CREATURE_TYPE", "PALETTE_TEMPLATE", "CLOTHING_PRIORITY", "ENCUMB_VAL", "ITEMS_CAPACITY", "CONTAINERS_CAPACITY", "MASS", "LOCATIONS", "CURRENT_WIELDED_LOCATION", "MAX_STACK_SIZE", "STACK_SIZE", "STACK_UNIT_ENCUMB", "STACK_UNIT_MASS", "STACK_UNIT_VALUE", "ITEM_USEABLE", "RARE_ID", "UI_EFFECTS", "VALUE", "COIN_VALUE", "TOTAL_EXPERIENCE", "AVAILABLE_CHARACTER", "TOTAL_SKILL_CREDITS", "AVAILABLE_SKILL_CREDITS", "LEVEL", "ACCOUNT_REQUIREMENTS", "ARMOR_TYPE", "ARMOR_LEVEL", "ALLEGIANCE_CP_POOL", "ALLEGIANCE_RANK", "CHANNELS_ALLOWED", "CHANNELS_ACTIVE", "BONDED", "MONARCHS_RANK", "ALLEGIANCE_FOLLOWERS", "RESIST_MAGIC", "RESIST_ITEM_APPRAISAL", "RESIST_LOCKPICK", "DEPRECATED_RESIST_REPAIR", "COMBAT_MODE", "CURRENT_ATTACK_HEIGHT", "COMBAT_COLLISIONS", "NUM_DEATHS", "DAMAGE", "DAMAGE_TYPE", "DEFAULT_COMBAT_STYLE", "ATTACK_TYPE", "WEAPON_SKILL", "WEAPON_TIME", "AMMO_TYPE", "COMBAT_USE", "PARENT_LOCATION", "PLACEMENT_POSITION", "WEAPON_ENCUMBRANCE", "WEAPON_MASS", "SHIELD_VALUE", "SHIELD_ENCUMBRANCE", "MISSILE_INVENTORY_LOCATION", "FULL_DAMAGE_TYPE", "WEAPON_RANGE", "ATTACKERS_SKILL", "DEFENDERS_SKILL", "ATTACKERS_SKILL_VALUE", "ATTACKERS_CLASS", "PLACEMENT", "CHECKPOINT_STATUS", "TOLERANCE", "TARGETING_TACTIC", "COMBAT_TACTIC", "HOMESICK_TARGETING_TACTIC", "NUM_FOLLOW_FAILURES", "FRIEND_TYPE", "FOE_TYPE", "MERCHANDISE_ITEM_TYPES", "MERCHANDISE_MIN_VALUE", "MERCHANDISE_MAX_VALUE", "NUM_ITEMS_SOLD", "NUM_ITEMS_BOUGHT", "MONEY_INCOME", "MONEY_OUTFLOW", "MAX_GENERATED_OBJECTS", "INIT_GENERATED_OBJECTS", "ACTIVATION_RESPONSE", "ORIGINAL_VALUE", "NUM_MOVE_FAILURES", "MIN_LEVEL", "MAX_LEVEL", "LOCKPICK_MOD", "BOOSTER_ENUM", "BOOST_VALUE", "MAX_STRUCTURE", "STRUCTURE", "PHYSICS_STATE", "TARGET_TYPE", "RADARBLIP_COLOR", "ENCUMB_CAPACITY", "LOGIN_TIMESTAMP", "CREATION_TIMESTAMP", "PK_LEVEL_MODIFIER", "GENERATOR_TYPE", "AI_ALLOWED_COMBAT_STYLE", "LOGOFF_TIMESTAMP", "GENERATOR_DESTRUCTION_TYPE", "ACTIVATION_CREATE_CLASS", "ITEM_WORKMANSHIP", "ITEM_SPELLCRAFT", "ITEM_CUR_MANA", "ITEM_MAX_MANA", "ITEM_DIFFICULTY", "ITEM_ALLEGIANCE_RANK_LIMIT", "PORTAL_BITMASK", "ADVOCATE_LEVEL", "GENDER", "ATTUNED", "ITEM_SKILL_LEVEL_LIMIT", "GATE_LOGIC", "ITEM_MANA_COST", "LOGOFF", "ACTIVE", "ATTACK_HEIGHT", "NUM_ATTACK_FAILURES", "AI_CP_THRESHOLD", "AI_ADVANCEMENT_STRATEGY", "VERSION", "AGE", "VENDOR_HAPPY_MEAN", "VENDOR_HAPPY_VARIANCE", "CLOAK_STATUS", "VITAE_CP_POOL", "NUM_SERVICES_SOLD", "MATERIAL_TYPE", "NUM_ALLEGIANCE_BREAKS", "SHOWABLE_ON_RADAR", "PLAYER_KILLER_STATUS", "VENDOR_HAPPY_MAX_ITEMS", "SCORE_PAGE_NUM", "SCORE_CONFIG_NUM", "SCORE_NUM_SCORES", "DEATH_LEVEL", "AI_OPTIONS", "OPEN_TO_EVERYONE", "GENERATOR_TIME_TYPE", "GENERATOR_START_TIME", "GENERATOR_END_TIME", "GENERATOR_END_DESTRUCTION_TYPE", "XP_OVERRIDE", "NUM_CRASH_AND_TURNS", "COMPONENT_WARNING_THRESHOLD", "HOUSE_STATUS", "HOOK_PLACEMENT", "HOOK_TYPE", "HOOK_ITEM_TYPE", "AI_PP_THRESHOLD", "GENERATOR_VERSION", "HOUSE_TYPE", "PICKUP_EMOTE_OFFSET", "WEENIE_ITERATION", "WIELD_REQUIREMENTS", "WIELD_SKILLTYPE", "WIELD_DIFFICULTY", "HOUSE_MAX_HOOKS_USABLE", "HOUSE_CURRENT_HOOKS_USABLE", "ALLEGIANCE_MIN_LEVEL", "ALLEGIANCE_MAX_LEVEL", "HOUSE_RELINK_HOOK_COUNT", "SLAYER_CREATURE_TYPE", "CONFIRMATION_IN_PROGRESS", "CONFIRMATION_TYPE_IN_PROGRESS", "TSYS_MUTATION_DATA", "NUM_ITEMS_IN_MATERIAL", "NUM_TIMES_TINKERED", "APPRAISAL_LONG_DESC_DECORATION", "APPRAISAL_LOCKPICK_SUCCESS_PERCENT", "APPRAISAL_PAGES", "APPRAISAL_MAX_PAGES", "APPRAISAL_ITEM_SKILL", "GEM_COUNT", "GEM_TYPE", "IMBUED_EFFECT", "ATTACKERS_RAW_SKILL_VALUE", "CHESS_RANK", "CHESS_TOTALGAMES", "CHESS_GAMESWON", "CHESS_GAMESLOST", "TYPE_OF_ALTERATION", "SKILL_TO_BE_ALTERED", "SKILL_ALTERATION_COUNT", "HERITAGE_GROUP", "TRANSFER_FROM_ATTRIBUTE", "TRANSFER_TO_ATTRIBUTE", "ATTRIBUTE_TRANSFER_COUNT", "FAKE_FISHING_SKILL", "NUM_KEYS", "DEATH_TIMESTAMP", "PK_TIMESTAMP", "VICTIM_TIMESTAMP", "HOOK_GROUP", "ALLEGIANCE_SWEAR_TIMESTAMP", "HOUSE_PURCHASE_TIMESTAMP", "REDIRECTABLE_EQUIPPED_ARMOR_COUNT", "MELEEDEFENSE_IMBUEDEFFECTTYPE_CACHE", "MISSILEDEFENSE_IMBUEDEFFECTTYPE_CACHE", "MAGICDEFENSE_IMBUEDEFFECTTYPE_CACHE", "ELEMENTAL_DAMAGE_BONUS", "IMBUE_ATTEMPTS", "IMBUE_SUCCESSES", "CREATURE_KILLS", "PLAYER_KILLS_PK", "PLAYER_KILLS_PKL", "RARES_TIER_ONE", "RARES_TIER_TWO", "RARES_TIER_THREE", "RARES_TIER_FOUR", "RARES_TIER_FIVE", "AUGMENTATION_STAT", "AUGMENTATION_FAMILY_STAT", "AUGMENTATION_INNATE_FAMILY", "AUGMENTATION_INNATE_STRENGTH", "AUGMENTATION_INNATE_ENDURANCE", "AUGMENTATION_INNATE_COORDINATION", "AUGMENTATION_INNATE_QUICKNESS", "AUGMENTATION_INNATE_FOCUS", "AUGMENTATION_INNATE_SELF", "AUGMENTATION_SPECIALIZE_SALVAGING", "AUGMENTATION_SPECIALIZE_ITEM_TINKERING", "AUGMENTATION_SPECIALIZE_ARMOR_TINKERING", "AUGMENTATION_SPECIALIZE_MAGIC_ITEM_TINKERING", "AUGMENTATION_SPECIALIZE_WEAPON_TINKERING", "AUGMENTATION_EXTRA_PACK_SLOT", "AUGMENTATION_INCREASED_CARRYING_CAPACITY", "AUGMENTATION_LESS_DEATH_ITEM_LOSS", "AUGMENTATION_SPELLS_REMAIN_PAST_DEATH", "AUGMENTATION_CRITICAL_DEFENSE", "AUGMENTATION_BONUS_XP", "AUGMENTATION_BONUS_SALVAGE", "AUGMENTATION_BONUS_IMBUE_CHANCE", "AUGMENTATION_FASTER_REGEN", "AUGMENTATION_INCREASED_SPELL_DURATION", "AUGMENTATION_RESISTANCE_FAMILY", "AUGMENTATION_RESISTANCE_SLASH", "AUGMENTATION_RESISTANCE_PIERCE", "AUGMENTATION_RESISTANCE_BLUNT", "AUGMENTATION_RESISTANCE_ACID", "AUGMENTATION_RESISTANCE_FIRE", "AUGMENTATION_RESISTANCE_FROST", "AUGMENTATION_RESISTANCE_LIGHTNING", "RARES_TIER_ONE_LOGIN", "RARES_TIER_TWO_LOGIN", "RARES_TIER_THREE_LOGIN", "RARES_TIER_FOUR_LOGIN", "RARES_TIER_FIVE_LOGIN", "RARES_LOGIN_TIMESTAMP", "RARES_TIER_SIX", "RARES_TIER_SEVEN", "RARES_TIER_SIX_LOGIN", "RARES_TIER_SEVEN_LOGIN", "ITEM_ATTRIBUTE_LIMIT", "ITEM_ATTRIBUTE_LEVEL_LIMIT", "ITEM_ATTRIBUTE_2ND_LIMIT", "ITEM_ATTRIBUTE_2ND_LEVEL_LIMIT", "CHARACTER_TITLE_ID", "NUM_CHARACTER_TITLES", "RESISTANCE_MODIFIER_TYPE", "FREE_TINKERS_BITFIELD", "EQUIPMENT_SET_ID", "PET_CLASS", "LIFESPAN", "REMAINING_LIFESPAN", "USE_CREATE_QUANTITY", "WIELD_REQUIREMENTS_2", "WIELD_SKILLTYPE_2", "WIELD_DIFFICULTY_2", "WIELD_REQUIREMENTS_3", "WIELD_SKILLTYPE_3", "WIELD_DIFFICULTY_3", "WIELD_REQUIREMENTS_4", "WIELD_SKILLTYPE_4", "WIELD_DIFFICULTY_4", "UNIQUE", "SHARED_COOLDOWN", "FACTION1_BITS", "FACTION2_BITS", "FACTION3_BITS", "HATRED1_BITS", "HATRED2_BITS", "HATRED3_BITS", "SOCIETY_RANK_CELHAN", "SOCIETY_RANK_ELDWEB", "SOCIETY_RANK_RADBLO", "HEAR_LOCAL_SIGNALS", "HEAR_LOCAL_SIGNALS_RADIUS", "CLEAVING", "AUGMENTATION_SPECIALIZE_GEARCRAFT", "AUGMENTATION_INFUSED_CREATURE_MAGIC", "AUGMENTATION_INFUSED_ITEM_MAGIC", "AUGMENTATION_INFUSED_LIFE_MAGIC", "AUGMENTATION_INFUSED_WAR_MAGIC", "AUGMENTATION_CRITICAL_EXPERTISE", "AUGMENTATION_CRITICAL_POWER", "AUGMENTATION_SKILLED_MELEE", "AUGMENTATION_SKILLED_MISSILE", "AUGMENTATION_SKILLED_MAGIC", "IMBUED_EFFECT_2", "IMBUED_EFFECT_3", "IMBUED_EFFECT_4", "IMBUED_EFFECT_5", "DAMAGE_RATING", "DAMAGE_RESIST_RATING", "AUGMENTATION_DAMAGE_BONUS", "AUGMENTATION_DAMAGE_REDUCTION", "IMBUE_STACKING_BITS", "HEAL_OVER_TIME", "CRIT_RATING", "CRIT_DAMAGE_RATING", "CRIT_RESIST_RATING", "CRIT_DAMAGE_RESIST_RATING", "HEALING_RESIST_RATING", "DAMAGE_OVER_TIME", "ITEM_MAX_LEVEL", "ITEM_XP_STYLE", "EQUIPMENT_SET_EXTRA", "AETHERIA_BITFIELD", "HEALING_BOOST_RATING", "HERITAGE_SPECIFIC_ARMOR", "ALTERNATE_RACIAL_SKILLS", "AUGMENTATION_JACK_OF_ALL_TRADES", "AUGMENTATION_RESISTANCE_NETHER", "AUGMENTATION_INFUSED_VOID_MAGIC", "WEAKNESS_RATING", "NETHER_OVER_TIME", "NETHER_RESIST_RATING", "LUMINANCE_AWARD", "LUM_AUG_DAMAGE_RATING", "LUM_AUG_DAMAGE_REDUCTION_RATING", "LUM_AUG_CRIT_DAMAGE_RATING", "LUM_AUG_CRIT_REDUCTION_RATING", "LUM_AUG_SURGE_EFFECT_RATING", "LUM_AUG_SURGE_CHANCE_RATING", "LUM_AUG_ITEM_MANA_USAGE", "LUM_AUG_ITEM_MANA_GAIN", "LUM_AUG_VITALITY", "LUM_AUG_HEALING_RATING", "LUM_AUG_SKILLED_CRAFT", "LUM_AUG_SKILLED_SPEC", "LUM_AUG_NO_DESTROY_CRAFT", "RESTRICT_INTERACTION", "OLTHOI_LOOT_TIMESTAMP", "OLTHOI_LOOT_STEP", "USE_CREATES_CONTRACT_ID", "DOT_RESIST_RATING", "LIFE_RESIST_RATING", "CLOAK_WEAVE_PROC", "WEAPON_TYPE", "MELEE_MASTERY", "RANGED_MASTERY", "SNEAK_ATTACK_RATING", "RECKLESSNESS_RATING", "DECEPTION_RATING", "COMBAT_PET_RANGE", "WEAPON_AURA_DAMAGE", "WEAPON_AURA_SPEED", "SUMMONING_MASTERY", "HEARTBEAT_LIFESPAN", "USE_LEVEL_REQUIREMENT", "LUM_AUG_ALL_SKILLS", "USE_REQUIRES_SKILL", "USE_REQUIRES_SKILL_LEVEL", "USE_REQUIRES_SKILL_SPEC", "USE_REQUIRES_LEVEL", "GEAR_DAMAGE", "GEAR_DAMAGE_RESIST", "GEAR_CRIT", "GEAR_CRIT_RESIST", "GEAR_CRIT_DAMAGE", "GEAR_CRIT_DAMAGE_RESIST", "GEAR_HEALING_BOOST", "GEAR_NETHER_RESIST", "GEAR_LIFE_RESIST", "GEAR_MAX_HEALTH", "UNKNOWN_380", "PK_DAMAGE_RATING", "PK_DAMAGE_RESIST_RATING", "GEAR_PK_DAMAGE_RATING", "GEAR_PK_DAMAGE_RESIST_RATING", "UNKNOWN_385_SEEN", "OVERPOWER", "OVERPOWER_RESIST", "GEAR_OVERPOWER", "GEAR_OVERPOWER_RESIST", "ENLIGHTENMENT"}
var STypeDIDEnum []string = []string{"UNDEF", "SETUP", "MOTION_TABLE", "SOUND_TABLE", "COMBAT_TABLE", "QUALITY_FILTER", "PALETTE_BASE", "CLOTHINGBASE", "ICON", "EYES_TEXTURE", "NOSE_TEXTURE", "MOUTH_TEXTURE", "DEFAULT_EYES_TEXTURE", "DEFAULT_NOSE_TEXTURE", "DEFAULT_MOUTH_TEXTURE", "HAIR_PALETTE", "EYES_PALETTE", "SKIN_PALETTE", "HEAD_OBJECT", "ACTIVATION_ANIMATION", "INIT_MOTION", "ACTIVATION_SOUND", "PHYSICS_EFFECT_TABLE", "USE_SOUND", "USE_TARGET_ANIMATION", "USE_TARGET_SUCCESS_ANIMATION", "USE_TARGET_FAILURE_ANIMATION", "USE_USER_ANIMATION", "SPELL", "SPELL_COMPONENT", "PHYSICS_SCRIPT", "LINKED_PORTAL_ONE", "WIELDED_TREASURE_TYPE", "INVENTORY_TREASURE_TYPE", "SHOP_TREASURE_TYPE", "DEATH_TREASURE_TYPE", "MUTATE_FILTER", "ITEM_SKILL_LIMIT", "USE_CREATE_ITEM", "DEATH_SPELL", "VENDORS_CLASSID", "ITEM_SPECIALIZED_ONLY", "HOUSEID", "ACCOUNT_HOUSEID", "RESTRICTION_EFFECT", "CREATION_MUTATION_FILTER", "TSYS_MUTATION_FILTER", "LAST_PORTAL", "LINKED_PORTAL_TWO", "ORIGINAL_PORTAL", "ICON_OVERLAY", "ICON_OVERLAY_SECONDARY", "ICON_UNDERLAY", "AUGMENTATION_MUTATION_FILTER", "AUGMENTATION_EFFECT", "PROC_SPELL", "AUGMENTATION_CREATE_ITEM", "ALTERNATE_CURRENCY", "BLUE_SURGE_SPELL", "YELLOW_SURGE_SPELL", "RED_SURGE_SPELL", "OLTHOI_DEATH_TREASURE_TYPE"}
var STypeIIDEnum []string = []string{"UNDEF", "OWNER", "CONTAINER", "WIELDER", "FREEZER", "VIEWER", "GENERATOR", "SCRIBE", "CURRENT_COMBAT_TARGET", "CURRENT_ENEMY", "PROJECTILE_LAUNCHER", "CURRENT_ATTACKER", "CURRENT_DAMAGER", "CURRENT_FOLLOW_TARGET", "CURRENT_APPRAISAL_TARGET", "CURRENT_FELLOWSHIP_APPRAISAL_TARGET", "ACTIVATION_TARGET", "CREATOR", "VICTIM", "KILLER", "VENDOR", "CUSTOMER", "BONDED", "WOUNDER", "ALLEGIANCE", "PATRON", "MONARCH", "COMBAT_TARGET", "HEALTH_QUERY_TARGET", "LAST_UNLOCKER", "CRASH_AND_TURN_TARGET", "ALLOWED_ACTIVATOR", "HOUSE_OWNER", "HOUSE", "SLUMLORD", "MANA_QUERY_TARGET", "CURRENT_GAME", "REQUESTED_APPRAISAL_TARGET", "ALLOWED_WIELDER", "ASSIGNED_TARGET", "LIMBO_SOURCE", "SNOOPER", "TELEPORTED_CHARACTER", "PET", "PET_OWNER", "PET_DEVICE"}
var STypeFloatEnum []string = []string{"UNDEF", "HEARTBEAT_INTERVAL", "HEARTBEAT_TIMESTAMP", "HEALTH_RATE", "STAMINA_RATE", "MANA_RATE", "HEALTH_UPON_RESURRECTION", "STAMINA_UPON_RESURRECTION", "MANA_UPON_RESURRECTION", "START_TIME", "STOP_TIME", "RESET_INTERVAL", "SHADE", "ARMOR_MOD_VS_SLASH", "ARMOR_MOD_VS_PIERCE", "ARMOR_MOD_VS_BLUDGEON", "ARMOR_MOD_VS_COLD", "ARMOR_MOD_VS_FIRE", "ARMOR_MOD_VS_ACID", "ARMOR_MOD_VS_ELECTRIC", "COMBAT_SPEED", "WEAPON_LENGTH", "DAMAGE_VARIANCE", "CURRENT_POWER_MOD", "ACCURACY_MOD", "STRENGTH_MOD", "MAXIMUM_VELOCITY", "ROTATION_SPEED", "MOTION_TIMESTAMP", "WEAPON_DEFENSE", "WIMPY_LEVEL", "VISUAL_AWARENESS_RANGE", "AURAL_AWARENESS_RANGE", "PERCEPTION_LEVEL", "POWERUP_TIME", "MAX_CHARGE_DISTANCE", "CHARGE_SPEED", "BUY_PRICE", "SELL_PRICE", "DEFAULT_SCALE", "LOCKPICK_MOD", "REGENERATION_INTERVAL", "REGENERATION_TIMESTAMP", "GENERATOR_RADIUS", "TIME_TO_ROT", "DEATH_TIMESTAMP", "PK_TIMESTAMP", "VICTIM_TIMESTAMP", "LOGIN_TIMESTAMP", "CREATION_TIMESTAMP", "MINIMUM_TIME_SINCE_PK", "DEPRECATED_HOUSEKEEPING_PRIORITY", "ABUSE_LOGGING_TIMESTAMP", "LAST_PORTAL_TELEPORT_TIMESTAMP", "USE_RADIUS", "HOME_RADIUS", "RELEASED_TIMESTAMP", "MIN_HOME_RADIUS", "FACING", "RESET_TIMESTAMP", "LOGOFF_TIMESTAMP", "ECON_RECOVERY_INTERVAL", "WEAPON_OFFENSE", "DAMAGE_MOD", "RESIST_SLASH", "RESIST_PIERCE", "RESIST_BLUDGEON", "RESIST_FIRE", "RESIST_COLD", "RESIST_ACID", "RESIST_ELECTRIC", "RESIST_HEALTH_BOOST", "RESIST_STAMINA_DRAIN", "RESIST_STAMINA_BOOST", "RESIST_MANA_DRAIN", "RESIST_MANA_BOOST", "TRANSLUCENCY", "PHYSICS_SCRIPT_INTENSITY", "FRICTION", "ELASTICITY", "AI_USE_MAGIC_DELAY", "ITEM_MIN_SPELLCRAFT_MOD", "ITEM_MAX_SPELLCRAFT_MOD", "ITEM_RANK_PROBABILITY", "SHADE2", "SHADE3", "SHADE4", "ITEM_EFFICIENCY", "ITEM_MANA_UPDATE_TIMESTAMP", "SPELL_GESTURE_SPEED_MOD", "SPELL_STANCE_SPEED_MOD", "ALLEGIANCE_APPRAISAL_TIMESTAMP", "POWER_LEVEL", "ACCURACY_LEVEL", "ATTACK_ANGLE", "ATTACK_TIMESTAMP", "CHECKPOINT_TIMESTAMP", "SOLD_TIMESTAMP", "USE_TIMESTAMP", "USE_LOCK_TIMESTAMP", "HEALKIT_MOD", "FROZEN_TIMESTAMP", "HEALTH_RATE_MOD", "ALLEGIANCE_SWEAR_TIMESTAMP", "OBVIOUS_RADAR_RANGE", "HOTSPOT_CYCLE_TIME", "HOTSPOT_CYCLE_TIME_VARIANCE", "SPAM_TIMESTAMP", "SPAM_RATE", "BOND_WIELDED_TREASURE", "BULK_MOD", "SIZE_MOD", "GAG_TIMESTAMP", "GENERATOR_UPDATE_TIMESTAMP", "DEATH_SPAM_TIMESTAMP", "DEATH_SPAM_RATE", "WILD_ATTACK_PROBABILITY", "FOCUSED_PROBABILITY", "CRASH_AND_TURN_PROBABILITY", "CRASH_AND_TURN_RADIUS", "CRASH_AND_TURN_BIAS", "GENERATOR_INITIAL_DELAY", "AI_ACQUIRE_HEALTH", "AI_ACQUIRE_STAMINA", "AI_ACQUIRE_MANA", "RESIST_HEALTH_DRAIN", "LIFESTONE_PROTECTION_TIMESTAMP", "AI_COUNTERACT_ENCHANTMENT", "AI_DISPEL_ENCHANTMENT", "TRADE_TIMESTAMP", "AI_TARGETED_DETECTION_RADIUS", "EMOTE_PRIORITY", "LAST_TELEPORT_START_TIMESTAMP", "EVENT_SPAM_TIMESTAMP", "EVENT_SPAM_RATE", "INVENTORY_OFFSET", "CRITICAL_MULTIPLIER", "MANA_STONE_DESTROY_CHANCE", "SLAYER_DAMAGE_BONUS", "ALLEGIANCE_INFO_SPAM_TIMESTAMP", "ALLEGIANCE_INFO_SPAM_RATE", "NEXT_SPELLCAST_TIMESTAMP", "APPRAISAL_REQUESTED_TIMESTAMP", "APPRAISAL_HEARTBEAT_DUE_TIMESTAMP", "MANA_CONVERSION_MOD", "LAST_PK_ATTACK_TIMESTAMP", "FELLOWSHIP_UPDATE_TIMESTAMP", "CRITICAL_FREQUENCY", "LIMBO_START_TIMESTAMP", "WEAPON_MISSILE_DEFENSE", "WEAPON_MAGIC_DEFENSE", "IGNORE_SHIELD", "ELEMENTAL_DAMAGE_MOD", "START_MISSILE_ATTACK_TIMESTAMP", "LAST_RARE_USED_TIMESTAMP", "IGNORE_ARMOR", "PROC_SPELL_RATE", "RESISTANCE_MODIFIER", "ALLEGIANCE_GAG_TIMESTAMP", "ABSORB_MAGIC_DAMAGE", "CACHED_MAX_ABSORB_MAGIC_DAMAGE", "GAG_DURATION", "ALLEGIANCE_GAG_DURATION", "GLOBAL_XP_MOD", "HEALING_MODIFIER", "ARMOR_MOD_VS_NETHER", "RESIST_NETHER", "COOLDOWN_DURATION", "WEAPON_AURA_OFFENSE", "WEAPON_AURA_DEFENSE", "WEAPON_AURA_ELEMENTAL", "WEAPON_AURA_MANA_CONV"}
var STypeInt64Enum []string = []string{"UNDEF", "TOTAL_EXPERIENCE", "AVAILABLE_EXPERIENCE", "AUGMENTATION_COST", "ITEM_TOTAL_XP", "ITEM_BASE_XP", "AVAILABLE_LUMINANCE", "MAXIMUM_LUMINANCE", "INTERACTION_REQS"}
var STypeStringEnum []string = []string{"UNDEF", "NAME", "TITLE", "SEX", "HERITAGE_GROUP", "TEMPLATE", "ATTACKERS_NAME", "INSCRIPTION", "SCRIBE_NAME", "VENDORS_NAME", "FELLOWSHIP", "MONARCHS_NAME", "LOCK_CODE", "KEY_CODE", "USE", "SHORT_DESC", "LONG_DESC", "ACTIVATION_TALK", "USE_MESSAGE", "ITEM_HERITAGE_GROUP_RESTRICTION", "PLURAL_NAME", "MONARCHS_TITLE", "ACTIVATION_FAILURE", "SCRIBE_ACCOUNT", "TOWN_NAME", "CRAFTSMAN_NAME", "USE_PK_SERVER_ERROR", "SCORE_CACHED_TEXT", "SCORE_DEFAULT_ENTRY_FORMAT", "SCORE_FIRST_ENTRY_FORMAT", "SCORE_LAST_ENTRY_FORMAT", "SCORE_ONLY_ENTRY_FORMAT", "SCORE_NO_ENTRY", "QUEST", "GENERATOR_EVENT", "PATRONS_TITLE", "HOUSE_OWNER_NAME", "QUEST_RESTRICTION", "APPRAISAL_PORTAL_DESTINATION", "TINKER_NAME", "IMBUER_NAME", "HOUSE_OWNER_ACCOUNT", "DISPLAY_NAME", "DATE_OF_BIRTH", "THIRD_PARTY_API", "KILL_QUEST", "AFK", "ALLEGIANCE_NAME", "AUGMENTATION_ADD_QUEST", "KILL_QUEST_2", "KILL_QUEST_3", "USE_SENDS_SIGNAL", "GEAR_PLATING_NAME"}
var STypePositionEnum []string = []string{"UNDEF", "LOCATION", "DESTINATION", "INSTANTIATION", "SANCTUARY", "HOME", "ACTIVATION_MOVE", "TARGET", "LINKED_PORTAL_ONE", "LAST_PORTAL", "PORTAL_STORM", "CRASH_AND_TURN", "PORTAL_SUMMON_LOC", "HOUSE_BOOT", "LAST_OUTSIDE_DEATH", "LINKED_LIFESTONE", "LINKED_PORTAL_TWO", "SAVE_1", "SAVE_2", "SAVE_3", "SAVE_4", "SAVE_5", "SAVE_6", "SAVE_7", "SAVE_8", "SAVE_9", "RELATIVE_DESTINATION", "TELEPORTED_CHARACTER"}
var VendorTypeEnum []string = []string{"UNDEF", "OpenVendor", "LeaveVendor", "Sell", "Buy", "Motion"}
var SoundTypeEnum []string = []string{"Invalid", "Speak1", "Random", "Attack1", "Attack2", "Attack3", "SpecialAttack1", "SpecialAttack2", "SpecialAttack3", "Damage1", "Damage2", "Damage3", "Wound1", "Wound2", "Wound3", "Death1", "Death2", "Death3", "Grunt1", "Grunt2", "Grunt3", "Oh1", "Oh2", "Oh3", "Heave1", "Heave2", "Heave3", "Knockdown1", "Knockdown2", "Knockdown3", "Swoosh1", "Swoosh2", "Swoosh3", "Thump1", "Smash1", "Scratch1", "Spear", "Sling", "Dagger", "ArrowWhiz1", "ArrowWhiz2", "CrossbowPull", "CrossbowRelease", "BowPull", "BowRelease", "ThrownWeaponRelease1", "ArrowLand", "Collision", "HitFlesh1", "HitLeather1", "HitChain1", "HitPlate1", "HitMissile1", "HitMissile2", "HitMissile3", "Footstep1", "Footstep2", "Walk1", "Dance1", "Dance2", "Dance3", "Hidden1", "Hidden2", "Hidden3", "Eat1", "Drink1", "Open", "Close", "OpenSlam", "CloseSlam", "Ambient1", "Ambient2", "Ambient3", "Ambient4", "Ambient5", "Ambient6", "Ambient7", "Ambient8", "Waterfall", "LogOut", "LogIn", "LifestoneOn", "AttribUp", "AttribDown", "SkillUp", "SkillDown", "HealthUp", "HealthDown", "ShieldUp", "ShieldDown", "EnchantUp", "EnchantDown", "VisionUp", "VisionDown", "Fizzle", "Launch", "Explode", "TransUp", "TransDown", "BreatheFlaem", "BreatheAcid", "BreatheFrost", "BreatheLightning", "Create", "Destroy", "Lockpicking", "UI_EnterPortal", "UI_ExitPortal", "UI_GeneralQuery", "UI_GeneralError", "UI_TransientMessage", "UI_IconPickUp", "UI_IconSuccessfulDrop", "UI_IconInvalid_Drop", "UI_ButtonPress", "UI_GrabSlider", "UI_ReleaseSlider", "UI_NewTargetSelected", "UI_Roar", "UI_Bell", "UI_Chant1", "UI_Chant2", "UI_DarkWhispers1", "UI_DarkWhispers2", "UI_DarkLaugh", "UI_DarkWind", "UI_DarkSpeech", "UI_Drums", "UI_GhostSpeak", "UI_Breathing", "UI_Howl", "UI_LostSouls", "UI_Squeal", "UI_Thunder1", "UI_Thunder2", "UI_Thunder3", "UI_Thunder4", "UI_Thunder5", "UI_Thunder6", "RaiseTrait", "WieldObject", "UnwieldObject", "ReceiveItem", "PickUpItem", "DropItem", "ResistSpell", "PicklockFail", "LockSuccess", "OpenFailDueToLock", "TriggerActivated", "SpellExpire", "ItemManaDepleted", "TriggerActivated1", "TriggerActivated2", "TriggerActivated3", "TriggerActivated4", "TriggerActivated5", "TriggerActivated6", "TriggerActivated7", "TriggerActivated8", "TriggerActivated9", "TriggerActivated10", "TriggerActivated11", "TriggerActivated12", "TriggerActivated13", "TriggerActivated14", "TriggerActivated15", "TriggerActivated16", "TriggerActivated17", "TriggerActivated18", "TriggerActivated19", "TriggerActivated20", "TriggerActivated21", "TriggerActivated22", "TriggerActivated23", "TriggerActivated24", "TriggerActivated25", "TriggerActivated26", "TriggerActivated27", "TriggerActivated28", "TriggerActivated29", "TriggerActivated30", "TriggerActivated31", "TriggerActivated32", "TriggerActivated33", "TriggerActivated34", "TriggerActivated35", "TriggerActivated36", "TriggerActivated37", "TriggerActivated38", "TriggerActivated39", "TriggerActivated40", "TriggerActivated41", "TriggerActivated42", "TriggerActivated43", "TriggerActivated44", "TriggerActivated45", "TriggerActivated46", "TriggerActivated47", "TriggerActivated48", "TriggerActivated49", "TriggerActivated50", "HealthDownVoid", "RegenDownVoid", "SkillDownVoid"}
var PScriptTypeEnum []string = []string{"Invalid", "Test1", "Test2", "Test3", "Launch", "Explode", "AttribUpRed", "AttribDownRed", "AttribUpOrange", "AttribDownOrange", "AttribUpYellow", "AttribDownYellow", "AttribUpGreen", "AttribDownGreen", "AttribUpBlue", "AttribDownBlue", "AttribUpPurple", "AttribDownPurple", "SkillUpRed", "SkillDownRed", "SkillUpOrange", "SkillDownOrange", "SkillUpYellow", "SkillDownYellow", "SkillUpGreen", "SkillDownGreen", "SkillUpBlue", "SkillDownBlue", "SkillUpPurple", "SkillDownPurple", "SkillDownBlack", "HealthUpRed", "HealthDownRed", "HealthUpBlue", "HealthDownBlue", "HealthUpYellow", "HealthDownYellow", "RegenUpRed", "RegenDownREd", "RegenUpBlue", "RegenDownBlue", "RegenUpYellow", "RegenDownYellow", "ShieldUpRed", "ShieldDownRed", "ShieldUpOrange", "ShieldDownOrange", "ShieldUpYellow", "ShieldDownYellow", "ShieldUpGreen", "ShieldDownGreen", "ShieldUpBlue", "ShieldDownBlue", "ShieldUpPurple", "ShieldDownPurple", "ShieldUpGrey", "ShieldDownGrey", "EnchantUpRed", "EnchantDownRed", "EnchantUpOrange", "EnchantDownOrange", "EnchantUpYellow", "EnchantDownYellow", "EnchantUpGreen", "EnchantDownGreen", "EnchantUpBlue", "EnchantDownBlue", "EnchantUpPurple", "EnchantDownPurple", "VitaeUpWhite", "VitaeDownBlack", "VisionUpWhite", "VisionDownBlack", "SwapHealth_Red_To_Yellow", "SwapHealth_Red_To_Blue", "SwapHealth_Yellow_To_Red", "SwapHealth_Yellow_To_Blue", "SwapHealth_Blue_To_Red", "SwapHealth_Blue_To_Yellow", "TransUpWhite", "TransDownBlack", "Fizzle", "PortalEntry", "PortalExit", "BreatheFlame", "BreatheFrost", "BreatheAcid", "BreatheLightning", "Create", "Destroy", "ProjectileCollision", "SplatterLowLeftBack", "SplatterLowLeftFront", "SplatterLowRightBack", "SplatterLowRightFront", "SplatterMidLeftBack", "SplatterMidLeftFront", "SplatterMidRightBack", "SplatterMidRightFront", "SplatterUpLeftBack", "SplatterUpLeftFront", "SplatterUpRightBack", "SplatterUpRightFront", "SparkLowLeftBack", "SparkLowLeftFront", "SparkLowRightBack", "SparkLowRightFront", "SparkMidLeftBack", "SparkMidLeftFront", "SparkMidRightBack", "SparkMidRightFront", "SparkUpLeftBack", "SparkUpLeftFront", "SparkUpRightBack", "SparkUpRightFront", "PortalStorm", "Hide", "UnHide", "Hidden", "DisappearDestroy", "cialState1", "cialState2", "cialState3", "cialState4", "cialState5", "cialState6", "cialState7", "cialState8", "cialState9", "cialState0", "cialStateRed", "cialStateOrange", "cialStateYellow", "cialStateGreen", "cialStateBlue", "cialStatePurple", "cialStateWhite", "cialStateBlack", "LevelUp", "EnchantUpGrey", "EnchantDownGrey", "WeddingBliss", "EnchantUpWhite", "EnchantDownWhite", "CampingMastery", "CampingIneptitude", "DispelLife", "DispelCreature", "DispelAll", "BunnySmite", "BaelZharonSmite", "WeddingSteele", "RestrictionEffectBlue", "RestrictionEffectGreen", "RestrictionEffectGold", "LayingofHands", "AugmentationUseAttribute", "AugmentationUseSkill", "AugmentationUseResistances", "AugmentationUseOther", "BlackMadness", "AetheriaLevelUp", "AetheriaSurgeDestruction", "AetheriaSurgeProtection", "AetheriaSurgeRegeneration", "AetheriaSurgeAffliction", "AetheriaSurgeFestering", "HealthDownVoid", "RegenDownVoid", "SkillDownVoid", "DirtyFightingHealDebuff", "DirtyFightingAttackDebuff", "DirtyFightingDefenseDebuff", "DirtyFightingDamageOverTime"}
var EmoteCategoryEnum []string = []string{"Invalid", "Refuse", "Vendor", "Death", "Portal", "HeartBeat", "Give", "Use", "Activation", "Generation", "PickUp", "Drop", "QuestSuccess", "QuestFailure", "Taunt", "WoundedTaunt", "KillTaunt", "NewEnemy", "Scream", "Homesick", "ReceiveCritical", "ResistSpell", "TestSuccess", "TestFailure", "HearChat", "Wield", "UnWield", "EventSuccess", "EventFailure", "TestNoQuality", "QuestNoFellow", "TestNoFellow", "GotoSet", "NumFellowsSuccess", "NumFellowsFailure", "NumCharacterTitlesSuccess", "NumCharacterTitlesFailure", "ReceiveLocalSignal", "ReceiveTalkDirect"}
var EmoteTypeEnum []string = []string{"Invalid", "Act", "AwardXP", "Give", "MoveHome", "Motion", "Move", "PhysScript", "Say", "Sound", "Tell", "Turn", "TurnToTarget", "TextDirect", "CastSpell", "Activate", "WorldBroadcast", "LocalBroadcast", "DirectBroadcast", "CastSpellInstant", "UpdateQuest", "InqQuest", "StampQuest", "StartEvent", "StopEvent", "BLog", "AdminSpam", "TeachSpell", "AwardSkillXP", "AwardSkillPoints", "InqQuestSolves", "EraseQuest", "DecrementQuest", "IncrementQuest", "AddCharacterTitle", "InqBoolStat", "InqIntStat", "InqFloatStat", "InqStringStat", "InqAttributeStat", "InqRawAttributeStat", "InqSecondaryAttributeStat", "InqRawSecondaryAttributeStat", "InqSkillStat", "InqRawSkillStat", "InqSkillTrained", "InqSkillSpecialized", "AwardTrainingCredits", "InflictVitaePenalty", "AwardLevelProportionalXP", "AwardLevelProportionalSkillXP", "InqEvent", "ForceMotion", "SetIntStat", "IncrementIntStat", "DecrementIntStat", "CreateTreasure", "ResetHomePosition", "InqFellowQuest", "InqFellowNum", "UpdateFellowQuest", "StampFellowQuest", "AwardNoShareXP", "SetSanctuaryPosition", "TellFellow", "FellowBroadcast", "LockFellow", "Goto", "PopUp", "SetBoolStat", "SetQuestCompletions", "InqNumCharacterTitles", "Generate", "PetCastSpellOnOwner", "TakeItems", "InqYesNo", "InqOwnsItems", "DeleteSelf", "KillSelf", "UpdateMyQuest", "InqMyQuest", "StampMyQuest", "InqMyQuestSolves", "EraseMyQuest", "DecrementMyQuest", "IncrementMyQuest", "SetMyQuestCompletions", "MoveToPos", "LocalSignal", "InqPackSpace", "RemoveVitaePenalty", "SetEyeTexture", "SetEyePalette", "SetNoseTexture", "SetNosePalette", "SetMouthTexture", "SetMouthPalette", "SetHeadObject", "SetHeadPalette", "TeleportTarget", "TeleportSelf", "StartBarber", "InqQuestBitsOn", "InqQuestBitsOff", "InqMyQuestBitsOn", "InqMyQuestBitsOff", "SetQuestBitsOn", "SetQuestBitsOff", "SetMyQuestBitsOn", "SetMyQuestBitsOff", "UntrainSkill", "SetAltRacialSkills", "SpendLuminance", "AwardLuminance", "InqInt64Stat", "SetInt64Stat", "OpenMe", "CloseMe", "SetFloatStat", "AddContract", "RemoveContract", "InqContractsFull"}
var STypeSkillEnum []string = []string{"UNDEF", "AXE", "BOW", "CROSSBOW", "DAGGER", "MACE", "MELEE_DEFENSE", "MISSILE_DEFENSE", "SLING", "SPEAR", "STAFF", "SWORD", "THROWN_WEAPON", "UNARMED_COMBAT", "ARCANE_LORE", "MAGIC_DEFENSE", "MANA_CONVERSION", "SPELLCRAFT", "ITEM_APPRAISAL", "PERSONAL_APPRAISAL", "DECEPTION", "HEALING", "JUMP", "LOCKPICK", "RUN", "AWARENESS", "ARMS_AND_ARMOR_REPAIR", "CREATURE_APPRAISAL", "WEAPON_APPRAISAL", "ARMOR_APPRAISAL", "MAGIC_ITEM_APPRAISAL", "CREATURE_ENCHANTMENT", "ITEM_ENCHANTMENT", "LIFE_MAGIC", "WAR_MAGIC", "LEADERSHIP", "LOYALTY", "FLETCHING", "ALCHEMY", "COOKING", "SALVAGING", "TWO_HANDED_COMBAT", "GEARCRAFT", "VOID_MAGIC", "HEAVY_WEAPONS", "LIGHT_WEAPONS", "FINESSE_WEAPONS", "MISSILE_WEAPONS", "SHIELD", "DUAL_WIELD", "RECKLESSNESS", "SNEAK_ATTACK", "DIRTY_FIGHTING", "CHALLENGE", "SUMMONING"}
var WeenieTypeEnum []string = []string{"Undef", "Generic", "Clothing", "MissileLauncher", "Missile", "Ammunition", "MeleeWeapon", "Portal", "Book", "Coin", "Creature", "Admin", "Vendor", "HotSpot", "Corpse", "Cow", "AI", "Machine", "Food", "Door", "Chest", "Container", "Key", "Lockpick", "PressurePlate", "LifeStone", "Switch", "PKModifier", "Healer", "LightSource", "Allegiance", "Type32", "SpellComponent", "ProjectileSpell", "Scroll", "Caster", "Channel", "ManaStone", "Gem", "AdvocateFane", "AdvocateItem", "Sentinel", "GSpellEconomy", "LSpellEconomy", "CraftTool", "LScoreKeeper", "GScoreKeeper", "GScoreGatherer", "ScoreBook", "EventCoordinator", "Entity", "Stackable", "HUD", "House", "Deed", "SlumLord", "Hook", "Storage", "BootSpot", "HousePortal", "Game", "GamePiece", "SkillAlterationDevice", "AttributeTransferDevice", "Hooker", "AllegianceBindstone", "InGameStatKeeper", "AugmentationDevice", "SocialManager", "Pet", "PetDevice", "CombatPet"}
var BodyHeightEnum []string = []string{"UNDEF", "HIGH", "MEDIUM", "LOW"}
var RegenerationTypeEnum []string = []string{"Undef", "Destruction", "PickUp", "Death"}

func GetEnum(id uint32, enum *[]string) string {
	if int(id) < len(*enum) {
		return (*enum)[id]
	}
	return fmt.Sprintf("Invalid_%d", id)
}
func GetFlags(flags uint32, enum *[]string) []string {
	var reta []string = make([]string, 0)
	for it := uint32(0); it < 32; it++ {
		if flags&(1<<it) != 0 {
			if enum != nil && it < uint32(len(*enum)) {
				reta = append(reta, (*enum)[it])
			} else {
				reta = append(reta, fmt.Sprintf("0x%08X", 1<<it))
			}
		}
	}
	if len(reta) == 0 {
		return []string{"UNDEF"}
	}
	return reta
}

type DType int32
type RegenLocationType int32
type DestinationType int32
type ChannelIDType int32
type RadarBlipType int32

var DTypeFlags []string = []string{"SLASH", "PIERCE", "BLUDGEON", "COLD", "FIRE", "ACID", "ELECTRIC", "HEALTH", "STAMINA", "MANA", "NETHER", "0x800", "0x1000", "0x2000", "0x4000", "0x8000", "0x10000", "0x20000", "0x40000", "0x80000", "0x100000", "0x200000", "0x400000", "0x800000", "0x1000000", "0x2000000", "0x4000000", "0x8000000", "BASE", "0x20000000", "0x40000000", "0x80000000"}
var RegenLocationTypeFlags []string = []string{"OnTop", "Scatter", "Specific", "Contain", "Wield", "Shop", "Treasure"}
var DestinationTypeFlags []string = []string{"Contain", "Wield", "Shop", "Treasure", "HouseBuy", "HouseRent"}
var ChannelIDFlags []string = []string{"Abuse", "Admin", "Audit", "Advocate1", "Advocate2", "Advocate3", "QA1", "QA2", "Debug", "Sentinel", "Help", "Fellow", "Vassals", "Patron", "Monarch", "AlArqas", "Holtburg", "Lytelthorpe", "Nanto", "Rithwic", "Samsur", "Shoushi", "Yanshi", "Yaraq", "Covassals", "AllegianceBroadcast", "FellowBroadcast"}
var RadarBlipFlags []string = []string{"Blue(LifeStone)", "Gold(Creature)", "White(Default)", "Purple(Portal)", "Red(PlayerKiller)", "Pink(PKLite|Advocate)", "Green(Allgeiance)", "Yellow(Vendor|NPC)", "Cyan(Sentinel|Admin)", "BrightGreen(Fellowship|FellowshipLeader)"}

type StatMod struct {
	Key  uint32  `json:"key"`
	Type uint32  `json:"type"`
	Val  float32 `json:"val"`
}

func (t *StatMod) UnPack(s *SBuffer) any {
	t = new(StatMod)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type EmoteTable struct {
	Key   EmoteCategoryKey      `json:"key"`
	Value *List[*EmoteCategory] `json:"value"`
}

func (t *EmoteTable) UnPack(s *SBuffer) any {
	t = new(EmoteTable)
	t.Key = ReadT[EmoteCategoryKey](s)
	t.Value = new(List[*EmoteCategory]).UnPack(s).(*List[*EmoteCategory])
	return t
}

type EmoteCategory struct {
	Category    EmoteCategoryKey `json:"category"`
	Probability float32          `json:"probability"`
	ClassID     *uint32          `json:"classID,omitempty"`
	Style       *uint32          `json:"style,omitempty"`
	SubStyle    *uint32          `json:"substyle,omitempty"`
	VendorType  *VendorType      `json:"vendorType,omitempty"`
	Quest       *PString         `json:"quest,omitempty"`
	MinHealth   *float32         `json:"minhealth,omitempty"`
	MaxHealth   *float32         `json:"maxhealth,omitempty"`
	Emotes      *List[*Emote]    `json:"emotes"`
}

func (t *EmoteCategory) UnPack(s *SBuffer) any {
	t = new(EmoteCategory)
	t.Category = ReadT[EmoteCategoryKey](s)
	t.Probability = ReadT[float32](s)
	switch t.Category {

	case 1: // Refuse_EmoteCategory
		fallthrough
	case 6: // Give_EmoteCategory
		t.ClassID = ReadTP[uint32](s)

	case 5: // HeartBeat_EmoteCategory
		t.Style = ReadTP[uint32](s)
		t.SubStyle = ReadTP[uint32](s)

	case 12: // QuestSuccess_EmoteCategory
		fallthrough
	case 13: // QuestFailure_EmoteCategory
		fallthrough
	case 14: // TestSuccess_EmoteCategory
		fallthrough
	case 23: // TestFailure_EmoteCategory
		fallthrough
	case 27: // EventSuccess_EmoteCategory
		fallthrough
	case 28: // EventFailure_EmoteCategory
		fallthrough
	case 29: // TestNoQuality_EmoteCategory
		fallthrough
	case 30: // QuestNoFellow_EmoteCategory
		fallthrough
	case 31: // TestNoFellow_EmoteCategory
		fallthrough
	case 32: // GotoSet_EmoteCategory
		fallthrough
	case 33: // NumFellowsSuccess_EmoteCategory
		fallthrough
	case 34: // NumFellowsFailure_EmoteCategory
		fallthrough
	case 35: // NumCharacterTitlesSuccess_EmoteCategory
		fallthrough
	case 36: // NumCharacterTitlesFailure_EmoteCategory
		fallthrough
	case 37: // ReceiveLocalSignal_EmoteCategory
		fallthrough
	case 38: // ReceiveTalkDirect_EmoteCategory
		t.Quest.UnPack(s)

	case 2: // Vendor_EmoteCategory
		t.VendorType = (*VendorType)(ReadTP[uint32](s))

	case 15: // WoundedTaunt_EmoteCategory
		t.MinHealth = ReadTP[float32](s)
		t.MaxHealth = ReadTP[float32](s)
	}

	t.Emotes = new(List[*Emote]).UnPack(s).(*List[*Emote])
	return t
}

type Emote struct {
	Type   EmoteType `json:"type"`
	Delay  float32   `json:"delay"`
	Extent float32   `json:"extent"`

	Msg           *PString         `json:"msg,omitempty"`
	Stat          *uint32          `json:"stat,omitempty"`
	Amount        *uint32          `json:"amount,omitempty"`
	Amount64      *int64           `json:"amount64,omitempty"`
	Heroxp64      *int64           `json:"heroxp64,omitempty"`
	Percent       *float64         `json:"percent,omitempty"`
	Min           *uint32          `json:"min,omitempty"`
	Max           *uint32          `json:"max,omitempty"`
	Motion        *uint32          `json:"motion,omitempty"`
	Sound         *SoundType       `json:"sound,omitempty"`
	SpellID       *uint32          `json:"spellid,omitempty"`
	CProf         *CreationProfile `json:"cprof,omitempty"`
	WealthRating  *uint32          `json:"wealth_rating,omitempty"`
	TreasureClass *uint32          `json:"treasure_class,omitempty"`
	TreasureType  *uint32          `json:"treasure_type,omitempty"`
	Frame         *Frame           `json:"frame,omitempty"`
	PScript       *PScriptType     `json:"pscript,omitempty"`
	Teststring    *PString         `json:"teststring,omitempty"`
	Min64         *uint64          `json:"min64,omitempty"`
	Max64         *uint64          `json:"max64,omitempty"`
	FMin          *float64         `json:"fmin,omitempty"`
	FMax          *float64         `json:"fmax,omitempty"`
	Display       *int32           `json:"display,omitempty"`
	MPosition     *Position        `json:"mPosition,omitempty"`
}

func (t *Emote) UnPack(s *SBuffer) any {
	t = new(Emote)
	t.Type = ReadT[EmoteType](s)
	t.Delay = ReadT[float32](s)
	t.Extent = ReadT[float32](s)
	switch t.Type {
	case 0x01:
		fallthrough
	case 0x08:
		fallthrough
	case 0x0A:
		fallthrough
	case 0x0D:
		fallthrough
	case 0x10:
		fallthrough
	case 0x11:
		fallthrough
	case 0x12:
		fallthrough
	case 0x14:
		fallthrough
	case 0x15:
		fallthrough
	case 0x16:
		fallthrough
	case 0x17:
		fallthrough
	case 0x18:
		fallthrough
	case 0x19:
		fallthrough
	case 0x1A:
		fallthrough
	case 0x1F:
		fallthrough
	case 0x33:
		fallthrough
	case 0x3A:
		fallthrough
	case 0x3C:
		fallthrough
	case 0x3D:
		fallthrough
	case 0x40:
		fallthrough
	case 0x41:
		fallthrough
	case 0x43:
		fallthrough
	case 0x44:
		fallthrough
	case 0x4F:
		fallthrough
	case 0x50:
		fallthrough
	case 0x51:
		fallthrough
	case 0x53:
		fallthrough
	case 0x58:
		fallthrough
	case 0x79:
		t.Msg.UnPack(s)
	case 0x20:
		fallthrough
	case 0x21:
		fallthrough
	case 0x46:
		fallthrough
	case 0x54:
		fallthrough
	case 0x55:
		fallthrough
	case 0x56:
		fallthrough
	case 0x59:
		fallthrough
	case 0x66:
		fallthrough
	case 0x67:
		fallthrough
	case 0x68:
		fallthrough
	case 0x69:
		fallthrough
	case 0x6A:
		fallthrough
	case 0x6B:
		fallthrough
	case 0x6C:
		fallthrough
	case 0x6D:
		t.Msg.UnPack(s)
		t.Amount = ReadTP[uint32](s)
	case 0x35:
		fallthrough
	case 0x36:
		fallthrough
	case 0x37:
		fallthrough
	case 0x45:
		t.Stat = ReadTP[uint32](s)
		t.Amount = ReadTP[uint32](s)
	case 0x73:
		t.Stat = ReadTP[uint32](s)
		t.Amount64 = ReadTP[int64](s)
	case 0x76:
		t.Stat = ReadTP[uint32](s)
		t.Percent = ReadTP[float64](s)
	case 0x1E:
		fallthrough
	case 0x3B:
		fallthrough
	case 0x47:
		fallthrough
	case 0x52:
		t.Msg.UnPack(s)
		t.Min = ReadTP[uint32](s)
		t.Max = ReadTP[uint32](s)
	case 0x02: // AwardXP_EmoteType
		fallthrough
	case 0x3E: // AwardNoShareXP_EmoteType
		t.Amount64 = ReadTP[int64](s)
		t.Heroxp64 = ReadTP[int64](s)
	case 0x70:
		fallthrough
	case 0x71:
		t.Amount64 = ReadTP[int64](s)
		t.Heroxp64 = ReadTP[int64](s)
	case 0x22:
		fallthrough
	case 0x2F:
		fallthrough
	case 0x30:
		fallthrough
	case 0x5A:
		fallthrough
	case 0x77:
		fallthrough
	case 0x78:
		t.Amount = ReadTP[uint32](s)
	case 0x0E:
		fallthrough
	case 0x13:
		fallthrough
	case 0x1B:
		fallthrough
	case 0x49:
		t.SpellID = ReadTP[uint32](s)
	case 0x03: // Give_EmoteType
		fallthrough
	case 0x4A: // TakeItems_EmoteType
		t.CProf.UnPack(s)
	case 0x4C: // InqOwnsItems_EmoteType
		t.Msg.UnPack(s)
		t.CProf.UnPack(s)
	case 0x38: // CreateTreasure_EmoteType
		t.WealthRating = ReadTP[uint32](s)
		t.TreasureClass = ReadTP[uint32](s)
		t.TreasureType = ReadTP[uint32](s)
	case 0x05: // Motion_EmoteType
		fallthrough
	case 0x34: // ForceMotion_EmoteType
		t.Motion = ReadTP[uint32](s)
	case 0x04:
		fallthrough
	case 0x06:
		fallthrough
	case 0x0B:
		fallthrough
	case 0x57:
		t.Frame.UnPack(s)
	case 0x07:
		t.PScript = (*PScriptType)(ReadTP[uint32](s))
	case 0x09:
		t.Sound = (*SoundType)(ReadTP[uint32](s))
	case 0x1C:
		fallthrough
	case 0x1D:
		t.Amount = ReadTP[uint32](s)
		t.Stat = ReadTP[uint32](s)
	case 0x6E:
		t.Stat = ReadTP[uint32](s)
	case 0x6F:
		t.Amount = ReadTP[uint32](s)
	case 0x23:
		fallthrough
	case 0x2D:
		fallthrough
	case 0x2E:
		t.Msg.UnPack(s)
		t.Stat = ReadTP[uint32](s)
	case 0x26:
		fallthrough
	case 0x4B:
		t.Msg.UnPack(s)
		t.Teststring.UnPack(s)
		t.Stat = ReadTP[uint32](s)
	case 0x24:
		fallthrough
	case 0x27:
		fallthrough
	case 0x28:
		fallthrough
	case 0x29:
		fallthrough
	case 0x2A:
		fallthrough
	case 0x2B:
		fallthrough
	case 0x2C:
		t.Msg.UnPack(s)
		t.Min = ReadTP[uint32](s)
		t.Max = ReadTP[uint32](s)
		t.Stat = ReadTP[uint32](s)
	case 0x72:
		t.Msg.UnPack(s)
		t.Min64 = ReadTP[uint64](s)
		t.Max64 = ReadTP[uint64](s)
		t.Stat = ReadTP[uint32](s)
	case 0x25:
		t.Msg.UnPack(s)
		t.FMin = ReadTP[float64](s)
		t.FMax = ReadTP[float64](s)
		t.Stat = ReadTP[uint32](s)
	case 0x31:
		t.Percent = ReadTP[float64](s)
		t.Min64 = ReadTP[uint64](s)
		t.Max64 = ReadTP[uint64](s)
		t.Display = ReadTP[int32](s)
	case 0x32:
		t.Stat = ReadTP[uint32](s)
		t.Percent = ReadTP[float64](s)
		t.Min = ReadTP[uint32](s)
		t.Max = ReadTP[uint32](s)
		t.Display = ReadTP[int32](s)
	case 0x3F:
		t.MPosition.UnPack(s)
	case 0x63:
		fallthrough
	case 0x64:
		t.MPosition.UnPack(s)
	case 0x48: // Generate_EmoteType:
		t.Amount = ReadTP[uint32](s)
	}
	return t
}

type CreationProfile struct {
	WCID        uint32          `json:"wcid"`
	Palette     uint32          `json:"palette"`
	Shade       float32         `json:"shade"`
	Destination DestinationType `json:"destination"`
	StackSize   int32           `json:"stack_size"`
	TryToBond   int32           `json:"try_to_bond"`
}

func (t *CreationProfile) UnPack(s *SBuffer) any {
	t = new(CreationProfile)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Generator struct {
	Probability float32           `json:"probability"`
	Type        uint32            `json:"type"`
	Delay       float64           `json:"delay"`
	InitCreate  int32             `json:"initCreate"`
	MaxNum      int32             `json:"maxNum"`
	WhenCreate  RegenerationType  `json:"whenCreate"`
	WhereCreate RegenLocationType `json:"whereCreate"`
	StackSize   int32             `json:"stackSize"`
	PTID        uint32            `json:"ptid"`
	Shade       float32           `json:"shade"`
	Position
	Slot uint32 `json:"slot"`
}

func (t *Generator) UnPack(s *SBuffer) any {
	t = new(Generator)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_LightInfo struct {
	Key                 uint32  `json:"key"`
	ViewerSpaceLocation Frame   `json:"viewer_space_location"`
	Color               uint32  `json:"color"`
	Intensity           float32 `json:"intensity"`
	Falloff             float32 `json:"falloff"`
	ConeAngle           float32 `json:"cone_angle"`
}

func (t *Dat_LightInfo) UnPack(s *SBuffer) any {
	t = new(Dat_LightInfo)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_CylSphere struct {
	Origin Vector3 `json:"origin"`
	Radius float32 `json:"radius"`
	Height float32 `json:"height"`
}

func (t *Dat_CylSphere) UnPack(s *SBuffer) any {
	t = new(Dat_CylSphere)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_Sphere struct {
	Origin Vector3 `json:"origin"`
	Radius float32 `json:"radius"`
}

func (t *Dat_Sphere) UnPack(s *SBuffer) any {
	t = new(Dat_Sphere)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_Stab struct {
	Id    uint32 `json:"id"`
	Frame Frame  `json:"frame"`
}

func (t *Dat_Stab) UnPack(s *SBuffer) any {
	t = new(Dat_Stab)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_CellPortal struct {
	Flags         uint16 `json:"flags"`
	PolygonId     uint16 `json:"polygon_id"`
	OtherCellId   uint16 `json:"other_cell_id"`
	OtherPortalId uint16 `json:"other_portal_id"`
}

func (t *Dat_CellPortal) UnPack(s *SBuffer) any {
	t = new(Dat_CellPortal)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_CBldPortal struct {
	Flags         uint16   `json:"flags"`
	OtherCellId   uint16   `json:"other_cell_id"`
	OtherPortalId uint16   `json:"other_portal_id"`
	NumStabs      uint16   `json:"num_stabs"`
	StabList      []uint16 `json:"stab_list"`
}

func (t *Dat_CBldPortal) UnPack(s *SBuffer) any {
	t = new(Dat_CBldPortal)
	t.Flags = ReadT[uint16](s)
	t.OtherCellId = ReadT[uint16](s)
	t.OtherPortalId = ReadT[uint16](s)
	t.NumStabs = ReadT[uint16](s)
	t.StabList = ReadTs[uint16](s, int(t.NumStabs))
	if t.NumStabs&1 == 1 { // very janky alignment.
		ReadTP[uint16](s)
	}
	return t
}

// ACE calls this CMostlyConsecutiveIntSet
type Dat_Iteration struct {
	I      []int32 `json:"i"`
	Sorted bool    `json:"sorted"`
}

func (t *Dat_Iteration) UnPack(s *SBuffer) any {
	t = new(Dat_Iteration)
	t.I = ReadTs[int32](s, 2)
	t.Sorted = ReadT[byte](s) == 1
	return t
}

type Dat_BuildInfo struct {
	ModelId   uint32                 `json:"model_id"`
	Frame     Frame                  `json:"frame"`
	NumLeaves uint32                 `json:"num_leaves"`
	Portals   *List[*Dat_CBldPortal] `json:"portals"`
}

func (t *Dat_BuildInfo) UnPack(s *SBuffer) any {
	t = new(Dat_BuildInfo)
	t.ModelId = ReadT[uint32](s)
	t.Frame.UnPack(s)
	t.NumLeaves = ReadT[uint32](s)
	t.Portals = new(List[*Dat_CBldPortal]).UnPack(s).(*List[*Dat_CBldPortal])
	return t
}

type Dat_PlacementType struct {
	Id             uint32                     `json:"id"`
	AnimationFrame *List[*Dat_AnimationFrame] `json:"animation_frame"`
}

func (t *Dat_PlacementType) UnPack(s *SBuffer, num_parts int) {
	t = new(Dat_PlacementType)
	t.Id = ReadT[uint32](s)
	t.AnimationFrame = new(List[*Dat_AnimationFrame]).UnPackFixed(s, num_parts).(*List[*Dat_AnimationFrame])
}

type Dat_LocationType struct {
	Id     uint32 `json:"id"`
	PartId uint32 `json:"part_id"`
	Frame  Frame  `json:"frame"`
}

func (t *Dat_LocationType) UnPack(s *SBuffer) any {
	t = new(Dat_LocationType)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_AttackCone struct {
	PartIndex uint32  `json:"hook_type"`
	LeftX     float32 `json:"left_x"`
	LeftY     float32 `json:"left_y"`
	RightX    float32 `json:"right_x"`
	RightY    float32 `json:"right_y"`
	Radius    float32 `json:"radius"`
	Height    float32 `json:"height"`
}

func (t *Dat_AttackCone) UnPack(s *SBuffer) any {
	t = new(Dat_AttackCone)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_Animation struct {
	Id         uint32                     `json:"id"`
	Flags      uint32                     `json:"flags"`
	NumParts   uint32                     `json:"num_parts"`
	NumFrames  uint32                     `json:"num_frames"`
	PosFrames  *List[*Frame]              `json:"pos_frames"`
	PartFrames *List[*Dat_AnimationFrame] `json:"part_frames"`
}

func (t *Dat_Animation) UnPack(s *SBuffer) any {
	t = new(Dat_Animation)
	t.Id = ReadT[uint32](s)
	t.Flags = ReadT[uint32](s)
	t.NumParts = ReadT[uint32](s)
	t.NumFrames = ReadT[uint32](s)
	if (t.Flags & 1) == 1 {
		t.PosFrames = new(List[*Frame]).UnPackFixed(s, int(t.NumFrames)).(*List[*Frame])

	}
	// feels like this should use NumFrames also?
	t.PartFrames = new(List[*Dat_AnimationFrame]).UnPackFixed(s, int(t.NumFrames)).(*List[*Dat_AnimationFrame])
	return t
}

type Dat_AnimationFrame struct {
	Frames *List[*Frame]             `json:"frames"`
	Hooks  *List[*Dat_AnimationHook] `json:"hooks"`
}

func (t *Dat_AnimationFrame) UnPack(s *SBuffer) any {
	t = new(Dat_AnimationFrame)
	t.Frames = new(List[*Frame]).UnPack(s).(*List[*Frame])
	t.Hooks = new(List[*Dat_AnimationHook]).UnPack(s).(*List[*Dat_AnimationHook])
	return t
}

type Dat_AnimationHook struct {
	HookType      uint32          `json:"HookType"`
	Direction     int32           `json:"Direction"`
	Id            *uint32         `json:"Id,omitempty"`
	SoundType     *uint32         `json:"SoundType,omitempty"`
	AttackCone    *Dat_AttackCone `json:"AttackCone,omitempty"`
	APChange      *AnimPartChange `json:"APChange,omitempty"`
	Ethereal      *int32          `json:"Ethereal,omitempty"`
	Part          *uint32         `json:"Part,omitempty"`
	Start         *float32        `json:"Start,omitempty"`
	End           *float32        `json:"End,omitempty"`
	Time          *float32        `json:"Time,omitempty"`
	EmitterId     *uint32         `json:"EmitterId,omitempty"`
	NoDraw        *uint32         `json:"NoDraw,omitempty"`
	PartIndex     *uint32         `json:"PartIndex,omitempty"`
	PES           *uint32         `json:"PES,omitempty"`
	Pause         *float32        `json:"Pause,omitempty"`
	SoundID       *uint32         `json:"SoundID,omitempty"`
	Priority      *float32        `json:"Priority,omitempty"`
	Probability   *float32        `json:"Probability,omitempty"`
	Volume        *float32        `json:"Volume,omitempty"`
	Axis          *Vector3        `json:"Axis,omitempty"`
	USpeed        *float32        `json:"USpeed,omitempty"`
	VSpeed        *float32        `json:"VSpeed,omitempty"`
	LightsOn      *int32          `json:"LightsOn,omitempty"`
	EmitterInfoId *uint32         `json:"EmitterInfoId,omitempty"`
	Offset        *Frame          `json:"Offset,omitempty"`
}

func (t *Dat_AnimationHook) UnPack(s *SBuffer) any {
	t = new(Dat_AnimationHook)
	t.HookType = ReadT[uint32](s)
	t.Direction = ReadT[int32](s)
	switch t.HookType {
	case 1: // Sound
		t.Id = ReadTP[uint32](s)
	case 2: // SoundTable
		t.SoundType = ReadTP[uint32](s)
	case 3: // Attack
		t.AttackCone = new(Dat_AttackCone).UnPack(s).(*Dat_AttackCone)
	case 4: // AnimationDone:
	// NO BODY
	case 5: // ReplaceObject
		t.APChange = new(AnimPartChange).UnPack(s).(*AnimPartChange)
	case 6: // Ethereal
		t.Ethereal = ReadTP[int32](s)
	case 7: // TransparentPart:
		t.Part = ReadTP[uint32](s)
		t.Start = ReadTP[float32](s)
		t.End = ReadTP[float32](s)
		t.Time = ReadTP[float32](s)
	case 8: // Luminous:
		t.Start = ReadTP[float32](s)
		t.End = ReadTP[float32](s)
		t.Time = ReadTP[float32](s)
	case 9: // LuminousPart:
		t.Part = ReadTP[uint32](s)
		t.Start = ReadTP[float32](s)
		t.End = ReadTP[float32](s)
		t.Time = ReadTP[float32](s)
	case 10: // Diffuse:
		t.Start = ReadTP[float32](s)
		t.End = ReadTP[float32](s)
		t.Time = ReadTP[float32](s)
	case 11: // DiffusePart:
		t.Part = ReadTP[uint32](s)
		t.Start = ReadTP[float32](s)
		t.End = ReadTP[float32](s)
		t.Time = ReadTP[float32](s)
	case 12: // Scale:
		t.End = ReadTP[float32](s)
		t.Time = ReadTP[float32](s)
	case 13: // CreateParticle:
		t.EmitterInfoId = ReadTP[uint32](s)
		t.PartIndex = ReadTP[uint32](s)
		t.Offset = ReadTP[Frame](s)
		t.EmitterId = ReadTP[uint32](s)
	case 14: // DestroyParticle:
		t.EmitterId = ReadTP[uint32](s)
	case 15: // StopParticle:
		t.EmitterId = ReadTP[uint32](s)
	case 16: // NoDraw:
		t.NoDraw = ReadTP[uint32](s)
	case 17: // DefaultScript:
		// NO BODY
	case 18: // DefaultScriptPart:
		t.PartIndex = ReadTP[uint32](s)
	case 19: // CallPES:
		t.PES = ReadTP[uint32](s)
		t.Pause = ReadTP[float32](s)
	case 20: // Transparent:
		t.Start = ReadTP[float32](s)
		t.End = ReadTP[float32](s)
		t.Time = ReadTP[float32](s)
	case 21: // SoundTweaked:
		t.SoundID = ReadTP[uint32](s)
		t.Priority = ReadTP[float32](s)
		t.Probability = ReadTP[float32](s)
		t.Volume = ReadTP[float32](s)
	case 22: // SetOmega:
		t.Axis = ReadTP[Vector3](s)
	case 23: // TextureVelocity:
		t.USpeed = ReadTP[float32](s)
		t.VSpeed = ReadTP[float32](s)
	case 24: // TextureVelocityPart:
		t.PartIndex = ReadTP[uint32](s)
		t.USpeed = ReadTP[float32](s)
		t.VSpeed = ReadTP[float32](s)
	case 25: // SetLight:
		t.LightsOn = ReadTP[int32](s)
	case 26: // CreateBlockingParticle:
		// NO BODY
	}
	return t
}

type AnimPartChange struct {
	PartIndex byte              `json:"part_index"`
	PartID    DataIDOfKnownType `json:"part_id"`
}

func (t *AnimPartChange) UnPack(s *SBuffer) any {
	t = new(AnimPartChange)
	t.PartIndex = ReadT[byte](s)
	t.PartID = *new(DataIDOfKnownType).UnPack(s, 0x01000000).(*DataIDOfKnownType)
	return t
}

type TextureMapChange struct {
	PartIndex byte              `json:"part_index"`
	OldTexID  DataIDOfKnownType `json:"old_tex_id"`
	NewTexID  DataIDOfKnownType `json:"new_tex_id"`
}

func (t *TextureMapChange) UnPack(s *SBuffer) any {
	t = new(TextureMapChange)
	t.PartIndex = ReadT[byte](s)
	t.OldTexID = *new(DataIDOfKnownType).UnPack(s, 0x05000000).(*DataIDOfKnownType)
	t.NewTexID = *new(DataIDOfKnownType).UnPack(s, 0x05000000).(*DataIDOfKnownType)
	return t
}

type Subpalette struct {
	SubID     DataIDOfKnownType `json:"subID"`
	Offset    uint32            `json:"offset"`
	NumColors uint32            `json:"numcolors"`
}

func (t *Subpalette) UnPack(s *SBuffer) any {
	t = new(Subpalette)
	t.SubID = *new(DataIDOfKnownType).UnPack(s, 0x04000000).(*DataIDOfKnownType)
	t.Offset = uint32(ReadT[byte](s)) << 3
	t.NumColors = uint32(ReadT[byte](s)) << 3
	if t.NumColors == 0 {
		t.NumColors = 2048
	}
	return t
}

// type PString string

//	func (t *PString) UnPack(s *SBuffer, len16 bool, align int, un_obfuscate bool) {
//		var res []byte
//		var length int
//		if len16 {
//			length = int(ReadT[uint16](s))
//			res = s.Next(length)
//			length += 2
//		} else {
//			length = int(*ReadT[byte](s))
//			res = s.Next(length)
//			length++
//		}
//		if un_obfuscate {
//			for i := range res {
//				res[i] = (res[i] >> 4) | (res[i] << 4)
//			}
//		}
//		*t = PString(res)
//		if align > 1 {
//			for length%align != 0 {
//				ReadT[byte](s)
//				length++
//			}
//		}
//	}

// Basic List of T.
type List[V _Object] []V

// Read Length of uint32 from s, then Length x T's
func (t *List[V]) UnPack(s *SBuffer) any {
	t = new(List[V])
	num := int(ReadT[uint32](s))
	*t = make([]V, num)
	for i := 0; i < num; i++ {
		(*t)[i] = (*new(V)).UnPack(s).(V)
	}
	// log.Printf("List.UnPack(): %s", ToJSON(*t))
	return t
}

// Read num x T's from s
func (t *List[V]) UnPackFixed(s *SBuffer, num int) any {
	t = new(List[V])
	*t = make([]V, num)
	for i := 0; i < num; i++ {
		(*t)[i] = (*new(V)).UnPack(s).(V)
	}
	// log.Printf("List.UnPackFixed(): %s", ToJSON(*t))
	return t
}

type HashMap[V _Object] map[uint32]HashMapData[V]

func (t *HashMap[V]) UnPack(s *SBuffer) any {
	t = new(HashMap[V])
	num := int(ReadT[uint16](s))
	ReadT[int16](s)
	*t = make(map[uint32]HashMapData[V])
	for i := 0; i < num; i++ {
		ret := new(HashMapData[V]).UnPack(s).(*HashMapData[V])
		(*t)[ret.Key] = *ret
	}
	// log.Printf("HashMap.UnPack(): %s", ToJSON(t))
	return t
}
func (t *HashMap[V]) UnPackFixed(s *SBuffer, num int) any {
	t = new(HashMap[V])
	*t = make(map[uint32]HashMapData[V])
	for i := 0; i < num; i++ {
		v := new(HashMapData[V]).UnPack(s)
		ret := v.(*HashMapData[V])
		(*t)[ret.Key] = *ret
	}
	// log.Printf("HashMap.UnPack(): %s", ToJSON(t))
	return t
}

func (W *HashMap[V]) Get(id uint32) V {
	if ob, ok := (*W)[id]; ok {
		return ob.Value
	}
	return *new(V)
}
func (t *HashMap[V]) MarshalJSON() ([]byte, error) {
	var slist []uint32
	for k := range *t {
		slist = append(slist, k)
	}
	sort.SliceStable(slist, func(i, j int) bool {
		return i < j
	})
	ret := []string{}
	for _, k := range slist {
		value, _ := json.Marshal((*t)[k])
		ret = append(ret, string(value))
	}
	return []byte("[" + strings.Join(ret, ",") + "]"), nil
}

type HashMapData[V _Object] struct {
	Key   uint32 `json:"key"`
	Value V      `json:"value"`
}

func (t *HashMapData[V]) UnPack(s *SBuffer) any {
	t = new(HashMapData[V])
	t.Key = ReadT[uint32](s)
	t.Value = t.Value.UnPack(s).(V)
	return t
}

type HashMapS[V _Object] map[string]HashMapSData[V]

func (t *HashMapS[V]) UnPack(s *SBuffer) any {
	t = new(HashMapS[V])
	num := int(ReadT[uint16](s))
	ReadT[int16](s)
	*t = make(map[string]HashMapSData[V])
	for i := 0; i < num; i++ {
		v := new(HashMapSData[V]).UnPack(s)
		ret := v.(*HashMapSData[V])
		(*t)[ret.Key] = *ret
	}
	// log.Printf("HashMap.UnPack(): %s", ToJSON(t))
	return t
}

func (W *HashMapS[V]) Get(id string) V {
	if W == nil {
		return *new(V)
	}
	if ob, ok := (*W)[id]; ok {
		// log.Printf("HashMap[%T, %T].Get(%v): %v", id, ob.Value, id, ob.Value)
		return ob.Value
	}
	return *new(V)
}
func (t *HashMapS[V]) MarshalJSON() ([]byte, error) {
	ret := []string{}
	for _, k := range *t {
		value, _ := json.Marshal(k)
		ret = append(ret, string(value))
	}
	return []byte("[" + strings.Join(ret, ",") + "]"), nil
}

type HashMapSData[V _Object] struct {
	Key   string `json:"key"`
	Value V      `json:"value"`
}

func (t *HashMapSData[V]) UnPack(s *SBuffer) any {
	t = new(HashMapSData[V])
	t.Key = string(*new(PString).UnPack(s).(*PString))
	t.Value = t.Value.UnPack(s).(V)
	return t
}

type Int32 int32
type Int64 int64
type Bool byte
type Bool4 byte
type Float float64
type Uint32 uint32

func (t *Int32) UnPack(s *SBuffer) any {
	return ReadTP[Int32](s)
}
func (t *Int64) UnPack(s *SBuffer) any {
	return ReadTP[Int64](s)
}
func (t *Bool) UnPack(s *SBuffer) any {
	return ReadTP[Bool](s)
}
func (t *Bool4) UnPack(s *SBuffer) any {
	t = new(Bool4)
	*t = Bool4(ReadT[byte](s) % 2)
	ReadTsP[byte](s, 3)
	return t
}
func (t *Float) UnPack(s *SBuffer) any {
	return ReadTP[Float](s)
}
func (t *Uint32) UnPack(s *SBuffer) any {
	return ReadTP[Uint32](s)
}

type PString string

// reads 2 LittleEndian bytes for length, then length x bytes, plus aligns to 32 bits
func (t *PString) UnPack(s *SBuffer) any {
	t = new(PString)
	num := int(ReadT[uint16](s))
	*t = PString(ReadTs[byte](s, num))
	align := 4 - ((num + 2) % 4)
	if align > 0 && align < 4 {
		ReadTsP[byte](s, align)
	}
	// log.Printf("PString.UnPack(): num:%d align:%d len:%d str:\"%s\"", num, align, len(*t), *t)
	// log.Printf("PString.UnPack():\n%s", hex.Dump(s.Bytes()[0:32]))
	return t
}

type Dat_CompressedUInt32 uint32

func (t *Dat_CompressedUInt32) UnPack(s *SBuffer) any {
	t = new(Dat_CompressedUInt32)
	val := uint32(ReadT[byte](s))
	if (val & 0x80) == 0 {
		*t = Dat_CompressedUInt32(val)
		return t
	}
	val |= (uint32(ReadT[byte](s)) << 8) & 0x7FFF
	if (val & 0x40) == 0 {
		*t = Dat_CompressedUInt32(val)
		return t
	}
	val |= (uint32(ReadT[uint16](s)) << 16) & 0x3FFFFFFF
	*t = Dat_CompressedUInt32(val)
	return t
}

type DataIDOfKnownType uint32

// gdle calls this Unpack_AsDataIDOfKnownType
func (t *DataIDOfKnownType) UnPack(s *SBuffer, did_class uint32) any {
	t = new(DataIDOfKnownType)
	val := uint32(ReadT[uint16](s))
	if (val & 0x8000) == 0 {
		*t = DataIDOfKnownType(val + did_class)
		return t
	}
	val &= 0x3FFF                   // trim high 2 bits
	val <<= 16                      // shift it up to the high nibble
	val |= uint32(ReadT[uint16](s)) // populate the lower nibble with another uint16
	*t = DataIDOfKnownType(val + did_class)
	return t
}

type WClassIDCompressed uint32

func (t *WClassIDCompressed) UnPack(s *SBuffer) any {
	t = new(WClassIDCompressed)
	val := uint32(ReadT[uint16](s))
	if (val & 0x8000) == 0 {
		*t = WClassIDCompressed(val)
		return t
	}
	val &= 0x7FFF                   // trim high bit
	val <<= 16                      // shift it up to the high nibble
	val |= uint32(ReadT[uint16](s)) // populate the lower nibble with another uint16
	*t = WClassIDCompressed(val)
	return t
}

func ToJSON(in any) string {
	resu, _ := json.Marshal(in)
	return string(resu)
}
func ToJSONFormat(in any, prefix, indent string) string {
	resu, _ := json.MarshalIndent(in, prefix, indent)
	return string(resu)
}
