// //////////////////////////////////////////////////////////////
//
//  WIP - GDLE cache.bin parser
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"bytes"
	"compress/zlib"
	"encoding/binary"
	"io"
	"log"
	"os"
	"time"
)

var CacheBin = &CPhatDataBin{
	Entries: []CPhatDataBinEntry{
		{FMagic1: 0xe8b00434, FMagic2: 0x82092270, SegName: "_regionData"},
		{FMagic1: 0x5D97BAEC, FMagic2: 0x41675123, SegName: "_spellTableData"},
		{FMagic1: 0x7DC126EB, FMagic2: 0x5F41B9AD, SegName: "_treasureTableData"},
		{FMagic1: 0x5F41B9AD, FMagic2: 0x7DC126EB, SegName: "_craftTableData"},
		{FMagic1: 0x887aef9c, FMagic2: 0xa92ec9ac, SegName: "_housePortalDests"},
		{FMagic1: 0xcd57fd07, FMagic2: 0x697a2224, SegName: "_data inferred cell data"},
		{FMagic1: 0x591c34e9, FMagic2: 0x2250b020, SegName: "avatarTable"},
		{FMagic1: 0xE80D81CA, FMagic2: 0x8ECA9786, SegName: "_questDefDB"},
		{FMagic1: 0xd8fd6b02, FMagic2: 0xa0427974, SegName: "CWeenieDefaults"},
		{FMagic1: 0x5f1fa913, FMagic2: 0xe345c74c, SegName: "mutation_table_t"},
		{FMagic1: 0x812a7823, FMagic2: 0x8b28e107, SegName: "_gameEvents"},
	},
}

type CPhatDataBin struct {
	FileName   string
	FileSize   uint32
	Version    uint32
	NumEntries uint32
	Entries    []CPhatDataBinEntry
	RegionData RegionData
	// SpellTable SpellTable
	// Seg3       TreasureTableData
	// Seg4       CraftTableData
	// Seg5       HousePortalDests
	// Seg6       InferredCellData
	// Seg7       AvatarTable
	// Seg8       QuestDefDB
	// CWeenieDefaults CWeenieDefaults
	// Seg10      MutationTable
	// Seg11      GameEvents
}

// loads/decrypts base cache.bin structure into CPhatDataBin.Entries
func (d *CPhatDataBin) Load(filepath string) bool {
	timeStart := time.Now()
	d.FileName = filepath

	fileData, err := os.ReadFile(d.FileName)
	if err != nil {
		log.Printf("CPhatDataBin::Load(%s) read err: %v", filepath, err)
		return false
	}
	d.FileSize = uint32(len(fileData))
	xor_val := d.FileSize + (d.FileSize << 15)
	for i := uint32(0); i < (d.FileSize - 3); i += 4 {
		fileData[i] ^= byte(xor_val)
		fileData[i+1] ^= byte(xor_val >> 8)
		fileData[i+2] ^= byte(xor_val >> 16)
		fileData[i+3] ^= byte(xor_val >> 24)
		xor_val <<= 3
		xor_val += d.FileSize
	}
	d.Version = binary.LittleEndian.Uint32(fileData[0:])
	d.NumEntries = binary.LittleEndian.Uint32(fileData[4:])

	log.Printf("File: %s version: %d size: %d num entries: %d\n", filepath, d.Version, d.FileSize, d.NumEntries)
	//os.WriteFile("cache_dec.bin", fileData, 0600)
	var pos int = 8
	for i := uint32(0); i < 11 && i < d.NumEntries; i++ {
		data_id := binary.LittleEndian.Uint32(fileData[pos:])
		pos += 4
		len := d.Entries[i].Read(fileData[pos:], data_id)
		pos += len
	}
	log.Printf("cache.bin processed %d segments in %s\n", d.NumEntries, time.Since(timeStart))
	log.Printf("reading %db as RegionData...\n", len(d.Entries[0].Data))

	d.RegionData.UnPack(&SBuffer{(bytes.NewBuffer(d.Entries[0].Data))})
	log.Printf("seg1: read %v\n", ToJSON(d.RegionData))
	// d.SpellTable.UnPack(&SBuffer{(bytes.NewBuffer(d.Entries[1].Data))})
	// log.Printf("seg2: read %v\n", d.SpellTable)
	// readb = d.Seg3.UnPack(d.Entries[2].Data)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[2].Len)
	// readb = d.Seg4.UnPack(d.Entries[3].Data)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[3].Len)
	// readb = d.Seg5.UnPack(d.Entries[4].Data)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[4].Len)
	// readb = d.Seg6.UnPack(d.Entries[5].Data)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[5].Len)
	// readb = d.Seg7.UnPack(d.Entries[6].Data)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[6].Len)
	// readb = d.Seg8.UnPack(d.Entries[7].Data)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[7].Len)
	// readb = d.Seg9.UnPack(d.Entries[8].Data)
	// d.CWeenieDefaults.UnPack(&SBuffer{(bytes.NewBuffer(d.Entries[8].Data))})
	// log.Printf("seg9: read %v\n", d.CWeenieDefaults)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[8].Len)
	// readb = d.Seg10.UnPack(d.Entries[9].Data)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[9].Len)
	// readb = d.Seg11.UnPack(d.Entries[10].Data)
	// log.Printf("seg3: read %d of %d\n", readb, d.Entries[10].Len)
	return false
}

type CPhatDataBinEntry struct {
	FileID  uint32 `json:"file_id"`
	Magic1  uint32 `json:"magic_1"`
	Magic2  uint32 `json:"magic_2"`
	Magic3  uint32 `json:"magic_3"`
	FMagic1 uint32 `json:"final_magic_1"`
	FMagic2 uint32 `json:"final_magic_2"`
	SegName string `json:"segment_name"`
	CLen    uint32 `json:"clen"`
	Len     uint32 `json:"len"`
	Data    []byte `json:"-"`
}

func (t *CPhatDataBinEntry) Read(buf []byte, fid uint32) int {
	t.FileID = binary.LittleEndian.Uint32(buf[0:])
	t.Magic1 = binary.LittleEndian.Uint32(buf[4:])
	t.Magic2 = binary.LittleEndian.Uint32(buf[8:])
	t.Magic3 = binary.LittleEndian.Uint32(buf[12:])
	var pos int = 17
	if buf[16] == 1 {
		t.CLen = binary.LittleEndian.Uint32(buf[pos:])
		t.Data = buf[pos+4 : pos+4+int(t.CLen)]
		t.Len = binary.LittleEndian.Uint32(buf[pos+4+int(t.CLen):])
		pos += 8 + int(t.CLen)

		b := bytes.NewReader(t.Data)
		r, err := zlib.NewReader(b)
		if err != nil {
			return pos
		}
		var res bytes.Buffer
		io.Copy(&res, r)
		r.Close()
		t.Data = res.Bytes()

		packing := t.Magic1
		fpacking := t.FMagic1
		for i := uint32(0); i < uint32(len(t.Data)>>2); i++ {
			packing = t.Magic3 + (t.Magic2 ^ packing)
			intpacking := binary.LittleEndian.Uint32(t.Data[i<<2:])
			intpacking += ((i + 1) * (packing + fpacking))
			t.Data[i<<2+0] = byte(intpacking)
			t.Data[i<<2+1] = byte(intpacking >> 8)
			t.Data[i<<2+2] = byte(intpacking >> 16)
			t.Data[i<<2+3] = byte(intpacking >> 24)
			fpacking ^= t.FMagic2
		}
		t.Len = binary.LittleEndian.Uint32(t.Data)
		ba := bytes.NewReader(t.Data[4:])
		ra, err := zlib.NewReader(ba)
		if err != nil {
			return pos
		}
		var res2 bytes.Buffer
		io.Copy(&res2, ra)
		ra.Close()
		t.Data = res2.Bytes()

	}
	//log.Printf("CPhatDataBinEntry %d: %s len %d:\n%s\n", fid, ToJSON(t), len(t.Data), hex.Dump(t.Data[0:min(64, len(t.Data)-1)]))
	return pos
}
