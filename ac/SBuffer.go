// //////////////////////////////////////////////////////////////
//
//            ac.SBuffer - Binary readable Endianness Swap buffer
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
)

type _Object interface {
	//Pack() *SBuffer
	UnPack(*SBuffer) any
}

type _CompObject interface {
	//Pack() *SBuffer
	UnPack(*SBuffer) any
	comparable
}
type FB[T _Object] struct {
	S T
}

// parses binary Bytes, into struct
func (f *FB[T]) FromBytes(buf []byte) error {
	if buf == nil {
		return fmt.Errorf("FBuffer[%T].FromBytes(<nil>) ... ", f.S)
	}
	sb := &SBuffer{(bytes.NewBuffer(buf))}
	f.S = (*new(T)).UnPack(sb).(T)
	if sb.Len() != 0 {
		return fmt.Errorf("FBuffer[%T].FromBytes(%d) ... %db remaining",
			f.S, len(buf), sb.Len(),
			//hex.Dump(buf), ToJSONFormat(f.S, "", "  "),
		)
	}
	return nil
}

// parses JSON Bytes, into struct
func (f *FB[T]) FromJSON(buf []byte) error {
	if buf == nil {
		return fmt.Errorf("FBuffer[%T].FromJSON(<nil>) ... ", f.S)
	}
	f.S = *new(T)
	return json.Unmarshal(buf, f.S)
}

// parses struct, into json
func (f *FB[T]) ToJSON() []byte {
	buf, _ := json.Marshal(f.S)
	return buf
}

// parses struct, into json, formatted
func (f *FB[T]) ToJSONf() []byte {
	buf, _ := json.MarshalIndent(f.S, "", "  ")
	return buf
}

// parses struct, into binary
// func (f *FB[T]) ToBytes() []byte {
// 	return f.S.Pack(&SBuffer{})
// }

type SBuffer struct{ *bytes.Buffer }

// read T from buffer
func ReadT[T any](s *SBuffer) T {
	ret := new(T)
	binary.Read(s, binary.LittleEndian, ret)
	return *ret
}

// read T from buffer, return pointer
func ReadTP[T any](s *SBuffer) *T {
	ret := new(T)
	binary.Read(s, binary.LittleEndian, ret)
	return ret
}

// read Array of T's from buffer
func ReadTs[T any](s *SBuffer, num int) []T {
	ret := make([]T, num)
	binary.Read(s, binary.LittleEndian, &ret)
	return ret
}

// read Array of T's from buffer, return pointer
func ReadTsP[T any](s *SBuffer, num int) *[]T {
	ret := make([]T, num)
	binary.Read(s, binary.LittleEndian, &ret)
	return &ret
}

// func (s *SBuffer) readByte() *byte {
// 	var ret byte
// 	binary.Read(s, binary.LittleEndian, &ret)
// 	return &ret
// }
// func (s *SBuffer) next(size int) *[]byte {
// 	ret := make([]byte, size)
// 	binary.Read(s, binary.LittleEndian, &ret)
// 	return &ret
// }

// func (s *SBuffer) readUint16s(size int) *[]uint16 {
// 	ret := make([]uint16, size)
// 	binary.Read(s, binary.LittleEndian, &ret)
// 	return &ret
// }

// func (s *SBuffer) readInt16() *int16 {
// 	ret := *new(int16)
// 	binary.Read(s, binary.LittleEndian, &ret)
// 	return &ret
// }

// func (s *SBuffer) readInt16s(size int) *[]int16 {
// 	ret := make([]int16, size)
// 	binary.Read(s, binary.LittleEndian, &ret)
// 	return &ret
// }

// func (s *SBuffer) readUint32() *uint32 {
// 	ret := *new(uint32)
// 	binary.Read(s, binary.LittleEndian, &ret)
// 	return &ret
// }
// func (s *SBuffer) readUint32s(size int) *[]uint32 {
// 	ret := make([]uint32, size)
// 	binary.Read(s, binary.LittleEndian, &ret)
// 	return &ret
// }

// func (s *SBuffer) readInt32() *int32 {
// 	ret := int32(binary.LittleEndian.Uint32(*s.next(4)))
// 	return &ret
// }
// func (s *SBuffer) readInt32s(size int) *[]int32 {
// 	ret := make([]int32, size)
// 	binary.Read(s, binary.LittleEndian, &ret)
// 	return &ret
// }

// func (s *SBuffer) readUint64() *uint64 {
// 	ret := binary.LittleEndian.Uint64(*s.next(8))
// 	return &ret
// }

// func (s *SBuffer) readInt64() *int64 {
// 	ret := int64(binary.LittleEndian.Uint64(*s.next(8)))
// 	return &ret
// }

// func (s *SBuffer) readSingle() *float32 {
// 	ret := math.Float32frombits(binary.LittleEndian.Uint32(*s.next(4)))
// 	return &ret
// }
// func (s *SBuffer) readDouble() *float64 {
// 	ret := math.Float64frombits(binary.LittleEndian.Uint64(*s.next(8)))
// 	return &ret
// }
