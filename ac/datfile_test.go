package ac

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"
)

func TestDatFile_Parse(t *testing.T) {
	t.Run("Portal.Parse", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		log.Printf("Portal.Parse: %s\n", ToJSON(&Portal))
	})

	// case 0x01: // GraphicsObject

	// case 0x02: // Setup
	t.Run("Portal_Dump_Setups", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry   `json:"header"`
			Dat *Dat_Setup `json:"setup"`
		}
		var content []byte
		for _, o := range Portal.PortalCat[0x02] {
			ret.Ent, content = Portal.GetDIDFile(o)
			if content != nil {
				ret.Dat.UnPack(&SBuffer{(bytes.NewBuffer(content))})
			}
			out, _ := json.MarshalIndent(ret, "", "  ")
			os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
		}
	})

	// case 0x03: // Animation
	t.Run("Portal_Dump_Animations", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry       `json:"header"`
			Dat *Dat_Animation `json:"animation"`
		}
		var content []byte
		for _, o := range Portal.PortalCat[0x03] {
			ret.Ent, content = Portal.GetDIDFile(o)
			if content != nil {
				ret.Dat.UnPack(&SBuffer{(bytes.NewBuffer(content))})
			}
			out, _ := json.MarshalIndent(ret, "", "  ")
			os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
		}
	})

	// case 0x04: // Palette
	t.Run("Portal_Dump_Palettes", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry     `json:"header"`
			Dat *Dat_Palette `json:"palette"`
		}
		var content []byte
		for _, o := range Portal.PortalCat[0x04] {
			ret.Ent, content = Portal.GetDIDFile(o)
			if content != nil {
				ret.Dat.UnPack(&SBuffer{(bytes.NewBuffer(content))})
			}
			out, _ := json.MarshalIndent(ret, "", "  ")
			os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
		}
	})

	// case 0x05: // SurfaceTexture

	// case 0x06: // Texture
	t.Run("Portal_Dump_Textures", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry     `json:"header"`
			Dat *Dat_Texture `json:"texture"`
		}
		var content []byte
		for _, o := range Portal.PortalCat[0x06] {
			ret.Ent, content = Portal.GetDIDFile(o)
			if content != nil {
				ret.Dat.UnPack(&SBuffer{(bytes.NewBuffer(content))})
			}
			out, _ := json.MarshalIndent(ret, "", "  ")
			os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
		}
	})

	// case 0x07: // ???
	// case 0x08: // Surface

	// case 0x09: // MotionTable
	t.Run("Portal_Dump_MotionTables", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry     `json:"header"`
			Dat *MotionTable `json:"motion_table"`
		}
		var content []byte
		for _, o := range Portal.PortalCat[0x09] {
			ret.Ent, content = Portal.GetDIDFile(o)
			if content != nil {
				ret.Dat.UnPack(&SBuffer{(bytes.NewBuffer(content))})
			}
			out, _ := json.MarshalIndent(ret, "", "  ")
			os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
		}
	})

	// case 0x0A: // Wave
	// case 0x0B: // ???
	// case 0x0C: // ???
	// case 0x0D: // Environment
	// case 0x0E: // Tables
	// case 0x0E01: // QualityFilter
	// case 0x0E02: // MonitoredProperties
	// case 0x0E00: // ClientTable
	// case 0x0E000000: //
	// case 0x0E000001: //
	// case 0x0E000002: // CharacterGenerator
	// case 0x0E000003: // SecondaryAttributeTable

	// case 0x0E000004: // SkillTable
	t.Run("Portal_Dump_SkillTable", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry        `json:"header"`
			Dat *Dat_SkillTable `json:"skill_table"`
		}
		ret.Ent, ret.Dat = Portal.Get_SkillTable()
		out, _ := json.MarshalIndent(ret, "", "  ")
		os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
	})

	// 		case 0x0E000005: //
	// 		case 0x0E000006: //
	// 		case 0x0E000007: // ChatPoseTable
	// 		case 0x0E000008: //
	// 		case 0x0E000009: //
	// 		case 0x0E00000A: //
	// 		case 0x0E00000B: //
	// 		case 0x0E00000C: //
	// 		case 0x0E00000D: // ObjectHierarchy
	// 		case 0x0E00000E: // SpellTable
	t.Run("Portal_Dump_SpellTable", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry        `json:"header"`
			Dat *Dat_SpellTable `json:"spell_table"`
		}
		ret.Ent, ret.Dat = Portal.Get_SpellTable()
		out, _ := json.MarshalIndent(ret, "", "  ")
		os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
	})
	// 		case 0x0E00000F: // SpellComponentTable
	// 		case 0x0E000010: //
	// 		case 0x0E000011: //
	// 		case 0x0E000012: //
	// 		case 0x0E000013: //
	// 		case 0x0E000014: //
	// 		case 0x0E000015: //
	// 		case 0x0E000016: //
	// 		case 0x0E000017: //
	// 		case 0x0E000018: // XpTable
	// 		case 0x0E000019: //
	// 		case 0x0E00001A: // BadData
	// 		case 0x0E00001B: //
	// 		case 0x0E00001C: //
	// 		case 0x0E00001D: // ContractTable
	// 		case 0x0E00001E: // TabooTable
	// 		case 0x0E00001F: // FileToId
	// 		case 0x0E000020: // NameFilterTable

	// case 0x0F: // PaletteSet
	t.Run("Portal_Dump_PaletteSets", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry        `json:"header"`
			Dat *Dat_PaletteSet `json:"palette_set"`
		}
		var content []byte
		for _, o := range Portal.PortalCat[0x0F] {
			ret.Ent, content = Portal.GetDIDFile(o)
			if content != nil {
				ret.Dat.UnPack(&SBuffer{(bytes.NewBuffer(content))})
			}
			out, _ := json.MarshalIndent(ret, "", "  ")
			os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
		}
	})

	// case 0x10: // Clothing
	// case 0x11: // DegradeInfo
	// case 0x12: // Scene
	t.Run("Portal_Dump_Scenes", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry   `json:"header"`
			Dat *Dat_Scene `json:"scene"`
		}
		var content []byte
		for _, o := range Portal.PortalCat[0x12] {
			ret.Ent, content = Portal.GetDIDFile(o)
			if content != nil {
				ret.Dat.UnPack(&SBuffer{(bytes.NewBuffer(content))})
			}
			out, _ := json.MarshalIndent(ret, "", "  ")
			os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
		}
	})
	// case 0x13: // Region
	t.Run("Portal_Dump_Regions", func(t *testing.T) {
		Portal.Parse("C:\\gdle\\Data\\")
		var ret struct {
			Ent *BTEntry    `json:"header"`
			Dat *Dat_Region `json:"region"`
		}
		var content []byte
		for _, o := range Portal.PortalCat[0x13] {
			ret.Ent, content = Portal.GetDIDFile(o)
			if content != nil {
				ret.Dat.UnPack(&SBuffer{(bytes.NewBuffer(content))})
			}
			out, _ := json.MarshalIndent(ret, "", "  ")
			os.WriteFile(fmt.Sprintf("json\\0x%08X.json", ret.Ent.GID), out, 0666)
		}
	})
	// case 0x14: // KeyMap
	// case 0x15: // RenderTexture
	// case 0x16: // RenderMaterial
	// case 0x17: // MaterialModifier
	// case 0x18: // MaterialInstance
	// case 0x19: //
	// case 0x1A: //
	// case 0x1B: //
	// case 0x1C: //
	// case 0x1D: //
	// case 0x1E: //
	// case 0x1F: //
	// case 0x20: // SoundTable
	// case 0x21: // UiLayout
	// case 0x22: // EnumMapper
	// case 0x23: // StringTable
	// case 0x24: //
	// case 0x25: // DidMapper
	// case 0x26: // ActionMap
	// case 0x27: // DualDidMapper
	// case 0x28: //
	// case 0x29: //
	// case 0x2A: //
	// case 0x2B: //
	// case 0x2C: //
	// case 0x2D: //
	// case 0x2E: //
	// case 0x2F: //
	// case 0x30: // CombatTable
	// case 0x31: // String
	// case 0x32: // ParticleEmitter
	// case 0x33: // PhysicsScript
	// case 0x34: // PhysicsScriptTable
	// case 0x35: //
	// case 0x36: //
	// case 0x37: //
	// case 0x38: //
	// case 0x39: // MasterProperty
	// case 0x40: // Font
	// case 0x41: // StringState
	// case 0x78: // DbProperties

	t.Run("Cell.Parse", func(t *testing.T) {
		Cell.Parse("C:\\gdle\\Data\\")
		log.Printf("Cell Parse: %s\n", ToJSON(&Cell))
	})
	t.Run("Cell_Dump_LandBlocks", func(t *testing.T) {
		Cell.Parse("C:\\gdle\\Data\\")
		out, _ := json.MarshalIndent(Cell.LandBlocks, "", "  ")
		os.WriteFile("json\\LandBlocks.json", out, 0666)
	})
	t.Run("Cell_Dump_LandBlockInfos", func(t *testing.T) {
		Cell.Parse("C:\\gdle\\Data\\")
		out, _ := json.MarshalIndent(Cell.LandBlockInfos, "", "  ")
		os.WriteFile("json\\LandBlockInfos.json", out, 0666)
	})
	t.Run("Cell_Dump_EnvCells", func(t *testing.T) {
		Cell.Parse("C:\\gdle\\Data\\")
		out, _ := json.MarshalIndent(Cell.EnvCells, "", "  ")
		os.WriteFile("json\\EnvCells.json", out, 0666)
	})
	t.Run("Cell.Get_LandBlock(0x7C65FFFF)", func(t *testing.T) {
		Cell.Parse("C:\\gdle\\Data\\")
		ent, lb := Cell.Get_LandBlock(0x7C65FFFF)
		log.Printf("Cell.Get_LandBlock(0x7C65FFFF): %s\n%s", ToJSON(&ent), ToJSON(&lb))
	})
	t.Run("HighRes.Parse", func(t *testing.T) {
		HighRes.Parse("C:\\gdle\\Data\\")
		log.Printf("HighRes.Parse: %s\n", ToJSON(&HighRes))
	})
	t.Run("LocalEnglish.Parse", func(t *testing.T) {
		LocalEnglish.Parse("C:\\gdle\\Data\\")
		log.Printf("LocalEnglish.Parse: %s\n", ToJSON(&LocalEnglish))
	})
}
