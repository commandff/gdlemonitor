package ac

import (
	"encoding/hex"
	"log"
	"os"
	"testing"
)

func TestWeenieSave(t *testing.T) {
	t.Run("WeenieSave.Unpack", func(t *testing.T) {
		buf, _ := os.ReadFile("ref/CWeenieSave.1342179251.bin")
		cws := &FB[*CWeenieSave]{S: new(CWeenieSave)}
		err := cws.FromBytes(buf)
		log.Printf("WeenieSave.Unpack: %s\n", cws.ToJSONf())
		if err != nil {
			panic(err)
		}
		panic(err)
	})
	t.Run("Parse.GDLEWeenies", func(t *testing.T) {
		//err := Load_GDLEWeenies([]uint32{2441121241, 2437377494, 2421959035, 2419123588, 2416264790, 2416264079, 2437432065, 2421975913, 2421531298, 2421976216, 2416547179, 2432996667, 2421248886, 2424904866, 2424904861, 2424904860, 2422784635, 2422784627, 2426293608, 2417168006, 2417189313, 2433119146, 2433119134, 2433104159, 2453066379, 2453066378, 2453066377, 2453066376, 2453066375, 2453066374, 2453066373, 2453066371, 2453066370, 2453066369, 2453066368, 2453066367, 2453066366, 2453066365, 2453066364, 2453066363, 2453066362, 2453066361, 2453039518, 2435078876, 2452055110, 2433347194, 2433016968, 2433016967, 2433016966, 2433016965, 2433016964, 2433347195, 2433016963, 2417192528, 2417159252, 2415985977, 2417167845, 2415975351, 2415989572, 2415928654, 2416441828, 2415928663, 2416441830, 2416441831, 2416441840, 2416441829})
		err := Parse_GDLEWeenies(0, 500)
		if err != nil {
			panic(err)
		}

		for id, cw := range GDLEWeenies {
			cws := new(FB[*CWeenieSave])
			err := cws.FromBytes(cw.Data)
			if err != nil {
				log.Printf("Failed to load %08x SaveVersion %d SaveTimestamp %d SaveInstanceTS %d WCID %d, weenieType %d %s:\n%s\n%s",
					id, cws.S.SaveVersion, cws.S.SaveTimestamp, cws.S.SaveInstanceTS,
					cws.S.Qualities.WCID, cws.S.Qualities.WeenieType,
					*cws.S.Qualities.StringStats.Get(1),

					hex.Dump(cw.Data), ToJSONFormat(cws.S, "", "  "))
				panic(err)
			}

			// log.Printf("Loaded %08x SaveVersion %d SaveTimestamp %d SaveInstanceTS %d WCID %d, weenieType %d: %s",
			// 	id, cws.S.SaveVersion, cws.S.SaveTimestamp, cws.S.SaveInstanceTS,
			// 	cws.S.Qualities.WCID, cws.S.Qualities.WeenieType,
			// 	*cws.S.Qualities.StringStats.Get(1))
		}
		panic(err)
	})

}
