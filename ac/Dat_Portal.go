// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"image"
	"log"
	"os"

	"github.com/sunshineplan/imgconv"
)

// //////////////////////////////////////////////////////////////
// Setup 0x02
// //////////////////////////////////////////////////////////////
type Dat_Setup struct {
	Id                 uint32                   `json:"id"`
	Flags              uint32                   `json:"flags"`
	NumParts           uint32                   `json:"num_Parts"`
	Parts              []uint32                 `json:"parts"`
	ParentIndex        *[]int32                 `json:"parent_index"`
	DefaultScale       []Vector3                `json:"default_scale"`
	HoldingLocations   *List[*Dat_LocationType] `json:"holding_locations"`
	ConnectionPoints   *List[*Dat_LocationType] `json:"connection_points"`
	PlacementFrames    []Dat_PlacementType      `json:"placement_frames"`
	CylSpheres         *List[*Dat_CylSphere]    `json:"cyl_spheres"`
	Spheres            *List[*Dat_Sphere]       `json:"spheres"`
	Height             float32                  `json:"height"`
	Radius             float32                  `json:"radius"`
	StepUpHeight       float32                  `json:"step_up_height"`
	StepDownHeight     float32                  `json:"step_down_height"`
	SortingSphere      Dat_Sphere               `json:"sorting_sphere"`
	SelectionSphere    Dat_Sphere               `json:"selection_sphere"`
	Lights             *List[*Dat_LightInfo]    `json:"lights"`
	DefaultAnimation   uint32                   `json:"default_animation"`
	DefaultScript      uint32                   `json:"default_script"`
	DefaultMotionTable uint32                   `json:"default_motion_table"`
	DefaultSoundTable  uint32                   `json:"default_sound_table"`
	DefaultScriptTable uint32                   `json:"default_script_table"`
}

func (t *Dat_Setup) UnPack(s *SBuffer) any {
	t = new(Dat_Setup)
	//log.Printf("Dat_Setup Parse:\n%s\n", hex.Dump(s.Bytes()))
	t.Id = ReadT[uint32](s)
	t.Flags = ReadT[uint32](s)
	t.NumParts = ReadT[uint32](s)
	t.Parts = ReadTs[uint32](s, int(t.NumParts))
	if (t.Flags & 1) == 1 { // HasParent
		t.ParentIndex = ReadTsP[int32](s, int(t.NumParts))
	}
	if (t.Flags & 2) == 2 { // HasDefaultScale
		t.DefaultScale = ReadTs[Vector3](s, int(t.NumParts))
	}
	t.HoldingLocations = new(List[*Dat_LocationType]).UnPack(s).(*List[*Dat_LocationType])
	t.ConnectionPoints = new(List[*Dat_LocationType]).UnPack(s).(*List[*Dat_LocationType])
	t.PlacementFrames = make([]Dat_PlacementType, ReadT[uint32](s))
	for i := range t.PlacementFrames {
		t.PlacementFrames[i].UnPack(s, int(t.NumParts))
	}
	t.CylSpheres = new(List[*Dat_CylSphere]).UnPack(s).(*List[*Dat_CylSphere])
	t.Spheres = new(List[*Dat_Sphere]).UnPack(s).(*List[*Dat_Sphere])
	t.Height = ReadT[float32](s)
	t.Radius = ReadT[float32](s)
	t.StepUpHeight = ReadT[float32](s)
	t.StepDownHeight = ReadT[float32](s)
	t.SortingSphere.UnPack(s)
	t.SelectionSphere.UnPack(s)
	t.Lights = new(List[*Dat_LightInfo]).UnPack(s).(*List[*Dat_LightInfo])
	t.DefaultAnimation = ReadT[uint32](s)
	t.DefaultScript = ReadT[uint32](s)
	t.DefaultMotionTable = ReadT[uint32](s)
	t.DefaultSoundTable = ReadT[uint32](s)
	t.DefaultScriptTable = ReadT[uint32](s)
	if s.Len() != 0 {
		log.Printf("Dat_Setup Parse failed %db remaining:\n%s\n%s\n", s.Len(), hex.Dump(s.Bytes()), ToJSONFormat(t, "", "  "))

	}
	return t
}

// //////////////////////////////////////////////////////////////
// Palette 0x04
// //////////////////////////////////////////////////////////////
type Dat_Palette struct {
	Id        uint32   `json:"id"`
	NumColors uint32   `json:"num_colors"`
	Colors    []uint32 `json:"colors"`
}

func (t *Dat_Palette) UnPack(s *SBuffer) any {
	t = new(Dat_Palette)
	t.Id = ReadT[uint32](s)
	t.NumColors = ReadT[uint32](s)
	t.Colors = ReadTs[uint32](s, int(t.NumColors))
	if s.Len() != 0 {
		log.Printf("Dat_Palette Parse failed %db remaining:\n%s\n%s\n", s.Len(), hex.Dump(s.Bytes()), ToJSONFormat(t, "", "  "))

	}
	return t
}

// //////////////////////////////////////////////////////////////
// Texture 0x06
// //////////////////////////////////////////////////////////////

type Dat_Texture struct {
	Id      uint32 `json:"id"`
	Unknown uint32 `json:"unknown"` // unknownVals:map[uint32]int{0x0:9, 0x1:206, 0x2:86, 0x3:42, 0x4:2049, 0x5:1042, 0x6:13253, 0x7:2243, 0x8:1364, 0x9:304, 0xa:86}
	// unknownVals:map[uint32]int{0x1:2, 0x2:8, 0x4:46, 0x5:476, 0x7:762, 0x8:998, 0x9:2},
	Width            uint32  `json:"width"`
	Height           uint32  `json:"height"`
	Format           uint32  `json:"format"`
	Length           uint32  `json:"length"`
	Data             []byte  `json:"-"`
	DefaultPaletteId *uint32 `json:"default_palette_id,omitempty"`
}

func (t *Dat_Texture) ToPNG() (string, []byte) {
	var src image.Image

	var in bytes.Buffer
	var pos int = 0
	switch t.Format {

	case 20: // PFID_R8G8B8
		for y := int(0); y < int(t.Height); y++ {
			for x := int(0); x < int(t.Width); x++ {
				in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos], 0xFF})
				pos += 3
			}
		}
		src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	case 243: // PFID_CUSTOM_LSCAPE_R8G8B8
		for y := int(0); y < int(t.Height); y++ {
			for x := int(0); x < int(t.Width); x++ {
				in.Write([]byte{t.Data[pos], t.Data[pos+1], t.Data[pos+2], 0xFF})
				pos += 3
			}
		}
		src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	case 21: // PFID_A8R8G8B8
		for y := int(0); y < int(t.Height); y++ {
			for x := int(0); x < int(t.Width); x++ {
				in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
				pos += 4
			}
		}
		src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 23: // PFID_R5G6B5 = 23, // 0x17:3, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+1] << 3, (t.Data[pos] << 5) + (t.Data[pos+1] & 0x03), t.Data[pos] & 0xF8, 0xFF})
	// 			pos += 2
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 26: // PFID_A4R4G4B4 = 26, // 0x1a:2, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+1] & 0xF0, t.Data[pos] << 4, t.Data[pos] & 0xF0, t.Data[pos+1] << 4})
	// 			pos += 2
	// 		}
	// 	}

	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 28: // PFID_A8 = 28, // 0x1c:86, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 41: // PFID_P8 = 41, // 0x29:6, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 101: // PFID_INDEX16 = 101, // 0x65:4182, 1283,
	// 	in.Write(t.Data)
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 2, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 244: // PFID_CUSTOM_LSCAPE_ALPHA = 244, // 0xf4:16, 8,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 500: // PFID_CUSTOM_RAW_JPEG = 500, // 0x1f4:79, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 827611204: // PFID_DXT1 = 827611204, // 0x31545844:1971, 986,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 861165636: // PFID_DXT3 = 861165636, // 0x33545844:5, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 894720068: // PFID_DXT5 = 894720068, // 0x35545844:127, 1,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}

	default:
		return ToJSON(t), []byte{}
	}

	var out bytes.Buffer

	// Write the resulting image.
	err := imgconv.Write(&out, src, &imgconv.FormatOption{Format: imgconv.PNG})
	if err != nil {
		return ToJSON(t), []byte{} // log.Fatalf("failed to convert image: %v", err)
	}
	fileName := fmt.Sprintf("img/0x%08X.png", t.Id)
	f, err := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		log.Fatalf("failed to open %s: %v", fileName, err)
	}
	defer f.Close()

	_, err = f.Write(out.Bytes())
	if err != nil {
		log.Fatalf("failed to write %s: %v", fileName, err)
	}
	return ToJSON(t), out.Bytes()
}
func (t *Dat_Texture) UnPack(s *SBuffer) any {
	t = new(Dat_Texture)
	t.Id = ReadT[uint32](s)
	t.Unknown = ReadT[uint32](s)
	t.Width = ReadT[uint32](s)
	t.Height = ReadT[uint32](s)
	t.Format = ReadT[uint32](s)
	t.Length = ReadT[uint32](s)
	t.Data = ReadTs[byte](s, int(t.Length))
	if t.Format == 101 || t.Format == 41 {
		t.DefaultPaletteId = ReadTP[uint32](s)
	}
	return t
}

// //////////////////////////////////////////////////////////////
// MotionTable 0x09
// //////////////////////////////////////////////////////////////
type MotionTable struct {
	Id            uint32                         `json:"id"`
	DefaultStyle  uint32                         `json:"default_style"`
	StyleDefaults HashMap[*Int32]                `json:"style_defaults,omitempty"`
	Cycles        HashMap[*MotionData]           `json:"cycles"`
	Modifiers     HashMap[*MotionData]           `json:"modifiers"`
	Links         HashMap[*HashMap[*MotionData]] `json:"links"`
}

func (t *MotionTable) UnPack(s *SBuffer) any {
	t = new(MotionTable)
	t.Id = ReadT[uint32](s)
	t.DefaultStyle = ReadT[uint32](s)
	t.StyleDefaults.UnPack(s)
	t.Cycles.UnPack(s)
	t.Modifiers.UnPack(s)
	t.Links.UnPack(s)
	if s.Len() != 0 {
		log.Printf("MotionTable Parse failed %db remaining:\n%s\n%s\n", s.Len(), hex.Dump(s.Bytes()), ToJSONFormat(t, "", "  "))
	}
	return t
}

type MotionData struct {
	NumAnims byte       `json:"num_anims"`
	Bitfield byte       `json:"bitfield"`
	Flags    byte       `json:"flags"`
	Anims    []AnimData `json:"anims"`
	Velocity *Vector3   `json:"velocity,omitempty"`
	Omega    *Vector3   `json:"omega,omitempty"`
}

func (t *MotionData) UnPack(s *SBuffer) any {
	t = new(MotionData)
	t.NumAnims = ReadT[byte](s)
	t.Bitfield = ReadT[byte](s)
	t.Flags = ReadT[byte](s)
	ReadTP[byte](s)
	t.Anims = make([]AnimData, t.NumAnims)
	for i := range t.Anims {
		t.Anims[i].UnPack(s)
	}
	if t.Flags&1 != 0 { // MotionDataFlags.HasVelocity
		t.Velocity.UnPack(s)
	}
	if t.Flags&2 != 0 { // MotionDataFlags.HasOmega
		t.Omega.UnPack(s)
	}
	return t
}

type AnimData struct {
	AnimId    uint32  `json:"anim_id"`
	LowFrame  int32   `json:"low_frame"`
	HighFrame int32   `json:"high_frame"`
	Framerate float32 `json:"framerate"`
}

func (t *AnimData) UnPack(s *SBuffer) any {
	t = new(AnimData)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

// //////////////////////////////////////////////////////////////
// SkillTable 0x0E000004
// //////////////////////////////////////////////////////////////
type Dat_SkillTable struct {
	Id            uint32                  `json:"id"`
	SkillBaseHash HashMap[*Dat_SkillBase] `json:"skill_base_hash"`
}

func (t *Dat_SkillTable) UnPack(s *SBuffer) any {
	t = new(Dat_SkillTable)
	t.Id = ReadT[uint32](s)
	//log.Printf("Read Dat_SkillTable(%d)\n", t.Id)
	t.SkillBaseHash.UnPack(s)
	return t
}

type Dat_SkillBase struct {
	Description     PString          `json:"description"`
	Name            PString          `json:"name"`
	IconId          uint32           `json:"icon_id"`
	TrainedCost     int32            `json:"trained_cost"`
	SpecializedCost int32            `json:"specialized_cost"`
	Category        uint32           `json:"category"`
	ChargenUse      uint32           `json:"chargen_use"`
	MinLevel        uint32           `json:"min_level"`
	Formula         Dat_SkillFormula `json:"formula"`
	UpperBound      float64          `json:"upper_bound"`
	LowerBound      float64          `json:"lower_bound"`
	LearnMod        float64          `json:"learn_mod"`
}

func (t *Dat_SkillBase) UnPack(s *SBuffer) any {
	t = new(Dat_SkillBase)
	t.Description = *t.Description.UnPack(s).(*PString)
	t.Name = *t.Name.UnPack(s).(*PString)
	t.IconId = ReadT[uint32](s)
	t.TrainedCost = ReadT[int32](s)
	t.SpecializedCost = ReadT[int32](s)
	t.Category = ReadT[uint32](s)
	t.ChargenUse = ReadT[uint32](s)
	t.MinLevel = ReadT[uint32](s)
	t.Formula = *t.Formula.UnPack(s).(*Dat_SkillFormula)
	t.UpperBound = ReadT[float64](s)
	t.LowerBound = ReadT[float64](s)
	t.LearnMod = ReadT[float64](s)
	// log.Printf("Dat_SkillBase.UnPack() %s", ToJSON(*t))
	return t
}

type Dat_SkillFormula struct {
	W     uint32      `json:"w"`
	X     uint32      `json:"x"`
	Y     uint32      `json:"y"`
	Z     uint32      `json:"z"`
	Attr1 AttributeID `json:"attr1"`
	Attr2 AttributeID `json:"attr2"`
}

func (t *Dat_SkillFormula) UnPack(s *SBuffer) any {
	t = new(Dat_SkillFormula)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

// //////////////////////////////////////////////////////////////
// SpellTable 0x0E00000E
// //////////////////////////////////////////////////////////////
type Dat_SpellTable struct {
	Id       uint32                            `json:"id"`
	Spells   HashMap[*SpellBase]               `json:"Spells"`
	SpellSet HashMap[*HashMap[*SpellSetTiers]] `json:"SpellSet"`
}

func (t *Dat_SpellTable) UnPack(s *SBuffer) any {
	t = new(Dat_SpellTable)
	t.Id = ReadT[uint32](s)
	t.Spells.UnPack(s)
	t.SpellSet.UnPack(s)
	return t
}

type SpellSetTiers struct {
	Spells []uint32 `json:"Spells"`
}

func (t *SpellSetTiers) UnPack(s *SBuffer) any {
	t = new(SpellSetTiers)
	t.Spells = ReadTs[uint32](s, int(ReadT[uint32](s)&0xFFFF))
	return t
}

// //////////////////////////////////////////////////////////////
// PaletteSet 0x0F
// //////////////////////////////////////////////////////////////
type Dat_PaletteSet struct {
	Id          uint32   `json:"id"`
	NumPalettes uint32   `json:"num_palettes"`
	Palettes    []uint32 `json:"palettes"`
}

func (t *Dat_PaletteSet) UnPack(s *SBuffer) any {
	t = new(Dat_PaletteSet)
	t.Id = ReadT[uint32](s)
	t.NumPalettes = ReadT[uint32](s)
	t.Palettes = ReadTs[uint32](s, int(t.NumPalettes))
	if s.Len() != 0 {
		log.Printf("Dat_PaletteSet Parse failed %db remaining:\n%s\n%s\n", s.Len(), hex.Dump(s.Bytes()), ToJSONFormat(t, "", "  "))
	}
	return t
}
func (t *Dat_PaletteSet) GetPaletteId(shade float64) uint32 {
	if t.NumPalettes == 0 || shade < 0.0 || shade > 1.0 {
		return 0
	}
	var idx int = int((float64(t.NumPalettes) - 0.000001) * shade)
	if idx < 0 {
		idx = 0
	}
	if idx > int(t.NumPalettes-1) {
		idx = int(t.NumPalettes - 1)
	}
	return t.Palettes[idx]
}

// //////////////////////////////////////////////////////////////
// Scene 0x12
// //////////////////////////////////////////////////////////////

type Dat_Scene struct {
	Id      uint32           `json:"id"`
	Objects []Dat_ObjectDesc `json:"objects"`
}

func (t *Dat_Scene) UnPack(s *SBuffer) any {
	t = new(Dat_Scene)
	t.Id = ReadT[uint32](s)
	t.Objects = ReadTs[Dat_ObjectDesc](s, int(ReadT[uint32](s)&0xFFFF))
	if s.Len() != 0 {
		log.Printf("Dat_Scene Parse failed %db remaining:\n%s\n%s\n", s.Len(), hex.Dump(s.Bytes()), ToJSONFormat(t, "", "  "))
	}
	return t
}

type Dat_ObjectDesc struct {
	ObjId       uint32  `json:"obj_id"`
	BaseLoc     Frame   `json:"base_loc"`
	Freq        float32 `json:"freq"`
	DisplaceX   float32 `json:"displace_x"`
	DisplaceY   float32 `json:"displace_y"`
	MinScale    float32 `json:"min_scale"`
	MaxScale    float32 `json:"max_scale"`
	MaxRotation float32 `json:"max_rotation"`
	MinSlope    float32 `json:"min_slope"`
	MaxSlope    float32 `json:"max_slope"`
	Align       uint32  `json:"align"`
	Orient      uint32  `json:"orient"`
	WeenieObj   uint32  `json:"weenie_obj"`
}

func (t *Dat_ObjectDesc) UnPack(s *SBuffer) any {
	t = new(Dat_ObjectDesc)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

// //////////////////////////////////////////////////////////////
// Region 0x13
// //////////////////////////////////////////////////////////////

type Dat_Region struct {
	Id           uint32          `json:"id"`
	RegionNumber uint32          `json:"region_number"`
	Version      uint32          `json:"version"`
	RegionName   PString         `json:"region_name"`
	LandDefs     Dat_LandDefs    `json:"land_defs"`
	GameTime     Dat_GameTime    `json:"game_time"`
	PartsMask    uint32          `json:"parts_mask"`
	SkyInfo      *Dat_SkyDesc    `json:"sky_info,omitempty"`
	SoundInfo    *Dat_SoundDesc  `json:"sound_info,omitempty"`
	SceneInfo    *Dat_SceneDesc  `json:"scene_info,omitempty"`
	TerrainInfo  Dat_TerrainDesc `json:"terrain_info"`
	RegionMisc   *Dat_RegionMisc `json:"region_misc,omitempty"`
}

func (t *Dat_Region) UnPack(s *SBuffer) any {
	t = new(Dat_Region)
	t.Id = ReadT[uint32](s)
	t.RegionNumber = ReadT[uint32](s)
	t.Version = ReadT[uint32](s)
	t.RegionName.UnPack(s)
	t.LandDefs.UnPack(s)
	t.GameTime.UnPack(s)
	t.PartsMask = ReadT[uint32](s)
	if t.PartsMask&0x10 != 0 {
		t.SkyInfo.UnPack(s)
	}
	if t.PartsMask&0x01 != 0 {
		t.SoundInfo.UnPack(s)
	}
	if t.PartsMask&0x02 != 0 {
		t.SceneInfo.UnPack(s)
	}
	t.TerrainInfo.UnPack(s)
	if t.PartsMask&0x200 != 0 {
		t.RegionMisc.UnPack(s)
	}
	if s.Len() != 0 {
		log.Printf("Dat_Region Parse failed %db remaining:\n%s\n%s\n", s.Len(), hex.Dump(s.Bytes()), ToJSONFormat(t, "", "  "))
	}
	return t
}

type Dat_LandDefs struct {
	NumBlockLength  int32        `json:"NumBlockLength"`
	NumBlockWidth   int32        `json:"NumBlockWidth"`
	SquareLength    float32      `json:"SquareLength"`
	LBlockLength    int32        `json:"LBlockLength"`
	VertexPerCell   int32        `json:"VertexPerCell"`
	MaxObjHeight    float32      `json:"MaxObjHeight"`
	SkyHeight       float32      `json:"SkyHeight"`
	RoadWidth       float32      `json:"RoadWidth"`
	LandHeightTable [256]float32 `json:"LandHeightTable"`
}

func (t *Dat_LandDefs) UnPack(s *SBuffer) any {
	t = new(Dat_LandDefs)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_GameTime struct {
	ZeroTimeOfYear float64               `json:"ZeroTimeOfYear"`
	ZeroYear       uint32                `json:"ZeroYear"`
	DayLength      float32               `json:"DayLength"`
	DaysPerYear    uint32                `json:"DaysPerYear"`
	YearSpec       PString               `json:"YearSpec"`
	TimesOfDay     *List[*Dat_TimeOfDay] `json:"TimesOfDay"`
	DaysOfTheWeek  *List[*PString]       `json:"DaysOfTheWeek"`
	Seasons        *List[*Dat_Season]    `json:"Seasons"`
}

func (t *Dat_GameTime) UnPack(s *SBuffer) any {
	t = new(Dat_GameTime)
	t.ZeroTimeOfYear = ReadT[float64](s)
	t.ZeroYear = ReadT[uint32](s)
	t.DayLength = ReadT[float32](s)
	t.DaysPerYear = ReadT[uint32](s)
	t.YearSpec.UnPack(s)
	t.TimesOfDay = new(List[*Dat_TimeOfDay]).UnPack(s).(*List[*Dat_TimeOfDay])
	t.DaysOfTheWeek = new(List[*PString]).UnPack(s).(*List[*PString])
	t.Seasons = new(List[*Dat_Season]).UnPack(s).(*List[*Dat_Season])
	return t
}

type Dat_Season struct {
	StartDate uint32  `json:"StartDate"`
	Name      PString `json:"Name"`
}

func (t *Dat_Season) UnPack(s *SBuffer) any {
	t = new(Dat_Season)
	t.StartDate = ReadT[uint32](s)
	t.Name.UnPack(s)
	return t
}

type Dat_TimeOfDay struct {
	Start   uint32  `json:"Start"`
	IsNight bool    `json:"IsNight"`
	Name    PString `json:"Name"`
}

func (t *Dat_TimeOfDay) UnPack(s *SBuffer) any {
	t = new(Dat_TimeOfDay)
	t.Start = ReadT[uint32](s)
	t.IsNight = ReadT[uint32](s) == 1
	t.Name.UnPack(s)
	return t
}

type Dat_SkyDesc struct {
	TickSize      float64              `json:"TickSize"`
	LightTickSize float64              `json:"LightTickSize"`
	DayGroups     *List[*Dat_DayGroup] `json:"DayGroups"`
}

func (t *Dat_SkyDesc) UnPack(s *SBuffer) any {
	t = new(Dat_SkyDesc)
	t.TickSize = ReadT[float64](s)
	t.LightTickSize = ReadT[float64](s)
	t.DayGroups = new(List[*Dat_DayGroup]).UnPack(s).(*List[*Dat_DayGroup])
	return t
}

type Dat_DayGroup struct {
	ChanceOfOccur float32                  `json:"ChanceOfOccur"`
	DayName       PString                  `json:"DayName"`
	SkyObjects    *List[*Dat_SkyObject]    `json:"SkyObjects"`
	SkyTime       *List[*Dat_SkyTimeOfDay] `json:"SkyTime"`
}

func (t *Dat_DayGroup) UnPack(s *SBuffer) any {
	t = new(Dat_DayGroup)
	t.ChanceOfOccur = ReadT[float32](s)
	t.DayName.UnPack(s)
	t.SkyObjects = new(List[*Dat_SkyObject]).UnPack(s).(*List[*Dat_SkyObject])
	t.SkyTime = new(List[*Dat_SkyTimeOfDay]).UnPack(s).(*List[*Dat_SkyTimeOfDay])
	return t
}

type Dat_SkyObject struct {
	BeginTime          float32 `json:"BeginTime"`
	EndTime            float32 `json:"EndTime"`
	BeginAngle         float32 `json:"BeginAngle"`
	EndAngle           float32 `json:"EndAngle"`
	TexVelocityX       float32 `json:"TexVelocityX"`
	TexVelocityY       float32 `json:"TexVelocityY"`
	DefaultGFXObjectId uint32  `json:"DefaultGFXObjectId"`
	DefaultPESObjectId uint32  `json:"DefaultPESObjectId"`
	Properties         uint32  `json:"Properties"`
}

func (t *Dat_SkyObject) UnPack(s *SBuffer) any {
	t = new(Dat_SkyObject)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_SkyTimeOfDay struct {
	Begin         float32                      `json:"Begin"`
	DirBright     float32                      `json:"DirBright"`
	DirHeading    float32                      `json:"DirHeading"`
	DirPitch      float32                      `json:"DirPitch"`
	DirColor      uint32                       `json:"DirColor"`
	AmbBright     float32                      `json:"AmbBright"`
	AmbColor      uint32                       `json:"AmbColor"`
	MinWorldFog   float32                      `json:"MinWorldFog"`
	MaxWorldFog   float32                      `json:"MaxWorldFog"`
	WorldFogColor uint32                       `json:"WorldFogColor"`
	WorldFog      uint32                       `json:"WorldFog"`
	SkyObjReplace *List[*Dat_SkyObjectReplace] `json:"SkyObjReplace"`
}

func (t *Dat_SkyTimeOfDay) UnPack(s *SBuffer) any {
	t = new(Dat_SkyTimeOfDay)
	t.Begin = ReadT[float32](s)
	t.DirBright = ReadT[float32](s)
	t.DirHeading = ReadT[float32](s)
	t.DirPitch = ReadT[float32](s)
	t.DirColor = ReadT[uint32](s)
	t.AmbBright = ReadT[float32](s)
	t.AmbColor = ReadT[uint32](s)
	t.MinWorldFog = ReadT[float32](s)
	t.MaxWorldFog = ReadT[float32](s)
	t.WorldFogColor = ReadT[uint32](s)
	t.WorldFog = ReadT[uint32](s)
	t.SkyObjReplace = new(List[*Dat_SkyObjectReplace]).UnPack(s).(*List[*Dat_SkyObjectReplace])
	return t
}

type Dat_SkyObjectReplace struct {
	ObjectIndex uint32  `json:"ObjectIndex"`
	GFXObjId    uint32  `json:"GFXObjId"`
	Rotate      float32 `json:"Rotate"`
	Transparent float32 `json:"Transparent"`
	Luminosity  float32 `json:"Luminosity"`
	MaxBright   float32 `json:"MaxBright"`
}

func (t *Dat_SkyObjectReplace) UnPack(s *SBuffer) any {
	t = new(Dat_SkyObjectReplace)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_SoundDesc struct {
	STBDesc *List[*Dat_AmbientSTBDesc] `json:"STBDesc"`
}

func (t *Dat_SoundDesc) UnPack(s *SBuffer) any {
	t = new(Dat_SoundDesc)
	t.STBDesc = new(List[*Dat_AmbientSTBDesc]).UnPack(s).(*List[*Dat_AmbientSTBDesc])
	return t
}

type Dat_AmbientSTBDesc struct {
	STBId         uint32                       `json:"STBId"`
	AmbientSounds *List[*Dat_AmbientSoundDesc] `json:"AmbientSounds"`
}

func (t *Dat_AmbientSTBDesc) UnPack(s *SBuffer) any {
	t = new(Dat_AmbientSTBDesc)
	t.STBId = ReadT[uint32](s)
	t.AmbientSounds = new(List[*Dat_AmbientSoundDesc]).UnPack(s).(*List[*Dat_AmbientSoundDesc])
	return t
}

type Dat_AmbientSoundDesc struct {
	SType      uint32  `json:"SType"`
	Volume     float32 `json:"Volume"`
	BaseChance float32 `json:"BaseChance"`
	MinRate    float32 `json:"MinRate"`
	MaxRate    float32 `json:"MaxRate"`
}

func (t *Dat_AmbientSoundDesc) UnPack(s *SBuffer) any {
	t = new(Dat_AmbientSoundDesc)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_SceneDesc struct {
	SceneTypes *List[*Dat_SceneType] `json:"SceneTypes"`
}

func (t *Dat_SceneDesc) UnPack(s *SBuffer) any {
	t = new(Dat_SceneDesc)
	t.SceneTypes = new(List[*Dat_SceneType]).UnPack(s).(*List[*Dat_SceneType])
	return t
}

type Dat_SceneType struct {
	StbIndex uint32   `json:"StbIndex"`
	Scenes   []uint32 `json:"Scenes"`
}

func (t *Dat_SceneType) UnPack(s *SBuffer) any {
	t = new(Dat_SceneType)
	t.StbIndex = ReadT[uint32](s)
	t.Scenes = ReadTs[uint32](s, int(ReadT[uint32](s)))
	return t
}

type Dat_TerrainDesc struct {
	TerrainTypes *List[*Dat_TerrainType] `json:"TerrainTypes"`
	LandSurfaces Dat_LandSurf            `json:"LandSurfaces"`
}

func (t *Dat_TerrainDesc) UnPack(s *SBuffer) any {
	t = new(Dat_TerrainDesc)
	t.TerrainTypes = new(List[*Dat_TerrainType]).UnPack(s).(*List[*Dat_TerrainType])
	t.LandSurfaces.UnPack(s)
	return t
}

type Dat_TerrainType struct {
	TerrainName  PString  `json:"TerrainName"`
	TerrainColor uint32   `json:"TerrainColor"`
	SceneTypes   []uint32 `json:"SceneTypes"`
}

func (t *Dat_TerrainType) UnPack(s *SBuffer) any {
	t = new(Dat_TerrainType)
	t.TerrainName.UnPack(s)
	t.TerrainColor = ReadT[uint32](s)
	t.SceneTypes = ReadTs[uint32](s, int(ReadT[uint32](s)))
	return t
}

type Dat_LandSurf struct {
	Type     uint32       `json:"Type"`
	TexMerge Dat_TexMerge `json:"TexMerge"`
}

func (t *Dat_LandSurf) UnPack(s *SBuffer) any {
	t = new(Dat_LandSurf)
	t.Type = ReadT[uint32](s)
	t.TexMerge.UnPack(s)
	return t
}

type Dat_TexMerge struct {
	BaseTexSize       uint32                      `json:"BaseTexSize"`
	CornerTerrainMaps *List[*Dat_TerrainAlphaMap] `json:"CornerTerrainMaps"`
	SideTerrainMaps   *List[*Dat_TerrainAlphaMap] `json:"SideTerrainMaps"`
	RoadMaps          *List[*Dat_RoadAlphaMap]    `json:"RoadMaps"`
	TerrainDesc       *List[*Dat_TMTerrainDesc]   `json:"TerrainDesc"`
}

func (t *Dat_TexMerge) UnPack(s *SBuffer) any {
	t = new(Dat_TexMerge)
	t.BaseTexSize = ReadT[uint32](s)
	t.CornerTerrainMaps = new(List[*Dat_TerrainAlphaMap]).UnPack(s).(*List[*Dat_TerrainAlphaMap])
	t.SideTerrainMaps = new(List[*Dat_TerrainAlphaMap]).UnPack(s).(*List[*Dat_TerrainAlphaMap])
	t.RoadMaps = new(List[*Dat_RoadAlphaMap]).UnPack(s).(*List[*Dat_RoadAlphaMap])
	t.TerrainDesc = new(List[*Dat_TMTerrainDesc]).UnPack(s).(*List[*Dat_TMTerrainDesc])
	return t
}

type Dat_TMTerrainDesc struct {
	TerrainType uint32         `json:"TerrainType"`
	TerrainTex  Dat_TerrainTex `json:"TerrainTex"`
}

func (t *Dat_TMTerrainDesc) UnPack(s *SBuffer) any {
	t = new(Dat_TMTerrainDesc)
	t.TerrainType = ReadT[uint32](s)
	t.TerrainTex.UnPack(s)
	return t
}

type Dat_TerrainTex struct {
	TexGID          uint32 `json:"TexGID"`
	TexTiling       uint32 `json:"TexTiling"`
	MaxVertBright   uint32 `json:"MaxVertBright"`
	MinVertBright   uint32 `json:"MinVertBright"`
	MaxVertSaturate uint32 `json:"MaxVertSaturate"`
	MinVertSaturate uint32 `json:"MinVertSaturate"`
	MaxVertHue      uint32 `json:"MaxVertHue"`
	MinVertHue      uint32 `json:"MinVertHue"`
	DetailTexTiling uint32 `json:"DetailTexTiling"`
	DetailTexGID    uint32 `json:"DetailTexGID"`
}

func (t *Dat_TerrainTex) UnPack(s *SBuffer) any {
	t = new(Dat_TerrainTex)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_RoadAlphaMap struct {
	RCode      uint32 `json:"RCode"`
	RoadTexGID uint32 `json:"RoadTexGID"`
}

func (t *Dat_RoadAlphaMap) UnPack(s *SBuffer) any {
	t = new(Dat_RoadAlphaMap)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_TerrainAlphaMap struct {
	TCode  uint32 `json:"TCode"`
	TexGID uint32 `json:"TexGID"`
}

func (t *Dat_TerrainAlphaMap) UnPack(s *SBuffer) any {
	t = new(Dat_TerrainAlphaMap)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_RegionMisc struct {
	Version         uint32 `json:"Version"`
	GameMapID       uint32 `json:"GameMapID"`
	AutotestMapId   uint32 `json:"AutotestMapId"`
	AutotestMapSize uint32 `json:"AutotestMapSize"`
	ClearCellId     uint32 `json:"ClearCellId"`
	ClearMonsterId  uint32 `json:"ClearMonsterId"`
}

func (t *Dat_RegionMisc) UnPack(s *SBuffer) any {
	t = new(Dat_RegionMisc)
	binary.Read(s, binary.LittleEndian, t)
	return t
}
