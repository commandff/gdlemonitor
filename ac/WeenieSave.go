// //////////////////////////////////////////////////////////////
//
//          CWeenieSave - GDLE Weenie Save parser
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"encoding/binary"
	"log"
)

type CWeenieSave struct {
	SaveVersion    uint32                   `json:"save_version"`
	SaveTimestamp  uint32                   `json:"save_timestamp"`
	SaveInstanceTS uint32                   `json:"save_instance_timestamp"`
	Qualities      CACQualities             `json:"qualities"`
	ObjDesc        ObjDesc                  `json:"obj_desc"`
	WornObjDesc    ObjDesc                  `json:"WornObjDesc"`
	PlayerModule   *PlayerModule            `json:"PlayerModule,omitempty"`
	Equipment      *List[*Uint32]           `json:"equipment,omitempty"`
	Inventory      *List[*Uint32]           `json:"inventory,omitempty"`
	Packs          *List[*Uint32]           `json:"packs,omitempty"`
	QuestTable     *HashMapS[*QuestProfile] `json:"questTable,omitempty"`
}

func (t *CWeenieSave) UnPack(s *SBuffer) any {
	//var s *SBuffer = &SBuffer{Buffer: (bytes.NewBuffer(buf))}
	t = new(CWeenieSave)
	t.SaveVersion = ReadT[uint32](s)
	t.SaveTimestamp = ReadT[uint32](s)
	t.SaveInstanceTS = ReadT[uint32](s)
	t.Qualities = *new(CACQualities).UnPack(s).(*CACQualities)
	t.ObjDesc = *new(ObjDesc).UnPack(s).(*ObjDesc)
	if t.SaveVersion < 3 {
		ReadTs[uint32](s, 3)
	}
	t.WornObjDesc = *new(ObjDesc).UnPack(s).(*ObjDesc)
	// log.Printf("CWeenieSave:  %s", ToJSON(t))
	if t.SaveVersion < 2 {
		return t
	}
	fields := ReadT[uint32](s)
	// log.Printf("CWeenieSave: fields:%08x %s", fields, ToJSON(t))
	if fields&1 != 0 {
		t.PlayerModule = new(PlayerModule).UnPack(s).(*PlayerModule)
		// log.Printf("PlayerModule: %s", ToJSONFormat(t.PlayerModule, "", "  "))
	}
	if fields&2 != 0 {
		t.Equipment = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		// log.Printf("Equipment: %s", ToJSONFormat(t.Equipment, "", "  "))
		t.Inventory = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		// log.Printf("Inventory: %s", ToJSONFormat(t.Inventory, "", "  "))
		t.Packs = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		// log.Printf("Packs: %s", ToJSONFormat(t.Packs, "", "  "))
	}
	if fields&4 != 0 {
		t.QuestTable = new(HashMapS[*QuestProfile]).UnPack(s).(*HashMapS[*QuestProfile])
		// log.Printf("QuestTable: %s", ToJSONFormat(t.QuestTable, "", "  "))
	}
	return t
}

type ObjDesc struct {
	PaletteID   *DataIDOfKnownType       `json:"paletteID"`
	Subpalettes *List[*Subpalette]       `json:"subpalettes"`
	TMChanges   *List[*TextureMapChange] `json:"TMChanges"`
	APChanges   *List[*AnimPartChange]   `json:"APChanges"`
}

func (t *ObjDesc) UnPack(s *SBuffer) any {
	t = new(ObjDesc)
	startLen := s.Len()
	numEleven := ReadT[byte](s)
	if numEleven != 0x11 {
		return t
	}
	numSubpalettes := int(ReadT[byte](s))
	numTMCs := int(ReadT[byte](s))
	numAPCs := int(ReadT[byte](s))
	if numSubpalettes > 0 {
		t.PaletteID = new(DataIDOfKnownType).UnPack(s, 0x04000000).(*DataIDOfKnownType)
		t.Subpalettes = new(List[*Subpalette]).UnPackFixed(s, numSubpalettes).(*List[*Subpalette])
	}
	t.TMChanges = new(List[*TextureMapChange]).UnPackFixed(s, numTMCs).(*List[*TextureMapChange])
	t.APChanges = new(List[*AnimPartChange]).UnPackFixed(s, numAPCs).(*List[*AnimPartChange])
	align := (startLen - s.Len()) & 3
	if align > 0 {
		ReadTs[byte](s, (4 - align))
	}
	// log.Printf("ObjDesc: AL:%d %s", align, ToJSONFormat(t, "", "  "))

	return t
}

type PublicWeenieDesc struct {
	Name               PString
	Wcid               WClassIDCompressed
	IconID             DataIDOfKnownType
	Type               uint32
	Bitfield           uint32
	SecondHeader       uint32
	PluralName         PString
	ItemsCapacity      byte
	ContainersCapacity byte
	AmmoType           uint16
	Value              uint32
	Useability         uint32
	UseRadius          float32
	TargetType         uint32
	Effects            uint32
	CombatUse          byte
	IconOverlayID      DataIDOfKnownType
	IconUnderlayID     DataIDOfKnownType
	ContainerID        uint32
	WielderID          uint32
	Priority           uint32
	ValidLocations     uint32
	Location           uint32
	Structure          uint16
	MaxStructure       uint16
	StackSize          uint16
	MaxStackSize       uint16
	BlipColor          byte
	RadarEnum          byte
	Burden             uint16
	SpellID            uint16
	HouseOwnerIID      uint32
	Db                 *RestrictionDB
	Pscript            uint16
	HookType           uint16
	HookItemTypes      uint32
	Monarch            uint32
	MaterialType       uint32
	Workmanship        float32
	CooldownID         uint32
	CooldownDuration   float64
	PetOwner           uint32
}

func (t *PublicWeenieDesc) UnPack(s *SBuffer) any {
	t = new(PublicWeenieDesc)
	startLen := s.Len()
	header := ReadT[uint32](s)
	t.Name.UnPack(s)
	t.Wcid.UnPack(s)
	t.IconID.UnPack(s, 0x06000000)
	t.Type = ReadT[uint32](s)
	t.Bitfield = ReadT[uint32](s)

	align := (startLen - s.Len()) & 3
	if align > 0 {
		ReadTs[byte](s, (4 - align))
	}
	if t.Bitfield&0x4000000 != 0 { // BF_INCLUDES_SECOND_HEADER
		t.SecondHeader = ReadT[uint32](s)
	}
	if header&0x00000001 != 0 {
		t.PluralName.UnPack(s)
	}
	if header&0x00000002 != 0 {
		t.ItemsCapacity = ReadT[byte](s)
	}
	if header&0x00000004 != 0 {
		t.ContainersCapacity = ReadT[byte](s)
	}
	if header&0x00000100 != 0 {
		t.AmmoType = ReadT[uint16](s)
	}
	if header&0x00000008 != 0 {
		t.Value = ReadT[uint32](s)
	}
	if header&0x00000010 != 0 {
		t.Useability = ReadT[uint32](s)
	}
	if header&0x00000020 != 0 {
		t.UseRadius = ReadT[float32](s)
	}
	if header&0x00080000 != 0 {
		t.TargetType = ReadT[uint32](s)
	}
	if header&0x00000080 != 0 {
		t.Effects = ReadT[uint32](s)
	}
	if header&0x00000200 != 0 {
		t.CombatUse = ReadT[byte](s)
	}
	if header&0x00000400 != 0 {
		t.Structure = ReadT[uint16](s)
	}
	if header&0x00000800 != 0 {
		t.MaxStructure = ReadT[uint16](s)
	}
	if header&0x00001000 != 0 {
		t.StackSize = ReadT[uint16](s)
	}
	if header&0x00002000 != 0 {
		t.MaxStackSize = ReadT[uint16](s)
	}
	if header&0x00004000 != 0 {
		t.ContainerID = ReadT[uint32](s)
	}
	if header&0x00008000 != 0 {
		t.WielderID = ReadT[uint32](s)
	}
	if header&0x00010000 != 0 {
		t.ValidLocations = ReadT[uint32](s)
	}
	if header&0x00020000 != 0 {
		t.Location = ReadT[uint32](s)
	}
	if header&0x00040000 != 0 {
		t.Priority = ReadT[uint32](s)
	}
	if header&0x00100000 != 0 {
		t.BlipColor = ReadT[byte](s)
	}
	if header&0x00800000 != 0 {
		t.RadarEnum = ReadT[byte](s)
	}
	if header&0x08000000 != 0 {
		t.Pscript = ReadT[uint16](s)
	}
	if header&0x01000000 != 0 {
		t.Workmanship = ReadT[float32](s)
	}
	if header&0x00200000 != 0 {
		t.Burden = ReadT[uint16](s)
	}
	if header&0x00400000 != 0 {
		t.SpellID = ReadT[uint16](s)
	}
	if header&0x02000000 != 0 {
		t.HouseOwnerIID = ReadT[uint32](s)
	}
	if header&0x04000000 != 0 {
		t.Db = new(RestrictionDB).UnPack(s).(*RestrictionDB)
	}
	if header&0x20000000 != 0 {
		t.HookItemTypes = ReadT[uint32](s)
	}
	if header&0x00000040 != 0 {
		t.Monarch = ReadT[uint32](s)
	}
	if header&0x10000000 != 0 {
		t.HookType = ReadT[uint16](s)
	}
	if header&0x40000000 != 0 {
		t.IconOverlayID.UnPack(s, 0x06000000)
	}
	if t.SecondHeader&0x00000001 != 0 {
		t.IconUnderlayID.UnPack(s, 0x06000000)
	}
	if header&0x80000000 != 0 {
		t.MaterialType = ReadT[uint32](s)
	}
	if t.SecondHeader&0x00000002 != 0 {
		t.CooldownID = ReadT[uint32](s)
	}
	if t.SecondHeader&0x00000004 != 0 {
		t.CooldownDuration = ReadT[float64](s)
	}
	if t.SecondHeader&0x00000008 != 0 {
		t.PetOwner = ReadT[uint32](s)
	}
	align = (startLen - s.Len()) & 3
	if align > 0 {
		ReadTs[byte](s, (4 - align))
	}
	return t
}

type RestrictionDB struct {
	Version    uint32
	Bitmask    uint32
	MonarchIID uint32
	Table      HashMap[*Uint32]
}

func (t *RestrictionDB) UnPack(s *SBuffer) any {
	t = new(RestrictionDB)
	t.Version = ReadT[uint32](s)
	t.Bitmask = ReadT[uint32](s)
	t.MonarchIID = ReadT[uint32](s)
	t.Table.UnPack(s)
	return t
}

type PhysicsDesc struct {
	Bitfield               uint32
	State                  uint32
	MovementBuffer         []byte
	AutonomousMovement     int32
	AnimframeId            uint32
	Pos                    Position
	ObjectScale            float32
	Friction               float32
	Elasticity             float32
	Translucency           float32
	Velocity               Vector3
	Acceleration           Vector3
	Omega                  Vector3
	NumChildren            uint32
	Children               HashMap[*Uint32]
	ParentId               uint32
	LocationId             uint32
	MTableId               uint32
	STableId               uint32
	PhsTableId             uint32
	DefaultScript          PScriptType
	DefaultScriptIntensity float32
	SetupID                uint32
	Timestamps             []uint16
}

func (t *PhysicsDesc) UnPack(s *SBuffer) any {
	t = new(PhysicsDesc)
	startLen := s.Len()
	t.Bitfield = ReadT[uint32](s)
	t.State = ReadT[uint32](s)
	if t.Bitfield&0x00010000 != 0 { // MOVEMENT = 0x10000
		t.MovementBuffer = ReadTs[byte](s, int(ReadT[uint32](s)))
		t.AutonomousMovement = ReadT[int32](s)
	} else if t.Bitfield&0x00020000 != 0 { // ANIMFRAME_ID = 0x20000
		t.AnimframeId = ReadT[uint32](s)
	}
	if t.Bitfield&0x0008000 != 0 { // POSITION = 0x8000
		t.Pos.UnPack(s)
	}
	if t.Bitfield&0x00000002 != 0 { // MTABLE = 0x2
		t.MTableId = ReadT[uint32](s)
	}
	if t.Bitfield&0x00000800 != 0 { // STABLE = 0x800
		t.STableId = ReadT[uint32](s)
	}
	if t.Bitfield&0x00001000 != 0 { // PETABLE = 0x1000
		t.PhsTableId = ReadT[uint32](s)
	}
	if t.Bitfield&0x00000001 != 0 { // CSetup = 0x1
		t.SetupID = ReadT[uint32](s)
	}
	if t.Bitfield&0x00000020 != 0 { // PARENT = 0x20
		t.ParentId = ReadT[uint32](s)
		t.LocationId = ReadT[uint32](s)
	}
	if t.Bitfield&0x00000040 != 0 { // CHILDREN = 0x40
		t.Children.UnPack(s)
	}
	if t.Bitfield&0x00000080 != 0 { // OBJSCALE = 0x80
		t.ObjectScale = ReadT[float32](s)
	}
	if t.Bitfield&0x00000100 != 0 { // FRICTION = 0x100
		t.Friction = ReadT[float32](s)
	}
	if t.Bitfield&0x00000200 != 0 { // ELASTICITY = 0x200
		t.Elasticity = ReadT[float32](s)
	}
	if t.Bitfield&0x00040000 != 0 { // TRANSLUCENCY = 0x40000
		t.Translucency = ReadT[float32](s)
	}
	if t.Bitfield&0x00000004 != 0 { // VELOCITY = 0x4
		t.Velocity.UnPack(s)
	}
	if t.Bitfield&0x00000008 != 0 { // ACCELERATION = 0x8
		t.Acceleration.UnPack(s)
	}
	if t.Bitfield&0x00000010 != 0 { // OMEGA = 0x10
		t.Omega.UnPack(s)
	}
	if t.Bitfield&0x00002000 != 0 { // DEFAULT_SCRIPT = 0x2000
		t.DefaultScript = ReadT[PScriptType](s)
	}
	if t.Bitfield&0x00004000 != 0 { // DEFAULT_SCRIPT_INTENSITY = 0x4000
		t.DefaultScriptIntensity = ReadT[float32](s)
	}
	t.Timestamps = ReadTs[uint16](s, 9) // TIMESTAMPS = 0x400 - unused or always set
	// POSITION: t.Timestamps[0]
	// MOVEMENT, STATE, VECTOR, TELEPORT, SERVER_CONTROLLED_MOVE, FORCE_POSITION, OBJDESC
	// INSTANCE: t.Timestamps[8]
	align := (startLen - s.Len()) & 3
	if align > 0 {
		ReadTs[byte](s, (4 - align))
	}
	return t
}

type PlayerModule struct {
	ShortCuts         *List[*ShortCutData] `json:"shortcuts"`
	FavoriteSpells    [8]*List[*Uint32]    `json:"favorite_spells"`
	DesiredComps      *HashMap[*Uint32]    `json:"desired_comps"`
	Options           uint32               `json:"options"`
	Options2          uint32               `json:"options2"`
	SpellFilters      uint32               `json:"spell_filters"`
	PlayerOptionsData GenericQualitiesData `json:"player_options_data"`
	TimeStampFormat   PString              `json:"TimeStampFormat"`
	// WindowData        []byte               `json:"windowData"`
}

func (t *PlayerModule) UnPack(s *SBuffer) any {
	header := ReadT[uint32](s)
	t.Options = ReadT[uint32](s)
	if header&1 != 0 {
		t.ShortCuts = new(List[*ShortCutData]).UnPack(s).(*List[*ShortCutData])
	}
	t.FavoriteSpells[0] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
	if header&0x04 != 0 {
		t.FavoriteSpells[1] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[2] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[3] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[4] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
	} else if header&0x10 != 0 {
		t.FavoriteSpells[1] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[2] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[3] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[4] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[5] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[6] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
	} else if header&0x400 != 0 {
		t.FavoriteSpells[1] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[2] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[3] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[4] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[5] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[6] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		t.FavoriteSpells[7] = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
	}
	// log.Printf("PlayerModule.UnPack reserving %d for FavoriteSpells %08x", len(t.FavoriteSpells), header)

	// log.Printf("PlayerModule.UnPack didn't crash")
	if header&0x08 != 0 {
		t.DesiredComps = new(HashMap[*Uint32]).UnPack(s).(*HashMap[*Uint32])
		log.Printf("PlayerModule.UnPack DesiredComps: %s", ToJSON(t.DesiredComps))
	}

	if header&0x20 != 0 {
		t.SpellFilters = ReadT[uint32](s)
	} else {
		t.SpellFilters = 0x3FFF
	}

	if header&0x40 != 0 {
		t.Options2 = ReadT[uint32](s)
	} else {
		t.Options2 = 0x948700
	}

	if header&0x80 != 0 {
		t.TimeStampFormat.UnPack(s)
	}
	if header&0x100 != 0 {
		t.PlayerOptionsData.UnPack(s)
	}
	// if header&0x200 != 0 {
	// 	t.WindowData = s.readBytes(0)
	// }
	return t

}

type GenericQualitiesData struct {
	IntStats    *HashMap[*Int32]   `json:"intStats,omitempty"`    // STypeInt
	BoolStats   *HashMap[*Bool4]   `json:"boolStats,omitempty"`   // STypeBool
	FloatStats  *HashMap[*Float]   `json:"floatStats,omitempty"`  // STypeFloat
	StringStats *HashMap[*PString] `json:"stringStats,omitempty"` // STypeString
}

func (t *GenericQualitiesData) UnPack(s *SBuffer) any {
	header := ReadT[uint32](s)
	if header&0x01 != 0 {
		t.IntStats = new(HashMap[*Int32]).UnPack(s).(*HashMap[*Int32])
	}
	if header&0x02 != 0 {
		t.BoolStats = new(HashMap[*Bool4]).UnPack(s).(*HashMap[*Bool4])
	}
	if header&0x04 != 0 {
		t.FloatStats = new(HashMap[*Float]).UnPack(s).(*HashMap[*Float])
	}
	if header&0x08 != 0 {
		t.StringStats = new(HashMap[*PString]).UnPack(s).(*HashMap[*PString])
	}
	return t
}

type ShortCutData struct {
	Index    uint32 `json:"index"`
	ObjectID uint32 `json:"objectID"`
	SpellID  uint32 `json:"spellID"`
}

func (t *ShortCutData) UnPack(s *SBuffer) any {
	t = new(ShortCutData)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

// type QuestTableEntry struct {
// 	Key   PString  `json:"key"`
// 	Value QuestProfile `json:"value"`
// }

// func (t *QuestTableEntry) UnPack(s *SBuffer) any {
// 	binary.Read(s, binary.LittleEndian, t)
// 	return t
// }

type QuestProfile struct {
	Last_update     float64 `json:"last_update"`
	Real_time       int32   `json:"real_time"`
	Num_completions uint32  `json:"num_completions"`
}

func (t *QuestProfile) UnPack(s *SBuffer) any {
	t = new(QuestProfile)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type CACQualities struct {
	CBaseQualities
	WCID              uint32                          `json:"wcid"`
	Attributes        *AttributeCache                 `json:"attributes,omitempty"`
	Skills            *HashMap[*Skill]                `json:"skills,omitempty"` // STypeSkill
	Body              *Body                           `json:"body,omitempty"`
	SpellBook         *HashMap[*SpellBook]            `json:"spellbook,omitempty"`
	Enchantments      *CEnchantmentRegistry           `json:"enchantments,omitempty"`
	EventFilter       *List[*Uint32]                  `json:"events,omitempty"`
	EmoteTable        *HashMap[*List[*EmoteCategory]] `json:"emoteTable,omitempty"`
	CreateList        *List[*CreationProfile]         `json:"createList,omitempty"`
	PageDataList      *PageDataList                   `json:"pageDataList,omitempty"`
	GeneratorTable    *List[*Generator]               `json:"generatorTable,omitempty"`
	GeneratorRegistry *List[*GeneratorRegistry]       `json:"generatorRegistry,omitempty"`
	GeneratorQueue    *List[*GeneratorQueue]          `json:"generatorQueue,omitempty"`
}

func (t *CACQualities) UnPack(s *SBuffer) any {
	t = new(CACQualities)
	t.CBaseQualities.UnPack(s)
	contentFlags := ReadT[uint32](s)
	t.WCID = ReadT[uint32](s)
	if contentFlags&0x0001 != 0 {
		t.Attributes = new(AttributeCache).UnPack(s).(*AttributeCache)
		// log.Printf("Attributes: %s", ToJSONFormat(t.Attributes, "", "  "))
	}
	if contentFlags&0x0002 != 0 {
		t.Skills = new(HashMap[*Skill]).UnPack(s).(*HashMap[*Skill])
		// log.Printf("Skills: %s", ToJSONFormat(t.Skills, "", "  "))
	}
	if contentFlags&0x0004 != 0 {
		t.Body = new(Body).UnPack(s).(*Body)
		// log.Printf("Body: %s", ToJSONFormat(t.Body, "", "  "))
	}
	if contentFlags&0x0100 != 0 {
		t.SpellBook = new(HashMap[*SpellBook]).UnPack(s).(*HashMap[*SpellBook])
		// log.Printf("SpellBook: %s", ToJSONFormat(t.SpellBook, "", "  "))
	}
	if contentFlags&0x0200 != 0 {
		t.Enchantments = new(CEnchantmentRegistry).UnPack(s).(*CEnchantmentRegistry)
		// log.Printf("Enchantments: %s", ToJSONFormat(t.Enchantments, "", "  "))
	}
	if contentFlags&0x0008 != 0 {
		t.EventFilter = new(List[*Uint32]).UnPack(s).(*List[*Uint32])
		// log.Printf("EventFilter: %s", ToJSONFormat(t.EventFilter, "", "  "))

	}
	if contentFlags&0x0010 != 0 {
		t.EmoteTable = new(HashMap[*List[*EmoteCategory]]).UnPack(s).(*HashMap[*List[*EmoteCategory]])
		// log.Printf("EmoteTable: %s", ToJSONFormat(t.EmoteTable, "", "  "))
	}
	if contentFlags&0x0020 != 0 {
		t.CreateList = new(List[*CreationProfile]).UnPack(s).(*List[*CreationProfile])
		// log.Printf("CreateList: %s", ToJSONFormat(t.CreateList, "", "  "))
	}
	if contentFlags&0x0040 != 0 {
		t.PageDataList = new(PageDataList).UnPack(s).(*PageDataList)
		// log.Printf("PageDataList: %s", ToJSONFormat(t.PageDataList, "", "  "))
	}
	if contentFlags&0x0080 != 0 {
		t.GeneratorTable = new(List[*Generator]).UnPack(s).(*List[*Generator])
		// log.Printf("GeneratorTable: %s", ToJSONFormat(t.GeneratorTable, "", "  "))
	}
	if contentFlags&0x0400 != 0 {
		t.GeneratorRegistry = new(List[*GeneratorRegistry]).UnPack(s).(*List[*GeneratorRegistry])
		// log.Printf("GeneratorRegistry: %s", ToJSONFormat(t.GeneratorRegistry, "", "  "))
	}
	if contentFlags&0x0800 != 0 {
		t.GeneratorQueue = new(List[*GeneratorQueue]).UnPack(s).(*List[*GeneratorQueue])
		// log.Printf("GeneratorQueue: %s", ToJSONFormat(t.GeneratorQueue, "", "  "))
	}
	return t
}

type SpellBook struct {
	CastingLikelihood float32 `json:"casting_likelihood"`
}

func (t *SpellBook) UnPack(s *SBuffer) any {
	t = new(SpellBook)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type PageDataList struct {
	MaxNumPages        int32           `json:"maxNumPages"`
	MaxNumCharsPerPage int32           `json:"maxNumCharsPerPage"`
	Pages              List[*PageData] `json:"pages"`
}

func (t *PageDataList) UnPack(s *SBuffer) any {
	t = new(PageDataList)
	t.MaxNumPages = ReadT[int32](s)
	t.MaxNumCharsPerPage = ReadT[int32](s)
	t.Pages.UnPack(s)
	return t
}

type PageData struct {
	AuthorID      uint32  `json:"authorID"`
	AuthorName    PString `json:"authorName"`
	AuthorAccount PString `json:"authorAccount"`
	Version       uint32  `json:"version"`
	TextIncluded  int32   `json:"textIncluded"`
	IgnoreAuthor  int32   `json:"ignoreAuthor"`
	PageText      PString `json:"pageText"`
}

func (t *PageData) UnPack(s *SBuffer) any {
	// log.Printf("PageData.UnPack(): %s", hex.Dump(s.Bytes()[0:]))
	t = new(PageData)
	t.AuthorID = ReadT[uint32](s)
	t.AuthorName.UnPack(s)
	t.AuthorAccount.UnPack(s)
	t.Version = ReadT[uint32](s)
	switch t.Version {
	case 0xFFFF0002:
		t.TextIncluded = ReadT[int32](s)
		t.IgnoreAuthor = ReadT[int32](s)
	default:
		t.TextIncluded = int32(t.Version)
		t.IgnoreAuthor = 0
	}
	if t.TextIncluded == 1 {
		t.PageText.UnPack(s)
	} else {
		t.PageText = PString("")
	}
	return t
}

type GeneratorQueue struct {
	Slot uint32  `json:"slot"`
	When float64 `json:"when"`
}

func (t *GeneratorQueue) UnPack(s *SBuffer) any {
	t = new(GeneratorQueue)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type CEnchantmentRegistry struct {
	MultList     *List[*Enchantment] `json:"mult_list,omitempty"`
	AddList      *List[*Enchantment] `json:"add_list,omitempty"`
	CooldownList *List[*Enchantment] `json:"cooldown_list,omitempty"`
	Vitae        *Enchantment        `json:"vitae,omitempty"`
}

func (t *CEnchantmentRegistry) UnPack(s *SBuffer) any {
	t = new(CEnchantmentRegistry)
	header := ReadT[uint32](s)
	if header&1 != 0 {
		t.MultList = new(List[*Enchantment]).UnPack(s).(*List[*Enchantment])
	}
	if header&2 != 0 {
		t.AddList = new(List[*Enchantment]).UnPack(s).(*List[*Enchantment])
	}
	if header&8 != 0 {
		t.CooldownList = new(List[*Enchantment]).UnPack(s).(*List[*Enchantment])
	}
	if header&4 != 0 {
		t.Vitae = new(Enchantment).UnPack(s).(*Enchantment)
	}
	return t
}

type Enchantment struct {
	Id               uint32  `json:"id"`
	SpellCategory    uint32  `json:"spell_category"`
	PowerLevel       int32   `json:"power_level"`
	StartTime        float64 `json:"start_time"`
	Duration         float64 `json:"duration"`
	Caster           uint32  `json:"caster"`
	DegradeModifier  float32 `json:"degrade_modifier"`
	DegradeLimit     float32 `json:"degrade_limit"`
	LastTimeDegraded float64 `json:"last_time_degraded"`
	SMod             StatMod `json:"smod"`
	SpellSetID       *uint32 `json:"spellset_id"` // present if SpellCategory&0xFFFF0000 != 0
}

func (t *Enchantment) UnPack(s *SBuffer) any {
	t = new(Enchantment)
	t.Id = ReadT[uint32](s)
	t.SpellCategory = ReadT[uint32](s)
	t.PowerLevel = ReadT[int32](s)
	t.StartTime = ReadT[float64](s)
	t.Duration = ReadT[float64](s)
	t.Caster = ReadT[uint32](s)
	t.DegradeModifier = ReadT[float32](s)
	t.DegradeLimit = ReadT[float32](s)
	t.LastTimeDegraded = ReadT[float64](s)
	t.SMod.UnPack(s)
	if t.SpellCategory&0xFFFF0000 != 0 {
		t.SpellSetID = ReadTP[uint32](s)
	}
	return t
}

type GeneratorRegistry struct {
	WCID         uint32  `json:"wcidOrTtype"`
	TS           float64 `json:"ts"`
	TreasureType int32   `json:"bTreasureType"`
	Slot         uint32  `json:"slot"`
	Checkpointed int32   `json:"checkpointed"`
	Shop         int32   `json:"shop"`
	Amount       int32   `json:"amount"`
}

func (t *GeneratorRegistry) UnPack(s *SBuffer) any {
	t = new(GeneratorRegistry)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type CBaseQualities struct {
	WeenieType    WeenieType          `json:"weenieType"`
	IntStats      *HashMap[*Int32]    `json:"intStats,omitempty"`    // STypeInt
	Int64Stats    *HashMap[*Int64]    `json:"int64Stats,omitempty"`  // STypeInt64
	BoolStats     *HashMap[*Bool4]    `json:"boolStats,omitempty"`   // STypeBool
	FloatStats    *HashMap[*Float]    `json:"floatStats,omitempty"`  // STypeFloat
	StringStats   *HashMap[*PString]  `json:"stringStats,omitempty"` // STypeString
	DIDStats      *HashMap[*Uint32]   `json:"didStats,omitempty"`    // STypeDID
	IIDStats      *HashMap[*Uint32]   `json:"iidStats,omitempty"`    // STypeIID
	PositionStats *HashMap[*Position] `json:"posStats,omitempty"`    // STypePosition
}

func (t *CBaseQualities) UnPack(s *SBuffer) any {
	contentFlags := ReadT[uint32](s)
	t.WeenieType = WeenieType(ReadT[uint32](s))
	if contentFlags&0x01 != 0 {
		t.IntStats = new(HashMap[*Int32]).UnPack(s).(*HashMap[*Int32])
	}
	if contentFlags&0x80 != 0 {
		t.Int64Stats = new(HashMap[*Int64]).UnPack(s).(*HashMap[*Int64])
	}
	if contentFlags&0x02 != 0 {
		t.BoolStats = new(HashMap[*Bool4]).UnPack(s).(*HashMap[*Bool4])
	}
	if contentFlags&0x04 != 0 {
		t.FloatStats = new(HashMap[*Float]).UnPack(s).(*HashMap[*Float])
	}
	if contentFlags&0x10 != 0 {
		t.StringStats = new(HashMap[*PString]).UnPack(s).(*HashMap[*PString])
	}
	if contentFlags&0x08 != 0 {
		t.DIDStats = new(HashMap[*Uint32]).UnPack(s).(*HashMap[*Uint32])
	}
	if contentFlags&0x40 != 0 {
		t.IIDStats = new(HashMap[*Uint32]).UnPack(s).(*HashMap[*Uint32])
	}
	if contentFlags&0x20 != 0 {
		t.PositionStats = new(HashMap[*Position]).UnPack(s).(*HashMap[*Position])
	}
	// log.Printf("CBaseQualities.UnPack() pre-crash: contentFlags:%08x, WeenieType:%d, %s", contentFlags, t.WeenieType, ToJSON(t))

	return t
}
