// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
	"net"
	"os"
	"time"
)

type InboundPacket struct {
	Header    *ProtoHeader
	buf       *SBuffer
	dat       []byte
	packetLen int
	Checksum  uint32
}

type ACclient struct {
	Outbound  ACEndpoint
	Inbound   ACEndpoint
	RecID     uint16
	Iteration uint16
	NetID     uint16
	// interval         uint16
	ServerConn       *net.UDPConn //
	ReferralCookie   uint64       // mmmmm cookies
	Logger           *log.Logger  //
	Timestart        float64      // Timestamp that client was "started"
	GidPlayer        uint32       // GUID Player
	LastReceivedData time.Time
	LastPinged       time.Time
	PingTime         float32
	YellowLink       bool
	heartbeatQuit    chan struct{}

	SentPacketsMin uint32
	SentPackets    map[uint32][]byte
}
type ACEndpoint struct {
	Crypto        ISAAC        //
	Addr          *net.UDPAddr //
	LastPAK       uint32       //
	NextPAK       float64      // PAK cooldown timer
	Bytes         int          // Local = bytes SENT; remote = bytes RECEIVED
	NAKs          []uint32     // NAK list... kind of like a NOC list, just completely different.
	HighestID     uint32       // sending: next sequence #, receiving: last sequence #
	AckedInterval uint16       // ProtoHeader Interval // FLOW
	LastInterval  uint16       // ProtoHeader Interval // FLOW
}

var (
	acclient           *ACclient
	inBoundFragBuffer  = make(map[uint64]*Message)
	outBoundFragBuffer = make(chan *Frag, 16)
	inBoundPacketQueue = make(map[uint32]*InboundPacket)
)

//	func stopACClient() {
//		//todo send disconnect string and block and stuff.
//		acclient.ServerConn.Close()
//		acclient.heartbeatQuit <- struct{}{}
//	}
func RunACClient(server, username, password string) {
	var err error
	acclient = new(ACclient) //todo check if not nil, cleanup
	acclient.Logger = log.New(os.Stdout, "ACClient: ", log.Lmicroseconds|log.Lmsgprefix|log.LUTC)
	acclient.Inbound.Addr, _ = net.ResolveUDPAddr("udp", server)
	acclient.ServerConn, err = net.DialUDP("udp", nil, acclient.Inbound.Addr)
	acclient.Outbound.Addr, _ = net.ResolveUDPAddr("udp", acclient.ServerConn.LocalAddr().String())
	// note : you can use net.ResolveUDPAddr for LocalAddr as well
	//        for this tutorial simplicity sake, we will just use nil

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Established connection to %s \n", acclient.Inbound.Addr)
	log.Printf("Remote UDP address : %s \n", acclient.ServerConn.RemoteAddr().String())
	log.Printf("Local UDP client address : %s \n", acclient.ServerConn.LocalAddr().String())

	acclient.Inbound.LastPAK = 1
	acclient.Inbound.NextPAK = 0
	acclient.Timestart = acclient.Time()
	acclient.LastReceivedData = time.Now()
	acclient.LastPinged = time.Now()
	acclient.heartbeatQuit = make(chan struct{})
	acclient.SentPackets = make(map[uint32][]byte)
	acclient.SentPacketsMin = 2

	acclient.heartbeat()
	acclient.sendLogin(username, password)
	// receive messages from server
	buffer := make([]byte, 65536)
	for {
		n, addr, err := acclient.ServerConn.ReadFromUDP(buffer)
		if err != nil {
			acclient.Logger.Panic(err)
		}
		if n >= 20 && addr.IP.Equal(acclient.Inbound.Addr.IP) {
			acclient.handlePacket(n, buffer)
		}
	}
}
func (c *ACclient) Time() float64 {
	return (float64(time.Now().UnixMicro()) / 1000000) - 936144000
}
func (c *ACclient) UpTime() float64 {
	return c.Time() - c.Timestart
}
func (c *ACclient) Interval() uint16 {
	return (uint16(c.UpTime()) << 1) & 0xFFFC
}

func (c *ACclient) heartbeat() {
	ticker := time.NewTicker(250 * time.Millisecond)
	go func() {
		for {
			select {
			case <-ticker.C:
				c.flushOutboundFrags()
				b := new(bytes.Buffer)
				c.sendProtoHeader(0, b)
			case <-c.heartbeatQuit:
				ticker.Stop()
				c.Logger.Printf("heartbeat() died!\n")
				return
			}
		}
	}()
}

func (c *ACclient) flushOutboundFrags() {
	for {
		select {
		case f := <-outBoundFragBuffer:
			c.sendProtoHeader(pHeader_FRAG|pHeader_EncCRC, f.Buffer)
		default:
			return
		}
	}
}
func (c *ACclient) handlePacket(inLen int, in []byte) {
	ip := &InboundPacket{
		Header:    new(ProtoHeader),
		buf:       &SBuffer{bytes.NewBuffer(in[:inLen])},
		dat:       in,
		packetLen: inLen,
		Checksum:  PacketChecksum(in[:inLen]),
	}

	if ip.Checksum == 0 {
		binary.Read(ip.buf, binary.LittleEndian, ip.Header)
		//c.Logger.Printf("\033[34mReceived: (%d) %s\n%s\033[0m", inLen, ip.Header, hex.Dump(in[:inLen]))
		c.Logger.Printf("\033[34mReceived: (%d) %s\033[0m", inLen, ip.Header)
		c.processPacket(ip)
		return
	} else {
		if in[4]&2 == 0 {
			c.Logger.Printf("CRC INVALID rcvd %08X\n", ip.Checksum)
			return
		} else {
			binary.Read(ip.buf, binary.LittleEndian, ip.Header)
			expectedKey := c.Inbound.Crypto.GetIndex(ip.Header.SeqID - 2)
			if ip.Checksum != expectedKey {
				c.Logger.Printf("   EncCRC INVALID rcvd %08X, expected %08X\n", ip.Checksum, expectedKey)
				return
			}
		}
	}

	c.Logger.Printf("\033[34mReceived: (%d) %s\033[0m", inLen, ip.Header)
	//c.Logger.Printf("\033[31mReceived: (%d) %s\n%s\033[0m", inLen, ip.Header, hex.Dump(in[:inLen]))
evilgoto:
	if ip.Header.SeqID <= c.Inbound.HighestID {
		var ok bool
		c.processPacket(ip)
		ip, ok = inBoundPacketQueue[c.Inbound.HighestID]
		if ok {
			c.Logger.Printf("weeee, welcome to the endless loop ride!\n")
			goto evilgoto
		}

	} else {
		c.Logger.Printf("out of order packet queued, where it will live forever. received %d, expected %d\n", ip.Header.SeqID, c.Inbound.HighestID)
		inBoundPacketQueue[ip.Header.SeqID] = ip
	}
}

func (c *ACclient) processNAKs(NAKs []uint32) {
	for _, v := range NAKs {
		p, o := acclient.SentPackets[v]
		if o {
			//set resend flag
			p[4] |= 1

			//clear encCRC flag
			p[4] &= 0xFD

			//update interval
			in := c.Interval()
			p[14] = byte(in)
			p[15] = byte(in >> 8)

			//re-jigger checksum
			csm := PacketChecksum(p)
			p[8] = byte(csm)
			p[9] = byte(csm >> 8)
			p[10] = byte(csm >> 16)
			p[11] = byte(csm >> 24)

			acclient.Logger.Printf("\033[31m[NAK:%d] Re-sending %d\n%s\033[0m\n", v, v, hex.Dump(p))
			//pull trigger
			c.Outbound.Bytes += len(p)
			c.ServerConn.Write(p)
		} else {
			acclient.Logger.Printf("\033[31m[NAK:%d]Server wants things I do not have.\033[0m", v)
			// TODO: send disconnect, re-initialize client.
		}
	}
}
func (c *ACclient) queueFlush(server_pak uint32) {
	if acclient.SentPacketsMin >= server_pak {
		return
	}
	for {
		if acclient.SentPacketsMin == server_pak {
			break
		}
		delete(acclient.SentPackets, acclient.SentPacketsMin)
		acclient.SentPacketsMin++
	}
	acclient.Logger.Printf("\033[32m[PAK:%d]\033[0m", server_pak)
}

func (c *ACclient) sendLogin(username, password string) {
	login := new(bytes.Buffer)
	login.Write(makePStringChar("1802"))
	writeAlign(login)
	cbAuthData := c.makecbAuthData(1, username, password)
	binary.Write(login, binary.LittleEndian, uint32(cbAuthData.Len()))
	binary.Write(login, binary.LittleEndian, cbAuthData.Bytes())

	pHeader := ProtoHeader{
		SeqID:     0,
		Header:    pHeader_LoginRequest,
		RecID:     0,
		Interval:  0,
		Datalen:   uint16(login.Len()),
		Iteration: 0,
	}
	netBuffer := new(bytes.Buffer)
	binary.Write(netBuffer, binary.LittleEndian, pHeader)
	login.WriteTo(netBuffer)
	bufa := netBuffer.Bytes()

	checksum := LoginChecksum(bufa)
	bufa[8] = byte(checksum)
	bufa[9] = byte(checksum >> 8)
	bufa[10] = byte(checksum >> 16)
	bufa[11] = byte(checksum >> 24)
	acclient.Logger.Printf("\033[33mSending: (%d) %s\n%s\033[0m", len(bufa), &pHeader, hex.Dump(bufa))
	//acclient.Logger.Printf("\033[33mSending: (%d) %s\033[0m", len(bufa), &pHeader)

	c.Outbound.Bytes += len(bufa)
	c.ServerConn.Write(bufa)

}
func (c *ACclient) sendConnectResponse() {
	pHeader := ProtoHeader{
		SeqID:     0,
		Header:    pHeader_ConnectResponse,
		RecID:     c.NetID,
		Interval:  0,
		Datalen:   8,
		Iteration: c.Iteration,
	}
	netBuffer := new(bytes.Buffer)
	binary.Write(netBuffer, binary.LittleEndian, pHeader)
	binary.Write(netBuffer, binary.LittleEndian, c.ReferralCookie)
	//binary.Write(netBuffer, binary.LittleEndian, uint64(0x652E3F0006A8A8E1))

	bufa := netBuffer.Bytes()

	checksum := LoginChecksum(bufa)
	bufa[8] = byte(checksum)
	bufa[9] = byte(checksum >> 8)
	bufa[10] = byte(checksum >> 16)
	bufa[11] = byte(checksum >> 24)
	//acclient.Logger.Printf("\033[33mSending: (%d) %s\033[0m", len(bufa), &pHeader)
	acclient.Logger.Printf("\033[33mSending: (%d) %s\n%s\033[0m", len(bufa), &pHeader, hex.Dump(bufa))

	c.Outbound.Bytes += len(bufa)

	loginAddr, _ := net.ResolveUDPAddr("udp", string(acclient.Inbound.Addr.IP.String())+fmt.Sprintf(":%d", acclient.Inbound.Addr.Port+1))
	loginConn, _ := net.DialUDP("udp", nil, loginAddr)

	loginConn.Write(bufa)
	loginConn.Close()

}

// func (c *ACclient) _IncrementLocalInterval(cIntervals uint16) {
// 	v4 := (c.interval + cIntervals) % 6
// 	if v4 == 1 || cIntervals >= 6 || v4 > 0 && v4 < (c.interval%6) {
// 		optionalHeader := new(bytes.Buffer)
// 		//TIME data
// 		binary.Write(optionalHeader, binary.LittleEndian, c.Time())
// 		//PING data
// 		binary.Write(optionalHeader, binary.LittleEndian, c.UpTime())
// 		c.sendProtoHeader(pHeader_TIME|pHeader_PING|pHeader_EncCRC, optionalHeader)
// 	}
// 	v7 := (c.interval + cIntervals) % 0xDC
// 	if v7 == 1 || cIntervals >= 0xDC || v7 > 0 && v7 < (c.interval%0xDC) {
// 		optionalHeader := new(bytes.Buffer)
// 		binary.Write(optionalHeader, binary.LittleEndian, uint64(1))
// 		addrServerWrite := &sockaddr_in{
// 			sin_family: 2,
// 			sin_port:   uint16(c.Local.Addr.Port) + 1,
// 			sin_addr:   c.Local.Addr.AddrPort().Addr().As4(),
// 		}
// 		binary.Write(optionalHeader, binary.LittleEndian, addrServerWrite)
// 		c.sendProtoHeader(pHeader_CICMDCommand|pHeader_ServerSwitch, optionalHeader)
// 	}
// 	c.interval += cIntervals
// }
// func (c *ACclient) _IncrementLocalInterval2() {
// 	var newInterval uint16 = c.Interval()
// 	var cIntervals uint16 = newInterval - c.interval
// 	v4 := newInterval % 6
// 	if v4 == 1 || cIntervals >= 6 || v4 > 0 && v4 < (c.interval%6) {
// 		optionalHeader := new(bytes.Buffer)
// 		binary.Write(optionalHeader, binary.LittleEndian, c.Time())
// 		binary.Write(optionalHeader, binary.LittleEndian, c.UpTime())
// 		c.sendProtoHeader(pHeader_TIME|pHeader_PING|pHeader_EncCRC, optionalHeader)
// 	}
// 	v7 := newInterval % 0xDC
// 	if v7 == 1 || cIntervals >= 0xDC || v7 > 0 && v7 < (c.interval%0xDC) {
// 		optionalHeader := new(bytes.Buffer)
// 		binary.Write(optionalHeader, binary.LittleEndian, uint64(1))
// 		addrServerWrite := &sockaddr_in{
// 			sin_family: 2,
// 			sin_port:   uint16(c.Local.Addr.Port) + 1,
// 			sin_addr:   c.Local.Addr.AddrPort().Addr().As4(),
// 		}
// 		binary.Write(optionalHeader, binary.LittleEndian, addrServerWrite)
// 		c.sendProtoHeader(pHeader_CICMDCommand|pHeader_ServerSwitch, optionalHeader)
// 	}
// 	c.interval = newInterval
// }

func (c *ACclient) makecbAuthData(loginType uint8, username string, password string) *bytes.Buffer {
	cbAuthData := new(bytes.Buffer)

	var longHeader uint64 = uint64(loginType) // loginType 1
	binary.Write(cbAuthData, binary.LittleEndian, longHeader)

	//Client Timestamp
	binary.Write(cbAuthData, binary.LittleEndian, uint32(time.Now().Unix()))
	//binary.Write(cbAuthData, binary.LittleEndian, uint32(0x62D7264C))

	switch loginType {
	case 1:
		cbAuthData.Write(makePStringChar(fmt.Sprintf("%s:%s", username, password)))
		cbAuthData.Write([]byte{0, 0, 0, 0}) // padding
		writeAlign(cbAuthData)
	case 2:
		cbAuthData.Write(makePStringChar(username))
		writeAlign(cbAuthData)
		//token length
		binary.Write(cbAuthData, binary.LittleEndian, uint32(len(password)+1))
		binary.Write(cbAuthData, binary.LittleEndian, byte(len(password)))
		binary.Write(cbAuthData, binary.LittleEndian, []byte(password))

	}
	return cbAuthData
}

func makePStringChar(in string) []byte {
	ret := new(bytes.Buffer)
	binary.Write(ret, binary.LittleEndian, uint16(len(in)))
	binary.Write(ret, binary.LittleEndian, []byte(in))
	ret.Write([]byte{0})
	writeAlign(ret)
	return ret.Bytes()
}

func HeaderChecksum(in []byte) (ret uint32) {
	if len(in) < 20 {
		return 1
	}
	ret = 0x00140000 + 0xBADD70DD + binary.LittleEndian.Uint32(in[0:4]) + binary.LittleEndian.Uint32(in[4:8]) + binary.LittleEndian.Uint32(in[12:16]) + binary.LittleEndian.Uint32(in[16:20])
	return
}
func Checksum(in []byte) (ret uint32) {
	ret = uint32(len(in) << 16)
	for i := 0; i < len(in)-3; i += 4 {
		ret += binary.LittleEndian.Uint32(in[i:])
	}
	if len(in)%4 == 2 {
		ret += (uint32(in[len(in)-1]) << 16) + (uint32(in[len(in)-2]) << 24)
	}
	return
}
func PacketChecksum(in []byte) uint32 {
	if len(in) < 20 {
		return 1
	}
	return (binary.LittleEndian.Uint32(in[8:12]) - HeaderChecksum(in)) ^ Checksum(in[20:])
}
func LoginChecksum(in []byte) uint32 {
	if len(in) < 20 {
		return 1
	}
	return HeaderChecksum(in) + Checksum(in[20:])
}
func writeAlign(in *bytes.Buffer) {
	switch in.Len() % 4 {
	case 1:
		in.Write([]byte{0, 0, 0})
	case 2:
		in.Write([]byte{0, 0})
	case 3:
		in.Write([]byte{0})
	}
}
