// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

type FragHeader struct {
	BlobID       uint64
	NumFrags     uint16
	BlobFragSize uint16
	BlobNum      uint16
	QueueID      uint16
}
type Frag struct {
	*FragHeader
	*bytes.Buffer
}

/*
public enum MessageQueue
{
    Invalid = 0x00,
    Event = 0x01,
    Control = 0x02,
    Weenie = 0x03,
    Login = 0x04,
    Database = 0x05,
    SecureControl = 0x06,
    SecureWeenie = 0x07, // Autonomous Position
    SecureLogin = 0x08,
    UI = 0x09,
    Smartbox = 0x0A,
    Observer = 0x0B,
    QueueMax = 0x0C
}
*/

func (f *FragHeader) String() string {
	return fmt.Sprintf("BlobID:%16x,NumFrags:%d,BlobFragSize:%d,BlobNum:%d,QueueID:%d",
		f.BlobID, f.NumFrags, f.BlobFragSize, f.BlobNum, f.QueueID)
}
func (f *Frag) String() string {
	return fmt.Sprintf("BlobID:%16x,NumFrags:%d,BlobFragSize:%d,BlobNum:%d,QueueID:%d: %#v",
		f.BlobID, f.NumFrags, f.BlobFragSize, f.BlobNum, f.QueueID, f.Bytes())
}
func (c *ACclient) handleFrags(s *SBuffer) bool {
thisgotoisthelesserof2evils:
	if s.Len() >= 16 {
		frag := *new(FragHeader)
		binary.Read(s, binary.LittleEndian, frag)
		if uint16(s.Len()) >= (frag.BlobFragSize - 16) {
			var msg *Message
			var ok bool
			if frag.BlobNum == 0 { // I can smell your eyeballs on this.
				msg = &Message{
					QueueID: frag.QueueID,
					buf:     new(SBuffer),
				}
				inBoundFragBuffer[frag.BlobID] = msg
			} else {
				msg, ok = inBoundFragBuffer[frag.BlobID]
				if !ok {
					c.Logger.Printf("Server Error\n")
					return true
				}
			}
			fragData := ReadTs[byte](s, int(frag.BlobFragSize%464)-16)
			msg.buf.Write(fragData)
			if frag.NumFrags-1 == frag.BlobNum {
				if ok {
					delete(inBoundFragBuffer, frag.BlobID)
				}
				msg.handle()
			}
			goto thisgotoisthelesserof2evils
		}
	}
	return false
}

var send_blobid uint64 = 1

func (c *ACclient) makeFrags(buf *bytes.Buffer, queueid uint32) {
	blobFragSize := 0x1C0
	blobCnt := (buf.Len() + blobFragSize - 1) / blobFragSize
	blobNum := 0
	off := 0
	for {
		if off >= buf.Len() {
			break
		}
		if (buf.Len() - off) < blobFragSize {
			blobFragSize = buf.Len() - off
		}
		paddedLen := (blobFragSize + 3) & (^3)
		f := &Frag{
			&FragHeader{
				BlobID:       send_blobid,
				NumFrags:     uint16(blobCnt),
				BlobFragSize: 16 + uint16(paddedLen),
				//BlobFragSize: 0,
				BlobNum: uint16(blobNum),
				QueueID: uint16(queueid),
			},
			new(bytes.Buffer),
		}
		binary.Write(f, binary.LittleEndian, f.FragHeader)
		f.Write(buf.Bytes()[off : off+blobFragSize])
		switch f.Len() & 3 {
		case 3:
			f.Write([]byte{0})
		case 2:
			f.Write([]byte{0, 0})
		case 1:
			f.Write([]byte{0, 0, 0})
		}
		outBoundFragBuffer <- f
		//aids.InfoLog.Printf("blob(%d)=%d %s\n", buf.Len(), f.Len(), f)
		off += blobFragSize
		blobNum++
	}
	send_blobid++
}
