// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

package ac

type S2C_0x0003_AllegianceUpdateAborted struct { // UIQueue

}
type S2C_0x0004_PopUpString struct { // UIQueue

}
type C2S_0x0005_PlayerOptionChangedEvent struct { // Weenie

}
type C2S_0x0008_TargetedMeleeAttack struct { // Weenie

}
type C2S_0x000A_TargetedMissileAttack struct { // Weenie

}
type C2S_0x000F_SetAFKMode struct { // Weenie

}
type C2S_0x0010_SetAFKMessage struct { // Weenie

}
type S2C_0x0013_PlayerDescription struct { // UIQueue
	Qualities     CACQualities
	PlayerModule  PlayerModule
	Contents      List[*ContentProfile]
	InvPlacements List[*InventoryPlacement]
}

func (t *S2C_0x0013_PlayerDescription) UnPack(s *SBuffer) any {
	// t = new(S2C_0x0013_PlayerDescription)
	t.Qualities.UnPack(s)
	t.PlayerModule.UnPack(s)
	t.Contents.UnPack(s)
	t.InvPlacements.UnPack(s)
	return t
}

type ContentProfile struct {
	Id                  uint32
	ContainerProperties uint32
}

// binary.Read(s, binary.LittleEndian, t)
func (t *ContentProfile) UnPack(s *SBuffer) any {
	// t = new(ContentProfile)
	t.Id = ReadT[uint32](s)
	t.ContainerProperties = ReadT[uint32](s)
	return t
}

type InventoryPlacement struct {
	Id       uint32
	Loc      uint32
	Priority uint32
}

func (t *InventoryPlacement) UnPack(s *SBuffer) any {
	// t = new(InventoryPlacement)
	t.Id = ReadT[uint32](s)
	t.Loc = ReadT[uint32](s)
	t.Priority = ReadT[uint32](s)
	return t
}

type C2S_0x0015_Talk struct { // Weenie

}
type C2S_0x0017_RemoveFriend struct { // Weenie

}
type C2S_0x0018_AddFriend struct { // Weenie

}
type C2S_0x0019_PutItemInContainer struct { // Weenie

}
type C2S_0x001A_GetAndWieldItem struct { // Weenie

}
type C2S_0x001B_DropItem struct { // Weenie

}
type C2S_0x001D_SwearAllegiance struct { // Weenie

}
type C2S_0x001E_BreakAllegiance struct { // Weenie

}
type C2S_0x001F_UpdateRequest struct { // Weenie

}
type S2C_0x0020_AllegianceUpdate struct { // UIQueue

}
type S2C_0x0021_FriendsUpdate struct { // UIQueue

}
type S2C_0x0022_ServerSaysContainID struct { // UIQueue

}
type S2C_0x0023_WearItem struct { // UIQueue

}
type S2C_0x0024_ServerSaysRemove struct { // UIQueue
	ObjectID uint32
}
type C2S_0x0025_ClearFriends struct { // Weenie

}
type C2S_0x0026_TeleToPKLArena struct { // Weenie

}
type C2S_0x0027_TeleToPKArena struct { // Weenie

}
type S2C_0x0029_CharacterTitleTable struct { // UIQueue

}
type S2C_0x002B_AddOrSetCharacterTitle struct { // UIQueue

}
type C2S_0x002C_SetDisplayCharacterTitle struct { // Weenie

}
type C2S_0x0030_QueryAllegianceName struct { // Weenie

}
type C2S_0x0031_ClearAllegianceName struct { // Weenie

}
type C2S_0x0032_TalkDirect struct { // Weenie

}
type C2S_0x0033_SetAllegianceName struct { // Weenie

}
type C2S_0x0035_UseWithTargetEvent struct { // Weenie

}
type C2S_0x0036_UseEvent struct { // Weenie

}
type C2S_0x003B_SetAllegianceOfficer struct { // Weenie

}
type C2S_0x003C_SetAllegianceOfficerTitle struct { // Weenie

}
type C2S_0x003D_ListAllegianceOfficerTitles struct { // Weenie

}
type C2S_0x003E_ClearAllegianceOfficerTitles struct { // Weenie

}
type C2S_0x003F_DoAllegianceLockAction struct { // Weenie

}
type C2S_0x0040_SetAllegianceApprovedVassal struct { // Weenie

}
type C2S_0x0041_AllegianceChatGag struct { // Weenie

}
type C2S_0x0042_DoAllegianceHouseAction struct { // Weenie

}
type C2S_0x0044_TrainAttribute2nd struct { // Weenie

}
type C2S_0x0045_TrainAttribute struct { // Weenie

}
type C2S_0x0046_TrainSkill struct { // Weenie

}
type C2S_0x0047_TrainSkillAdvancementClass struct { // Weenie

}
type C2S_0x0048_CastUntargetedSpell struct { // Weenie

}
type C2S_0x004A_CastTargetedSpell struct { // Weenie

}
type S2C_0x0052_StopViewingObjectContents struct { // UIQueue

}
type C2S_0x0053_ChangeCombatMode struct { // Weenie

}
type C2S_0x0054_StackableMerge struct { // Weenie

}
type C2S_0x0055_StackableSplitToContainer struct { // Weenie

}
type C2S_0x0056_StackableSplitTo3D struct { // Weenie

}
type C2S_0x0058_ModifyCharacterSquelch struct { // Weenie

}
type C2S_0x0059_ModifyAccountSquelch struct { // Weenie

}
type C2S_0x005B_ModifyGlobalSquelch struct { // Weenie

}
type C2S_0x005D_TalkDirectByName struct { // Weenie

}
type C2S_0x005F_Buy struct { // Weenie

}
type C2S_0x0060_Sell struct { // Weenie

}
type S2C_0x0062_VendorInfo struct { // UIQueue

}
type C2S_0x0063_TeleToLifestone struct { // Weenie

}
type S2C_0x0075_StartBarber struct { // UIQueue

}
type S2C_0x00A0_ServerSaysAttemptFailed struct { // UIQueue

}
type C2S_0x00A1_LoginCompleteNotification struct { // Weenie

}
type C2S_0x00A2_Create struct { // Weenie

}
type S2C_0x00A3_Quit struct { // UIQueue

}
type C2S_0x00A3_Quit struct { // Weenie

}
type S2C_0x00A4_Dismiss struct { // UIQueue

}
type C2S_0x00A4_Dismiss struct { // Weenie

}
type C2S_0x00A5_Recruit struct { // Weenie

}
type C2S_0x00A6_UpdateRequest struct { // Weenie

}
type C2S_0x00AA_BookAddPage struct { // Weenie

}
type C2S_0x00AB_BookModifyPage struct { // Weenie

}
type C2S_0x00AC_BookData struct { // Weenie

}
type C2S_0x00AD_BookDeletePage struct { // Weenie

}
type C2S_0x00AE_BookPageData struct { // Weenie

}
type S2C_0x00B4_BookOpen struct { // UIQueue

}
type S2C_0x00B5_BookUnknown1 struct { // UIQueue

}
type S2C_0x00B6_BookAddPageResponse struct { // UIQueue

}
type S2C_0x00B7_BookDeletePageResponse struct { // UIQueue

}
type S2C_0x00B8_BookPageDataResponse struct { // UIQueue

}
type C2S_0x00BF_SetInscription struct { // Weenie

}
type S2C_0x00C3_GetInscriptionResponse struct { // UIQueue

}
type C2S_0x00C8_Appraise struct { // Weenie

}
type S2C_0x00C9_SetAppraiseInfo struct { // UIQueue

}
type C2S_0x00CD_GiveObjectRequest struct { // Weenie

}
type C2S_0x00D6_Teleport struct { // Weenie

}
type C2S_0x0140_AbuseLogRequest struct { // Weenie

}
type C2S_0x0145_AddToChannel struct { // Weenie

}
type C2S_0x0146_RemoveFromChannel struct { // Weenie

}
type C2S_0x0147_ChannelBroadcast struct { // UIQueue

}
type S2C_0x0147_ChannelBroadcast struct { // Weenie

}
type S2C_0x0148_ChannelList struct { // UIQueue

}
type C2S_0x0148_ChannelList struct { // Weenie

}
type S2C_0x0149_ChannelIndex struct { // UIQueue

}
type C2S_0x0149_ChannelIndex struct { // Weenie

}
type C2S_0x0195_NoLongerViewingContents struct { // Weenie

}
type S2C_0x0196_OnViewContents struct { // UIQueue

}
type S2C_0x0197_UpdateStackSize struct { // UIQueue
	Seq     byte
	ItemIID uint32
	Amount  uint32
	Value   uint32
}
type S2C_0x019A_ServerSaysMoveItem struct { // UIQueue

}
type C2S_0x019B_StackableSplitToWield struct { // Weenie

}
type C2S_0x019C_AddShortCut struct { // Weenie

}
type C2S_0x019D_RemoveShortCut struct { // Weenie

}
type S2C_0x019E_HandlePlayerDeathEvent struct { // UIQueue
	Text      PString
	KilledIID uint32
	KillerIID uint32
}
type C2S_0x01A1_CharacterOptionsEvent struct { // Weenie

}
type S2C_0x01A7_HandleAttackDoneEvent struct { // UIQueue

}
type S2C_0x01A8_RemoveSpell struct { // UIQueue

}
type C2S_0x01A8_RemoveSpell struct { // Weenie

}
type S2C_0x01AC_HandleVictimNotificationEventSelf struct { // UIQueue

}
type S2C_0x01AD_HandleVictimNotificationEventOther struct { // UIQueue

}
type S2C_0x01B1_HandleAttackerNotificationEvent struct { // UIQueue

}
type S2C_0x01B2_HandleDefenderNotificationEvent struct { // UIQueue

}
type S2C_0x01B3_HandleEvasionAttackerNotificationEvent struct { // UIQueue

}
type S2C_0x01B4_HandleEvasionDefenderNotificationEvent struct { // UIQueue

}
type C2S_0x01B7_CancelAttack struct { // Weenie

}
type S2C_0x01B8_HandleCommenceAttackEvent struct { // UIQueue

}
type C2S_0x01BF_QueryHealth struct { // Weenie

}
type S2C_0x01C0_QueryHealthResponse struct { // UIQueue

}
type C2S_0x01C2_QueryAge struct { // Weenie

}
type S2C_0x01C3_QueryAgeResponse struct { // UIQueue

}
type C2S_0x01C4_QueryBirth struct { // Weenie

}
type S2C_0x01C7_UseDone struct { // UIQueue

}
type S2C_0x01C8_AllegiancePanelOpen struct { // UIQueue

}
type S2C_0x01C9_FellowUpdateDone struct { // UIQueue

}
type S2C_0x01CA_FellowStatsDone struct { // UIQueue

}
type S2C_0x01CB_AppraiseDone struct { // UIQueue

}
type S2C_0x01D1_PrivateRemoveIntEvent struct { // UIQueue

}
type S2C_0x01D2_RemoveIntEvent struct { // UIQueue

}
type S2C_0x01D3_PrivateRemoveBoolEvent struct { // UIQueue

}
type S2C_0x01D4_RemoveBoolEvent struct { // UIQueue

}
type S2C_0x01D5_PrivateRemoveFloatEvent struct { // UIQueue

}
type S2C_0x01D6_RemoveFloatEvent struct { // UIQueue

}
type S2C_0x01D7_PrivateRemoveStringEvent struct { // UIQueue

}
type S2C_0x01D8_RemoveStringEvent struct { // UIQueue

}
type S2C_0x01D9_PrivateRemoveDataIDEvent struct { // UIQueue

}
type S2C_0x01DA_RemoveDataIDEvent struct { // UIQueue

}
type S2C_0x01DB_PrivateRemoveInstanceIDEvent struct { // UIQueue

}
type S2C_0x01DC_RemoveInstanceIDEvent struct { // UIQueue

}
type S2C_0x01DD_PrivateRemovePositionEvent struct { // UIQueue

}
type S2C_0x01DE_RemovePositionEvent struct { // UIQueue

}
type C2S_0x01DF_Emote struct { // Weenie

}
type S2C_0x01E0_HearEmote struct { // UIQueue
	SenderIID  uint32
	SenderName PString
	Text       PString
}
type C2S_0x01E1_SoulEmote struct { // Weenie

}
type S2C_0x01E2_HearSoulEmote struct { // UIQueue
	SenderIID  uint32
	SenderName PString
	Text       PString
}
type C2S_0x01E3_AddSpellFavorite struct { // Weenie

}
type C2S_0x01E4_RemoveSpellFavorite struct { // Weenie

}
type C2S_0x01E9_RequestPing struct { // Weenie

}
type S2C_0x01EA_ReturnPing struct { // UIQueue

}
type S2C_0x01F4_SetSquelchDB struct { // UIQueue

}
type C2S_0x01F6_OpenTradeNegotiations struct { // Weenie

}
type C2S_0x01F7_CloseTradeNegotiations struct { // Weenie

}
type C2S_0x01F8_ClientAddToTrade struct { // Weenie

}
type C2S_0x01FA_ClientAcceptTrade struct { // Weenie

}
type C2S_0x01FB_ClientDeclineTrade struct { // Weenie

}
type S2C_0x01FD_ServerRegisterTrade struct { // UIQueue

}
type S2C_0x01FE_ServerOpenTrade struct { // UIQueue

}
type S2C_0x01FF_ServerCloseTrade struct { // UIQueue

}
type S2C_0x0200_ServerAddToTrade struct { // UIQueue

}
type S2C_0x0201_ServerRemoveFromTrade struct { // UIQueue

}
type S2C_0x0202_ServerAcceptTrade struct { // UIQueue

}
type S2C_0x0203_ServerDeclineTrade struct { // UIQueue

}
type C2S_0x0204_ClientResetTrade struct { // Weenie

}
type S2C_0x0205_ServerResetTrade struct { // UIQueue

}
type S2C_0x0207_ServerTradeFailure struct { // UIQueue

}
type S2C_0x0208_ServerClearTradeAcceptance struct { // UIQueue

}
type C2S_0x0216_ClearPlayerConsentList struct { // Weenie

}
type C2S_0x0217_DisplayPlayerConsentList struct { // Weenie

}
type C2S_0x0218_RemoveFromPlayerConsentList struct { // Weenie

}
type C2S_0x0219_AddPlayerPermission struct { // Weenie

}
type C2S_0x021C_BuyHouse struct { // Weenie

}
type S2C_0x021D_HouseProfile struct { // UIQueue

}
type C2S_0x021E_QueryHouse struct { // Weenie

}
type C2S_0x021F_AbandonHouse struct { // Weenie

}
type C2S_0x0220_RemovePlayerPermission struct { // Weenie

}
type C2S_0x0221_RentHouse struct { // Weenie

}
type C2S_0x0224_SetDesiredComponentLevel struct { // Weenie

}
type S2C_0x0225_HouseData struct { // UIQueue

}
type S2C_0x0226_HouseStatus struct { // UIQueue

}
type S2C_0x0227_UpdateRentTime struct { // UIQueue

}
type S2C_0x0228_UpdateRentPayment struct { // UIQueue

}
type C2S_0x0245_AddPermanentGuest struct { // Weenie

}
type C2S_0x0246_RemovePermanentGuest struct { // Weenie

}
type C2S_0x0247_SetOpenHouseStatus struct { // Weenie

}
type S2C_0x0248_UpdateRestrictions struct { // UIQueue

}
type C2S_0x0249_ChangeStoragePermission struct { // Weenie

}
type C2S_0x024A_BootSpecificHouseGuest struct { // Weenie

}
type C2S_0x024C_RemoveAllStoragePermission struct { // Weenie

}
type C2S_0x024D_RequestFullGuestList struct { // Weenie

}
type C2S_0x0254_SetMotd struct { // Weenie

}
type C2S_0x0255_QueryMotd struct { // Weenie

}
type C2S_0x0256_ClearMotd struct { // Weenie

}
type S2C_0x0257_UpdateHAR struct { // UIQueue

}
type C2S_0x0258_QueryLord struct { // Weenie

}
type S2C_0x0259_HouseTransaction struct { // UIQueue

}
type C2S_0x025C_AddAllStoragePermission struct { // Weenie

}
type C2S_0x025E_RemoveAllPermanentGuests struct { // Weenie

}
type C2S_0x025F_BootEveryone struct { // Weenie

}
type C2S_0x0262_TeleToHouse struct { // Weenie

}
type C2S_0x0263_QueryItemMana struct { // Weenie

}
type S2C_0x0264_QueryItemManaResponse struct { // UIQueue

}
type C2S_0x0266_SetHooksVisibility struct { // Weenie

}
type C2S_0x0267_ModifyAllegianceGuestPermission struct { // Weenie

}
type C2S_0x0268_ModifyAllegianceStoragePermission struct { // Weenie

}
type C2S_0x0269_Join struct { // Weenie

}
type C2S_0x026A_Quit struct { // Weenie

}
type C2S_0x026B_Move struct { // Weenie

}
type C2S_0x026D_MovePass struct { // Weenie

}
type C2S_0x026E_Stalemate struct { // Weenie

}
type C2S_0x0270_ListAvailableHouses struct { // Weenie

}
type S2C_0x0271_AvailableHouses struct { // UIQueue

}
type S2C_0x0274_ConfirmationRequest struct { // UIQueue

}
type C2S_0x0275_ConfirmationResponse struct { // Weenie

}
type S2C_0x0276_ConfirmationDone struct { // UIQueue

}
type C2S_0x0277_BreakAllegianceBoot struct { // Weenie

}
type C2S_0x0278_TeleToMansion struct { // Weenie

}
type C2S_0x0279_Suicide struct { // Weenie

}
type S2C_0x027A_AllegianceLoginNotificationEvent struct { // UIQueue

}
type C2S_0x027B_AllegianceInfoRequest struct { // Weenie

}
type S2C_0x027C_AllegianceInfoResponseEvent struct { // UIQueue

}
type C2S_0x027D_CreateTinkeringTool struct { // Weenie

}
type S2C_0x0281_JoinGameResponse struct { // UIQueue

}
type S2C_0x0282_StartGame struct { // UIQueue

}
type S2C_0x0283_MoveResponse struct { // UIQueue

}
type S2C_0x0284_OpponentTurn struct { // UIQueue

}
type S2C_0x0285_OpponentStalemateState struct { // UIQueue

}
type C2S_0x0286_SpellbookFilterEvent struct { // Weenie

}
type S2C_0x028A_WeenieError struct { // UIQueue

}
type S2C_0x028B_WeenieErrorWithString struct { // UIQueue

}
type S2C_0x028C_GameOver struct { // UIQueue

}
type C2S_0x028D_TeleToMarketplace struct { // Weenie

}
type C2S_0x028F_EnterPKLite struct { // Weenie

}
type C2S_0x0290_AssignNewLeader struct { // Weenie

}
type C2S_0x0291_ChangeFellowOpeness struct { // Weenie

}
type S2C_0x0295_ChatRoomTracker struct { // UIQueue

}
type C2S_0x02A0_AllegianceChatBoot struct { // Weenie

}
type C2S_0x02A1_AddAllegianceBan struct { // Weenie

}
type C2S_0x02A2_RemoveAllegianceBan struct { // Weenie

}
type C2S_0x02A3_ListAllegianceBans struct { // Weenie

}
type C2S_0x02A5_RemoveAllegianceOfficer struct { // Weenie

}
type C2S_0x02A6_ListAllegianceOfficers struct { // Weenie

}
type C2S_0x02A7_ClearAllegianceOfficers struct { // Weenie

}
type C2S_0x02AB_RecallAllegianceHometown struct { // Weenie

}
type S2C_0x02AE_QueryPluginList struct { // UIQueue

}
type C2S_0x02AF_QueryPluginListResponse struct { // Weenie

}
type S2C_0x02B1_QueryPlugin struct { // UIQueue

}
type C2S_0x02B2_ClientQueryPluginResponse struct { // Weenie

}
type S2C_0x02B3_ServerQueryPluginResponse struct { // UIQueue

}
type S2C_0x02B4_SalvageOperationsResultData struct { // UIQueue

}
type S2C_0x02B8_PrivateRemoveInt64Event struct { // UIQueue

}
type S2C_0x02B9_RemoveInt64Event struct { // UIQueue

}
type S2C_0x02BB_HearSpeech struct { // UIQueue
	Msg             PString
	SenderName      PString
	SenderIID       uint32
	ChatMessageType uint32
}
type S2C_0x02BC_HearRangedSpeech struct { // UIQueue

}
type S2C_0x02BD_HearDirectSpeech struct { // UIQueue

}
type S2C_0x02BE_FullUpdate struct { // UIQueue

}
type S2C_0x02BF_Disband struct { // UIQueue

}
type S2C_0x02C0_UpdateFellow struct { // UIQueue

}
type S2C_0x02C1_UpdateSpell struct { // UIQueue

}
type S2C_0x02C2_UpdateEnchantment struct { // UIQueue

}
type S2C_0x02C3_RemoveEnchantment struct { // UIQueue

}
type S2C_0x02C4_UpdateMultipleEnchantments struct { // UIQueue

}
type S2C_0x02C5_RemoveMultipleEnchantments struct { // UIQueue

}
type S2C_0x02C6_PurgeEnchantments struct { // UIQueue

}
type S2C_0x02C7_DispelEnchantment struct { // UIQueue

}
type S2C_0x02C8_DispelMultipleEnchantments struct { // UIQueue

}
type S2C_0x02C9_PortalStormBrewing struct { // UIQueue

}
type S2C_0x02CA_PortalStormImminent struct { // UIQueue

}
type S2C_0x02CB_PortalStorm struct { // UIQueue

}
type S2C_0x02CC_PortalStormSubsided struct { // UIQueue

}
type S2C_0x02CD_PrivateUpdateInt struct { // UIQueue

}
type S2C_0x02CE_UpdateInt struct { // UIQueue

}
type S2C_0x02CF_PrivateUpdateInt64 struct { // UIQueue

}
type S2C_0x02D0_UpdateInt64 struct { // UIQueue

}
type S2C_0x02D1_PrivateUpdateBool struct { // UIQueue

}
type S2C_0x02D2_UpdateBool struct { // UIQueue

}
type S2C_0x02D3_PrivateUpdateFloat struct { // UIQueue

}
type S2C_0x02D4_UpdateFloat struct { // UIQueue

}
type S2C_0x02D5_PrivateUpdateString struct { // UIQueue

}
type S2C_0x02D6_UpdateString struct { // UIQueue

}
type S2C_0x02D7_PrivateUpdateDataID struct { // UIQueue

}
type S2C_0x02D8_UpdateDataID struct { // UIQueue

}
type S2C_0x02D9_PrivateUpdateInstanceID struct { // UIQueue

}
type S2C_0x02DA_UpdateInstanceID struct { // UIQueue

}
type S2C_0x02DB_PrivateUpdatePosition struct { // UIQueue

}
type S2C_0x02DC_UpdatePosition struct { // UIQueue

}
type S2C_0x02DD_PrivateUpdateSkill struct { // UIQueue

}
type S2C_0x02DE_UpdateSkill struct { // UIQueue

}
type S2C_0x02DF_PrivateUpdateSkillLevel struct { // UIQueue

}
type S2C_0x02E0_UpdateSkillLevel struct { // UIQueue

}
type S2C_0x02E1_PrivateUpdateSkillAC struct { // UIQueue

}
type S2C_0x02E2_UpdateSkillAC struct { // UIQueue

}
type S2C_0x02E3_PrivateUpdateAttribute struct { // UIQueue

}
type S2C_0x02E4_UpdateAttribute struct { // UIQueue

}
type S2C_0x02E5_PrivateUpdateAttributeLevel struct { // UIQueue

}
type S2C_0x02E6_UpdateAttributeLevel struct { // UIQueue

}
type S2C_0x02E7_PrivateUpdateAttribute2nd struct { // UIQueue

}
type S2C_0x02E8_UpdateAttribute2nd struct { // UIQueue

}
type S2C_0x02E9_PrivateUpdateAttribute2ndLevel struct { // UIQueue

}
type S2C_0x02EA_UpdateAttribute2ndLevel struct { // UIQueue

}
type S2C_0x02EB_TransientString struct { // UIQueue

}
type C2S_0x0311_FinishBarber struct { // Weenie

}
type S2C_0x0312_PurgeBadEnchantments struct { // UIQueue

}
type S2C_0x0314_SendClientContractTrackerTable struct { // UIQueue

}
type S2C_0x0315_SendClientContractTracker struct { // UIQueue

}
type C2S_0x0316_AbandonContract struct { // Weenie

}
type S2C_0xEA60_Environs struct { // UIQueue

}
type S2C_0xF619_PositionAndMovementEvent struct { // SmartBox

}
type C2S_0xF61B_Jump struct { // Weenie

}
type C2S_0xF61C_MoveToState struct { // Weenie

}
type C2S_0xF61E_DoMovementCommand struct { // Weenie

}
type S2C_0xF625_ObjDescEvent struct { // SmartBox

}
type S2C_0xF630_SetPlayerVisualDesc struct { // UIQueue

}
type S2C_0xF643_CharGenVerificationResponse struct { // UIQueue

}
type C2S_0xF649_TurnToEvent struct { // Weenie

}
type S2C_0xF651_AwaitingSubscriptionExpiration struct { // UIQueue

}
type C2S_0xF653_LogOffCharacter struct { // Logon

}
type S2C_0xF653_LogOffCharacter struct { // UIQueue

}
type C2S_0xF655_CharacterDelete struct { // Logon

}
type S2C_0xF655_CharacterDelete struct { // UIQueue

}
type C2S_0xF656_SendCharGenResult struct { // Logon

}
type C2S_0xF657_SendEnterWorld struct { // Logon

}
type S2C_0xF658_LoginCharacterSet struct { // UIQueue
	Status             uint32
	Set                List[*CharacterIdentity]
	DelSet             List[*CharacterIdentity]
	AllowedCharacters  uint32
	Account            PString
	UseTurbineChat     uint32
	HasThroneofDestiny uint32
}

func (t *S2C_0xF658_LoginCharacterSet) UnPack(s *SBuffer) any {
	// t = new(S2C_0xF658_LoginCharacterSet)
	t.Status = ReadT[uint32](s)
	t.Set.UnPack(s)
	t.DelSet.UnPack(s)
	t.AllowedCharacters = ReadT[uint32](s)
	t.Account.UnPack(s)
	t.UseTurbineChat = ReadT[uint32](s)
	t.HasThroneofDestiny = ReadT[uint32](s)
	return t
}

type CharacterIdentity struct {
	ID            uint32  `json:"id"`
	Name          PString `json:"name"`
	DeleteTimeout uint32  `json:"delete_timeout"`
}

func (t *CharacterIdentity) UnPack(s *SBuffer) any {
	// t = new(CharacterIdentity)
	t.ID = ReadT[uint32](s)
	t.Name.UnPack(s)
	t.DeleteTimeout = ReadT[uint32](s)
	return t
}

type S2C_0xF659_CharacterError struct { // UIQueue
	Reason uint32
}

func (t *S2C_0xF659_CharacterError) UnPack(s *SBuffer) any {
	// t = new(S2C_0xF659_CharacterError)
	t.Reason = ReadT[uint32](s)
	return t
}

type C2S_0xF661_StopMovementCommand struct { // Weenie

}
type C2S_0xF6EA_SendForceObjdesc struct { // Control

}
type S2C_0xF745_CreateObject struct { // SmartBox

}
type S2C_0xF746_CreatePlayer struct { // SmartBox

}
type S2C_0xF747_DeleteObject struct { // SmartBox

}
type S2C_0xF748_PositionEvent struct { // SmartBox

}
type S2C_0xF749_ParentEvent struct { // SmartBox

}
type S2C_0xF74A_PickupEvent struct { // SmartBox

}
type S2C_0xF74B_SetState struct { // SmartBox

}
type S2C_0xF74C_SetObjectMovement struct { // SmartBox

}
type S2C_0xF74E_VectorUpdate struct { // SmartBox

}
type S2C_0xF750_SoundEvent struct { // SmartBox

}
type S2C_0xF751_PlayerTeleport struct { // SmartBox

}
type C2S_0xF752_AutonomyLevel struct { // Weenie

}
type C2S_0xF753_AutonomousPosition struct { // Weenie

}
type S2C_0xF754_PlayScriptID struct { // SmartBox

}
type S2C_0xF755_PlayScriptType struct { // SmartBox

}
type S2C_0xF7C1_AccountBanned struct { // UIQueue

}
type C2S_0xF7C8_SendEnterWorldRequest struct { // Logon

}
type C2S_0xF7C9_JumpNonAutonomous struct { // Weenie

}
type S2C_0xF7CA_ReceiveAccountData struct { // UIQueue

}
type S2C_0xF7CB_ReceivePlayerData struct { // UIQueue

}
type C2S_0xF7CC_SendAdminGetServerVersion struct { // Control

}
type C2S_0xF7CD_SendFriendsCommand struct { // Control

}
type C2S_0xF7D9_SendAdminRestoreCharacter struct { // Control

}
type S2C_0xF7DB_UpdateObject struct { // SmartBox
}
type S2C_0xF7DC_AccountBooted struct { // UIQueue
	Reason PString
}
type S2C_0xF7DE_TurbineChat struct { // Logon

}
type C2S_0xF7DE_TurbineChat struct { // Logon

}
type S2C_0xF7DF_EnterGameServerReady struct { // UIQueue
}
type S2C_0xF7E0_TextboxString struct { // UIQueue
	Msg             PString
	ChatMessageType uint32
}
type S2C_0xF7E1_WorldInfo struct { // UIQueue
	Connections    int32
	MaxConnections int32
	WorldName      PString
}

func (t *S2C_0xF7E1_WorldInfo) UnPack(s *SBuffer) any {
	// t = new(S2C_0xF7E1_WorldInfo)
	t.Connections = ReadT[int32](s)
	t.MaxConnections = ReadT[int32](s)
	t.WorldName.UnPack(s)
	return t
}

type S2C_0xF7E2_DataMessage struct { // CLCache
	DatFileType uint32
	DatFileID   uint32
	DID_Type    uint32
	DID_Id      uint32
	Iteration   int32
	Compressed  bool
	Version     int32
	Data        []byte
}

func (t *S2C_0xF7E2_DataMessage) UnPack(s *SBuffer) any {
	// t = new(S2C_0xF7E2_DataMessage)
	t.DatFileType = ReadT[uint32](s)
	t.DatFileID = ReadT[uint32](s)
	t.DID_Type = ReadT[uint32](s)
	t.DID_Id = ReadT[uint32](s)
	t.Iteration = ReadT[int32](s)
	t.Compressed = ReadT[byte](s) == 1
	t.Version = ReadT[int32](s)
	t.Data = ReadTs[byte](s, int(ReadT[int32](s)))
	return t
}

type C2S_0xF7E3_RequestDataMessage struct { // CLCache
	DID_ID   uint32
	DID_Type uint32
}

func (t *C2S_0xF7E3_RequestDataMessage) UnPack(s *SBuffer) any {
	t.DID_ID = ReadT[uint32](s)
	t.DID_Type = ReadT[uint32](s)
	return t
}

type S2C_0xF7E4_ErrorMessage struct { // CLCache
	DID_ID   uint32
	DID_Type uint32
	Error    uint32
}

func (t *S2C_0xF7E4_ErrorMessage) UnPack(s *SBuffer) any {
	t.DID_ID = ReadT[uint32](s)
	t.DID_Type = ReadT[uint32](s)
	t.Error = ReadT[uint32](s)
	return t
}

type S2C_0xF7E5_InterrogationMessage struct { // CLCache
	ServerRegion   uint32
	NameRuleLang   uint32
	PID            uint32
	SupportedLangs List[*Uint32]
}

func (t *S2C_0xF7E5_InterrogationMessage) UnPack(s *SBuffer) any {
	// t = new(S2C_0xF7E5_InterrogationMessage)
	t.ServerRegion = ReadT[uint32](s)
	t.NameRuleLang = ReadT[uint32](s)
	t.PID = ReadT[uint32](s)
	t.SupportedLangs.UnPack(s)
	return t
}

type C2S_0xF7E6_InterrogationResponseMessage struct { // CLCache
	ClientLanguage   uint32
	FilesWithKeys    List[*Tagged_Iteration]
	FilesWithoutKeys List[*Tagged_Iteration]
	Flags            uint32
}

type Tagged_Iteration struct {
	DatFileType uint32
	DatFileID   uint32
	Iteration   Dat_Iteration
}

func (t *Tagged_Iteration) UnPack(s *SBuffer) any {
	// t = new(Tagged_Iteration)
	t.DatFileType = ReadT[uint32](s)
	t.DatFileID = ReadT[uint32](s)
	t.Iteration.UnPack(s)
	return t
}

type S2C_0xF7E7_BeginDDDMessage struct { // CLCache
	DataExpected      uint32
	MissingIterations List[*MissingIteration]
}

type MissingIteration struct {
	DatFileID     uint32
	Iteration     int32
	IDsToDownload List[*Uint32]
	IDsToPurge    List[*Uint32]
}

func (t *MissingIteration) UnPack(s *SBuffer) any {
	// t = new(MissingIteration)
	t.DatFileID = ReadT[uint32](s)
	t.Iteration = ReadT[int32](s)
	t.IDsToDownload.UnPack(s)
	t.IDsToPurge.UnPack(s)
	return t
}

type C2S_0xF7EA_OnEndDDD struct { // CLCache
}

func (t *S2C_0xF7EA_OnEndDDD) Pack(s *SBuffer) any {
	return t
}

type S2C_0xF7EA_OnEndDDD struct { // CLCache
}

func (t *S2C_0xF7EA_OnEndDDD) UnPack(s *SBuffer) any {
	// t = new(S2C_0xF7EA_OnEndDDD)
	return t
}

type C2S_0xF7EB_EndDDDMessage struct { // CLCache

}
