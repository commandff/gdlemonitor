// //////////////////////////////////////////////////////////////
//
//    SpawnMaps - GDLE spawnMaps/worldspawns.json parser
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"

	"gitlab.com/commandff/gdlemonitor/aids"
)

var SpawnMaps = make(map[uint32]*HashMapData[*CLandBlockExtendedData])
var SpawnMaps_m sync.Mutex
var SpawnMap_IDs = make([]uint32, 0)

// SpawnMap_Parse("C:\\gdle\\Data\\json")
// SpawnMap_Stats()
func SpawnMap_Stats() {
	aids.InfoLog.Printf("Total SpawnMaps: SpawnMaps: %d SpawnMap_IDs: %d, 0x%08X thru 0x%08X\n", len(SpawnMaps), len(SpawnMap_IDs), SpawnMap_IDs[0], SpawnMap_IDs[len(SpawnMap_IDs)-1])
}

// Parse worldspawns.json, and spawnMaps/*.json (recursively)
func SpawnMap_Parse(root string) error {
	SpawnMaps_m.Lock()
	defer SpawnMaps_m.Unlock()
	// SpawnMaps = make(map[uint32]*CLandBlockExtendedDataHash)
	// SpawnMap_IDs = make([]uint32, 0)

	file := filepath.Join(root, "worldspawns.json")
	content, err := os.ReadFile(file)
	if err == nil {
		var w CLandBlockExtendedDataTable
		if err := json.Unmarshal(content, &w); err != nil {
			aids.ErrorLog.Printf("ERROR: failed to parse %s: %v", file, err)
		} else {
			aids.InfoLog.Printf("%s Version %s, %d landblocks.\n", file, w.Version, len(w.LandBlocks))
			for _, spawnMap := range w.LandBlocks {
				SpawnMap_Add(&spawnMap)
			}
		}
	} else {
		aids.ErrorLog.Printf("ERROR: Unable to read worldspawns.json(%s): %s\n", file, err)
	}

	recurseParseSpawnMap(filepath.Join(root, "spawnMaps"))

	sort.SliceStable(SpawnMap_IDs, func(i, j int) bool {
		return SpawnMap_IDs[i] < SpawnMap_IDs[j]
	})
	return nil
}

// Recursively parse .json files, as GDLE SpawnMaps, inside folder `path`
func recurseParseSpawnMap(path string) {
	edir, err := os.ReadDir(path)
	if err != nil {
		log.Printf("Could not open \"%s\" folder: %v", path, err)
		return
	}
	for _, e := range edir {
		file := filepath.Join(path, e.Name())
		if e.IsDir() {
			recurseParseSpawnMap(file)
			continue
		}
		info, _ := e.Info()
		if info.Size() == 0 {
			continue
		}
		if !strings.HasSuffix(file, ".json") {
			continue
		}
		content, err := os.ReadFile(file)
		if err == nil {
			var w HashMapData[*CLandBlockExtendedData]
			if err := json.Unmarshal(content, &w); err != nil {
				aids.ErrorLog.Printf("ERROR: failed to parse %s: %v", file, err)
				continue
			}
			SpawnMap_Add(&w)
		} else {
			aids.ErrorLog.Printf("ERROR: Unable to read spawnMap %s: %s\n", file, err)
		}
	}
}

func SpawnMap_Add(spawnMap *HashMapData[*CLandBlockExtendedData]) {
	if _, ok := SpawnMaps[spawnMap.Key]; ok {
		log.Printf("ERROR: duplicate spawnMap %#04X", spawnMap.Key>>16)
	} else {
		SpawnMap_IDs = append(SpawnMap_IDs, spawnMap.Key)
	}
	SpawnMaps[spawnMap.Key] = spawnMap
	//aids.InfoLog.Printf("loaded %s: %#04X, %d links, %d weenies\n", file, w.Key>>16, len(w.Value.Links), len(w.Value.Weenies))
}

type CLandBlockExtendedDataTable struct {
	Version    string                           `json:"_version"`
	LandBlocks HashMap[*CLandBlockExtendedData] `json:"landblocks"`
}

type CLandBlockExtendedData struct {
	Weenies List[*CLandBlockWeenie]     `json:"weenies"`
	Links   List[*CLandBlockWeenieLink] `json:"links"`
}

func (t *CLandBlockExtendedData) UnPack(s *SBuffer) any {
	t = new(CLandBlockExtendedData)
	t.Weenies.UnPack(s)
	t.Links.UnPack(s)
	return t
}

type CLandBlockWeenie struct {
	WCID uint32   `json:"wcid"`
	Pos  Position `json:"pos"`
	IID  uint32   `json:"id"`
	Desc *string  `json:"desc,omitempty"`
}

func (t *CLandBlockWeenie) UnPack(s *SBuffer) any {
	t = new(CLandBlockWeenie)
	t.WCID = ReadT[uint32](s)
	t.Pos.UnPack(s)
	t.IID = ReadT[uint32](s) & 0x0FFF
	return t
}

type CLandBlockWeenieLink struct {
	Source uint32 `json:"source"`
	Target uint32 `json:"target"`
}

func (t *CLandBlockWeenieLink) UnPack(s *SBuffer) any {
	t = new(CLandBlockWeenieLink)
	t.Source = ReadT[uint32](s) & 0x0FFF
	t.Target = ReadT[uint32](s) & 0x0FFF
	return t
}

// RegionData (gathered from cache.bin)
type RegionData struct {
	TotalObjects    uint32                 `json:"tableSize"`
	EncounterTables *List[*EncounterTable] `json:"encounters"`
	EncounterMap    EncounterMap           `json:"encounterMap"`
}
type EncounterMap []uint8

func (t *EncounterMap) MarshalJSON() ([]byte, error) {
	log.Printf("doop")
	return []byte(strings.Join(strings.Fields(fmt.Sprintf("%d", *t)), ",")), nil
}
func (t *RegionData) UnPack(s *SBuffer) any {
	// t = new(RegionData)
	startLen := s.Len()
	t.TotalObjects = ReadT[uint32](s)
	t.EncounterTables = new(List[*EncounterTable]).UnPack(s).(*List[*EncounterTable])
	t.EncounterMap = ReadTs[uint8](s, 65025) // 255 * 255
	align := (startLen - s.Len()) & 3
	if align > 0 {
		ReadTs[byte](s, (4 - align))
	}
	// log.Printf("RegionData.UnPack(%d...%d): %s", startLen, s.Len(), ToJSON(t))
	return t
}

type EncounterTable struct {
	Index  uint32     `json:"key"`
	Values [16]uint32 `json:"value"`
}

func (t *EncounterTable) UnPack(s *SBuffer) any {
	t = new(EncounterTable)
	binary.Read(s, binary.LittleEndian, t)
	return t
}
