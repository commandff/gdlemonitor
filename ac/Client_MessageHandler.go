// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"bytes"
	"fmt"
)

type Message struct {
	QueueID uint16
	buf     *SBuffer
}

func (m *Message) handle() {
	if 4 > m.buf.Len() {
		acclient.Logger.Printf("   messageHandler not enough bytes\n")
		return
	}
	// opcode := binary.LittleEndian.Uint32(m.buf.Bytes()[:4])
	opcode := ReadT[uint32](m.buf)
	var msg _Object
	switch opcode {
	case 0xF658: // Character List
		msg = new(S2C_0xF658_LoginCharacterSet).UnPack(m.buf).(*S2C_0xF658_LoginCharacterSet)
	case 0xF7E1: // WorldInfo
		msg = new(S2C_0xF7E1_WorldInfo).UnPack(m.buf).(*S2C_0xF7E1_WorldInfo)
	case 0xF7E5: // InterrogationMessage
		msg = new(S2C_0xF7E5_InterrogationMessage).UnPack(m.buf).(*S2C_0xF7E5_InterrogationMessage)
		acclient.sendInterrogationMessage()
	case 0xF659: // CharacterError
		msg = new(S2C_0xF659_CharacterError).UnPack(m.buf).(*S2C_0xF659_CharacterError)
	case 0xF7EA: // EndDDD
		msg = new(S2C_0xF7EA_OnEndDDD).UnPack(m.buf).(*S2C_0xF7EA_OnEndDDD)
		acclient.sendEndDDDMessage()

	default:
		if m.buf.Len() > 128 {
			acclient.Logger.Printf(" TODO:  (queue:%d,opcode:%04X,len:%d) % x ...\n", m.QueueID, opcode, m.buf.Len(), m.buf.Bytes()[:128])
		} else {
			acclient.Logger.Printf(" TODO:  (queue:%d,opcode:%04X,len:%d) % x\n", m.QueueID, opcode, m.buf.Len(), m.buf.Bytes())
		}
	}
	acclient.Logger.Printf("RCVD 0x%04x: %s", opcode, ToJSON(msg))
}
func (c *ACclient) sendEndDDDMessage() {
	c.makeFrags(bytes.NewBuffer([]byte{0xEA, 0xF7, 0x00, 0x00}), 5)
}
func (c *ACclient) sendInterrogationMessage() {
	c.makeFrags(bytes.NewBuffer([]byte{
		0xE6, 0xF7,
		0x00, 0x00,
		0x01, 0x00, 0x00, 0x00,
		0x03, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x18, 0x08, 0x00, 0x00, 0xE8, 0xF7, 0xFF, 0xFF, 0x01, 0x00, 0x00, 0x00,
		0x01, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xED, 0x01, 0x00, 0x00, 0x13, 0xFE, 0xFF, 0xFF, 0x01, 0x00, 0x00, 0x00,
		0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0xD6, 0x03, 0x00, 0x00, 0x2A, 0xFC, 0xFF, 0xFF, 0x01, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}), 5)
}

var characterErrorType = map[uint32]string{0x00000001: "Logon", 0x00000003: "AccountLogin", 0x00000004: "ServerCrash1", 0x00000005: "Logoff", 0x00000006: "Delete", 0x00000008: "ServerCrash2", 0x00000009: "AccountInvalid", 0x0000000A: "AccountDoesntExist", 0x0000000B: "EnterGameGeneric", 0x0000000C: "EnterGameStressAccount", 0x0000000D: "EnterGameCharacterInWorld", 0x0000000E: "EnterGamePlayerAccountMissing", 0x0000000F: "EnterGameCharacterNotOwned", 0x00000010: "EnterGameCharacterInWorldServer", 0x00000011: "EnterGameOldCharacter", 0x00000012: "EnterGameCorruptCharacter", 0x00000013: "EnterGameStartServerDown", 0x00000014: "EnterGameCouldntPlaceCharacter", 0x00000015: "LogonServerFull", 0x00000017: "EnterGameCharacterLocked", 0x00000018: "SubscriptionExpired"}

func (c *ACclient) CharacterErrorType(reason uint32) string {
	msg, ok := characterErrorType[reason]
	if !ok {
		msg = fmt.Sprintf("Unknown Error %08X", reason)
	}
	return msg
}
