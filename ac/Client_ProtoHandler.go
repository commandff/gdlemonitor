// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"strings"
	"time"
)

type ProtoHeader struct {
	SeqID     uint32
	Header    uint32
	Checksum  uint32
	RecID     uint16
	Interval  uint16
	Datalen   uint16
	Iteration uint16
}

const (
	pHeader_None               = 0x00000000
	pHeader_RESEND             = 0x00000001
	pHeader_EncCRC             = 0x00000002 // can't be paired with 0x00000001, see FlowQueue::DequeueAck
	pHeader_FRAG               = 0x00000004
	pHeader_ServerSwitch       = 0x00000100 // Server Switch
	pHeader_LogonServerAddr    = 0x00000200 // Logon Server Addr
	pHeader_EmptyHeader1       = 0x00000400 // Empty Header 1
	pHeader_Referral           = 0x00000800 // Referral
	pHeader_NAK                = 0x00001000 // Nak
	pHeader_EACK               = 0x00002000 // Empty Ack
	pHeader_PAK                = 0x00004000 // Pak
	pHeader_Disconnect         = 0x00008000 // Disconnect
	pHeader_LoginRequest       = 0x00010000 // Login
	pHeader_WorldLoginRequest  = 0x00020000 // ULong 1
	pHeader_ConnectRequest     = 0x00040000 // Connect
	pHeader_ConnectResponse    = 0x00080000 // ULong 2
	pHeader_NetError           = 0x00100000 // Net Error
	pHeader_NetErrorDisconnect = 0x00200000 // Net Error Disconnect
	pHeader_CICMDCommand       = 0x00400000 // ICmd
	pHeader_EmptyHeader2       = 0x00800000 // Empty Header 2
	pHeader_TIME               = 0x01000000 // Time Sync
	pHeader_PING               = 0x02000000 // Echo Request
	pHeader_PONG               = 0x04000000 // Echo Response
	pHeader_Flow               = 0x08000000 // Flow

)

func (c *ACclient) processPacket(ip *InboundPacket) {
	if ip.Header.Header&pHeader_RESEND != 0 {
		c.Logger.Printf("    TODO:  RESEND\n")
	}
	if ip.Header.Header&pHeader_EncCRC != 0 {
		expectedKey := c.Inbound.Crypto.GetIndex(ip.Header.SeqID - 2)
		if ip.Checksum != expectedKey {
			c.Logger.Printf("   EncCRC INVALID rcvd %08X, expected %08X\n", ip.Checksum, expectedKey)
			return
		}
		c.Inbound.HighestID = ip.Header.SeqID + 1

	} else {
		if ip.Checksum != 0 {
			c.Logger.Printf("   CRC INVALID rcvd %08X\n", ip.Checksum)
			return
		}
	}
	c.Inbound.Bytes += ip.packetLen
	c.Outbound.LastInterval = ip.Header.Interval
	c.LastReceivedData = time.Now()
	if ip.Header.Header&pHeader_ServerSwitch != 0 {
		seqID := ReadT[uint32](ip.buf)
		changeType := ReadT[uint32](ip.buf)
		c.Logger.Printf("   ServerSwitch: seq:%d type:%d\n", seqID, changeType)
	}
	if ip.Header.Header&pHeader_LogonServerAddr != 0 {
		addr := *new(SockAddr)
		addr.UnPack(ip.buf)
		c.Logger.Printf("  TODO:  LogonServerAddr: %s\n", &addr)
	}
	if ip.Header.Header&pHeader_EmptyHeader1 != 0 {
		c.Logger.Printf("  TODO:  EmptyHeader1\n")
	}
	if ip.Header.Header&pHeader_Referral != 0 {
		Cookie := ReadT[uint64](ip.buf)
		addr := *new(SockAddr)
		addr.UnPack(ip.buf)
		c.Logger.Printf("  TODO:  Referral: Cookie:%16x %s\n", Cookie, &addr)
	}
	if ip.Header.Header&pHeader_NAK != 0 {
		num := ReadT[uint32](ip.buf)
		naks := ReadTs[uint32](ip.buf, int(num))
		defer c.processNAKs(naks)
	}
	if ip.Header.Header&pHeader_EACK != 0 {
		num := ReadT[uint32](ip.buf)
		eacks := ReadTs[uint32](ip.buf, int(num))
		c.Logger.Printf("  TODO:  EACK: num:%d ids:%v\n", num, eacks)
	}
	if ip.Header.Header&pHeader_PAK != 0 {
		pak := ReadT[uint32](ip.buf)
		c.queueFlush(pak)
	}
	if ip.Header.Header&pHeader_Disconnect != 0 {
		c.Logger.Printf("  TODO:  Disconnect\n")
	}
	if ip.Header.Header&pHeader_WorldLoginRequest != 0 {
		Cookie := ReadT[uint64](ip.buf)
		c.Logger.Printf("  TODO:     WorldLoginRequest: %16x\n", Cookie)
	}
	if ip.Header.Header&pHeader_ConnectRequest != 0 {
		ServerTime := ReadT[float64](ip.buf)
		c.ReferralCookie = ReadT[uint64](ip.buf)
		c.NetID = uint16(ReadT[uint32](ip.buf))
		InSeed := ReadT[uint32](ip.buf)
		OutSeed := ReadT[uint32](ip.buf)

		for ip.buf.Len()%8 != 0 {
			ReadTsP[byte](ip.buf, 7-(ip.buf.Len()%8))
		}

		c.Iteration = ip.Header.Iteration
		c.Logger.Printf("  ConnectRequest: ServerTime:%f,Cookie:%16x,NetId:%d,InSeed:%08X,OutSeed:%08X\n", ServerTime, c.ReferralCookie, c.NetID, InSeed, OutSeed)
		c.Inbound.Crypto.Init(InSeed)
		c.Inbound.HighestID = 2
		c.Outbound.Crypto.Init(OutSeed)
		c.Outbound.HighestID = 1
		defer c.sendConnectResponse()
	}

	if ip.Header.Header&pHeader_ConnectResponse != 0 {
		Cookie := ReadT[uint64](ip.buf)
		c.Logger.Printf("  TODO:  ConnectResponse: Cookie:%16x\n", Cookie)
	}
	if ip.Header.Header&pHeader_NetError != 0 {
		stringID := ReadT[uint32](ip.buf)
		tableID := ReadT[uint32](ip.buf)
		c.Logger.Printf("  TODO:  NetError: table:%8x string:%8x\n", tableID, stringID)
	}
	if ip.Header.Header&pHeader_NetErrorDisconnect != 0 {
		stringID := ReadT[uint32](ip.buf)
		tableID := ReadT[uint32](ip.buf)
		c.Logger.Printf("    TODO:   NetErrorDisconnect: table:%8x string:%8x\n", tableID, stringID)
	}
	if ip.Header.Header&pHeader_CICMDCommand != 0 {
		Cmd := ReadT[uint32](ip.buf)
		Param := ReadT[uint32](ip.buf)
		c.Logger.Printf("  TODO:  CICMDCommand: Cmd:%8x Param:%8x\n", Cmd, Param)
	}
	if ip.Header.Header&pHeader_TIME != 0 {
		Time := ReadT[float64](ip.buf)
		c.Logger.Printf("  TODO:  TIME: %f - %f\n", Time, c.Time())
	}
	if ip.Header.Header&pHeader_PING != 0 {
		c.PingTime = ReadT[float32](ip.buf)
	}
	if ip.Header.Header&pHeader_PONG != 0 {
		Time := ReadT[float32](ip.buf)
		HoldTime := ReadT[float32](ip.buf)
		c.Logger.Printf("  TODO:  PONG: %f hold:%f\n", Time, HoldTime)
	}
	if ip.Header.Header&pHeader_Flow != 0 {
		if 6 > ip.buf.Len() {
			c.Logger.Printf("   Flow not enough bytes\n")
			return
		}
		/// .... what?
		dataRCVD := ReadTP[uint32](ip.buf)
		//binary.Read(ip.buf, binary.LittleEndian, &dataRCVD)
		flowInt := ReadTP[uint16](ip.buf)
		//binary.Read(ip.buf, binary.LittleEndian, &flowInt)
		c.Logger.Printf("  TODO:  Flow: rcvd:%d int:%d\n", dataRCVD, flowInt)
	}
	if ip.Header.Header&pHeader_FRAG != 0 {
		if c.handleFrags(ip.buf) {
			return
		}
	}
}
func ProtoHeaderFlags(header uint32) string {
	var resu []string
	if header&pHeader_RESEND != 0 {
		resu = append(resu, "RESEND")
	}
	if header&pHeader_ServerSwitch != 0 {
		resu = append(resu, "ServerSwitch")
	}
	if header&pHeader_LogonServerAddr != 0 {
		resu = append(resu, "LogonServerAddr")
	}
	if header&pHeader_EmptyHeader1 != 0 {
		resu = append(resu, "EmptyHeader1")
	}
	if header&pHeader_Referral != 0 {
		resu = append(resu, "Referral")
	}
	if header&pHeader_NAK != 0 {
		resu = append(resu, "NAK")
	}
	if header&pHeader_EACK != 0 {
		resu = append(resu, "EACK")
	}
	if header&pHeader_PAK != 0 {
		resu = append(resu, "PAK")
	}
	if header&pHeader_Disconnect != 0 {
		resu = append(resu, "Disconnect")
	}
	if header&pHeader_LoginRequest != 0 {
		resu = append(resu, "LoginRequest")
	}
	if header&pHeader_WorldLoginRequest != 0 {
		resu = append(resu, "WorldLoginRequest")
	}
	if header&pHeader_ConnectRequest != 0 {
		resu = append(resu, "ConnectRequest")
	}
	if header&pHeader_ConnectResponse != 0 {
		resu = append(resu, "ConnectResponse")
	}
	if header&pHeader_NetError != 0 {
		resu = append(resu, "NetError")
	}
	if header&pHeader_NetErrorDisconnect != 0 {
		resu = append(resu, "NetErrorDisconnect")
	}
	if header&pHeader_CICMDCommand != 0 {
		resu = append(resu, "CICMDCommand")
	}
	if header&pHeader_EmptyHeader2 != 0 {
		resu = append(resu, "EmptyHeader2")
	}
	if header&pHeader_TIME != 0 {
		resu = append(resu, "TIME")
	}
	if header&pHeader_PING != 0 {
		resu = append(resu, "PING")
	}
	if header&pHeader_PONG != 0 {
		resu = append(resu, "PONG")
	}
	if header&pHeader_Flow != 0 {
		resu = append(resu, "Flow")
	}
	if header&pHeader_FRAG != 0 {
		resu = append(resu, "FRAG")
	}
	if header&pHeader_EncCRC != 0 {
		resu = append(resu, "EncCRC")
	}
	return strings.Join(resu, "|")
}

func (p *ProtoHeader) String() string {
	return fmt.Sprintf("SeqID:%d,header:%s,RecID:%d,Interval:%d,datalen:%d,Iteration:%d",
		p.SeqID, ProtoHeaderFlags(p.Header), p.RecID, p.Interval, p.Datalen, p.Iteration)
}

func (c *ACclient) sendProtoHeader(header uint32, payloadData *bytes.Buffer) {
	pHeader := ProtoHeader{
		SeqID:     c.Outbound.HighestID,
		Header:    header,
		RecID:     c.NetID,
		Interval:  c.Interval(),
		Datalen:   uint16(payloadData.Len()),
		Iteration: c.Iteration,
	}

	optionals := new(bytes.Buffer)
	pHeader.Header |= c.flushOptionals(optionals)

	if pHeader.Header == 0 {
		return
	}

	if pHeader.Header&pHeader_EncCRC != 0 {
		c.Outbound.HighestID++
		pHeader.SeqID = c.Outbound.HighestID
		pHeader.Checksum += c.Outbound.Crypto.GetIndex(c.Outbound.HighestID - 1)
	}
	pHeader.Datalen += uint16(optionals.Len())

	netBuffer := new(bytes.Buffer)
	binary.Write(netBuffer, binary.LittleEndian, pHeader)
	optionals.WriteTo(netBuffer)
	if payloadData != nil {
		payloadData.WriteTo(netBuffer)
	}
	bufa := netBuffer.Bytes()
	checksum := PacketChecksum(bufa)
	bufa[8] = byte(checksum)
	bufa[9] = byte(checksum >> 8)
	bufa[10] = byte(checksum >> 16)
	bufa[11] = byte(checksum >> 24)

	//acclient.Logger.Printf("\033[33mSending: (%d) %s\033[0m", len(bufa), &pHeader)
	c.Outbound.Bytes += len(bufa)
	if pHeader.Header&pHeader_EncCRC != 0 {
		c.SentPackets[pHeader.SeqID] = bufa
		// }
		// 	if pHeader.SeqID == 5 {
		// 		acclient.Logger.Printf("\033[31mmy bad, forgot to press send on %s.\033[0m", &pHeader)
		// 	} else {
		// 		acclient.Logger.Printf("\033[33mSending: (%d) %s\n%s\033[0m", len(bufa), &pHeader, hex.Dump(bufa))
		// 		c.ServerConn.Write(bufa)
		// 	}
		// } else {
	}
	c.ServerConn.Write(bufa)
}

func (c *ACclient) flushOptionals(b *bytes.Buffer) (ret uint32) {

	//PAK
	if c.Outbound.HighestID > 1 && c.Inbound.HighestID-1 > c.Inbound.LastPAK && c.Time() > c.Inbound.NextPAK {
		c.Inbound.LastPAK = c.Inbound.HighestID - 1
		if c.Inbound.HighestID > 2 {
			binary.Write(b, binary.LittleEndian, c.Inbound.HighestID-1)
			c.Inbound.NextPAK = c.Time() + 6
			ret += pHeader_PAK
		}
	}

	//TIME
	if time.Since(c.LastPinged) > time.Second*120 {
		c.LastPinged = time.Now()
		binary.Write(b, binary.LittleEndian, c.Time())
		ret += pHeader_TIME
	}

	//PING
	if time.Since(c.LastReceivedData) > time.Second*2 && time.Since(c.LastPinged) > time.Second*10 {
		c.LastPinged = time.Now()
		binary.Write(b, binary.LittleEndian, c.UpTime())
		ret += pHeader_PING
	}

	//PONG
	if c.PingTime != 0 {
		binary.Write(b, binary.LittleEndian, c.PingTime)
		c.PingTime = 0
		ret += pHeader_PONG
	}

	//FLOW
	if c.Outbound.LastInterval-c.Outbound.AckedInterval > 6 {
		if c.Outbound.AckedInterval > 0 {
			binary.Write(b, binary.LittleEndian, uint32(c.Inbound.Bytes))
			binary.Write(b, binary.LittleEndian, c.Outbound.AckedInterval)
			//binary.Write(b, binary.LittleEndian, uint16(0))
			ret += pHeader_Flow | pHeader_EncCRC
		}
		c.Outbound.AckedInterval = c.Outbound.LastInterval
	}

	return
}

/*

var Reset  = "\033[0m"
var Red    = "\033[31m"
var Green  = "\033[32m"
var Yellow = "\033[33m"
var Blue   = "\033[34m"
var Purple = "\033[35m"
var Cyan   = "\033[36m"
var Gray   = "\033[37m"
var White  = "\033[97m"

*/
