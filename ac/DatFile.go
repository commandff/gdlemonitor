// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"sync"
	"unsafe"
)

// Cell.Parse("C:\\Projects\\")
// Cell.Stats()
// ent, contentany := Cell.GetDIDObj(0xBBA0FFFF)
// content := contentany.(*ac.Dat_LandBlock)
// log.Printf("DumpDID(0xBBA0FFFF): %s\n\t%s", ent.ToString(&Cell), ac.ToJSONFormat(content, "", "  "))
// CacheBin.Load("cache.bin")
// Portal.Parse("C:\\Projects\\")
// Portal.Stats()
// for _, did := range Portal.PortalCat[0x06] {
// 	//ent, content := Portal.GetDIDObj(Portal.PortalCat[0x06][rand.Int63()%int64(len(Portal.PortalCat[0x06]))])
// 	_, content := Portal.GetDIDObj(did)
// 	object := content.(*ac.Dat_Texture)
// 	_, _ = object.ToPNG()
// 	// log.Printf("DumpDID(0x%08X): %s\n\t%s", ent.GID, ent.ToString(&Portal), hdr)
// }
// OpenDat("C:\\gdle\\Data\\client_portal.dat", PortalDat)
// Portal.Parse("C:\\Projects\\")
// Portal.Stats()

// unknownVals := make(map[uint32]int)
// for _, did := range Portal.PortalCat[6] {
// 	_, content := Portal.GetDIDObj(did)
// 	object := content.(*Dat_Texture)
// 	unknownVals[object.Unknown]++
// 	formatVals[object.Format]++
// 	//log.Printf("DumpDID(0x%08X): %s\n\t%s", did, ent.ToString(&Portal), ToJSON(object))
// }
// log.Printf("unknownVals:%#v, formatVals:%#v", unknownVals, formatVals)
//formatVals := make(map[uint32]int)
// unknownVals = make(map[uint32]int)
// formatVals = make(map[uint32]int)
// for _, did := range HighRes.PortalCat[6] {
// 	_, content := HighRes.GetDIDObj(did)
// 	object := content.(*Dat_Texture)
// 	unknownVals[object.Unknown]++
// 	formatVals[object.Format]++
// 	//log.Printf("DumpDID(0x%08X): %s\n\t%s", did, ent.ToString(&Portal), ToJSON(object))
// }
// log.Printf("unknownVals:%#v, formatVals:%#v", unknownVals, formatVals)
//2023/01/18 23:07:37 Total PortalCat 0x06: 2294: [06003797 0600379C 060037A4 060037BA 060037C3 06003801 06003803 06003805 06003867 06003869] ... [060072D3 060072D5 060073A7 060073A8 060073A9 060073AA 060073AB 060073AC 060073AD 060073AE]

// LocalEnglish.Parse("C:\\Projects\\")
// LocalEnglish.Stats()
// Portal.DumpDID(0xFFFF0001)
// Cell.DumpDID(0xFFFF0001)
// HighRes.DumpDID(0xFFFF0001)
// LocalEnglish.DumpDID(0xFFFF0001)
//Cell.DumpJSON(1491533823)
//Cell.DumpJSON(0x0001FFFF)FFFF0001
//Cell.DumpDID(0xFA220119)
// Cell.DumpDID(0x6BE5FFFF)
// Cell.DumpDID(0x6BE6FFFF)
// Cell.DumpDID(0x6CE5FFFF)
// Cell.DumpDID(0x6CE6FFFF)
//log.Printf("cell:\n%s", ToJSON(&Cell))
// Cell.DumpDID(0x58E6FFFF)
// Portal.DumpDID(0x06001B21)
// Portal.DumpDID(0x0E000004)

// ac.Dat Public Interfaces:
var Portal DatFile = DatFile{Filename: "client_portal.dat"}
var Cell DatFile = DatFile{Filename: "client_cell_1.dat"}
var HighRes DatFile = DatFile{Filename: "client_highres.dat"}
var LocalEnglish DatFile = DatFile{Filename: "client_local_English.dat"}

type DatFile struct {
	M                 sync.Mutex          `json:"-"`
	Dat               map[uint32]*BTEntry `json:"-"` // `json:"dat"`
	Tree              map[uint32]*BTNode  `json:"-"`
	F                 *os.File            `json:"-"`
	Filename          string              `json:"file_name"`
	LoadedPath        string              `json:"loaded_path"`
	NumDIDs           uint32              `json:"num_dids"`
	DIDs              []uint32            `json:"-"` // `json:"dids"`
	NumLandBlocks     uint32              `json:"num_landblocks"`
	LandBlocks        []uint16            `json:"-"` // `json:"landblocks"`
	NumLandBlockInfos uint32              `json:"num_landblockinfos"`
	LandBlockInfos    []uint16            `json:"-"` // `json:"landblockinfos"`
	NumEnvCells       uint32              `json:"num_envcells"`
	EnvCells          []uint32            `json:"-"` // `json:"envcells"`
	PortalCat         map[byte][]uint32   `json:"-"` // `json:"portal_categories"`
	Header            *DiskFileInfo_t     `json:"header"`
	Iteration         Dat_Iteration       `json:"iteration"`
}

func (dat *DatFile) recurseBTree(offset uint32) error {
	thisNode := new(BTNode)
	dat.Tree[offset] = thisNode
	buf := dat.ReadDat(offset, int(unsafe.Sizeof(*thisNode)))
	if buf.Len() == 0 {
		return fmt.Errorf("failed to Fetch node %d %s", offset, dat.LoadedPath)
	}
	err := binary.Read(buf, binary.LittleEndian, thisNode)
	if err != nil {
		return fmt.Errorf("failed to Decode node 0x%08X %s: %v -- Read %db from %s as Node expected %d", offset, dat.LoadedPath, err, buf.Len(), dat.LoadedPath, unsafe.Sizeof(*thisNode))
	}
	//iterate child nodes
	for i, nn := range thisNode.NextNode {
		if i >= int(thisNode.NumEntries)+1 {
			break
		}
		if nn == 0 || nn == 0xCDCDCDCD {
			break
		}
		if _, ok := dat.Tree[nn]; !ok {
			err := dat.recurseBTree(nn)
			if err != nil {
				log.Println(err)
				break
			}
		}
	}
	//iterate file entries in this node
	for i, ent := range thisNode.Entries {
		if uint32(i) >= thisNode.NumEntries {
			break
		}
		if _, ok := dat.Dat[ent.GID]; !ok {
			if ent.GID == 0xFFFF0001 {
				dat.Iteration.UnPack(dat.ReadDat(ent.Offset, int(ent.Size)))
				continue
			}
			dat.Dat[ent.GID] = &thisNode.Entries[i] // add file entry
			dat.DIDs = append(dat.DIDs, ent.GID)
			if dat.Header.DataSetLM == 2 {
				if (ent.GID & 0x0000FFFF) == 0x0000FFFF {
					dat.LandBlocks = append(dat.LandBlocks, uint16(ent.GID>>16))
				} else if (ent.GID & 0x0000FFFF) == 0x0000FFFE {
					dat.LandBlockInfos = append(dat.LandBlockInfos, uint16(ent.GID>>16))
				} else {
					dat.EnvCells = append(dat.EnvCells, ent.GID)
				}
			} else {
				dat.PortalCat[byte(ent.GID>>24)] = append(dat.PortalCat[byte(ent.GID>>24)], ent.GID)
			}
		}
	}
	return nil
}

// returns pointer to entry, and a raw byte array of file contents.
// returns nil, nil on not found.
func (dat *DatFile) GetDIDFile(did uint32) (*BTEntry, []byte) {
	if obj, ok := dat.Dat[did]; ok {
		buf := dat.readDat(obj.Offset, int(obj.Size))
		return obj, buf
	} else {
		return nil, nil
	}
}

// case 0x00: // ???

func (dat *DatFile) Get_Iteration() (obj *BTEntry, ret *Dat_Iteration) {
	var content []byte
	obj, content = dat.GetDIDFile(0xFFFF0001)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// valid did == landblock_id + 0x0000FFFF && dat.Header.DataSetLM == 2
func (dat *DatFile) Get_LandBlock(did uint32) (obj *BTEntry, ret *Dat_LandBlock) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// valid did == landblockinfo_id + 0x0000FFFE && dat.Header.DataSetLM == 2
func (dat *DatFile) Get_LandBlockInfo(did uint32) (obj *BTEntry, ret *Dat_LandBlockInfo) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// valid dat.Header.DataSetLM == 2
func (dat *DatFile) Get_EnvCell(did uint32) (obj *BTEntry, ret *Dat_EnvCell) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x01: // GraphicsObject

// case 0x02: // Setup
// valid did == 0x02xxxxxx && dat.Header.DataSetLM != 2
func (dat *DatFile) Get_Setup(did uint32) (obj *BTEntry, ret *Dat_Setup) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x03: // Animation
// valid did == 0x03xxxxxx && dat.Header.DataSetLM != 2
func (dat *DatFile) Get_Animation(did uint32) (obj *BTEntry, ret *Dat_Animation) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x04: // Palette
// valid did == 0x04xxxxxx && dat.Header.DataSetLM != 2
func (dat *DatFile) Get_Palette(did uint32) (obj *BTEntry, ret *Dat_Palette) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x05: // SurfaceTexture

// case 0x06: // Texture
// valid did == 0x06xxxxxx && dat.Header.DataSetLM != 2
func (dat *DatFile) Get_Texture(did uint32) (obj *BTEntry, ret *Dat_Texture) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x07: // ???
// case 0x08: // Surface

// case 0x09: // MotionTable
// valid did == 0x09xxxxxx && dat.Header.DataSetLM != 2
func (dat *DatFile) Get_MotionTable(did uint32) (obj *BTEntry, ret *MotionTable) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	//log.Printf("Get_MotionTable loaded %s of %d\n", ToJSON(obj), len(content))
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x0A: // Wave
// case 0x0B: // ???
// case 0x0C: // ???
// case 0x0D: // Environment
// case 0x0E: // Tables
// case 0x0E01: // QualityFilter
// case 0x0E02: // MonitoredProperties
// case 0x0E00: // ClientTable
// case 0x0E000000: //
// case 0x0E000001: //
// case 0x0E000002: // CharacterGenerator
// case 0x0E000003: // SecondaryAttributeTable

// case 0x0E000004: // SkillTable
func (dat *DatFile) Get_SkillTable() (obj *BTEntry, ret *Dat_SkillTable) {
	var content []byte
	obj, content = dat.GetDIDFile(0x0E000004)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x0E000005: //
// case 0x0E000006: //
// case 0x0E000007: // ChatPoseTable
// case 0x0E000008: //
// case 0x0E000009: //
// case 0x0E00000A: //
// case 0x0E00000B: //
// case 0x0E00000C: //
// case 0x0E00000D: // ObjectHierarchy
// case 0x0E00000E: // SpellTable
func (dat *DatFile) Get_SpellTable() (obj *BTEntry, ret *Dat_SpellTable) {
	var content []byte
	obj, content = dat.GetDIDFile(0x0E00000E)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// 		case 0x0E00000F: // SpellComponentTable
// 		case 0x0E000010: //
// 		case 0x0E000011: //
// 		case 0x0E000012: //
// 		case 0x0E000013: //
// 		case 0x0E000014: //
// 		case 0x0E000015: //
// 		case 0x0E000016: //
// 		case 0x0E000017: //
// 		case 0x0E000018: // XpTable
// 		case 0x0E000019: //
// 		case 0x0E00001A: // BadData
// 		case 0x0E00001B: //
// 		case 0x0E00001C: //
// 		case 0x0E00001D: // ContractTable
// 		case 0x0E00001E: // TabooTable
// 		case 0x0E00001F: // FileToId
// 		case 0x0E000020: // NameFilterTable

// case 0x0F: // PaletteSet
// valid did == 0x0Fxxxxxx && dat.Header.DataSetLM != 2
func (dat *DatFile) Get_PaletteSet(did uint32) (obj *BTEntry, ret *Dat_PaletteSet) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x10: // Clothing
// case 0x11: // DegradeInfo

// case 0x12: // Scene
func (dat *DatFile) Get_Scene(did uint32) (obj *BTEntry, ret *Dat_Scene) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x13: // Region
func (dat *DatFile) Get_Region(did uint32) (obj *BTEntry, ret *Dat_Region) {
	var content []byte
	obj, content = dat.GetDIDFile(did)
	if content != nil {
		ret.UnPack(&SBuffer{(bytes.NewBuffer(content))})
	}
	return
}

// case 0x14: // KeyMap
// case 0x15: // RenderTexture
// case 0x16: // RenderMaterial
// case 0x17: // MaterialModifier
// case 0x18: // MaterialInstance
// case 0x19: //
// case 0x1A: //
// case 0x1B: //
// case 0x1C: //
// case 0x1D: //
// case 0x1E: //
// case 0x1F: //
// case 0x20: // SoundTable
// case 0x21: // UiLayout
// case 0x22: // EnumMapper
// case 0x23: // StringTable
// case 0x24: //
// case 0x25: // DidMapper
// case 0x26: // ActionMap
// case 0x27: // DualDidMapper
// case 0x28: //
// case 0x29: //
// case 0x2A: //
// case 0x2B: //
// case 0x2C: //
// case 0x2D: //
// case 0x2E: //
// case 0x2F: //
// case 0x30: // CombatTable
// case 0x31: // String
// case 0x32: // ParticleEmitter
// case 0x33: // PhysicsScript
// case 0x34: // PhysicsScriptTable
// case 0x35: //
// case 0x36: //
// case 0x37: //
// case 0x38: //
// case 0x39: // MasterProperty
// case 0x40: // Font
// case 0x41: // StringState
// case 0x78: // DbProperties

func (dat *DatFile) Parse(root string) error {
	var err error
	dat.M.Lock()
	defer dat.M.Unlock()

	newPath := filepath.Join(root, dat.Filename)
	if newPath == dat.LoadedPath {
		return nil
	}
	dat.LoadedPath = newPath

	dat.Dat = make(map[uint32]*BTEntry)
	dat.Tree = make(map[uint32]*BTNode)
	dat.DIDs = make([]uint32, 0)
	dat.LandBlocks = make([]uint16, 0)
	dat.LandBlockInfos = make([]uint16, 0)
	dat.EnvCells = make([]uint32, 0)
	dat.PortalCat = make(map[byte][]uint32, 0)
	dat.Header = &DiskFileInfo_t{}

	dat.F, err = os.Open(dat.LoadedPath)
	if err != nil {
		return fmt.Errorf("failed to open %s: %v", dat.LoadedPath, err)
	}
	b := make([]byte, unsafe.Sizeof(*dat.Header))
	_, err = dat.F.ReadAt(b, 0x140)
	if err != nil {
		return fmt.Errorf("failed to Read Header %s: %v", dat.LoadedPath, err)
	}
	// log.Printf("Read %db from %s as Header", r, dat.LoadedPath)

	buf := bytes.NewBuffer(b)

	err = binary.Read(buf, binary.LittleEndian, dat.Header)
	if err != nil {
		return fmt.Errorf("failed to Decode Header %s: %v", dat.LoadedPath, err)
	}
	if dat.Header.Magic != 0x00005442 {
		return fmt.Errorf("ignored \"%s\": Invalid Header", dat.LoadedPath)
	}

	dat.recurseBTree(dat.Header.BTreeRoot)

	sort.SliceStable(dat.DIDs, func(i, j int) bool {
		return dat.DIDs[i] < dat.DIDs[j]
	})
	dat.NumDIDs = uint32(len(dat.DIDs))

	sort.SliceStable(dat.LandBlocks, func(i, j int) bool {
		return dat.LandBlocks[i] < dat.LandBlocks[j]
	})
	dat.NumLandBlocks = uint32(len(dat.LandBlocks))

	sort.SliceStable(dat.LandBlockInfos, func(i, j int) bool {
		return dat.LandBlockInfos[i] < dat.LandBlockInfos[j]
	})
	dat.NumLandBlockInfos = uint32(len(dat.LandBlockInfos))

	sort.SliceStable(dat.EnvCells, func(i, j int) bool {
		return dat.EnvCells[i] < dat.EnvCells[j]
	})
	dat.NumEnvCells = uint32(len(dat.EnvCells))

	for _, c := range dat.PortalCat {
		sort.SliceStable(c, func(i, j int) bool {
			return c[i] < c[j]
		})
	}

	return nil
}

func (dat *DatFile) readDat(offset uint32, size int) []byte {
	var buffer []byte = make([]byte, size)
	var bufferOffset int = 0
	var datSize int = int(dat.Header.BlockSize) - 4
	for size > 0 {
		var nextAddress uint32 = dat.GetNextAddress(offset)
		offset += 4
		if size < datSize {
			_, err := dat.F.ReadAt(buffer[bufferOffset:bufferOffset+size], int64(offset))
			if err != nil {
				log.Printf("ReadDat(0x%08X,%d): Read Error: %s", offset, size, err)
				return nil
			}
			size = 0
		} else {
			_, err := dat.F.ReadAt(buffer[bufferOffset:(bufferOffset+datSize)], int64(offset))
			if err != nil {
				log.Printf("ReadDat(0x%08X,%d): Read Error: %s", offset, size, err)
				return nil
			}
			bufferOffset += datSize
			offset = nextAddress
			size -= datSize
		}
	}
	return buffer
}

func (dat *DatFile) ReadDat(offset uint32, size int) *SBuffer {
	return &SBuffer{(bytes.NewBuffer(dat.readDat(offset, size)))}
}

func (dat *DatFile) GetNextAddress(offset uint32) uint32 {
	var buffer []byte = make([]byte, 4)
	n, err := dat.F.ReadAt(buffer, int64(offset))
	if err != nil {
		log.Printf("GetNextAddress(0x%08X): Read Error: %s", offset, err)
		return 0
	}
	if n != 4 {
		log.Printf("GetNextAddress(0x%08X): Read %d", offset, n)
		return 0
	}
	return binary.LittleEndian.Uint32(buffer)
}

type DiskFileInfo_t struct {
	Magic               uint32      `json:"magic"`
	BlockSize           uint32      `json:"block_size"`
	FileSize            uint32      `json:"file_size"`
	DataSetLM           DatFileType `json:"data_set_lm"`
	DataSubsetLM        uint32      `json:"data_subset_lm"`
	FirstFree           uint32      `json:"first_free"`
	FinalFree           uint32      `json:"final_free"`
	FreeBlocks          uint32      `json:"free_blocks"`
	BTreeRoot           uint32      `json:"btree_root"`
	YoungLRULM          uint32      `json:"young_lru_lm"`
	OldLRULM            uint32      `json:"old_lru_lm"`
	UseLRUFM            uint32      `json:"use_lru_fm"`
	MasterMapID         uint32      `json:"master_map_id"`
	EngineVersionNumber uint32      `json:"engine_version"`
	GameVersionNumber   uint32      `json:"game_version"`
	MajorVersionNumber  GUID        `json:"guid"`
	MinorVersionNumber  uint32      `json:"version"`
}

type DatFileType uint32

var DATFILE_TYPE []string = []string{"UNDEF_DISK", "PORTAL_DATFILE", "CELL_DATFILE", "LOCAL_DATFILE"}
var DatPortalType []string = []string{}

func (d DatFileType) String() string {
	if int(d) < len(DATFILE_TYPE) {
		return DATFILE_TYPE[d]
	}
	return fmt.Sprintf("UNKNOWN_%d)", d)
}

type GUID struct {
	Data1 uint32
	Data2 uint16
	Data3 uint16
	Data4 [8]uint8
}

func (g *GUID) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("\"%08x-%04x-%04x-%016x\"", g.Data1, g.Data2, g.Data3, binary.LittleEndian.Uint64(g.Data4[:]))), nil
}

type BTEntry struct {
	Bitmask uint32 `json:"bitmask"` /* unsigned __int32 comp_ : 1; unsigned __int32 resv_ : 15; unsigned __int32 ver_ : 16; */
	GID     uint32 `json:"gid"`
	Offset  uint32 `json:"offset"`
	Size    uint32 `json:"size"`
	Date    uint32 `json:"date"`
	Iter    uint32 `json:"iter"`
}

type BTNode struct {
	NextNode   [62]uint32
	NumEntries uint32
	Entries    [61]BTEntry
}
