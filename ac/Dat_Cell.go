// //////////////////////////////////////////////////////////////
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"encoding/binary"
	"fmt"
)

type Dat_EnvCell struct {
	Id             uint32            `json:"id"`                 // offset 0
	Flags          uint32            `json:"flags"`              // offset 4
	CellID         uint32            `json:"cell_id"`            // offset 8
	NumSurfaces    uint8             `json:"num_surfaces"`       // offset 12
	NumPortals     uint8             `json:"num_portals"`        // offset 13
	NumStabs       uint16            `json:"num_stabs"`          // offset 14
	Surfaces       []Dat_Surface     `json:"surfaces,omitempty"` // len: NumSurfaces, Add 0x08000000 for DID
	EnvironmentId  Dat_EnvironmentId `json:"environment_id"`     // Add 0x0D000000 for DID
	CellStructure  uint16            `json:"cell_structure"`
	Position       Frame             `json:"position"`
	Portals        []Dat_CellPortal  `json:"portals,omitempty"`         // len: NumPortals
	VisibleCells   []uint16          `json:"visible_cells,omitempty"`   // len: NumStabs
	StaticObjects  []Dat_Stab        `json:"static_objects,omitempty"`  // len: (Flags & 2)>>1
	RestrictionObj []uint32          `json:"restriction_obj,omitempty"` // len: (Flags & 8)>>3
}

func (t *Dat_EnvCell) UnPack(s *SBuffer) any {
	t = new(Dat_EnvCell)
	t.Id = ReadT[uint32](s)
	t.Flags = ReadT[uint32](s)
	t.CellID = ReadT[uint32](s)
	t.NumSurfaces = ReadT[byte](s)
	t.NumPortals = ReadT[byte](s)
	t.NumStabs = ReadT[uint16](s)
	t.Surfaces = ReadTs[Dat_Surface](s, int(t.NumSurfaces))
	t.EnvironmentId = ReadT[Dat_EnvironmentId](s)
	t.CellStructure = ReadT[uint16](s)
	t.Position = ReadT[Frame](s)
	t.Portals = ReadTs[Dat_CellPortal](s, int(t.NumPortals))
	t.VisibleCells = ReadTs[uint16](s, int(t.NumStabs))
	if (t.Flags & 2) == 2 {
		t.StaticObjects = ReadTs[Dat_Stab](s, 1)
	}
	if (t.Flags & 8) == 8 {
		t.RestrictionObj = ReadTs[uint32](s, 1)
	}
	return t
}

type Dat_Surface uint16

type Dat_EnvironmentId uint16

func (t Dat_Surface) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%d", uint32(t)+0x08000000)), nil
}
func (t Dat_EnvironmentId) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%d", uint32(t)+0x0D000000)), nil
}

// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

type Dat_LandBlockInfo struct {
	Id                uint32               `json:"id"`
	NumCells          uint32               `json:"num_cells"`
	Objects           *List[*Dat_Stab]     `json:"objects"`
	PackMask          uint16               `json:"pack_mask"`
	Buildings         []Dat_BuildInfo      `json:"buildings"`
	RestrictionTables HashMapData[*Uint32] `json:"restriction_tables"`
}

func (t *Dat_LandBlockInfo) UnPack(s *SBuffer) any {
	t = new(Dat_LandBlockInfo)
	t.Id = ReadT[uint32](s)
	t.NumCells = ReadT[uint32](s)
	t.Objects = new(List[*Dat_Stab]).UnPack(s).(*List[*Dat_Stab])
	t.Buildings = make([]Dat_BuildInfo, ReadT[uint16](s))
	t.PackMask = ReadT[uint16](s)
	for i := range t.Buildings {
		t.Buildings[i].UnPack(s)
	}
	if (t.PackMask & 1) == 1 {
		t.RestrictionTables.UnPack(s)
	}
	return t
}

// //////////////////////////////////////////////////////////////
//
//
//
// //////////////////////////////////////////////////////////////

type Dat_LandBlock struct {
	Id         uint32          `json:"id"`
	HasObjects uint32          `json:"has_objects"`
	Terrain    [81]Dat_Terrain `json:"terrain"`
	Height     [81]uint8       `json:"height"`
}

func (t *Dat_LandBlock) UnPack(s *SBuffer) any {
	t = new(Dat_LandBlock)
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_Terrain uint16

// func (t Dat_Terrain) MarshalJSON() ([]byte, error) {
// 	//st := (t << 8) | (t >> 8)
// 	return []byte(fmt.Sprintf("{\"terrain\":%d,\"road\":%d,\"type\":%d,\"scenery\":%d}",
// 		t,
// 		t&3,
// 		(t&0x7C)>>2,
// 		(t >> 11),
// 	)), nil
// }
