package ac

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	"sync"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/commandff/gdlemonitor/models"
)

var DB *GDLEDB

type GDLEDB struct {
	*sql.DB
}

var GDLEWeenies_m sync.Mutex
var GDLEWeenies = make(map[uint32]*GDLECWeenieSave)
var GDLEWeenie_IDs = make([]uint32, 0)

type GDLECWeenieSave struct {
	Id               uint32 `json:"id"`
	TopLevelObjectId uint32 `json:"top_level_object_id"`
	BlockId          uint32 `json:"block_id"`
	Data             []byte `json:"data"`
	WeenieTS         uint32 `json:"weenie_ts"`
}

func Parse_GDLEWeenies(startIdx int, per_page int) error {
	if DB == nil {
		DB = new(GDLEDB)
		err := DB.Connect(db_user, db_pass, db_address, db_name)
		if err != nil {
			log.Printf("Unable to Connect to gdledb: %s", err.Error())
			return err
		}
	}

	var weenieCount int = 0
	var minId uint32 = 4294967295
	var maxId uint32 = 0

	GDLEWeenies_m.Lock()
	defer GDLEWeenies_m.Unlock()
	//rows, err := DB.Query(fmt.Sprintf("SELECT id,top_level_object_id,block_id,weenie_ts FROM weenies ORDER BY id LIMIT %d,500", startIdx))
	rows, err := DB.Query(fmt.Sprintf("SELECT id,top_level_object_id,block_id,data,weenie_ts FROM weenies ORDER BY id LIMIT %d,%d", startIdx, per_page))
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		r := new(GDLECWeenieSave)
		if err := rows.Scan(&r.Id, &r.TopLevelObjectId, &r.BlockId, &r.Data, &r.WeenieTS); err != nil {
			return err
		}
		GDLEWeenies[r.Id] = r
		weenieCount++
		if r.Id > maxId {
			maxId = r.Id
		}
		if r.Id < minId {
			minId = r.Id
		}
	}
	log.Printf("Parse_GDLEWeenies loaded %d weenies, %08x thru %08x", weenieCount, minId, maxId)
	return nil
}
func Load_GDLEWeenies(ids []uint32) error {
	if DB == nil {
		DB = new(GDLEDB)
		err := DB.Connect(db_user, db_pass, db_address, db_name)
		if err != nil {
			log.Printf("Unable to Connect to gdledb: %s", err.Error())
			return err
		}
	}
	var loaded_ids []uint32
	GDLEWeenies_m.Lock()
	defer GDLEWeenies_m.Unlock()
	var rids []string
	for _, o := range ids {
		rids = append(rids, strconv.FormatUint(uint64(o), 10))
	}
	rows, err := DB.Query(fmt.Sprintf("SELECT id,top_level_object_id,block_id,data,weenie_ts FROM weenies WHERE id in (%s)", strings.Join(rids, ",")))
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		r := new(GDLECWeenieSave)
		if err := rows.Scan(&r.Id, &r.TopLevelObjectId, &r.BlockId, &r.Data, &r.WeenieTS); err != nil {
			return err
		}
		GDLEWeenies[r.Id] = r
		loaded_ids = append(loaded_ids, r.Id)
	}
	log.Printf("Load_GDLEWeenies loaded %d of %d weenies, %08x", len(loaded_ids), len(ids), loaded_ids)
	return nil
}

func (db *GDLEDB) ShowFields(db_name, table_name string) ([]string, error) {
	rows, err := db.DB.Query(fmt.Sprintf("SHOW FIELDS FROM `%s`.`%s`", db_name, table_name))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var field *string
	var ftype *string
	var fnull *string
	var fkey *string
	var fdef *string
	var fextra *string
	resu := make([]string, 0, 16)
	for rows.Next() {
		if err := rows.Scan(&field, &ftype, &fnull, &fkey, &fdef, &fextra); err != nil {
			return nil, err
		}
		resu = append(resu, *field)
	}
	return resu, nil
}

func (db *GDLEDB) ShowKeys(db_name, table_name string) ([]string, error) {
	if db.DB == nil {
		return nil, fmt.Errorf("DB is not connected")
	}
	rows, err := db.DB.Query(fmt.Sprintf("SHOW Keys FROM `%s`.`%s`", db_name, table_name))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var kTable *string
	var kNon_unique *int
	var kKey_name *string
	var kSeq_in_index *int
	var kColumn_name *string
	var kCollation *string
	var kCardinality *int
	var kSub_part *string
	var kPacked *string
	var kNull *string
	var kIndex_type *string
	var kComment *string
	var kIndex_comment *string
	var kIgnored *string
	resu := make([]string, 0, 16)
	for rows.Next() {
		if err := rows.Scan(&kTable, &kNon_unique, &kKey_name, &kSeq_in_index, &kColumn_name, &kCollation, &kCardinality, &kSub_part, &kPacked, &kNull, &kIndex_type, &kComment, &kIndex_comment, &kIgnored); err != nil {
			return nil, err
		}
		resu = append(resu, *kColumn_name)
	}
	return resu, nil
}

func (db *GDLEDB) Connect(db_username, db_assword, db_address, db_name string) error {
	if db.DB == nil {
		var err error
		db.DB, err = sql.Open("mysql", fmt.Sprintf("%s:%s@%s/%s", url.QueryEscape(db_username), url.QueryEscape(db_assword), db_address, url.QueryEscape(db_name)))
		if err != nil {
			return err
		}
		db.DB.SetMaxOpenConns(1)
		db.DB.SetMaxIdleConns(0)

		var HasLastChange, HasJSON, HasKeyEmail, HasKeyCreatedIPAddress, HasKeyLastChange bool

		AccountsColumns, err := DB.ShowFields(db_name, "accounts")
		if err != nil {
			return err
		}
		for _, col := range AccountsColumns {
			switch col {
			case "last_change":
				HasLastChange = true
			case "json":
				HasJSON = true
			}
		}
		AccountsKeys, err := DB.ShowKeys(db_name, "accounts")
		if err != nil {
			return err
		}
		for _, col := range AccountsKeys {
			switch col {
			case "email":
				HasKeyEmail = true
			case "created_ip_address":
				HasKeyCreatedIPAddress = true
			case "last_change":
				HasKeyLastChange = true
			}
		}

		mods := make([]string, 0, 5)
		if !HasLastChange {
			mods = append(mods, "ADD `last_change` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()")
		}
		if !HasJSON {
			mods = append(mods, "ADD `json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '{}' CHECK (json_valid(`json`))")
		}
		if !HasKeyEmail {
			mods = append(mods, "ADD KEY `email` (`email`) USING BTREE")
		}
		if !HasKeyCreatedIPAddress {
			mods = append(mods, "ADD KEY `created_ip_address` (`created_ip_address`) USING BTREE")
		}
		if !HasKeyLastChange {
			mods = append(mods, "ADD KEY `last_change` (`last_change`) USING BTREE")
		}
		if len(mods) != 0 {
			_, err := db.DB.Exec(fmt.Sprintf("ALTER TABLE `%s`.`accounts` %s;", db_name, strings.Join(mods, ",")))
			if err != nil {
				return err
			}
		}

	}
	log.Printf("GDLEDB Connected: %s", fmt.Sprintf("%s@%s/%s", url.QueryEscape(db_username), db_address, url.QueryEscape(db_name)))
	return nil
}

func (db *GDLEDB) CountEmails(email string) int {
	if db.DB == nil {
		return 0
	}
	var cnt int = 0
	db.DB.QueryRow("SELECT COUNT(*) FROM accounts WHERE email=?", email).Scan(&cnt)
	return cnt
}

func (db *GDLEDB) CountIPs(ip string) int {
	if db.DB == nil {
		return 0
	}
	var cnt int = 0
	db.DB.QueryRow("SELECT COUNT(*) FROM accounts WHERE created_ip_address=?", ip).Scan(&cnt)
	return cnt
}

func (db *GDLEDB) CheckUsername(username string) bool {
	if db.DB == nil {
		return true // if we don't have a db con, return that the username is in use.
	}
	var cnt int = 0
	db.DB.QueryRow("SELECT COUNT(*) FROM accounts WHERE username=?", username).Scan(&cnt)
	return cnt == 1
}

func (db *GDLEDB) LoadAccount(username string) (ret *models.Account, err error) {
	if db.DB == nil {
		return nil, fmt.Errorf("no database connection")
	}
	var banned int
	err = db.DB.QueryRow("SELECT id,username,password,password_salt,email,date_created,access,created_ip_address,banned FROM accounts WHERE username=?", username).Scan(
		&ret.ID, &ret.Username, &ret.Password, &ret.PasswordSalt, &ret.Email, &ret.DateCreated, &ret.Access,
		&ret.CreatedIPAddress, &banned)
	if err == sql.ErrNoRows {
		return nil, fmt.Errorf("no such user")
	}
	if err != nil {
		return nil, err
	}
	ret.Banned = banned == 1
	ret.Admin = ret.Access == 6
	return
}

func (db *GDLEDB) SaveAccount(acct *models.Account) error {
	if db.DB == nil {
		return fmt.Errorf("no database connection")
	}
	var banned int
	if acct.Banned {
		banned = 1
	}
	res, err := db.DB.Exec("UPDATE accounts SET username=?,password=?,password_salt=?,access=?,banned=?,email=?,emailsetused=? WHERE id=?",
		acct.Username, acct.Password, acct.PasswordSalt, acct.Access, banned, acct.Email, len(acct.Email) > 0,
		acct.ID)
	if err != nil {
		return err
	}
	ra, err := res.RowsAffected()
	if ra == 0 {
		return fmt.Errorf("no rows affected")
	}
	return err
}

func (db *GDLEDB) CreateAccount(acct *models.Account) error {
	if db.DB == nil {
		return fmt.Errorf("no database connection")
	}
	var banned int
	if acct.Banned {
		banned = 1
	}
	res, err := db.DB.Exec("INSERT INTO accounts (username,password,password_salt,access,banned,email,emailsetused) VALUES (?,?,?,?,?,?,?)",
		acct.Username, acct.Password, acct.PasswordSalt, acct.Access, banned, acct.Email, len(acct.Email) > 0)
	if err != nil {
		return err
	}
	ra, err := res.LastInsertId()
	if ra == 0 {
		return fmt.Errorf("no rows affected")
	}
	acct.ID = int(ra)
	return err
}

func (db *GDLEDB) ValidateAccount(username string, password string) (acct *models.Account, err error) {
	acct, err = db.LoadAccount(username)
	if err != nil || !acct.TestPW(password) {
		return nil, fmt.Errorf("validation failure")
	}
	return acct, nil
}
