// //////////////////////////////////////////////////////////////
//
//     ISAAC - fast impl.
//
// //////////////////////////////////////////////////////////////

package ac

type ISAAC struct {
	rsl        [256]uint32
	mem        [256]uint32
	lastrsl    [256]uint32
	seed       uint32
	generation uint32
	aa, bb, cc uint32
}

func (r *ISAAC) GetIndex(idx uint32) uint32 {
	expectedGeneration := int(idx >> 8)
	variance := int(r.generation) - expectedGeneration
	if variance == 0 {
		return r.rsl[255-(idx%256)] // fmt.Printf("CUR GEN\n")
	}
	if variance == 1 {
		return r.lastrsl[255-(idx%256)] // fmt.Printf("LAST GEN\n")
	}
	if variance > 1 {
		r.Init(r.seed) // fmt.Printf("%d generations old, regenerating...\n", variance)
	}
	for {
		if r.generation-uint32(expectedGeneration) == 0 {
			return r.rsl[255-(idx%256)]
		}
		r.isaac() // fmt.Printf("NEXT GEN\n")
	}
}

//wizardry. returns true if the sequence id is sane.
func (r *ISAAC) ValidateIndex(idx uint32) bool {
	return (r.generation-(idx>>8))&0x7FFFFFFF < 2
}

// returns the lowest index readily available
func (r *ISAAC) LowestIndex() uint32 {
	if r.generation == 0 {
		return 0
	}
	return (r.generation - 1) << 8
}

//returns the highest index readily available
func (r *ISAAC) HighestIndex() uint32 {
	if r.generation == 0 {
		return 255
	}
	return (r.generation << 8) + 255
}

func (r *ISAAC) isaac() {
	copy(r.lastrsl[:], r.rsl[:])
	r.generation++
	r.cc = r.cc + 1
	r.bb = r.bb + r.cc
	for i := 0; i < 256; i++ {
		x := r.mem[i]
		switch i % 4 {
		case 0:
			r.aa = r.aa ^ (r.aa << 13)
		case 1:
			r.aa = r.aa ^ (r.aa >> 6)
		case 2:
			r.aa = r.aa ^ (r.aa << 2)
		case 3:
			r.aa = r.aa ^ (r.aa >> 16)
		}
		r.aa = r.mem[(i+128)%256] + r.aa
		y := r.mem[(x>>2)%256] + r.aa + r.bb
		r.mem[i] = y
		r.bb = r.mem[(y>>10)%256] + x
		r.rsl[i] = r.bb
	}
}
func (r *ISAAC) dry(x *[8]uint32) {
	x[0] ^= x[1] << 11
	x[3] += x[0]
	x[1] += x[2]
	x[1] ^= x[2] >> 2
	x[4] += x[1]
	x[2] += x[3]
	x[2] ^= x[3] << 8
	x[5] += x[2]
	x[3] += x[4]
	x[3] ^= x[4] >> 16
	x[6] += x[3]
	x[4] += x[5]
	x[4] ^= x[5] << 10
	x[7] += x[4]
	x[5] += x[6]
	x[5] ^= x[6] >> 4
	x[0] += x[5]
	x[6] += x[7]
	x[6] ^= x[7] << 8
	x[1] += x[6]
	x[7] += x[0]
	x[7] ^= x[0] >> 9
	x[2] += x[7]
	x[0] += x[1]
}

/* slightly optimized :cough: - Yonneh */
func (r *ISAAC) Init(seed uint32) {
	r.seed = seed
	r.aa = seed
	r.bb = seed
	r.cc = seed
	x := [8]uint32{0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9 /* the golden ratio */}

	for i := range r.rsl {
		r.rsl[i] = 0
	}
	for i := 0; i < 4; i++ {
		r.dry(&x)
	}

	for i := 0; i < 256; i += 8 {
		x[0] += r.rsl[i]
		x[1] += r.rsl[i+1]
		x[2] += r.rsl[i+2]
		x[3] += r.rsl[i+3]
		x[4] += r.rsl[i+4]
		x[5] += r.rsl[i+5]
		x[6] += r.rsl[i+6]
		x[7] += r.rsl[i+7]
		r.dry(&x)
		//copy(r.mem[i:i+7], x[0:7])
		r.mem[i] = x[0]
		r.mem[i+1] = x[1]
		r.mem[i+2] = x[2]
		r.mem[i+3] = x[3]
		r.mem[i+4] = x[4]
		r.mem[i+5] = x[5]
		r.mem[i+6] = x[6]
		r.mem[i+7] = x[7]
	}

	for i := 0; i < 256; i += 8 {
		x[0] += r.mem[i]
		x[1] += r.mem[i+1]
		x[2] += r.mem[i+2]
		x[3] += r.mem[i+3]
		x[4] += r.mem[i+4]
		x[5] += r.mem[i+5]
		x[6] += r.mem[i+6]
		x[7] += r.mem[i+7]
		r.dry(&x)
		//copy(r.mem[i:i+7], x[0:7])
		r.mem[i] = x[0]
		r.mem[i+1] = x[1]
		r.mem[i+2] = x[2]
		r.mem[i+3] = x[3]
		r.mem[i+4] = x[4]
		r.mem[i+5] = x[5]
		r.mem[i+6] = x[6]
		r.mem[i+7] = x[7]
	}
	r.generation = 4294967295
	r.isaac() //fmt.Printf("ISAAC INIT OF %08X complete. rsl: %#v", seed, r.rsl)
}
