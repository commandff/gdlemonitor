// //////////////////////////////////////////////////////////////
//
//  Spells - GDLE spells.json parser
//
// //////////////////////////////////////////////////////////////

package ac

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"sync"
	"time"

	"gitlab.com/commandff/gdlemonitor/aids"
)

/* Testing code-

err := gdle.Spells_Read(cGDLEPath)
if err != nil {
	log.Println(err.Error())
	return
}

//spell := gdle.Spells_get_object(1)

spell_json := gdle.Spells_get(1)
log.Printf("%#v", spell_json)
//*spell.BaseMana = 9
err = gdle.Spells_set(1, "{\"base_mana\":10,\"base_range_constant\":5.0,\"base_range_mod\":1.0,\"bitfield\":6,\"caster_effect\":0,\"category\":1,\"component_loss\":0.0,\"desc\":\"Increases the target's Strength by 10 points.\",\"display_order\":10412,\"fizzle_effect\":0,\"formula\":[1,7,33,44,49,0,0,0],\"formula_version\":1,\"iconID\":100668300,\"mana_mod\":0,\"name\":\"Strength Other I\",\"non_component_target_type\":16,\"power\":1,\"recovery_amount\":0.0,\"recovery_interval\":0.0,\"school\":4,\"spell_economy_mod\":1.0,\"target_effect\":6,\"meta_spell\":{\"sp_type\":1,\"spell\":{\"spell_id\":1,\"degrade_limit\":-666.0,\"degrade_modifier\":0.0,\"duration\":1800.0,\"smod\":{\"key\":1,\"type\":36865,\"val\":10.0},\"spellCategory\":1}}}")
if err != nil {
	log.Println(err.Error())
	return
}

err = gdle.Spells_Write(cGDLEPath)
if err != nil {
	log.Println(err.Error())
	return
}


*/

// hash of pointers, to quickly retreive by spell_id
var Spells = make(map[uint32]*SpellBase)
var Spells_m sync.Mutex
var Spells_Dirty bool = false

// SpellBaseHash_Parse("C:\\gdle\\Data\\json")
// SpellBaseHash_Stats()
const Spells_JSON string = "\\Data\\json\\spells.json"
const Spells_JSON_Write string = "\\Data\\json\\spells.json"

// get Spell json
func Spells_get(spell_id uint32) []byte {
	if spell := Spells_get_object(spell_id); spell != nil {
		if bv, err := json.Marshal(spell); err == nil {
			return bv
		}
	}
	return []byte("{}")
}

// get Spell object
func Spells_get_object(spell_id uint32) *SpellBase {
	Spells_m.Lock()
	defer Spells_m.Unlock()
	if len(Spells) == 0 { // check Spells, INSIDE of Spells_m lock; preventing other checks until it is populated :smart:
		err := Spells_Read(aids.WD)
		if err != nil {
			return nil
		}
	}
	if spell, ok := Spells[spell_id]; ok {
		return spell
	}
	return nil
}

// replace Spell with json
func Spells_set(spell_id uint32, spell_json string) error {
	Spells_m.Lock()
	defer Spells_m.Unlock()
	spell, ok := Spells[spell_id]
	if !ok {
		return fmt.Errorf("invalid spell_id-todo append logic")
	}
	err := json.Unmarshal([]byte(spell_json), spell)
	if err != nil {
		return err
	}
	Spells_Dirty = true
	// err = Spells_Write(aids.WD)
	// if err != nil {
	// 	return err
	// }
	return nil
}

// replace Spell with object
// to replace entire object, we have to find it in the parent array
func Spells_set_object(spell_id uint32, spell *SpellBase) error {
	Spells_m.Lock()
	defer Spells_m.Unlock()
	Spells[spell_id] = spell

	Spells_Dirty = true
	// err := Spells_Write(aids.WD)
	// if err != nil {
	// 	return err
	// }
	return nil
}

func Spells_Read(gdle_path string) error {
	stime := time.Now()
	Spells_m.Lock()
	defer Spells_m.Unlock()
	file, err := os.Open(gdle_path + Spells_JSON)
	if err != nil {
		return err
	}
	j := new(SpellTable)
	err = json.NewDecoder(file).Decode(&j)
	file.Close()
	if err != nil {
		return err
	}
	for _, sp := range j.Table.SpellBaseHash {
		Spells[sp.Key] = sp.Value
	}
	Spells_Dirty = false
	aids.DebugLog.Printf("parsed %d spells in %s.", len(Spells), time.Since(stime))
	return nil
}

func (t *SpellTable) Write_JSON(gdle_path string) error {
	stime := time.Now()
	Spells_m.Lock()
	defer Spells_m.Unlock()
	file, err := os.Create(gdle_path + Spells_JSON_Write + "~")
	if err != nil {
		return err
	}
	enc := json.NewEncoder(file)
	// enc.SetIndent("", "    ")
	err = enc.Encode(t)
	if err != nil {
		return err
	}
	err = file.Close()
	if err != nil {
		return err
	}
	err = os.Rename(gdle_path+Spells_JSON_Write+"~", gdle_path+Spells_JSON_Write)
	if err != nil {
		return err
	}
	Spells_Dirty = false
	aids.DebugLog.Printf("wrote %d in %s", len(t.Table.SpellBaseHash), time.Since(stime))
	return nil
}

var SpellBaseHash_m sync.Mutex
var SpellBaseHash = make(map[uint32]SpellBase)
var SpellBaseHash_IDs = make([]uint32, 0)

func SpellBaseHash_Stats() {
	firstSpellID := SpellBaseHash_IDs[0]
	lastSpellID := SpellBaseHash_IDs[len(SpellBaseHash_IDs)-1]
	aids.InfoLog.Printf("Total Spells: %d == %d, %s (%d) thru %s (%d)\n",
		len(SpellBaseHash), len(SpellBaseHash_IDs),
		SpellBaseHash[firstSpellID].Name, firstSpellID,
		SpellBaseHash[lastSpellID].Name, lastSpellID,
	)
}

func SpellBaseHash_Parse(root string) error {
	SpellBaseHash_m.Lock()
	defer SpellBaseHash_m.Unlock()
	// SpellBaseHash = make(map[uint32]SpellBase)
	// SpellBaseHash_IDs = make([]uint32, 0)
	parseSpells(root)

	sort.SliceStable(SpellBaseHash_IDs, func(i, j int) bool {
		return SpellBaseHash_IDs[i] < SpellBaseHash_IDs[j]
	})

	return nil
}
func parseSpells(root string) {
	file := filepath.Join(root, "spells.json")
	content, err := os.ReadFile(file)
	if err == nil {
		var w SpellTable
		if err := json.Unmarshal(content, &w); err != nil {
			log.Printf("ERROR: failed to parse %s: %v", file, err)
			return
		}
		for _, spellbase := range w.Table.SpellBaseHash {
			SpellBaseHash[spellbase.Key] = *spellbase.Value
			SpellBaseHash_IDs = append(SpellBaseHash_IDs, spellbase.Key)
		}
	} else {
		aids.ErrorLog.Printf("ERROR: Unable to read spells.json(%s): %s\n", file, err)
	}
}

type SpellTable struct {
	Table struct {
		SpellBaseHash HashMap[*SpellBase] `json:"spellBaseHash"`
	} `json:"table"`
}

func (t *SpellTable) UnPack(s *SBuffer) any {
	t = new(SpellTable)
	t.Table.SpellBaseHash.UnPack(s)
	return t
}

type SpellBase struct {
	Name                   PString       `json:"name"`
	Desc                   PString       `json:"desc"`
	School                 uint32        `json:"school"`
	IconID                 uint32        `json:"iconID"`
	Category               uint32        `json:"category"`
	Bitfield               uint32        `json:"bitfield"`
	Mana                   int32         `json:"base_mana"`
	RangeConstant          float32       `json:"base_range_constant"`
	RangeMod               float32       `json:"base_range_mod"`
	Power                  int32         `json:"power"`
	EconomyMod             float32       `json:"spell_economy_mod"`
	FormulaVersion         uint32        `json:"formula_version"`
	ComponentLoss          float32       `json:"component_loss"`
	MetaSpell              Dat_MetaSpell `json:"meta_spell"`
	Formula                []uint32      `json:"formula"`
	CasterEffect           uint32        `json:"caster_effect"`
	TargetEffect           uint32        `json:"target_effect"`
	FizzleEffect           uint32        `json:"fizzle_effect"`
	RecoveryInterval       float64       `json:"recovery_interval"`
	RecoveryAmount         float32       `json:"recovery_amount"`
	DisplayOrder           int32         `json:"display_order"`
	NonComponentTargetType uint32        `json:"non_component_target_type"`
	ManaMod                int32         `json:"mana_mod"`
}

func (t *SpellBase) UnPack(s *SBuffer) any {
	t = new(SpellBase)
	t.Name.UnPack(s)
	t.Desc.UnPack(s)
	t.School = ReadT[uint32](s)
	t.IconID = ReadT[uint32](s)
	t.Category = ReadT[uint32](s)
	t.Bitfield = ReadT[uint32](s)
	t.Mana = ReadT[int32](s)
	t.RangeConstant = ReadT[float32](s)
	t.RangeMod = ReadT[float32](s)
	t.Power = ReadT[int32](s)
	t.EconomyMod = ReadT[float32](s)
	t.FormulaVersion = ReadT[uint32](s)
	t.ComponentLoss = ReadT[float32](s)
	t.MetaSpell.UnPack(s)
	t.Formula = ReadTs[uint32](s, 8)
	t.CasterEffect = ReadT[uint32](s)
	t.TargetEffect = ReadT[uint32](s)
	t.FizzleEffect = ReadT[uint32](s)
	t.RecoveryInterval = ReadT[float64](s)
	t.RecoveryAmount = ReadT[float32](s)
	t.DisplayOrder = ReadT[int32](s)
	t.NonComponentTargetType = ReadT[uint32](s)
	t.ManaMod = ReadT[int32](s)
	return t
}

type Dat_MetaSpell struct {
	SpType int32     `json:"sp_type"`
	Spell  Dat_Spell `json:"spell"`
}

func (t *Dat_MetaSpell) UnPack(s *SBuffer) any {
	t = new(Dat_MetaSpell)
	t.SpType = ReadT[int32](s)
	t.Spell.UnPack(s, int(t.SpType))
	return t
}

type Dat_Spell struct {
	SpellID           uint32 `json:"spell_id"`
	*Dat_Spell_Group1 `json:",omitempty"`
	*Dat_Spell_Group2 `json:",omitempty"`
	*Dat_Spell_Group3 `json:",omitempty"`
	*Dat_Spell_Group4 `json:",omitempty"`
	*Dat_Spell_Group5 `json:",omitempty"`
	*Dat_Spell_Group6 `json:",omitempty"`
	*Dat_Spell_Group7 `json:",omitempty"`
	*Dat_Spell_Group8 `json:",omitempty"`
	*Dat_Spell_Group9 `json:",omitempty"`
}

func (t *Dat_Spell) UnPack(s *SBuffer, spType int) any {
	t = new(Dat_Spell)
	t.SpellID = ReadT[uint32](s)
	if spType == 1 || spType == 12 || spType == 15 {
		t.Dat_Spell_Group1 = ReadTP[Dat_Spell_Group1](s)
	}
	if spType == 2 || spType == 10 || spType == 15 {
		t.Dat_Spell_Group2 = ReadTP[Dat_Spell_Group2](s)
		if spType == 10 {
			t.Dat_Spell_Group3 = ReadTP[Dat_Spell_Group3](s)
		}
	}
	if spType == 3 || spType == 11 {
		t.Dat_Spell_Group4 = ReadTP[Dat_Spell_Group4](s)
	}
	if spType == 4 {
		t.Dat_Spell_Group5 = ReadTP[Dat_Spell_Group5](s)
	}
	if spType == 5 || spType == 6 {
		t.Dat_Spell_Group6 = ReadTP[Dat_Spell_Group6](s)
	}
	if spType == 7 {
		t.Dat_Spell_Group7 = ReadTP[Dat_Spell_Group7](s)
	}
	if spType == 8 || spType == 13 {
		t.Dat_Spell_Group8 = ReadTP[Dat_Spell_Group8](s)
	}
	if spType == 9 || spType == 14 {
		t.Dat_Spell_Group9 = ReadTP[Dat_Spell_Group9](s)
	}
	return t
}

type Dat_Spell_Group1 struct {
	// SPType_1 || SPType_12 || SPType_15
	DegradeLimit    float32 `json:"degrade_limit"`
	DegradeModifier float32 `json:"degrade_modifier"`
	Duration        float64 `json:"duration"`
	SpellCategory   uint32  `json:"spellCategory"`
	Smod            StatMod `json:"smod"`
}
type Dat_Spell_Group2 struct {
	// SPType_2 || SPType_10 || SPType_15
	EType                  uint32  `json:"etype"`
	BaseIntensity          int32   `json:"baseIntensity"`
	Variance               int32   `json:"variance"`
	WCID                   uint32  `json:"wcid"`
	NumProjectiles         int32   `json:"numProjectiles"`
	NumProjectilesVariance int32   `json:"numProjectilesVariance"`
	SpreadAngle            float32 `json:"spreadAngle"`
	VerticalAngle          float32 `json:"verticalAngle"`
	DefaultLaunchAngle     float32 `json:"defaultLaunchAngle"`
	NonTracking            int32   `json:"bNonTracking"`
	CreateOffset           Vector3 `json:"createOffset"`
	Padding                Vector3 `json:"padding"`
	Dims                   Vector3 `json:"dims"`
	Peturbation            Vector3 `json:"peturbation"`
	ImbuedEffect           uint32  `json:"imbuedEffect"`
	SlayerCreatureType     int32   `json:"slayerCreatureType"`
	SlayerDamageBonus      float32 `json:"slayerDamageBonus"`
	CritFreq               float64 `json:"critFreq"`
	CritMultiplier         float64 `json:"critMultiplier"`
	IgnoreMagicResist      int32   `json:"ignoreMagicResist"`
	ElementalModifier      float64 `json:"elementalModifier"`
}
type Dat_Spell_Group3 struct {
	// SPType_10
	DrainPercentage float32 `json:"drain_percentage"`
	DamageRatio     float32 `json:"damage_ratio"`
}
type Dat_Spell_Group4 struct {
	// SPType_3 || SPType_11
	DamageType    uint32 `json:"dt"`
	Boost         int32  `json:"boost"`
	BoostVariance int32  `json:"boostVariance"`
}
type Dat_Spell_Group5 struct {

	// SPType_4
	Source           int32   `json:"src"`
	Destination      int32   `json:"dest"`
	Proportion       float32 `json:"proportion"`
	LossPercent      float32 `json:"lossPercent"`
	SourceLoss       int32   `json:"sourceLoss"`
	TransferCap      int32   `json:"transferCap"`
	MaxBoostAllowed  int32   `json:"maxBoostAllowed"`
	TransferBitfield uint32  `json:"bitfield"`
}
type Dat_Spell_Group6 struct {
	// SPType_5 || SPType_6
	Index int32 `json:"index"`
}
type Dat_Spell_Group7 struct {
	// SPType_7
	PortalLifetime float64 `json:"portal_lifetime"`
	Link           int32   `json:"link"`
}
type Dat_Spell_Group8 struct {

	// SPType_8 || SPType_13
	POS Position `json:"pos"`
}
type Dat_Spell_Group9 struct {
	// SPType_9 || SPType_14
	MinPower       int32   `json:"min_power"`
	MaxPower       int32   `json:"max_power"`
	PowerVariance  float32 `json:"power_variance"`
	School         int32   `json:"school"`
	Align          int32   `json:"align"`
	Number         int32   `json:"number"`
	NumberVariance float32 `json:"number_variance"`
}
