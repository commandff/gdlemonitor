// Copyright 2012 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build windows
// +build windows

package main

import (
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/commandff/gdlemonitor/ac"
	"gitlab.com/commandff/gdlemonitor/aids"
	"gitlab.com/commandff/gdlemonitor/api"
	"gitlab.com/commandff/gdlemonitor/gdle"
	"golang.org/x/sys/windows/svc"
)

type myservice struct{}

type SVC_Timer struct {
	Running bool
	Spam    bool
	*time.Timer
}

func (s *SVC_Timer) Enable(t time.Duration) {
	if !s.Running {
		s.Running = true
		s.Spam = true
		s.Timer = time.NewTimer(t)
	} else {
		s.Reset(t)
	}
}
func (s *SVC_Timer) Disable() {
	if s.Running {
		s.Running = false
		s.Stop()
	}
	s.Timer = nil
}

var (
	heartbeat SVC_Timer
)

// (re) loads config, and applies, regardless of previous condition. need checks to kill things that now, should not be running.
func gdlemonitormain() {
	rand.Seed(rand.Int63() ^ time.Now().UTC().UnixNano()) //seeds the RNG, for password generation later.
	ac.Weenie_Parse(filepath.Join(aids.WD, "Data/json"))
	ac.Weenie_Stats()

	prt := fmt.Sprintf(":%d", aids.P.C.API.Port)
	if aids.P.C.API.Port == 443 {
		prt = ""
	}
	api.AuthHost = fmt.Sprintf("%s%s", aids.P.C.DomainName, prt)
	aids.P.Cd.Origin = "https://" + api.AuthHost
	aids.P.Cd.PublicIP = aids.GetPublicIP()
	aids.P.GDLEcfg = gdle.Cfg_Read(filepath.Join(aids.WD, aids.P.C.Gdle.Config)) //attempt to read GDLE config.
	if aids.P.GDLEcfg != nil {
		if gdle.DB != nil {
			gdle.DB.DB.Close() //if we still have a db connection, drop it
		}
		gdle.DB = new(gdle.GDLEDB)
		err := gdle.DB.Connect( // open Database
			aids.P.GDLEcfg["database_username"].(string),
			aids.P.GDLEcfg["database_password"].(string),
			fmt.Sprintf("tcp(%s:%d)", aids.P.GDLEcfg["database_ip"].(string), aids.P.GDLEcfg["database_port"].(int64)),
			aids.P.GDLEcfg["database_name"].(string),
		)
		if err != nil {
			aids.ErrorLog.Printf("Unable to Connect to gdledb: %s", err.Error())
		} else {
			aids.P.Cd.ACServerName = aids.P.GDLEcfg["world_name"].(string)
			aids.P.Cd.ACServer = fmt.Sprintf("%s:%d", aids.P.C.DomainName, aids.P.GDLEcfg["bind_port"].(int64))
		}
	} else {
		aids.ErrorLog.Printf("Unable to read GDLE server config: %s", filepath.Join(aids.WD, aids.P.C.Gdle.Config))
	}
	for k, v := range aids.P.C.ENV {
		os.Setenv(k, v)
	}
	if api.HTTPSListener == nil {
		api.SH.CompileWWW()
		api.SH.WatchWWW()
		api.RouterInit()
		api.CertInit()
		api.MakeConsolePassword()
		go api.ListenHTTPS()
	} else {
		api.SH.CompileWWWEmbeded(".html")
		api.SH.RecurseCompileWWWWD("", ".html")
	}

	if aids.P.C.API.RedirectNonsecurePort {
		if api.HTTPListener == nil {
			go api.ListenHTTP()
		}
	} else {
		if api.HTTPListener != nil {
			//InfoLog.Print("HTTP API disabled")
			api.HTTPListener.Close()
		}
	}
	go aids.Intel_Init(aids.P.C.Whitelist)
	go aids.LoadTails()
}

func (m *myservice) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {
	changes <- svc.Status{State: svc.StartPending} //broadcast Windows Service StartPending, Accepts: nil
	aids.Setup_logging()
	heartbeat.Timer = time.NewTimer(0) //initialize timer, and immediately consume the initial value.
	<-heartbeat.C
	gdlemonitormain()
	heartbeat.Enable(time.Duration(aids.P.C.Heartbeat) * time.Second)
	changes <- svc.Status{State: svc.Running, Accepts: svc.AcceptStop | svc.AcceptShutdown} //broadcast Windows Service Running, Accepts: Stop, Shutdown
loop:
	for {
		select {
		//must reset it's own interval, each iteration.
		case <-heartbeat.C:
			heartbeat.Reset(time.Duration(aids.P.C.Heartbeat) * time.Second)
			aids.P.HeartBeat()
		case comd := <-r:
			switch comd.Cmd {
			case svc.Interrogate:
				changes <- comd.CurrentStatus
				time.Sleep(100 * time.Millisecond)
				changes <- comd.CurrentStatus
			case svc.Stop, svc.Shutdown:
				aids.ErrorLog.Print("Shutdown Received")
				break loop
			default:
				aids.ErrorLog.Printf("unexpected control request #%d", comd)
			}
		}
	}
	changes <- svc.Status{State: svc.StopPending} //broadcast Windows Service StopPending, Accepts: nil
	heartbeat.Disable()
	aids.P.Write()                            // write persistance data
	changes <- svc.Status{State: svc.Stopped} //broadcast Windows Service Stopped, Accepts: nil
	return
}
