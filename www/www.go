package www

import (
	"embed"
	"time"
)

//go:embed schema/*.json *.svg *.html *.css *.js
var FS embed.FS

//go:embed base.html
var Base_html []byte
var Base_html_modTime time.Time
