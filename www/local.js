function getCookie(cookie_name) {
    let name = cookie_name + '=';
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
const Access = ["None", "User", "ERROR(2)", "Advocate", "Sentinel", "ERROR(5)", "Admin", "ERROR(7)"];

function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};
var pageWants = -1;
var claims;
function getclaims() {
    let gdle_account = getCookie('gdlemonitor')
    if (gdle_account.length > 0) {
        claims = parseJwt(gdle_account)
        return true;
    }
    claims = null;
    return false;
}
getclaims();
function toggleElem(elem) {
    let e = document.getElementById(elem);
    if (e.style.display == 'none') {
        e.style.display = '';
    } else {
        e.style.display = 'none';
    }
    return true;
}
function hideElem(elem) {
    document.getElementById(elem).style.display = 'none';
}
function showElem(elem) {
    document.getElementById(elem).style.display = '';
}

function updateUser() {
    if (getclaims()) {
        document.getElementById("user_name").innerHTML = "User (" + claims.username + ")";
        hideElem("login");
        showElem("logout");
        if (claims.access >= 1) showElem("user");
        if (claims.access >= 3) showElem("advocate");
        if (claims.access >= 4) showElem("sentinel");
        if (claims.access >= 6) showElem("admin");
        let curDt = Math.floor(Date.now() / 1000);
        let jwt_expires_in = claims.exp - curDt;
        console.log("updateUser() jwt_expires_in " + jwt_expires_in + " and ", claims);
    } else {
        document.getElementById("user_name").innerHTML = "User (none)";
        showElem("login");
        hideElem("logout");
        hideElem("user");
        hideElem("advocate");
        hideElem("sentinel");
        hideElem("admin");
    }
    if (pageWants > -1 && (!claims || claims.Access < pageWants)) {
        window.location.replace("/");
    }
}

function login(e, t) {
    e.preventDefault();
    let username = document.getElementById("username").value
    if (username.length < 1) {
        document.getElementById("username").focus();
        alert("Username Required");
        return false;
    }
    let password = document.getElementById("password-login").value
    if (password.length < 1) {
        document.getElementById("password-login").focus();
        alert("Password Required");
        return false;
    }

    tempDisableButton(document.getElementById("loginButton"), 2000);
    fetch('/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password)
    })
        .then(response => {
            const data = response.text();
            if (response.ok) {
                updateUser();
            } else {
                document.getElementById("userWarningContent").innerHTML = "Request error " + response.status + ": " + data;
                showElem("userWarning");
            }
            document.getElementById("loginButton").innerHTML = 'Login';
            document.getElementById("loginButton").disabled = false;
        });

}
function logout(e, t) {
    e.preventDefault();
    tempDisableButton(document.getElementById("logoutButton"), 2000);
    fetch('/logout', { method: 'GET' })
        .then(response => {
            const data = response.text();
            if (response.ok) {
                document.cookie = 'gdlemonitor=; Max-Age=-99999999;'
                updateUser();
            } else {
                document.getElementById("userWarningContent").innerHTML = "Request error " + response.status + ": " + data;
                showElem("userWarning");
            }
            document.getElementById("logoutButton").innerHTML = 'Logout';
            document.getElementById("logoutButton").disabled = false;
        });
}

var LastDropdown = null;
function DropdownToggle(t) {
    if (LastDropdown) {
        LastDropdown.style.display = '';
        LastDropdown.parentElement.firstElementChild.style.backgroundColor = 'rgba(var(--light-bg-color), 1)';
    }
    if (LastDropdown == t.parentElement.lastElementChild) { LastDropdown = null; return; }
    LastDropdown = t.parentElement.lastElementChild;
    if (LastDropdown.style.display == '') {
        LastDropdown.style.display = 'block';
        t.style.backgroundColor = 'rgba(var(--action-bg-color), 0.2)';
    }
}


function TogglePasswordEvent(event) {
    if (event.target.previousElementSibling.getAttribute("type") === "password") {
        ShowPassword(event.target.previousElementSibling);
    }
}
function HidePasswordEvent(event) {
    HidePassword(event.target);
}
function ShowPassword(elem) {
    if (elem.getAttribute("type") === "password") {
        elem.setAttribute("type", "text");
        elem.nextElementSibling.classList.add("bi-eye");
    }
    elem.select();
}
function HidePassword(elem) {
    if (elem.getAttribute("type") === "text") {
        elem.setAttribute("type", "password");
        elem.nextElementSibling.classList.remove("bi-eye");
    }
}
function GenFormPassword(event, length, chars) {
    let elem = event.target.parentElement.children[0];
    var randomNumber = Math.floor(Math.random() * 36);
    if (randomNumber >= chars.length) {
        randomNumber = Math.floor(Math.random() * chars.length);
    }
    var password = chars.substring(randomNumber, randomNumber + 1);
    for (var i = 1; i <= length; i++) {
        var randomNumber = Math.floor(Math.random() * chars.length);
        password += chars.substring(randomNumber, randomNumber + 1);
    }

    elem.value = password;
    elem.select();
    document.execCommand('copy');

    ShowPassword(elem);
    return false;
}

function tempDisableButton(elem, timeout) {
    if (elem) {
        let origText = elem.innerHTML;
        elem.innerHTML = '<div class="ms-loading ms-small"></div>';
        elem.disabled = true;
        return setTimeout(() => {
            elem.innerHTML = origText;
            elem.disabled = false;
        }, timeout);
    }
    return setTimeout(() => { }, timeout);
}

/* @dark-mode-switcher - https://github.com/Airmime/dark-mode-switcher */
/*
* dark-mode-switcher - Pure JS script allow you to simply switch your web interface in dark mode.
* Repo : https://github.com/Airmime/dark-mode-switcher
* Licence : MIT
* Version : v0.0.1
*/


/**
* Function allowing to change the mode by adding the attribute 'data-theme' on the element <html>. It also creates a cookie to record the mode selected by the user.
*/
function switchTheme() {
    if (document.documentElement.getAttribute('data-theme') === 'dark') {
        document.documentElement.setAttribute('data-theme', 'light');
        setcookie('light');
    } else {
        document.documentElement.setAttribute('data-theme', 'dark');
        setcookie('dark');
    }
}

/**
* Function to create a cookie for the selected theme.
* @param cookieValue Mode to be valued in the cookie.
* The cookie will have a lifespan of 14 days.
*/
function setcookie(cookieValue) {
    var today = new Date();
    var expire = new Date();
    expire.setTime(today.getTime() + 3600000 * 24 * 14);
    document.cookie = "theme =" + encodeURI(cookieValue) + "; expires=" + expire.toGMTString() + ";path=/";
}

/**
* A method of retrieving the value of a cookie.
* @param name Cookie name.
*/
function getThemeCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

/**
* Automatic mode assignment according to user-selected system preference.
*/
if (getThemeCookie('theme') != null) {
    document.documentElement.setAttribute('data-theme', getThemeCookie('theme'));
} else {
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        document.documentElement.setAttribute('data-theme', 'dark');
    } else {
        document.documentElement.setAttribute('data-theme', 'light');
    }
}
