package api

import (
	"bytes"
	"compress/gzip"
	"encoding/hex"
	ejson "encoding/json"
	"fmt"
	"io/fs"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/css"
	"github.com/tdewolff/minify/v2/html"
	"github.com/tdewolff/minify/v2/js"
	"github.com/tdewolff/minify/v2/json"
	"github.com/tdewolff/minify/v2/svg"
	"github.com/tdewolff/minify/v2/xml"
	"gitlab.com/commandff/gdlemonitor/aids"
	"gitlab.com/commandff/gdlemonitor/www"
)

type cachedFile struct {
	data         []byte
	isgz         bool
	modTime      time.Time
	lastModified string
	contentType  string
	length       string
	etag         string
}

type staticHandler struct {
	cachedFiles   map[string]cachedFile // key is URI; IE `/`, `/schema/status.json`
	cachedFiles_m sync.Mutex
	watcher       *fsnotify.Watcher
	minifier      *minify.M
	baseHTML      *template.Template
	basemodTime   time.Time
}

var SH = &staticHandler{}

// //////////////////////////////////////////////////////////////
// loads contents of www/base.html into (string) sh.baseHTML
func (sh *staticHandler) loadBaseHTML(modTime time.Time, content []byte) bool {
	var err error
	if modTime.Before(sh.basemodTime) {
		return false
	}
	if len(content) == 0 {
		return false
	}
	sh.baseHTML, err = template.New("").Parse(string(content))
	if err != nil {
		aids.DebugLog.Printf("e: %v", err)
		return false
	}
	sh.basemodTime = modTime
	return true
}

type basehtml_data struct {
	Title   string
	Content string
	APIConf string
	*aids.GDLEMonitorConf
	API *aids.GDLEMonitorConfig_Dynamic
}

// //////////////////////////////////////////////////////////////
// receives content .html, returns templated string
// template is just:
// First Line: page title
// All other lines: content

// base.html's `{{title}}“ will be replaced with the above title
// `{{content}}“ will be replaced by the above content
func (sh *staticHandler) ApplyBaseTemplate(file []byte) []byte {
	contentS := strings.SplitN(string(file), "\n", 2) // split file on first new line
	bv, _ := ejson.Marshal(&aids.P.C.API)
	templ_data := &basehtml_data{
		GDLEMonitorConf: &aids.P.C,
		API:             &aids.P.Cd,
		APIConf:         `<script>var api_conf=` + string(bv) + `;</script>`,
		Title:           strings.Trim(contentS[0], "\r"),
		Content:         contentS[1],
	}
	html_temp, err := template.New("").Parse(templ_data.Content)
	if err != nil {
		aids.DebugLog.Printf(err.Error())
		return []byte("")
	}
	var buf bytes.Buffer
	err = html_temp.Execute(&buf, &aids.P.C) // template Content against conf struct
	if err != nil {
		aids.DebugLog.Printf(err.Error())
		return []byte("")
	}
	templ_data.Content = buf.String()
	buf.Reset()
	err = sh.baseHTML.Execute(&buf, &templ_data)
	if err != nil {
		aids.DebugLog.Printf(err.Error())
		return []byte("")
	}
	return buf.Bytes()
}

// creates a new cachedFile entry, discarding the old, if newer
// path should be relative to binary location; ie `\\index.html`
func (sh *staticHandler) newCache(path string, modTime time.Time, content []byte) {
	uri := sh.getURI(path)
	if len(content) == 0 {
		return
	}
	sh.cachedFiles_m.Lock()
	if t, ok := sh.cachedFiles[uri]; ok && !t.modTime.Before(modTime) {
		sh.cachedFiles_m.Unlock()
		return // cache exists, and modtime is older. skiiiiip
	}
	sh.cachedFiles_m.Unlock()
	fileExt := filepath.Ext(uri)
	if uri[len(uri)-1] == '/' {
		fileExt = ".html"
	}
	contentType := mime.TypeByExtension(fileExt)
	if contentType == "" {
		contentType = http.DetectContentType(content)
	}

	if fileExt == ".html" { // if .html, template against base.html and conf struct (twice)
		content = sh.ApplyBaseTemplate(content)
		if len(content) == 0 {
			return
		}
	}

	// poll length after applying base.html
	origLEN := len(content)

	// minify if extension is .html, .json, .css, .js, or .svg
	if fileExt == ".html" || fileExt == ".css" || fileExt == ".js" || fileExt == ".svg" { //  || fileExt == ".json"
		min_src := bytes.NewReader(content)
		var min_res bytes.Buffer
		sh.minifier.Minify(contentType, &min_res, min_src)
		content = min_res.Bytes()
	}

	// gzip (minified) content
	var buf bytes.Buffer
	gz := gzip.NewWriter(&buf)
	var gzipLEN int = 0
	_, err := gz.Write(content)
	if err == nil {
		gz.Close()
		gzipLEN = buf.Len()
	}

	// prepare cache data
	cache := &cachedFile{
		modTime:      modTime,
		lastModified: modTime.Format(time.RFC1123),
		contentType:  contentType,
		etag:         fmt.Sprintf("\"%d-%s\"", modTime.Unix(), hex.EncodeToString([]byte(uri))),
	}

	// if gzip reduces filesize by 80% or more, add `Content-Encoding: gzip` and serve gzip'd file
	if gzipLEN > 0 && (float64(gzipLEN)/float64(origLEN)) < 0.80 {
		cache.data = buf.Bytes()
		cache.isgz = true
		cache.length = fmt.Sprint(gzipLEN)
		//compPCT := 100 - (100 * (float64(gzipLEN) / float64(origLEN)))
		//DebugLog.Printf("Compressed by %.0f%% to %sb from %sb (min:%sb): %s %s", compPCT, aids.Format(gzipLEN), aids.Format(origLEN), aids.Format(minimizedLEN), contentType, uri)
	} else {
		// else serve (minified) content
		cache.data = content
		cache.isgz = false
		cache.length = fmt.Sprint(len(content))
		//InfoLog.Printf("Loaded %sb (gz:%s,min:%s): %s %s", aids.Format(origLEN), aids.Format(gzipLEN), aids.Format(minimizedLEN), contentType, uri)

	}
	sh.cachedFiles_m.Lock()
	sh.cachedFiles[uri] = *cache
	sh.cachedFiles_m.Unlock()
}

// //////////////////////////////////////////////////////////////
// setup minifier. TODO- only call once per service init
func (sh *staticHandler) MinifierInit() {
	sh.minifier = minify.New()
	sh.minifier.AddFunc("text/css", css.Minify)
	sh.minifier.AddFunc("text/html", html.Minify)
	sh.minifier.AddFunc("image/svg+xml", svg.Minify)
	sh.minifier.AddFuncRegexp(regexp.MustCompile("^(application|text)/(x-)?(java|ecma)script$"), js.Minify)
	sh.minifier.AddFuncRegexp(regexp.MustCompile("[/+]json$"), json.Minify)
	sh.minifier.AddFuncRegexp(regexp.MustCompile("[/+]xml$"), xml.Minify)
}

func (sh *staticHandler) CompileWWWEmbeded(suffix string) {
	err := fs.WalkDir(www.FS, ".", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		info, _ := d.Info()
		if info.IsDir() || info.Size() == 0 || !strings.HasSuffix(path, suffix) || path == "base.html" {
			return nil
		}
		content, _ := www.FS.ReadFile(path)
		sh.newCache("/"+path, www.Base_html_modTime, content)
		return nil
	})
	if err != nil {
		aids.DebugLog.Printf("err %s", err.Error())
		return
	}

}

func (sh *staticHandler) RecurseCompileWWWWD(path string, suffix string) {
	edir, err := os.ReadDir(aids.WD + "\\www" + path)
	if err != nil {
		aids.DebugLog.Printf("Could not open www folder; disabling www hot reloading until next restart. %s", err.Error())
		return
	}
	for _, e := range edir {
		info, _ := e.Info()
		file := path + "/" + e.Name()
		if info.IsDir() {
			sh.RecurseCompileWWWWD(file, suffix)
			continue
		}
		if info.Size() == 0 {
			continue
		}
		if !strings.HasSuffix(file, suffix) {
			continue
		}
		if file == "/base.html" {
			continue
		}
		content, _ := os.ReadFile(aids.WD + "\\www\\" + file)
		// if suffix == ".html" {
		// 	sh.newCache(file, sh.basemodTime, content)
		// } else {
		sh.newCache(file, info.ModTime(), content)
		// }
	}
}

// //////////////////////////////////////////////////////////////
// (re)loads Minifier, base.html, and ALL www/ content, including .html's
func (sh *staticHandler) CompileWWW() {
	sh.MinifierInit()

	if fi, err := os.Stat(aids.WD + "/www/base.html"); err == nil {
		if fi.ModTime().After(sh.basemodTime) {
			if content, err := os.ReadFile(aids.WD + "/www/base.html"); err == nil {
				sh.loadBaseHTML(fi.ModTime(), content)
			}
		}
	}
	if sh.baseHTML == nil {
		var err error
		sh.baseHTML, err = template.New("").Parse(string(www.Base_html))
		if err != nil {
			aids.DebugLog.Printf(err.Error())
		}
		sh.basemodTime = www.Base_html_modTime
		aids.DebugLog.Printf("Loaded embedded base.html, %db modified %s", len(www.Base_html), www.Base_html_modTime)
	}

	// generate cachedfiles
	sh.cachedFiles = make(map[string]cachedFile)
	sh.CompileWWWEmbeded("")
	sh.RecurseCompileWWWWD("", "")
}

// //////////////////////////////////////////////////////////////
// (re)loads base.html, and .html files in www/
func (sh *staticHandler) reloadHTML(modTime time.Time, content []byte) {
	if !sh.loadBaseHTML(modTime, content) {
		return
	}
	sh.CompileWWWEmbeded(".html")
	sh.RecurseCompileWWWWD("", ".html")
}

// //////////////////////////////////////////////////////////////
// setup FileWatcher `sh.watcher` on `www` and `www/schema` subdirs, to trigger cache reloads
func (sh *staticHandler) WatchWWW() {
	sh.watcher, _ = fsnotify.NewWatcher()
	go func() {
		for {
			select {
			case event, ok := <-sh.watcher.Events:
				if !ok {
					return
				}
				if event.Op == fsnotify.Rename || event.Op == fsnotify.Remove {
					uri := sh.getURI(strings.TrimPrefix(event.Name, "www\\"))
					aids.InfoLog.Printf("Watcher: %s deleted", uri)

					sh.cachedFiles_m.Lock()
					delete(sh.cachedFiles, uri)
					sh.cachedFiles_m.Unlock()
					continue
				}
				// file in www folder has been written to, or created. Generate associated cache
				if event.Op == fsnotify.Write || event.Op == fsnotify.Create {

					fi, err := os.Stat(event.Name)
					if err != nil {
						aids.DebugLog.Printf(err.Error())
						continue
					}
					if fi.IsDir() || fi.Size() == 0 {
						continue
					}
					// Load file into []byte
					content, err := os.ReadFile(event.Name)
					if err != nil {
						aids.DebugLog.Printf(err.Error())
						continue
					}
					if event.Name == aids.WD+"\\www\\base.html" {
						sh.reloadHTML(fi.ModTime(), content)
					} else {
						sh.newCache(strings.TrimPrefix(event.Name, aids.WD+"\\www"), fi.ModTime(), content)
					}
				}
			case err, ok := <-sh.watcher.Errors:
				if !ok {
					return
				}
				aids.ErrorLog.Printf("FileWatcher error: %v", err)
			}
		}

	}()

	if err := sh.watcher.Add(aids.WD + "/www"); err == nil {
		if err = sh.watcher.Add(aids.WD + "/www/schema"); err == nil {
		} else {
			aids.DebugLog.Printf("Could not open www/schema folder; disabling schema hot reload monitoring until next restart. %s", err.Error())
		}
	} else {
		aids.DebugLog.Printf("Could not open www folder; disabling www hot reload monitoring until next restart. %s", err.Error())
	}
}

// returns appropriate server uri ("/", "/user.html", "/local.js", "/schema/server.json")
// from local relalive path ("\\index.html", "\\user.html", "\\local.js", "\\schema\\server.json")
func (sh *staticHandler) getURI(path string) string {
	base := strings.Replace(path, `\`, "/", -1)
	if base == `/index.html` { // if this is `/index.html`, report as `/`
		base = `/`
	}
	return base
}

// static handler
// ANY /*
func (sh *staticHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	cache, ok := sh.cachedFiles[r.URL.Path]
	if !ok {
		cache, ok = sh.cachedFiles[r.URL.Path+".html"]
		if !ok {
			w.WriteHeader(http.StatusNotFound)
			return
		}
	}
	if r.Header.Get("If-None-Match") == cache.etag {
		w.WriteHeader(http.StatusNotModified)
		return
	}
	if r.Header.Get("If-Modified-Since") == cache.lastModified {
		w.WriteHeader(http.StatusNotModified)
		return
	}
	w.Header().Add("ETag", cache.etag)
	w.Header().Add("Last-Modified", cache.lastModified)
	w.Header().Add("Content-Length", cache.length)
	w.Header().Add("Content-Type", cache.contentType)
	if cache.isgz {
		w.Header().Add("Content-Encoding", "gzip")
	}
	w.WriteHeader(http.StatusOK)
	w.Write(cache.data)
}
