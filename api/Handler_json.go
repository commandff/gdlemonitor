package api

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/commandff/gdlemonitor/aids"
	"gitlab.com/commandff/gdlemonitor/gdle"
)

type Json_cache struct {
	// Base Name, URI_Prefix "/", and Suffix ".json" are assumed
	// ex "gdlemonitor"
	BaseName string
	// Required Access to GET resource (Default is 0)
	// ex -1, 0, 6
	GetAccess int
	// Required Access to PUT resource (Default is 0)
	// ex -1, 0, 6
	PutAccess int
	// cache Valid Duration (Default is 0, effectively no cache; but it hangs out in memory... meh)
	// ex 30 * time.Minute
	Exp time.Duration
	// (optional) Dynamic Get function- should only populate cache.Content; handler will update
	//  last modified to time.Now().UTC()., unless cache.modified is modified in function.
	Get func(cache *Json_cache) error
	// (optional) Dynamic Put function- should only act on cache.Content; handler will update
	//  last modified to time.Now().UTC()., unless cache.modified is modified in function.
	Put          func(cache *Json_cache) error
	ts           time.Time
	modified     time.Time
	etag         string
	lastmodified string
	content      []byte
	sync.Mutex
}

func (cache *Json_cache) Invalidate() {
	cache.ts = time.Time{}
}
func (cache *Json_cache) UpdateModified(mod time.Time) {
	cache.modified = mod
	cache.lastmodified = cache.modified.Format(time.RFC1123)
	cache.etag = fmt.Sprintf("\"%d-%s\"", cache.modified.Unix(), hex.EncodeToString([]byte("/"+cache.BaseName+".json")))
}
func (cache *Json_cache) ReadFile() (err error) {
	targetFile := filepath.Join(aids.WD, cache.BaseName+".json")
	fi, err := os.Stat(targetFile)
	if err != nil {
		aids.ErrorLog.Printf("Error Getting File Info %s: %v", targetFile, err)
		return err
	}
	if fi.ModTime() == cache.modified {
		return nil
	}

	cache.Invalidate()
	cache.content, err = os.ReadFile(targetFile)
	if err != nil {
		aids.ErrorLog.Printf("Error Reading %s: %v", targetFile, err)
		return err
	}
	cache.UpdateModified(fi.ModTime())
	return nil
}
func (cache *Json_cache) WriteFile() (err error) {
	targetFile := filepath.Join(aids.WD, cache.BaseName+".json")
	err = os.WriteFile(targetFile+"~", cache.content, 0644)
	if err != nil {
		aids.ErrorLog.Printf("Error Writing to %s: %v", targetFile, err)
		return err
	}
	err = os.Rename(targetFile+"~", targetFile)
	if err != nil {
		aids.ErrorLog.Printf("Error Renaming %s: %v", targetFile, err)
		return err
	}
	aids.DebugLog.Printf("wrote %db to %s", len(cache.content), targetFile)
	return nil
}

var json_cache map[string]*Json_cache = map[string]*Json_cache{
	// "gdle/wcids": {
	// 	BaseName:  "gdle/wcids",
	// 	GetAccess: -1,
	// 	Exp:       time.Second * 15,
	// 	Get: func(cache *Json_cache) error {
	// 		gdle.WCID_m.Lock()
	// 		defer gdle.WCID_m.Unlock()
	// 		keys := make([]int, 0, len(gdle.WCID))
	// 		for k := range gdle.WCID {
	// 			keys = append(keys, k)
	// 		}
	// 		cache.content, _ = json.Marshal(keys)
	// 		return nil
	// 	},
	// },
	// "gdle/wcidtypes": {
	// 	BaseName:  "gdle/wcidtypes",
	// 	GetAccess: -1,
	// 	Exp:       time.Second * 15,
	// 	Get: func(cache *Json_cache) error {
	// 		gdle.WCID_m.Lock()
	// 		defer gdle.WCID_m.Unlock()
	// 		keys := make([]string, 0, len(gdle.WCID_Type))
	// 		for k := range gdle.WCID_Type {
	// 			keys = append(keys, fmt.Sprintf("\"%d\":\"%d\"", k, len(*gdle.WCID_Type[k])))
	// 		}
	// 		cache.content = []byte("{" + strings.Join(keys, ",") + "}")
	// 		return nil
	// 	},
	// },
	"config": {
		BaseName:  "config",
		GetAccess: 6,
		PutAccess: 6,
		Exp:       time.Second * 15,
		Get: func(cache *Json_cache) error {
			cache.content, _ = json.MarshalIndent(aids.P.C, "", " ")
			return nil
		},
		Put: func(cache *Json_cache) error {
			var newConfig aids.GDLEMonitorConf
			if err := json.Unmarshal(cache.content, &newConfig); err != nil {
				return err
			}
			aids.InfoLog.Printf("Read %db for GDLE Monitor Config", len(cache.content))
			aids.P.C = newConfig
			return nil
		},
	},
	"server": {
		BaseName:  "server",
		GetAccess: 6,
		PutAccess: 6,
		Exp:       time.Second * 15,
		Get: func(cache *Json_cache) error {
			cache.content, _ = json.MarshalIndent(aids.P.GDLEcfg, "", " ")
			return nil
		},
		Put: func(cache *Json_cache) error {
			if err := json.Unmarshal(cache.content, &aids.P.GDLEcfg); err != nil {
				return err
			}
			aids.InfoLog.Printf("Read %db for server.cfg", len(cache.content))
			err := gdle.Cfg_Save(filepath.Join(aids.WD, aids.P.C.Gdle.Config), aids.P.GDLEcfg)
			if err != nil {
				return err
			}
			return nil
		},
	},
}

func handle_get_json(w http.ResponseWriter, r *http.Request) {
	ruri, ok := mux.Vars(r)["json"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	cache, ok := json_cache[ruri]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	claims := aids.VerifyAccess(cache.GetAccess, w, r)
	if claims == nil && cache.GetAccess != -1 {
		return
	}
	cache.Lock() // lock out the current ruri globally, until we return- avoids race on expirations, but limits to 1 concurrent req
	// todo bring this back in, so concurrent requests can be handled by ruri level
	defer cache.Unlock()
	if cache.ts.IsZero() || time.Since(cache.ts) > cache.Exp { // cache is nil, or expired - reload
		var err error
		if cache.Get == nil { // cache.Get is undefined- use ReadFile
			err = cache.ReadFile()
			if aids.CheckInternalError(err, &w) {
				return
			}
		} else {
			// cache.Get is defined, use that
			cache.modified = time.Now().UTC()
			err = cache.Get(cache)
			if aids.CheckInternalError(err, &w) {
				return
			}
			cache.UpdateModified(cache.modified) // Dynamic method; time.Now().UTC(). from above leaks through, if .Get method does not set one.
		}
		cache.ts = time.Now().UTC()
	}
	// regardless if cache.TS expired; the fresh ETag or LastModified could still match
	if r.Header.Get("If-None-Match") == cache.etag || r.Header.Get("If-Modified-Since") == cache.lastmodified {
		w.WriteHeader(http.StatusNotModified)
		return
	}
	w.Header().Add("ETag", cache.etag)
	w.Header().Add("Last-Modified", cache.lastmodified)
	w.Header().Set("Content-Type", "application/json")
	w.Write(cache.content)
}
func handle_put_json(w http.ResponseWriter, r *http.Request) {
	ruri, ok := mux.Vars(r)["json"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	cache, ok := json_cache[ruri]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	claims := aids.VerifyAccess(cache.PutAccess, w, r)
	if claims == nil && cache.PutAccess != -1 {
		return
	}
	body, err := io.ReadAll(r.Body)

	if aids.CheckInternalError(err, &w) {
		return
	}
	if !json.Valid(body) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	cache.Lock()
	defer cache.Unlock()
	cache.Invalidate()
	cache.content = body // That is a nice pointer you have there. I'll take it.
	cache.modified = time.Now().UTC()
	if cache.Put == nil {
		err := cache.WriteFile()
		if aids.CheckInternalError(err, &w) {
			return
		}

	} else {
		err := cache.Put(cache)
		if aids.CheckInternalError(err, &w) {
			return
		}
	}
	cache.ts = time.Now().UTC()
	cache.UpdateModified(cache.modified)

	aids.InfoLog.Printf("Read %db for %s.json", len(cache.content), ruri)
	w.WriteHeader(http.StatusNoContent)
}
