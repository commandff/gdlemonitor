package api

import (
	"net"
	"net/http"

	"gitlab.com/commandff/gdlemonitor/aids"
	"gitlab.com/commandff/gdlemonitor/gdle"
	"gitlab.com/commandff/gdlemonitor/models"
)

// login
// POST /login
// username=^[a-zA-Z0-9_]{3,}$
// password=^[a-z0-9&?!*^%#@\-_=|]{8,}$
func handle_login(w http.ResponseWriter, r *http.Request) {
	ip_address, _, err := net.SplitHostPort(r.RemoteAddr)
	if aids.CheckInternalError(err, &w) {
		return
	}
	if aids.P.C.API.LoginLimitProxy && aids.P.Pull_ip(ip_address).Proxy {
		http.Error(w, "Login Limit: Proxy", http.StatusUnauthorized)
		return
	}
	if aids.CheckInternalError(r.ParseForm(), &w) {
		return
	}
	username := r.PostForm.Get("username") // does not accept query strings.
	password := r.PostForm.Get("password") // does not accept query strings.
	if len(username) == 0 || len(password) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	acct, err := gdle.DB.ValidateAccount(username, password)
	if aids.CheckInternalError(err, &w) {
		return
	}
	aids.IssueJWT(acct, w)
	aids.InfoLog.Printf("LOGIN from %s: %s(access:%d)", ip_address, username, acct.Access)
	//http.Redirect(w, r, r.Header.Get("Referer"), http.StatusFound)
	w.WriteHeader(http.StatusOK)
}

// logout
// GET /logout
func handle_logout(w http.ResponseWriter, r *http.Request) {
	claims := aids.GetJWTClaims(w, r)
	aids.DropJWT(w, claims.Id)
	http.Redirect(w, r, r.Header.Get("Referer"), http.StatusFound)
}

// keepalive
// shortcut to refresh jwt; todo: convert to websocket
// GET /keepalive
func handle_keepalive(w http.ResponseWriter, r *http.Request) {
	aids.GetJWTClaims(w, r)
	w.WriteHeader(http.StatusNoContent)
}

// console login bypass
// get /console?xxxxx
func handle_console(w http.ResponseWriter, r *http.Request) {
	if r.URL.RawQuery == console_password {
		ip_address, _, _ := net.SplitHostPort(r.RemoteAddr)
		aids.InfoLog.Printf("CONSOLE LOGIN from %s", ip_address)
		MakeConsolePassword()
		aids.IssueJWT(&models.Account{
			Claim: models.Claim{
				Username: "Server Owner",
				Access:   6,
			},
		}, w)
		http.Redirect(w, r, "/", http.StatusMovedPermanently)
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
}
