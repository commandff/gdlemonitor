package api

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/commandff/gdlemonitor/aids"
	"gitlab.com/commandff/gdlemonitor/gdle"
	"gitlab.com/commandff/gdlemonitor/models"
)

type accounts_username_get struct {
	ID               int                       `json:"id"`
	Username         string                    `json:"username"`
	Password         string                    `json:"password"`
	PasswordSalt     string                    `json:"password_salt"`
	DateCreated      int                       `json:"date_created"`
	Access           int                       `json:"access"`
	CreatedIPAddress string                    `json:"created_ip_address"`
	Email            string                    `json:"email"`
	Banned           bool                      `json:"banned"`
	LastUpdate       int                       `json:"last_update"`
	Security         []models.Account_security `json:"security"`
}

type accounts_username_put struct {
	Password string `json:"password"`
	Access   int    `json:"access"`
	Banned   bool   `json:"banned"`
}

// test access, confirm that it is 0, 1, 3, 4, or 6.
func testAccess(w http.ResponseWriter, access int) bool {
	switch access {
	case 0: // None
		fallthrough
	case 1: // User
		fallthrough
	case 3: // Advocate
		fallthrough
	case 4: // Sentinel
		fallthrough
	case 6: // Admin
		return true
	default: // Invalid
		http.Error(w, "Invalid Access", http.StatusBadRequest)
		return false
	}
}

// advocate/sentinel/admin account list
// GET /accounts[?page & perpage & search [&username | &email | &ip | &access]]
// page: optional, 1-5000, page number, default 1
// perpage: optional, 10-2000, records per page, default 50
// search: optional, sql like statement, default '%' (%25)
// search type: either username, email, ip, or access

// returns: json array of account names, matching the criteria; ordered by account id.
// GET https://funkyac.ga/accounts?page=650&perpage=10&search=%25&username X-Api: page:650 pages:650 perpage:10 totalrecords:6494 records:4 search:"%25" runtime:3.5123ms
// GET https://funkyac.ga/accounts?page=1&perpage=10&search=yon%25&username X-Api: page:1 pages:2 perpage:10 totalrecords:12 records:10 search:"yon%25" runtime:2.0866ms
// GET https://funkyac.ga/accounts?page=1&perpage=2000&search=a%25&username X-Api: page:1 pages:1 perpage:2000 totalrecords:499 records:499 search:"a%25" runtime:2.359ms
// GET https://funkyac.ga/accounts&username X-Api: page:1 pages:130 perpage:50 totalrecords:6494 records:50 search:"%25" runtime:2.2858ms
// GET https://funkyac.ga/accounts?search=yonneh%25&username X-Api: page:1 pages:1 perpage:50 totalrecords:12 records:12 search:"yonneh%25" runtime:1.9851ms
// ["yonneh","yonneh2","yonneh5","yonneh3","yonneh4","yonneh6","yonneh_real","yonnehreal","yonneh7","yonneh8","yonneh9","yonnehh"]
func handle_accounts_get(w http.ResponseWriter, r *http.Request) {
	claims := aids.VerifyAccess(3, w, r)
	if claims == nil {
		return
	}
	if gdle.DB == nil {
		http.Error(w, "Database Error", http.StatusInternalServerError)
		return
	}

	search := get_query_string(r, "search", "")
	var criteria string = ""
	if _, ok := r.URL.Query()["username"]; ok {
		criteria = "WHERE username LIKE ?"
		search += "%"
	} else if _, ok := r.URL.Query()["email"]; ok {
		criteria = "WHERE email LIKE ?"
		search += "%"
	} else if _, ok := r.URL.Query()["ip"]; ok {
		criteria = "WHERE created_ip_address LIKE ?"
		search += "%"
	} else if _, ok := r.URL.Query()["access"]; ok {
		criteria = "WHERE access = ?"
		access, err := strconv.ParseInt(search, 10, 32)
		if aids.CheckInternalError(err, &w) {
			return
		}
		if !testAccess(w, int(access)) {
			return
		}
	}
	page := get_query_int(r, "page", 1)
	if page < 1 || page > 5000 {
		http.Error(w, "Invalid Page", http.StatusBadRequest)
		return
	}

	perpage := get_query_int(r, "perpage", 50)
	if perpage < 10 || perpage > 2000 {
		http.Error(w, "Invalid Perpage", http.StatusBadRequest)
		return
	}

	recordcount := 0
	if criteria == "" {
		gdle.DB.QueryRow("SELECT COUNT(id) FROM accounts").Scan(&recordcount)
	} else {
		gdle.DB.QueryRow(fmt.Sprintf("SELECT COUNT(id) FROM accounts %s", criteria), search).Scan(&recordcount)
	}
	if recordcount < 1 {
		w.Header().Add("X-Api", fmt.Sprintf(`{"page":1,"pages":1,"perpage":%d,"totalrecords":0,"records":0,"search":"%s"}`, perpage, url.PathEscape(search)))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("[]"))
		return
	}
	var rows *sql.Rows
	var err error

	if criteria == "" {
		stmt, err := gdle.DB.Prepare("SELECT username FROM accounts ORDER BY id ASC LIMIT ?, ?")
		if aids.CheckInternalError(err, &w) {
			return
		}
		rows, err = stmt.Query((page-1)*perpage, perpage)
		if aids.CheckInternalError(err, &w) {
			return
		}
	} else {
		stmt, err := gdle.DB.Prepare(fmt.Sprintf("SELECT username FROM accounts %s ORDER BY id ASC LIMIT ?, ?", criteria))
		if aids.CheckInternalError(err, &w) {
			return
		}
		rows, err = stmt.Query(search, (page-1)*perpage, perpage)
		if aids.CheckInternalError(err, &w) {
			return
		}
	}
	defer rows.Close()
	resu := make([]string, perpage)
	it := 0
	for rows.Next() {
		var user string
		if aids.CheckInternalError(rows.Scan(&user), &w) {
			return
		}
		resu[it] = user
		it++
	}
	resu_json, err := json.Marshal(resu[:it])
	if aids.CheckInternalError(err, &w) {
		return
	}
	w.Header().Add("X-Api", fmt.Sprintf(`{"page":%d,"pages":%d,"perpage":%d,"totalrecords":%d,"records":%d,"search":"%s"}`, page, 1+int(recordcount/perpage), perpage, recordcount, it, url.PathEscape(search)))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resu_json)
}

// advocate/sentinel/admin view account
// GET /accounts/:{username:[a-zA-Z0-9_]+}
func handle_accounts_username_get(w http.ResponseWriter, r *http.Request) {
	claims := aids.VerifyAccess(3, w, r)
	if claims == nil {
		return
	}
	username, ok := mux.Vars(r)["username"]
	if !ok {
		http.Error(w, "username missing", http.StatusBadRequest)
		return
	}
	if gdle.DB == nil {
		http.Error(w, "Database Error", http.StatusInternalServerError)
		return
	}
	var ga accounts_username_get
	var sec []byte

	err := gdle.DB.QueryRow(`SELECT id, username, password,
	password_salt, date_created, access, created_ip_address,
	email, banned, UNIX_TIMESTAMP(last_update),
	security FROM accounts WHERE username = ? LIMIT 1`, username).Scan(&ga.ID, &ga.Username, &ga.Password, &ga.PasswordSalt, &ga.DateCreated, &ga.Access, &ga.CreatedIPAddress, &ga.Email, &ga.Banned, &ga.LastUpdate, &sec)
	if err == sql.ErrNoRows {
		http.Error(w, "Username not found", http.StatusNotFound)
		return
	}
	if aids.CheckInternalError(err, &w) {
		return
	}
	if claims.Access < 4 { // Advocates can not change anyhow, so nulling out the sent password
		ga.Password = ""
		ga.PasswordSalt = ""
	}

	json.Unmarshal(sec, &ga.Security)
	file, _ := json.Marshal(ga)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(file)
	//w.Write([]byte(fmt.Sprintf(`{"id":420,"msg":"GET /accounts/%s endpoint"}`, username)))
}

// sentinel/admin write account
// PUT /accounts/:{username:[a-zA-Z0-9_]+}
// Admin can write anything; Sentinel can only write Access <=3 to Access <=3 accounts.
func handle_accounts_username_put(w http.ResponseWriter, r *http.Request) {
	claims := aids.VerifyAccess(4, w, r)
	if claims == nil {
		return
	}
	username, ok := mux.Vars(r)["username"]
	if !ok {
		http.Error(w, "username missing", http.StatusBadRequest)
		return
	}
	if gdle.DB == nil {
		http.Error(w, "Database Error", http.StatusInternalServerError)
		return
	}
	var ga accounts_username_put
	if aids.CheckInternalError(json.NewDecoder(r.Body).Decode(&ga), &w) {
		return
	}
	if !testAccess(w, ga.Access) {
		return
	}
	if len(ga.Password) != 80 {
		http.Error(w, "Invalid Password", http.StatusBadRequest)
		return
	}

	if claims.Access != 6 { // if accessing user is Sentinel (or invalid(5)), limit changes
		if ga.Access >= claims.Access { // if the access level they are assigning is equal to, or greater than theirs
			http.Error(w, "Invalid Access - You can only update accounts with a lower access level.", http.StatusBadRequest)
			return
		}
	}
	//set limit to SQL query for non-admins.
	// Sentinel can only update or assign Advocate/User/None accounts
	// INVALID(5) can only update or assign Sentinel/Advocate/User/None accounts
	var clingon string = ""
	if claims.Access != 6 {
		clingon = fmt.Sprintf(" AND access < %d", claims.Access)
	}

	stmt, err := gdle.DB.Prepare("UPDATE accounts SET password=?,password_salt=?,access=?,banned=? WHERE username=?" + clingon)
	if aids.CheckInternalError(err, &w) {
		return
	}
	rslt, err := stmt.Exec(ga.Password[16:], ga.Password[:16], ga.Access, ga.Banned, username)
	if aids.CheckInternalError(err, &w) {
		return
	}
	ra, _ := rslt.RowsAffected()
	if ra == 0 {
		http.Error(w, "No rows changed- username not found, access invalid, or there was no change", http.StatusNotFound)
		return
	}
	http.Error(w, "Account updated", http.StatusOK)
}
