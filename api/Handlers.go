package api

import (
	"encoding/json"
	"net"
	"net/http"
	"regexp"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/commandff/gdlemonitor/aids"
)

func handle_secret(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		http.Redirect(w, r, "https://www.youtube.com/watch?v=dQw4w9WgXcQ", http.StatusFound)
		return
	}
}

var reipvalid = regexp.MustCompile(`^(?:[0-9]{1,3})\.(?:[0-9]{1,3})\.(?:[0-9]{1,3})\.(?:[0-9]{1,3})$`)

// check ip
// GET /ip/{ip}
func handle_ip_get_ip(w http.ResponseWriter, r *http.Request) {
	claims := aids.VerifyAccess(3, w, r)
	if claims == nil {
		return
	}
	vars := mux.Vars(r)
	ip := vars["ip"]
	if !reipvalid.MatchString(ip) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	resu, err := json.Marshal(aids.P.Pull_ip(ip))
	if aids.CheckInternalError(err, &w) {
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resu)
}

// check ip
// GET /ip
func handle_ip_get(w http.ResponseWriter, r *http.Request) {
	ip_address, _, err := net.SplitHostPort(r.RemoteAddr)
	if aids.CheckInternalError(err, &w) {
		return
	}
	response := aids.P.Pull_ip(ip_address)
	if response.CountryCode == "" { // if response is blank, that means it's a local address... serve our own data.
		response = aids.P.Pull_ip(aids.P.Cd.PublicIP)
	}
	resu, err := json.Marshal(response)
	if aids.CheckInternalError(err, &w) {
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resu)
}

func get_query_int(r *http.Request, field string, default_value int) int {
	arra, present := r.URL.Query()[field]
	if !present || len(arra) != 1 {
		return default_value
	} else {
		intvalue, err := strconv.ParseInt(arra[0], 10, 32)
		if err != nil {
			return default_value
		}
		return int(intvalue)
	}
}
func get_query_string(r *http.Request, field string, default_value string) string {
	arra, present := r.URL.Query()[field]
	if !present || len(arra) != 1 {
		return default_value
	} else {
		return arra[0]
	}
}
