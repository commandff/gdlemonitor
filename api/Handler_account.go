package api

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/commandff/gdlemonitor/aids"
	"gitlab.com/commandff/gdlemonitor/gdle"
	"gitlab.com/commandff/gdlemonitor/models"
)

type account_post struct {
	Username string                    `json:"username"`
	Email    string                    `json:"email"`
	Password string                    `json:"password"`
	Security []models.Account_security `json:"security"`
}
type account_put struct {
	Username        *string                    `json:"username,omitempty"`
	Email           *string                    `json:"email,omitempty"`
	Password        *string                    `json:"password,omitempty"`
	CurrentPassword *string                    `json:"current_password,omitempty"`
	Security        *[]models.Account_security `json:"security,omitempty"`
}

// get current security questions
// GET /account
func handle_account_get(w http.ResponseWriter, r *http.Request) {
	claims := aids.VerifyAccess(0, w, r)
	if claims == nil {
		return
	}
	acct, err := gdle.DB.LoadAccount(claims.Username)
	if err != nil {
		aids.DropJWT(w, claims.Id)
		http.Error(w, "account missing", http.StatusInternalServerError)
		return
	}
	bv, _ := json.Marshal(acct.JSON)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(bv)
}

// update account
// PUT /account
// username=^[a-zA-Z0-9_]{3,}$
// password=^[a-z0-9&?!*^%#@\-_=|]{8,}$
// email=^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$
func handle_account_put(w http.ResponseWriter, r *http.Request) {
	claims := aids.VerifyAccess(0, w, r)
	if claims == nil {
		return
	}
	ip_address, _, err := net.SplitHostPort(r.RemoteAddr)
	if aids.CheckInternalError(err, &w) {
		return
	}
	if aids.P.C.API.AccountLimitProxy && aids.P.Pull_ip(ip_address).Proxy {
		http.Error(w, "Account Limit: Proxy", http.StatusUnauthorized)
		return
	}
	var ga account_put
	if aids.CheckInternalError(json.NewDecoder(r.Body).Decode(&ga), &w) {
		return
	}
	acct, err := gdle.DB.LoadAccount(claims.Username)
	if err != nil {
		aids.DropJWT(w, claims.Id)
		http.Error(w, "account not found", http.StatusUnauthorized)
		return
	}
	var savesql bool
	if ga.Email != nil {
		email := strings.ToLower(*ga.Email)
		if aids.P.C.API.AccountRequireEmail && email == "" {
			http.Error(w, "Valid E-Mail Address is required.", http.StatusBadRequest)
			return
		}
		err = aids.Check_email(email)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		acct.Email = email
		claims.Email = email
		aids.RefreshJWT(claims, w)
		savesql = true
	}
	if ga.Password != nil && ga.CurrentPassword != nil {
		password := *ga.Password
		err = aids.Check_password(password)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if !acct.TestPW(*ga.CurrentPassword) {
			http.Error(w, "unable to validate current password", http.StatusBadRequest)
			return
		}
		acct.SetPW(password)
		aids.DropJWT(w, claims.Id)
		savesql = true
	}
	if ga.Security != nil {
		if aids.P.C.API.AccountSQMax == 0 {
			http.Error(w, "This server does not allow security questions.", http.StatusBadRequest)
			return
		}
		err := aids.Check_security(ga.Security)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		acct.JSON.Security = ga.Security
		savesql = true
	}
	if savesql {
		err = gdle.DB.SaveAccount(acct)
		if aids.CheckInternalError(err, &w) {
			return
		}
	}
	w.WriteHeader(http.StatusNoContent)

}

// create account
// POST /account
func handle_account(w http.ResponseWriter, r *http.Request) {
	ip_address, _, err := net.SplitHostPort(r.RemoteAddr)
	if aids.CheckInternalError(err, &w) {
		return
	}
	if gdle.DB == nil {
		http.Error(w, "Database Error", http.StatusInternalServerError)
		return
	}
	if aids.P.C.API.AccountLimitProxy && aids.P.Pull_ip(ip_address).Proxy {
		http.Error(w, "Account Limit: Proxy", http.StatusUnauthorized)
		return
	}
	var ga account_post
	if aids.CheckInternalError(json.NewDecoder(r.Body).Decode(&ga), &w) {
		return
	}

	err = aids.Check_security(&ga.Security)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ga.Email = strings.ToLower(ga.Email)
	err = aids.Check_email(ga.Email)
	if aids.P.C.API.AccountRequireEmail && err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = aids.Check_username(ga.Username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = aids.Check_password(ga.Password)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if aids.P.C.API.AccountLimitEmail > 0 {
		if gdle.DB.CountEmails(ga.Email) >= aids.P.C.API.AccountLimitEmail {
			http.Error(w, fmt.Sprintf("email address %s, exceeds account limit", ga.Email), http.StatusUnauthorized)
			return
		}
	}
	if aids.P.C.API.AccountLimitIP > 0 {
		if gdle.DB.CountIPs(ip_address) >= aids.P.C.API.AccountLimitIP {
			http.Error(w, fmt.Sprintf("ip address %s, exceeds account limit", ip_address), http.StatusUnauthorized)
			return
		}
	}
	if gdle.DB.CheckUsername(ga.Username) {
		http.Error(w, fmt.Sprintf("username %s in use", ga.Username), http.StatusBadRequest)
		return
	}

	acct := &models.Account{
		JSON: &models.Account_JSON{
			Security: &ga.Security,
		},
		Claim: models.Claim{
			Username: ga.Username,
			Email:    ga.Email,
			Access:   1,
		},
		Account_Auth: models.Account_Auth{
			DateCreated:      int(time.Now().UTC().Unix()),
			CreatedIPAddress: ip_address,
		},
	}
	acct.SetPW(ga.Password)
	err = gdle.DB.CreateAccount(acct)
	if aids.CheckInternalError(err, &w) {
		return
	}
	aids.IssueJWT(acct, w)
	aids.InfoLog.Printf("NEW ACCOUNT from %s: %s(id:%d,email:%s)", ip_address, ga.Username, acct.ID, ga.Email)
	http.Error(w, "Account Created", http.StatusOK)
}

// check account
// GET /account/{username}
// username=^[a-zA-Z0-9_]{3,}$
// 400 on username len < 3
// 404 on not found
// 204 on found
func handle_account_username_get(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	username := vars["username"]
	err := aids.Check_username(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if gdle.DB.CheckUsername(username) {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.WriteHeader(http.StatusNotFound)
}
