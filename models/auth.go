package models

import (
	"crypto/sha512"
	"encoding/hex"
	"math/rand"
)

// Just think of this all as one struct... it's split up for ease of assignment
type Account struct {
	JSON *Account_JSON `json:"JSON"`
	Account_Auth
	Claim
}
type Account_JSON struct {
	Security *[]Account_security `json:"security,omitempty"`
}
type Account_security struct {
	Q string `json:"q"`
	A string `json:"a"`
}
type Account_Auth struct {
	Admin            bool   `json:"admin"`
	Banned           bool   `json:"banned"`
	DateCreated      int    `json:"date_created"` // unix timestamp
	CreatedIPAddress string `json:"created_ip_address"`
	PasswordSalt     string `json:"password_salt"`
	Password         string `json:"password"`
}
type Claim struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Access   int    `json:"access"` // GDLE: 0=basic 3=advocate 4=sentinel 6=admin	// ACE: 0=Player,1=Advocate,2=Sentinel,3=Envoy,4=Developer,5=Admin
}

// Update acct's password
func (acct *Account) SetPW(plaintext_password string) {
	b := make([]byte, 16)
	for i := range b {
		b[i] = "0123456789abcdef"[rand.Int63()%16]
	}
	acct.PasswordSalt = string(b)
	hashed_password := sha512.Sum512([]byte(plaintext_password + acct.PasswordSalt))
	acct.Password = hex.EncodeToString(hashed_password[:32])
}

// returns true if plaintext_password matches acct's password.
func (acct *Account) TestPW(plaintext_password string) bool {
	hashed_password := sha512.Sum512([]byte(plaintext_password + acct.PasswordSalt))
	return acct.Password == hex.EncodeToString(hashed_password[:32])
}
